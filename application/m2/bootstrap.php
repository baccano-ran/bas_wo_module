<?php

/**
 * Class Workshop_Bootstrap
 */
class Workshop_Bootstrap extends Zend_Application_Module_Bootstrap
{
    /**
     * @throws Zend_Loader_Exception
     */
    public function _initNamespaces()
    {
        $resourceLoader = $this->getResourceLoader();
        $resourceLoader
            ->addResourceType('library', 'library/', 'Library')
            ->addResourceType('library2', 'library/', 'Library')
            ->addResourceType('formatter', 'formatter/', 'Formatter_')
            ->addResourceType('filter', 'filter/', 'Filter_')
            ->addResourceType('source', 'source/', 'Source_');
    }
}
