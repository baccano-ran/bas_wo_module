<?php

class Workshop_Filter_DateFilter extends Bvb_Grid_Filters_Render_RenderAbstract
{

    /**
     * Renders the current input
     *
     * @return string
     */
    public function render()
    {
        return $this->getView()->formText($this->getFieldName(), $this->getDefaultValue(), $this->getAttributes());
    }
}

