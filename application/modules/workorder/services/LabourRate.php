<?php

/**
 * Class Workshop_Service_LabourRate
 */
class Workshop_Service_LabourRate extends BAS_Shared_Service_Abstract
{

    /** @var BAS_Shared_Model_Workshop_LabourRateMapper */
    protected $mapper;

    public function __construct()
    {
        $this->mapper = $this->getMapper('Workshop_LabourRate');
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateData(array $data)
    {
        $errors = [];
        $rules = $this->getRules();
        foreach ($rules as $attr => $validators) {

            $value = array_key_exists($attr, $data) ? $data[$attr] : null;
            /** @var Zend_Validate_Abstract $validator */
            foreach ($validators as $validator) {
                if ($validator->isValid($value)) {
                    continue;
                }
                $messages = $validator->getMessages();
                foreach ($messages as $message) {
                    $errors[] = ['attr' => $attr, 'message' => $message];
                }
            }
        }
        return $errors;
    }

    /**
     * @return array
     */
    private function getRules()
    {
        return [
            'name' => [new Zend_Validate_NotEmpty()],
            'rate' => [
                new Zend_Validate_NotEmpty(),
                new BAS_Shared_Form_Validate_Numeric(),
            ],
        ];
    }

    /**
     * @param int $id
     * @return BAS_Shared_Model_Workshop_LabourRate
     */
    public function findById($id)
    {
        try {
            $labourRate = $this->mapper->findOneByCondition([
                $this->mapper->mapFieldToDb('id') . ' = ?' => (int)$id,
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            $labourRate = null;
        }

        return $labourRate;
    }

    /**
     * @param int $id
     * @param int $depotId
     * @return BAS_Shared_Model_Workshop_LabourRate|null
     */
    public function findByIdAndDepotId($id, $depotId)
    {
        $id = (int)$id;
        $depotId = (int)$depotId;

        $labourRate = null;

        try {
            $labourRate = $this->mapper->findOneByCondition([
                $this->mapper->mapFieldToDb('id') . ' = ?' => $id,
                $this->mapper->mapFieldToDb('depotId') . ' = ?' => $depotId,
            ]);
        } catch (BAS_Shared_NotFoundException $e) {}

        return $labourRate;
    }

    /**
     * @param int $depotId
     * @return BAS_Shared_Model_Workshop_LabourRate[]
     */
    public function findAllByDepotId($depotId)
    {
        $depotId = (int)$depotId;

        $rates = $this->mapper->findAllByCondition([
            $this->mapper->mapFieldToDb('depotId') . ' = ?' => $depotId,
        ]);

        return $rates;
    }

    /**
     * @param int $depotId
     * @param int $departmentId
     * @return BAS_Shared_Model_Workshop_LabourRate[]
     */
    public function findAllByDepotAndDepartment($depotId, $departmentId)
    {
        $depotId = (int)$depotId;
        $departmentId = (int)$departmentId;

        return $this->mapper->findAllByCondition([
            $this->mapper->mapFieldToDb('departmentId') . ' = ?' => $departmentId,
            $this->mapper->mapFieldToDb('depotId') . ' = ?' => $depotId,
        ]);
    }

    /**
     * @param array $data
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_LabourRate
     */
    public function createModelByData(array $data, $userId, $timestamp = 0)
    {
        if (!$timestamp) {
            $timestamp = time();
        }
        $data = array_intersect_key($data, $this->mapper->getMap());
        $labourRate = new BAS_Shared_Model_Workshop_LabourRate($data);

        if ((int)$labourRate->getId() > 0) {
            $labourRate->setUpdatedBy($userId);
            $labourRate->setUpdatedAt(date('Y-m-d H:i:s', $timestamp));
        } else {
            $labourRate->setCreatedBy($userId);
            $labourRate->setCreatedAt(date('Y-m-d H:i:s', $timestamp));
        }

        $endDate = DateTime::createFromFormat('d-m-Y', $labourRate->getEndDate());
        if ($endDate === false) {
            $labourRate->setEndDate('0000-00-00');
        } else {
            $labourRate->setEndDate($endDate->format('Y-m-d'));
        }

        return $labourRate;
    }

    /**
     * @param BAS_Shared_Model_Workshop_LabourRate $labourRate
     * @return BAS_Shared_Model_Workshop_LabourRate
     */
    public function saveModel(BAS_Shared_Model_Workshop_LabourRate $labourRate)
    {
        return $this->mapper->saveModel($labourRate);
    }

    /**
     * @param int $depotId
     * @return Zend_Db_Select
     */
    public function getGridSource($depotId)
    {
        return $this->mapper->getGridSource($depotId);
    }

    /**
     * @param int $labourRateId
     * @return float
     */
    public function getCostPrice($labourRateId)
    {
        $labourRateId = (int)$labourRateId;

        try {
            $costPriceData = $this->mapper
                ->setMapTo(BAS_Shared_Model_AbstractMapper::MAP_TO_ARRAY)
                ->findOneByCondition($this->mapper->getCostPriceSelect($labourRateId));

            $costPrice = $costPriceData['cost_price'];
        } catch (BAS_Shared_NotFoundException $e) {
            $costPrice = 0;
        }

        return $costPrice;
    }

}