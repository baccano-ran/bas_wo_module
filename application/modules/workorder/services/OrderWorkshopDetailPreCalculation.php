<?php

class Workshop_Service_OrderWorkshopDetailPreCalculation extends BAS_Shared_Service_Abstract
{

    /**
     * @param int $defectId
     * @return array
     */
    public function getDefectPreCalculationsData($defectId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculationMapper $detailPreCalculationMapper */
        $detailPreCalculationMapper = $this->getMapper('Workshop_OrderWorkshopDetailPreCalculation');
        return $detailPreCalculationMapper->getDefectPreCalculationsData($defectId);
    }

    /**
     * @param $workorderId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculation[]
     */
    public function findByWorkorderId($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculationMapper $detailPreCalculationMapper */
        $detailPreCalculationMapper = $this->getMapper('Workshop_OrderWorkshopDetailPreCalculation');
        return $detailPreCalculationMapper->findByWorkorderId($workorderId);
    }

    /**
     * @param int $workorderId
     * @param int $userId
     */
    public function makePreCalculationByWorkorderId($workorderId, $userId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculationMapper $detailPreCalculationMapper */
        $detailPreCalculationMapper = $this->getMapper('Workshop_OrderWorkshopDetailPreCalculation');
        $detailPreCalculationMapper->makePreCalculationByWorkorderId($workorderId, $userId);
    }

    /**
     * @param int $orderWorkshopDetailId
     * @return int
     */
    public function deleteByOrderWorkshopDetailId($orderWorkshopDetailId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculationMapper $detailPreCalculationMapper */
        $detailPreCalculationMapper = $this->getMapper('Workshop_OrderWorkshopDetailPreCalculation');

        return $detailPreCalculationMapper->deleteByCondition([
            $detailPreCalculationMapper->mapFieldToDb('orderWorkshopDetailId') . ' = ?' => (int)$orderWorkshopDetailId,
        ]);
    }

    /**
     * @param int $workorderId
     * @return array
     */
    public function deleteByWorkorderId($workorderId)
    {
        $workorderId = (int) $workorderId;

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculationMapper $detailPreCalculationMapper */
        $detailPreCalculationMapper = $this->getMapper('Workshop_OrderWorkshopDetailPreCalculation');

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $detailMapper */
        $detailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $details = $detailMapper->findAllByCondition([
            $detailMapper->mapFieldToDb('order_workshop_id') . '=?' => $workorderId,
            $detailMapper->mapFieldToDb('archived') . '=?' => BAS_Shared_Model_AbstractModel::NOT_ARCHIVED,
        ]);

        $detailsIds = array_map(function(BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail) {
            return $detail->getId();
        }, $details);

        if (!empty($detailsIds)) {
            $detailPreCalculationMapper->deleteByCondition([
                $detailPreCalculationMapper->mapFieldToDb('order_workshop_detail_id') . ' IN (?)' => (array)$detailsIds,
            ]);
        }

        return $detailsIds;
    }

    /**
     * @param int $detailId
     * @param string $description
     * @return bool|int
     */
    public function updateDescription($detailId, $description)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculationMapper $detailPreCalculationMapper */
        $detailPreCalculationMapper = $this->getMapper('Workshop_OrderWorkshopDetailPreCalculation');

        return $detailPreCalculationMapper->updateFieldsByCondition(
            [$detailPreCalculationMapper->mapFieldToDb('description') => $description],
            [$detailPreCalculationMapper->mapFieldToDb('orderWorkshopDetailId') . '= ?' => (int)$detailId]
        );
    }

    /**
     * @param int $detailId
     * @param string $amount
     * @return bool|int
     */
    public function updateAmount($detailId, $amount)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculationMapper $detailPreCalculationMapper */
        $detailPreCalculationMapper = $this->getMapper('Workshop_OrderWorkshopDetailPreCalculation');

        return $detailPreCalculationMapper->updateFieldsByCondition(
            [$detailPreCalculationMapper->mapFieldToDb('amount') => $amount],
            [$detailPreCalculationMapper->mapFieldToDb('orderWorkshopDetailId') . '= ?' => (int)$detailId]
        );
    }

}