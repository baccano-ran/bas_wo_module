<?php

/**
 * Class Workshop_Service_Workorder
 */
class Workshop_Service_Workorder extends BAS_Shared_Service_Abstract
{
    const DEFAULT_INVOICE_LANGUAGE = 'en';

    /**@var Warehouse_Service_OrderItemInvoice $_orderItemInvoiceService */
    protected $_orderItemInvoiceService;

    /**
     * Get order item invoice service
     *
     * @return \Warehouse_Service_OrderItemInvoice
     */
    public function getOrderItemInvoiceService()
    {
        if (null === $this->_orderItemInvoiceService) {
            $this->_orderItemInvoiceService = new Warehouse_Service_OrderItemInvoice();
        }

        return $this->_orderItemInvoiceService;
    }

    /**
     * Set order item invoice service
     *
     * @param Warehouse_Service_OrderItemInvoice $orderItemInvoiceService
     * @return $this
     */
    public function setOrderItemInvoiceService($orderItemInvoiceService)
    {
        $this->_orderItemInvoiceService = $orderItemInvoiceService;

        return $this;
    }
    
    /**
     * @param int $workorderId
     * @param int $userId
     * @throws Exception
     */
    public function makeOrder($workorderId, $userId)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailsService */
        $workorderDetailsService = $this->getService('Workshop_Service_OrderWorkshopDetail');
        /** @var Warehouse_Service_ItemStock $itemStockService */
        $itemStockService = $this->getService('Warehouse_Service_ItemStock');

        $workorder = $this->findById($workorderId);

        if ($workorder->getStatus() !== BAS_Shared_Model_OrderWorkshop::WORKORDER_OFFER) {
            throw new BAS_Shared_Exception('Cannot make workorder due to its status: ' . $workorder->getStatus());
        }

        $db = $this->getDb();

        try {
            $db->beginTransaction();

            $this->updateWorkorderStatus($workorderId, BAS_Shared_Model_OrderWorkshop::WORKORDER_START, $userId);
            $this->updateWorkorderItemsPickedValue($workorderId, BAS_Shared_Model_Workshop_OrderWorkshopDetail::ITEM_PICKED_ORDER, $userId);

            $workorder = $this->findById($workorderId);
            $workorderItemDetails = $workorderDetailsService->findByWorkorderIdAndType(
                $workorder->getWorkorderId(),
                BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM,
                true
            );

            foreach ($workorderItemDetails as $workorderItemDetail) {
                $itemStockService->updateQuantityByWorkorderDetailId(
                    $workorderItemDetail->getId(),
                    $workorderItemDetail->getQuantity(),
                    $userId
                );
            }

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return bool|int
     * @throws BAS_Shared_Exception
     */
    public function getWorkorderLegacyVehicleId(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        if ($workorder->getVehicleSource() === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_FLEET) {
            return false;
        }

        if ($workorder->getVehicleSource() === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_ORDER_VEHICLE) {
            /** @var Order_Service_OrderVehicle $orderVehicleService */
            $orderVehicleService = $this->getService('Order_Service_OrderVehicle');
            $orderVehicle = $orderVehicleService->find($workorder->getVehicleId());
            return $orderVehicle === null ? false : $orderVehicle->getLegacyVoertuigId();
        }

        return $workorder->getVehicleId();
    }

    /**
     * @param int $workorderId
     * @param int $status
     * @param int $userId
     * @return bool|int
     */
    public function updateWorkorderStatus($workorderId, $status, $userId)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        return $orderWorkshopMapper->updateFieldsByCondition([
            $orderWorkshopMapper->mapFieldToDb('status') => (int)$status,
            $orderWorkshopMapper->mapFieldToDb('updatedBy') => (int)$userId,
            $orderWorkshopMapper->mapFieldToDb('updatedAt') => date('Y-m-d H:i:s'),
        ], [
            $orderWorkshopMapper->mapFieldToDb('workorderId') . ' = ?' => (int)$workorderId,
        ]);
    }

    /**
     * @param int $workorderId
     * @param int $itemPicked
     * @param int $userId
     * @return bool|int
     */
    public function updateWorkorderItemsPickedValue($workorderId, $itemPicked, $userId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->updateFieldsByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('itemPicked') => $itemPicked,
            $orderWorkshopDetailMapper->mapFieldToDb('updatedBy') => (int)$userId,
            $orderWorkshopDetailMapper->mapFieldToDb('updatedAt') => date('Y-m-d H:i:s'),
        ], [
            $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
            $orderWorkshopDetailMapper->mapFieldToDb('type') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM,
            $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
        ]);
    }

    /**
     * @param int $workorderId
     * @return string
     */
    public function getInvoiceLanguageByOrderId($workorderId)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        /** @var BAS_Shared_Model_ContactMapper $contactMapper */
        $contactMapper = $this->getMapper('Contact');

        try {

            /** @var BAS_Shared_Model_OrderWorkshop $order */
            $order = $orderWorkshopMapper->findByWorkorderId((int) $workorderId);

            /** @var BAS_Shared_Model_Contact $person */
            $person = $contactMapper->findOneByCondition([
                'id = ?' => (int) $order->getPersonId(),
            ]);

        } catch (BAS_Shared_NotFoundException $e) {

            return self::DEFAULT_INVOICE_LANGUAGE;
        }

        return strtolower($person->getLanguageCode());
    }

    /**
     * @param int $userId
     * @param int $depotId
     * @return BAS_Shared_Model_OrderWorkshop
     * @throws Exception
     */
    public function create($userId, $depotId)
    {
        $userId = (int)$userId;
        $depotId = (int)$depotId;

        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        $workorder = new BAS_Shared_Model_OrderWorkshop();
        $workorder
            ->setVehicleId(0)
            ->setLocked(BAS_Shared_Model_OrderWorkshop::NOT_LOCKED)
            ->setArchived(BAS_Shared_Model_OrderWorkshop::NOT_ARCHIVED)
            ->setUserId($userId)
            ->setDepotId($depotId)
            ->setCreatedBy($userId)
            ->setCreatedAt(date('Y-m-d H:i:s'));

        $db = $this->getDb();

        try {
            $db->beginTransaction();

            /** @var BAS_Shared_Model_OrderWorkshop $workorder */
            $workorder = $orderWorkshopMapper->saveModel($workorder);
            $workorder->setAs400Id($workorder->getWorkorderId());

            $orderWorkshopMapper->updateFieldsByCondition(
                [$orderWorkshopMapper->mapFieldToDb('as400Id') => $workorder->getAs400Id()],
                [$orderWorkshopMapper->mapFieldToDb('workorderId') . ' = ?' => $workorder->getWorkorderId()]
            );

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        return $workorder;
    }

    /**
     * @param array $dataToUpdate
     * @param int $workorderId
     * @param int $userId
     * @return BAS_Shared_Model_OrderWorkshop|boolean
     */
    public function update(array $dataToUpdate, $workorderId, $userId)
    {
        if ($dataToUpdate === []) {
            return false;
        }

        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        $workorderId = (int)$workorderId;
        $userId = (int)$userId;
        $vatType = null;
        
        if (isset($dataToUpdate['vatType'])) {
            $vatType = $dataToUpdate['vatType'];
            unset($dataToUpdate['vatType']);
        }
        
        try {
            /** @var BAS_Shared_Model_Order $originalOrder */
            $originalOrder = $orderWorkshopMapper->findOneByCondition(['workorder_id = ?' => $workorderId]);
        } catch (BAS_Shared_NotFoundException $e) {
            return false;
        }

        $updatedOrder = new BAS_Shared_Model_OrderWorkshop(array_merge($originalOrder->toArray(), $dataToUpdate));

        $updatedOrder
            ->setUpdatedAt(date('Y-m-d H:i:s'))
            ->setUpdatedBy($userId);

        try {
            $orderWorkshopMapper->saveModel($updatedOrder, BAS_Shared_Model_AbstractMapper::SAVE_TYPE_UPDATE);
            // Update invoice total price
            $this->getEventManager()->trigger(
                BAS_Shared_Model_MapperEvent::EVENT_WORKORDER_UPDATE_TOTAL_INVOICE_PRICE,
                $this,
                ['workorderId' => (int)$workorderId]
            );
        } catch (Exception $e) {
            return false;
        }

        return $updatedOrder;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return BAS_Shared_Model_OrderWorkshop
     */
    public function saveModel(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        return $orderWorkshopMapper->saveModel($workorder);
    }

    /**
     * @param int $depotId
     * @return BAS_Shared_Model_ListOrderCancelReason[]
     */
    public function getListReasonCancelWorkorder($depotId)
    {
        /** @var BAS_Shared_Model_ListOrderCancelReasonMapper $listOrderCancelReasonMapper */
        $listOrderCancelReasonMapper = $this->getMapper('ListOrderCancelReason');

        return $listOrderCancelReasonMapper->findAllByDepot($depotId);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param int $reasonId
     * @param string $reasonText
     * @param int $userId
     * @return bool
     * @throws Exception
     */
    public function cancelWorkorder(BAS_Shared_Model_OrderWorkshop $workorder, $reasonId, $reasonText, $userId)
    {
        /** @var Stock_Service_StockDefects $stockDefectsService */
        $stockDefectsService = $this->getService('Stock_Service_StockDefects');
        /** @var Order_Service_OrderVehicle $orderVehicleService */
        $orderVehicleService = $this->getService('Order_Service_OrderVehicle');
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');
        /** @var Workshop_Service_WorkorderTime $workorderTimeService */
        $workorderTimeService = $this->getService('Workshop_Service_WorkorderTime');;

        if (!$this->hasUserAccess($userId, 'workshop.workorder.cancel')) {
            throw new BAS_Shared_Exception('Cancel workorder is not allowed for user ' . $userId);
        }

        if (!in_array($workorder->getStatus(), [
            BAS_Shared_Model_OrderWorkshop::WORKORDER_OFFER,
            BAS_Shared_Model_OrderWorkshop::WORKORDER_START,
            BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED,
        ])) {
            throw new BAS_Shared_Exception('Cannot cancel due to the workorder status: ' . $workorder->getStatusLabel());
        }

        if ($workorderTimeService->isTimeSpentAttachedToWorkorder($workorder->getWorkorderId())) {
            throw new BAS_Shared_Exception('Cannot cancel due to hours spent attached');
        }

        $db = $this->getDb();

        try {
            $db->beginTransaction();

            $this->returnWorkorderItems($workorder, $userId);

            $orderWorkshopMapper->updateFieldsByCondition([
                $orderWorkshopMapper->mapFieldToDb('status') => BAS_Shared_Model_OrderWorkshop::WORKORDER_ARCHIVED,
                $orderWorkshopMapper->mapFieldToDb('archived') => BAS_Shared_Model_OrderWorkshop::ARCHIVED,
                $orderWorkshopMapper->mapFieldToDb('orderClosedReasonId') => (int)$reasonId,
                $orderWorkshopMapper->mapFieldToDb('orderClosedReason') => $reasonText,
                $orderWorkshopMapper->mapFieldToDb('updatedAt') => date('Y-m-d H:i:s'),
                $orderWorkshopMapper->mapFieldToDb('updatedBy') => $userId,
            ], [
                $orderWorkshopMapper->mapFieldToDb('workorderId') . ' = ?' => (int)$workorder->getWorkorderId(),
            ]);

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        if ($workorder->getType() === BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT) {
            $stockDefectsService->updateWorkorderDefectPreparationWorkshop($workorder->getWorkorderId(), $workorder->getType(),
                BAS_Shared_Model_StockDefects::PREP_WORKSHOP_READY);
        }

        if ($workorder->getType() === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER && $workorder->getVehicleId()) {
            $orderVehicleService->updateOrderVehiclePreparationWorkshopStatus($workorder->getVehicleId());
            $orderVehicleService->updateOrderVehicleLocation($workorder->getVehicleId());
        }
    }

    /**
     * Returns all workorder items back to stock
     *
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param int $userId
     */
    public function returnWorkorderItems(BAS_Shared_Model_OrderWorkshop $workorder, $userId)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');
        /** @var Warehouse_Service_ItemStock $itemStockService */
        $itemStockService = $this->getService('Warehouse_Service_ItemStock');

        $workorderItems = $workorderDetailService->findByWorkorderIdAndType(
            $workorder->getWorkorderId(),
            BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM,
            false
        );

        foreach ($workorderItems as $workorderItem) {
            $quantityDelta = BAS_Shared_Utils_Math::neg($workorderItem->getQuantity(), BAS_Shared_Utils_Math::SCALE_2);

            $itemStockService->updateQuantityByWorkorderDetailId(
                $workorderItem->getId(),
                $quantityDelta,
                $userId
            );
        }
    }

    /**
     * @param int $id
     * @return bool|int
     * @throws BAS_Shared_Exception
     */
    public function unlockWorkorderById($id)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');
        /** @var BAS_Shared_Validate_WorkorderReopenValidator $workorderReopenValidator */
        $workorderReopenValidator = $this->getService('BAS_Shared_Validate_WorkorderReopenValidator');

        $workorder = $this->findById($id);

        if (!$workorderReopenValidator->canBeReopened($workorder)) {
            throw new BAS_Shared_Exception($workorderReopenValidator->getComposedErrorMessage());
        }

        $surcharges = $this->calculateSurcharges($workorder);

        return $orderWorkshopMapper->updateFieldsByCondition(
            $orderWorkshopMapper->mapToDb(array_merge($surcharges, ['status' => BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED])),
            [$orderWorkshopMapper->mapFieldToDb('workorder_id') . ' = ?' => (int)$id]
        );
    }

    /**
     * @param int $id
     * @return BAS_Shared_Model_OrderWorkshop|null
     */
    public function findById($id)
    {
        $id = (int)$id;

        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        try {
            /** @var BAS_Shared_Model_OrderWorkshop $orderWorkshop */
            $orderWorkshop = $orderWorkshopMapper->findOneByCondition([
                $orderWorkshopMapper->mapFieldToDb('workorderId') . ' = ?' => $id,
                $orderWorkshopMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_OrderWorkshop::NOT_ARCHIVED,
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            $orderWorkshop = null;
        }

        return $orderWorkshop;
    }

    /**
     * @param int $workorderId
     * @param int $status
     * @param string $comment
     * @param int $userId
     * @return bool|int
     */
    public function changeSalesPreparationStatus($workorderId, $status, $comment, $userId)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');
        /** @var Workshop_Service_OrderWorkshopDetailPreCalculation $detailsPreCalculationService */
        $detailsPreCalculationService = $this->getService('Workshop_Service_OrderWorkshopDetailPreCalculation');
        /** @var Stock_Service_StockDefects $stockDefectsService */
        $stockDefectsService = $this->getService('Stock_Service_StockDefects');

        $workorderId = (int) $workorderId;
        $status = (int) $status;
        $userId = (int) $userId;

        $updateFields = [
            $orderWorkshopMapper->mapFieldToDb('salesPreparationStatus') => $status,
            $orderWorkshopMapper->mapFieldToDb('salesPreparationComment') => $comment,
        ];

        switch ($status) {
            case BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_HOLD:
                $detailsPreCalculationService->deleteByWorkorderId($workorderId);
                break;

            case BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_ACTION_REQUIRED:
                $workorder = $this->findById($workorderId);
                if ($workorder !== null) {
                    $detailsPreCalculationService->makePreCalculationByWorkorderId($workorderId, $userId);
                    $defect = $stockDefectsService->findDefectByWorkorder($workorder);

                    if ($defect === null) {
                        $stockDefectsService->createApprovalWorkorderDefect($workorder, $userId);
                    } else {
                        $stockDefectsService->updateDefectTotalCosts($defect, $userId);
                    }
                }
                break;

            case BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_WAITING:
                $updateFields[$orderWorkshopMapper->mapFieldToDb('salesPreparationProposal')] = null;
                $updateFields[$orderWorkshopMapper->mapFieldToDb('salesPreparationProposalBy')] = $userId;
                break;

            case BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_APPROVED:
                $updateFields[$orderWorkshopMapper->mapFieldToDb('salesPreparationApprovedBy')] = $userId;
                $updateFields[$orderWorkshopMapper->mapFieldToDb('salesPreparationApprovedAt')] = $this->getCurrentDate();
                break;
        }

        if ($status !== BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_APPROVED) {
            $updateFields[$orderWorkshopMapper->mapFieldToDb('salesPreparationApprovedBy')] = null;
            $updateFields[$orderWorkshopMapper->mapFieldToDb('salesPreparationApprovedAt')] = null;
        }

        return $orderWorkshopMapper->updateFieldsByCondition($updateFields, [
            $orderWorkshopMapper->mapFieldToDb('workorderId') . ' = ?' => $workorderId,
            $orderWorkshopMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_AbstractModel::NOT_ARCHIVED,
        ]);
    }

    /**
     * @return array
     */
    public function getNonInvoicedRefuelingWorkorderTransactions()
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        $transactionsData = $orderWorkshopMapper->findNonInvoicedRefuelingWorkorderTransactions();

        $workorders = [];
        foreach ($transactionsData as $transactionData) {
            $workorders[$transactionData['workorderId']][] = $transactionData;
        }

        return $workorders;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param float $internalAmount
     * @param float $guaranteeAmount
     * @param float $externalAmount
     * @return bool
     */
    public function isEqualConsumablesSum(BAS_Shared_Model_OrderWorkshop $workorder, $internalAmount, $guaranteeAmount, $externalAmount)
    {
        $surcharges = $this->getSurchargesByWorkorder($workorder);
        $consumableSurcharges = $surcharges['consumable'];

        $sum = BAS_Shared_Utils_Math::sum([
            $internalAmount,
            $guaranteeAmount,
            $externalAmount,
        ], BAS_Shared_Utils_Math::SCALE_4);

        return BAS_Shared_Utils_Math::compareFloat($sum, $consumableSurcharges) === 0;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param float $internalAmount
     * @param float $guaranteeAmount
     * @param float $externalAmount
     * @return bool
     */
    public function isEqualEnvironmentSum(BAS_Shared_Model_OrderWorkshop $workorder, $internalAmount, $guaranteeAmount, $externalAmount)
    {
        $surcharges = $this->getSurchargesByWorkorder($workorder);
        $environmentSurcharges = $surcharges['environment'];

        $sum = BAS_Shared_Utils_Math::sum([
            $internalAmount,
            $guaranteeAmount,
            $externalAmount,
        ], BAS_Shared_Utils_Math::SCALE_4);

        return BAS_Shared_Utils_Math::compareFloat($sum, $environmentSurcharges) === 0;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return array
     */
    public function getCloseWorkorderFormDataByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        $radioChecked = true;

        $surcharges = $this->getSurchargesByWorkorder($workorder);
        $priceFilter = new Zend_Filter_NormalizedToLocalized([
            'locale' => 'nl_NL',
            'precision' => 2,
        ]);

        return [
            'radio_consumable' => (int) $radioChecked,
            'radio_environment' => (int) $radioChecked,

            'price_internal_consumables' => $priceFilter->filter($surcharges['consumableInternal']),
            'price_guarantee_consumables' => $priceFilter->filter($surcharges['consumableGuarantee']),
            'price_external_consumables' => $priceFilter->filter($surcharges['consumableExternal']),

            'price_internal_environment' => $priceFilter->filter($surcharges['environmentInternal']),
            'price_guarantee_environment' => $priceFilter->filter($surcharges['environmentGuarantee']),
            'price_external_environment' => $priceFilter->filter($surcharges['environmentExternal']),
        ];
    }

    /**
     * @param array $values
     * @param int $workorderId
     * @param int $userId
     * @return bool
     */
    public function saveCloseWorkorderFormByWorkorderId(array $values, $workorderId, $userId)
    {
        $workorder = $this->findById($workorderId);

        if (array_key_exists(Workshop_Form_CloseWorkorder::BELONG_ELEMENTS, $values)) {
            $values = $values[Workshop_Form_CloseWorkorder::BELONG_ELEMENTS];
        }

        $availableFields = [
            'price_internal_consumables',
            'price_guarantee_consumables',
            'price_external_consumables',
            'price_internal_environment',
            'price_guarantee_environment',
            'price_external_environment',
        ];

        $priceFilter = new Zend_Filter_LocalizedToNormalized([
            'locale' => 'nl_NL',
            'precision' => BAS_Shared_Utils_Math::SCALE_4,
        ]);

        $surcharges = array_intersect_key($values, array_flip($availableFields));

        $surcharges = array_map(function($v) use ($priceFilter) {
            return (float) $priceFilter->filter($v);
        }, $surcharges);

        return $this->closeWorkorder($workorder, $userId, $surcharges);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param int $userId
     * @param array|null $surcharges
     * @return bool
     * @throws BAS_Shared_Exception
     */
    public function closeWorkorder(BAS_Shared_Model_OrderWorkshop $workorder, $userId, $surcharges = null)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');
        /** @var BAS_Shared_Validate_WorkorderCloseValidator $workorderCloseValidator */
        $workorderCloseValidator = $this->getService('BAS_Shared_Validate_WorkorderCloseValidator');

        if (!$workorderCloseValidator->canBeClosed($workorder)) {
            throw new BAS_Shared_Exception($workorderCloseValidator->getComposedErrorMessage());
        }

        if ($surcharges === null) {
            $surcharges = $this->getSurchargesByWorkorder($workorder);
            $surcharges = [
                'priceInternalConsumables' => $surcharges['consumableInternal'],
                'priceGuaranteeConsumables' => $surcharges['consumableGuarantee'],
                'priceExternalConsumables' => $surcharges['consumableExternal'],
                'priceInternalEnvironment' => $surcharges['environmentInternal'],
                'priceGuaranteeEnvironment' => $surcharges['environmentGuarantee'],
                'priceExternalEnvironment' => $surcharges['environmentExternal'],
            ];
        }

        $updateData = $workorderMapper->mapToDb(array_merge($surcharges, [
            'status' => BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED,
            'updatedAt' => $this->getCurrentDate(),
            'updatedBy' => $userId,
        ]));

        $workorderMapper->updateFieldsByCondition($updateData, [
            $workorderMapper->mapFieldToDb('workorderId') . ' = ?' => $workorder->getWorkorderId(),
        ]);

        return true;
    }

    /**
     * @param int $workorderType
     * @return bool
     */
    public function isInternalWorkorder($workorderType)
    {
        return in_array($workorderType, [
            BAS_Shared_Model_OrderWorkshop::TYPE_ORDER,
            BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT,
            BAS_Shared_Model_OrderWorkshop::TYPE_INCOMING_VEHICLES,
            BAS_Shared_Model_OrderWorkshop::TYPE_STOCK,
            BAS_Shared_Model_OrderWorkshop::TYPE_INTERNAL_FIXED,
            BAS_Shared_Model_OrderWorkshop::TYPE_SALES_PREPARATION,
        ]);
    }

    /**
     * @param int $workorderType
     * @return bool
     */
    public function isExternalWorkorder($workorderType)
    {
        return in_array($workorderType, [
            BAS_Shared_Model_OrderWorkshop::TYPE_CUSTOMER,
            BAS_Shared_Model_OrderWorkshop::TYPE_PASSERBY,
            BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK,
        ]);
    }

    /**
     * @param int $workorderType
     * @param int|null $contactId
     * @return int|null
     */
    public function getVehicleSource($workorderType, $contactId)
    {
        if ($workorderType === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER) {
            return BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_ORDER_VEHICLE;
        }

        $isInternalFixed = $workorderType === BAS_Shared_Model_OrderWorkshop::TYPE_INTERNAL_FIXED;
        $isInternalFixedWithContact = $isInternalFixed && $contactId > 0;
        $isInternalFixedWithoutContact = $isInternalFixed && empty($contactId);

        if ($isInternalFixedWithoutContact || in_array($workorderType, [
            BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT,
            BAS_Shared_Model_OrderWorkshop::TYPE_STOCK,
            BAS_Shared_Model_OrderWorkshop::TYPE_INCOMING_VEHICLES,
            BAS_Shared_Model_OrderWorkshop::TYPE_SALES_PREPARATION,
        ])) {
            return BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_STOCK;
        }

        if ($isInternalFixedWithContact || in_array($workorderType, [
            BAS_Shared_Model_OrderWorkshop::TYPE_CUSTOMER,
            BAS_Shared_Model_OrderWorkshop::TYPE_PASSERBY,
            BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK,
        ])) {
            return BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_FLEET;
        }

        return null;
    }

    /**
     * @param int $workorderType
     * @return bool
     */
    public function isCustomerApplicableByWorkorderType($workorderType)
    {
        return in_array($workorderType, [
            BAS_Shared_Model_OrderWorkshop::TYPE_ORDER,
            BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT,
            BAS_Shared_Model_OrderWorkshop::TYPE_CUSTOMER,
            BAS_Shared_Model_OrderWorkshop::TYPE_PASSERBY,
            BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK,
        ]);
    }

    /**
     * @param int $id
     * @param int $depotId
     * @return BAS_Shared_Model_OrderWorkshop|null
     */
    public function findByIdAndDepot($id, $depotId)
    {
        $id = (int)$id;
        $depotId = (int)$depotId;

        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');
        /** @var Management_Service_DepotAffiliate $depotAffiliateService */
        $depotAffiliateService = $this->getService('Management_Service_DepotAffiliate');

        $affiliateMasterDepotIds = $depotAffiliateService->getDepotAndAffiliateDepotIds($depotId, BAS_Shared_Model_DepotAffiliate::TYPE_MASTER, true);

        try {
            /** @var BAS_Shared_Model_OrderWorkshop $orderWorkshop */
            $orderWorkshop = $orderWorkshopMapper->findOneByCondition([
                $orderWorkshopMapper->mapFieldToDb('workorderId') . ' = ?' => $id,
                $orderWorkshopMapper->mapFieldToDb('depotId') . ' IN(?)' => $affiliateMasterDepotIds,
                $orderWorkshopMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_OrderWorkshop::NOT_ARCHIVED,
            ]);

        } catch (BAS_Shared_NotFoundException $e) {
            $orderWorkshop = null;
        }

        return $orderWorkshop;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return BAS_Shared_Model_OrderVehicle|null
     * @throws BAS_Shared_Exception
     */
    public function getWorkorderOrderVehicle(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        if ((int)$workorder->getType() !== BAS_Shared_Model_OrderWorkshop::TYPE_ORDER) {
            return null;
        }

        if ($workorder->getVehicleId() <= 0) {
            return null;
        }

        /** @var Order_Service_OrderVehicle $orderVehicleService */
        $orderVehicleService = $this->getService('Order_Service_OrderVehicle');
        $orderVehicle = $orderVehicleService->find($workorder->getVehicleId());

        return $orderVehicle;
    }

    /**
     * @param string $vehicleLicencePlate
     * @return BAS_Shared_Model_OrderWorkshop|bool
     */
    public function findByContactVehicleLicencePlate($vehicleLicencePlate)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        try {
            return $orderWorkshopMapper->findByContactVehicleLicencePlate($vehicleLicencePlate);
        } catch (BAS_Shared_NotFoundException $e) {
            return false;
        }
    }

    /**
     * @param string $truckId
     * @return BAS_Shared_Model_OrderWorkshop|bool
     */
    public function findByTruckId($truckId)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        try {
            return  $orderWorkshopMapper->findByTruckId($truckId);
        } catch (BAS_Shared_NotFoundException $e) {
            return false;
        }
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float|int
     * @throws BAS_Shared_Exception
     */
    public function getWorkorderVatRate(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        /** @var Warehouse_Service_OrderItemInvoice $orderItemInvoiceService */
        $orderItemInvoiceService = $this->getService('Warehouse_Service_OrderItemInvoice');

        $invoiceVatType = $workorder->getInvoiceSelectedVatType();
        $invoiceVatRate = $orderItemInvoiceService->getVatRate($invoiceVatType, $workorder->getDepotId(), $workorder->getStatusOrderDate());

        return $invoiceVatRate;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return array
     */
    public function calcGrossAmountByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return [
            'internal' => (float) $this->calcInternalGrossAmount($workorder),
            'external' => (float) $this->calcExternalGrossAmount($workorder),
            'guarantee' => (float) $this->calcGuaranteeGrossAmount($workorder),
            'contra' => (float) $this->calcContraGrossAmount($workorder),
        ];
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float|int
     */
    public function calcInternalGrossAmount(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::add(
            BAS_Shared_Utils_Math::add(
                round($workorder->getPriceInternalTask(), 2),
                round($workorder->getPriceInternalItem(), 2),
                BAS_Shared_Utils_Math::SCALE_4
            ),
            round($workorder->getPriceInternalWorkExternal(), 2),
            BAS_Shared_Utils_Math::SCALE_4
        );
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float|int
     */
    public function calcExternalGrossAmount(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::add(
            BAS_Shared_Utils_Math::add(
                round($workorder->getPriceExternalTask(), 2),
                round($workorder->getPriceExternalItem(), 2),
                BAS_Shared_Utils_Math::SCALE_4
            ),
            round($workorder->getPriceExternalWorkExternal(), 2),
            BAS_Shared_Utils_Math::SCALE_4
        );
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float|int
     */
    public function calcGuaranteeGrossAmount(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::sum([
            $workorder->getPriceGuaranteeTask(),
            $workorder->getPriceGuaranteeItem(),
            $workorder->getPriceGuaranteeWorkExternal(),
        ], BAS_Shared_Utils_Math::SCALE_4);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float|int
     */
    public function calcContraGrossAmount(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::sum([
            $workorder->getPriceContraTask(),
            $workorder->getPriceContraItem(),
            $workorder->getPriceContraWorkExternal(),
        ], BAS_Shared_Utils_Math::SCALE_4);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return array
     */
    public function calcNetAmountByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return [
            'internal' => (float) $this->calcInternalNetAmount($workorder),
            'external' => (float) $this->calcExternalNetAmount($workorder),
            'guarantee' => (float) $this->calcGuaranteeNetAmount($workorder),
            'contra' => (float) $this->calcContraNetAmount($workorder),
        ];
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float|int
     */
    public function calcInternalNetAmount(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::sub(
            $this->calcInternalGrossAmount($workorder),
            $workorder->getDiscountInternal(),
            BAS_Shared_Utils_Math::SCALE_4
        );
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float|int
     */
    public function calcExternalNetAmount(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::sub(
            $this->calcExternalGrossAmount($workorder),
            $workorder->getDiscountExternal(),
            BAS_Shared_Utils_Math::SCALE_4
        );
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float|int
     */
    public function calcGuaranteeNetAmount(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::sub(
            $this->calcGuaranteeGrossAmount($workorder),
            $workorder->getDiscountGuarantee(),
            BAS_Shared_Utils_Math::SCALE_4
        );
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float|int
     */
    public function calcContraNetAmount(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::sub(
            $this->calcContraGrossAmount($workorder),
            $workorder->getDiscountContra(),
            BAS_Shared_Utils_Math::SCALE_4
        );
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return string
     */
    public function calcExternalTotalExclVatByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::sum([
            $this->calcExternalNetAmount($workorder),
            round($workorder->getPriceExternalConsumables(), 2),
            round($workorder->getPriceExternalEnvironment(), 2),
        ], BAS_Shared_Utils_Math::SCALE_4);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return string
     */
    public function calcInternalTotalExclVatByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::sum([
            $this->calcInternalNetAmount($workorder),
            round($workorder->getPriceInternalConsumables(), 2),
            round($workorder->getPriceInternalEnvironment(), 2),
        ], BAS_Shared_Utils_Math::SCALE_4);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return string
     */
    public function calcGuaranteeTotalExclVatByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return BAS_Shared_Utils_Math::sum([
            $this->calcGuaranteeNetAmount($workorder),
            round($workorder->getPriceGuaranteeEnvironment(), 2),
            round($workorder->getPriceGuaranteeConsumables(), 2),
        ], BAS_Shared_Utils_Math::SCALE_4);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return string
     */
    public function calcContraTotalExclVatByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return $this->calcContraNetAmount($workorder);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return string
     */
    public function calcExternalVatAmountByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        $total = $this->calcExternalTotalExclVatByWorkorder($workorder);
        $vatRate = $this->getWorkorderVatRate($workorder);

        return $this->calcVatAmount($total, $vatRate);
    }

    /**
     * @param $amount
     * @param $vatRate
     * @return string
     */
    public function calcVatAmount($amount, $vatRate)
    {
        return BAS_Shared_Utils_Math::mul(
            BAS_Shared_Utils_Math::div(
                $vatRate,
                100,
                BAS_Shared_Utils_Math::SCALE_4
            ),
            $amount,
            BAS_Shared_Utils_Math::SCALE_4
        );
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return string
     */
    public function calcExternalTotalInclVatByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        $total = $this->calcExternalTotalExclVatByWorkorder($workorder);
        $vatRate = $this->getWorkorderVatRate($workorder);
        $vatAmount = $this->calcVatAmount($total, $vatRate);

        return $this->calcTotalWithVat($total, $vatAmount);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return array
     */
    public function calcFinalTotalsByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return [
            'internal' => $this->calcInternalTotalExclVatByWorkorder($workorder),
            'external' => $this->calcExternalTotalExclVatByWorkorder($workorder),
            'guarantee' => $this->calcGuaranteeTotalExclVatByWorkorder($workorder),
            'contra' => $this->calcContraTotalExclVatByWorkorder($workorder),
        ];
    }

    /**
     * @param int $vehicleId
     * @return float
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function calcInternalTotalAmountByVehicleId($vehicleId)
    {
        $vehicleId = (int)$vehicleId;
        if (0 === $vehicleId) {
            throw new BAS_Shared_InvalidArgumentException('Invalid vehicleId specified');
        }

        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        /** @var BAS_Shared_Model_OrderWorkshop[] $workorders */
        $workorders = $orderWorkshopMapper->findAllByCondition([
            $orderWorkshopMapper->mapFieldToDb('vehicleId') . ' = ?' => $vehicleId,
            $orderWorkshopMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_OrderWorkshop::NOT_ARCHIVED,
            $orderWorkshopMapper->mapFieldToDb('type') . ' IN (?)' => [
                BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT,
                BAS_Shared_Model_OrderWorkshop::TYPE_STOCK,
                BAS_Shared_Model_OrderWorkshop::TYPE_INCOMING_VEHICLES,
                BAS_Shared_Model_OrderWorkshop::TYPE_SALES_PREPARATION,
            ],
            $orderWorkshopMapper->mapFieldToDb('status') . ' IN (?)' => [
                BAS_Shared_Model_OrderWorkshop::WORKORDER_START,
                BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED,
                BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED,
            ],
        ]);

        $internalTotalAmount = 0.0;

        foreach ($workorders as $workorder) {
            $internalTotalAmount = BAS_Shared_Utils_Math::add(
                $internalTotalAmount,
                $this->calcInternalTotalExclVatByWorkorder($workorder),
                BAS_Shared_Utils_Math::SCALE_4
            );
        }

        return $internalTotalAmount;
    }

    /**
     * @param $amount
     * @param $vatAmount
     * @return string
     */
    public function calcTotalWithVat($amount, $vatAmount)
    {
        return BAS_Shared_Utils_Math::add($amount, $vatAmount, BAS_Shared_Utils_Math::SCALE_4);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param int $bookingCodeType
     * @return string
     * @throws BAS_Shared_Exception
     */
    public function getTotalExclVat(BAS_Shared_Model_OrderWorkshop $workorder, $bookingCodeType)
    {
        if ($bookingCodeType === BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_INTERNAL) {
            return $this->calcInternalTotalExclVatByWorkorder($workorder);
        }

        if ($bookingCodeType === BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_EXTERNAL) {
            return $this->calcExternalTotalExclVatByWorkorder($workorder);
        }

        if ($bookingCodeType === BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_GUARANTEE) {
            return $this->calcGuaranteeTotalExclVatByWorkorder($workorder);
        }

        if ($bookingCodeType === BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_CONTRA_BOOKING) {
            return $this->calcContraTotalExclVatByWorkorder($workorder);
        }

        throw new BAS_Shared_Exception('Invalid booking code type value provided: ' . $bookingCodeType);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return array
     */
    public function getSurchargesByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        $surcharges = $this->calculateSurcharges($workorder);

        $consumableSum = BAS_Shared_Utils_Math::sum([
            $surcharges['priceInternalConsumables'],
            $surcharges['priceExternalConsumables'],
            $surcharges['priceGuaranteeConsumables'],
        ], BAS_Shared_Utils_Math::SCALE_4);

        $environmentSum = BAS_Shared_Utils_Math::sum([
            $surcharges['priceInternalEnvironment'],
            $surcharges['priceExternalEnvironment'],
            $surcharges['priceGuaranteeEnvironment'],
        ], BAS_Shared_Utils_Math::SCALE_4);

        return [
            'consumable' => $consumableSum,
            'environment' => $environmentSum,

            'consumableInternal' => $workorder->priceInternalConsumables === null ? $surcharges['priceInternalConsumables'] : $workorder->priceInternalConsumables,
            'consumableExternal' => $workorder->priceExternalConsumables === null ? $surcharges['priceExternalConsumables'] : $workorder->priceExternalConsumables,
            'consumableGuarantee' => $workorder->priceGuaranteeConsumables === null ? $surcharges['priceGuaranteeConsumables'] : $workorder->priceGuaranteeConsumables,

            'environmentInternal' => $workorder->priceInternalEnvironment === null ? $surcharges['priceInternalEnvironment'] : $workorder->priceInternalEnvironment,
            'environmentExternal' => $workorder->priceExternalEnvironment === null ? $surcharges['priceExternalEnvironment'] : $workorder->priceExternalEnvironment,
            'environmentGuarantee' => $workorder->priceGuaranteeEnvironment === null ? $surcharges['priceGuaranteeEnvironment'] : $workorder->priceGuaranteeEnvironment,
        ];
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return array
     */
    public function calculateSurcharges(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        /** @var BAS_Shared_Model_Workshop_WorkshopSurchargeMapper $surchargeMapper */
        $surchargeMapper = $this->getMapper('Workshop_WorkshopSurcharge');

        /** @var BAS_Shared_Model_Workshop_WorkshopSurcharge $consumableSurcharge */
        $consumableSurcharge = $surchargeMapper->findOneByType(BAS_Shared_Model_Workshop_WorkshopSurcharge::TYPE_CONSUMABLES);
        /** @var BAS_Shared_Model_Workshop_WorkshopSurcharge $environmentSurcharge */
        $environmentSurcharge = $surchargeMapper->findOneByType(BAS_Shared_Model_Workshop_WorkshopSurcharge::TYPE_ENVIRONMENT);

        // consumable
        $consumableInternal = $this->getAmountSurchargeByModel(
            $consumableSurcharge,
            $workorder->priceInternalTask,
            $workorder->priceInternalItem
        );

        $consumableExternal = $this->getAmountSurchargeByModel(
            $consumableSurcharge,
            $workorder->priceExternalTask,
            $workorder->priceExternalItem
        );

        $consumableGuarantee = $this->getAmountSurchargeByModel(
            $consumableSurcharge,
            $workorder->priceGuaranteeTask,
            $workorder->priceGuaranteeItem
        );

        // environment
        $environmentInternal = $this->getAmountSurchargeByModel(
            $environmentSurcharge,
            $workorder->priceInternalTask,
            $workorder->priceInternalItem
        );

        $environmentExternal = $this->getAmountSurchargeByModel(
            $environmentSurcharge,
            $workorder->priceExternalTask,
            $workorder->priceExternalItem
        );

        $environmentGuarantee = $this->getAmountSurchargeByModel(
            $environmentSurcharge,
            $workorder->priceGuaranteeTask,
            $workorder->priceGuaranteeItem
        );

        return [
            'priceInternalConsumables' => BAS_Shared_Utils_Math::add($consumableInternal, $consumableGuarantee, BAS_Shared_Utils_Math::SCALE_4),
            'priceExternalConsumables' => $consumableExternal,
            'priceGuaranteeConsumables' => 0,
            'priceInternalEnvironment' => BAS_Shared_Utils_Math::add($environmentInternal, $environmentGuarantee, BAS_Shared_Utils_Math::SCALE_4),
            'priceExternalEnvironment' => $environmentExternal,
            'priceGuaranteeEnvironment' => 0,
        ];
    }

    /**
     * @param BAS_Shared_Model_Workshop_WorkshopSurcharge $surcharge
     * @param int $totalTask
     * @param int $totalItem
     * @return int
     */
    public function getAmountSurchargeByModel(BAS_Shared_Model_Workshop_WorkshopSurcharge $surcharge, $totalTask, $totalItem)
    {
        $surchargeRate = $surcharge->getRate() / 100;
        $maxSurchargeAmount = $surcharge->getCalculationMaxAmount();

        switch ($surcharge->getCalculationType()) {

            case BAS_Shared_Model_Workshop_WorkshopSurcharge::CALCULATION_TYPE_OVER_TASKS:
                $surchargeAmount = BAS_Shared_Utils_Math::mul($surchargeRate, $totalTask, BAS_Shared_Utils_Math::SCALE_4);
                break;

            case BAS_Shared_Model_Workshop_WorkshopSurcharge::CALCULATION_TYPE_OVER_ITEMS:
                $surchargeAmount = BAS_Shared_Utils_Math::mul($surchargeRate, $totalItem, BAS_Shared_Utils_Math::SCALE_4);
                break;

            default:
                $surchargeAmount = BAS_Shared_Utils_Math::mul(
                    $surchargeRate,
                    BAS_Shared_Utils_Math::add($totalTask, $totalItem, BAS_Shared_Utils_Math::SCALE_4),
                    BAS_Shared_Utils_Math::SCALE_4
                );
        }

        if ($surchargeAmount > 0 && $surchargeAmount > $maxSurchargeAmount) {
            $surchargeAmount = $maxSurchargeAmount;
        } elseif ($surchargeAmount < 0 && abs($surchargeAmount) > $maxSurchargeAmount) {
            $surchargeAmount = -$maxSurchargeAmount;
        }

        return $surchargeAmount;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return array
     */
    public function getCustomerInformation(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        if (empty($workorder->getContactId())) {
            return [];
        }

        /** @var Contacts_Service_Contact $contactService */
        $contactService = $this->getService('Contacts_Service_Contact');

        /** @var Contacts_Service_Address $addressService */
        $addressService = $this->getService('Contacts_Service_Address');

        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        /** @var Management_Service_DepotSetting $depotSettingService */
        $depotSettingService = $this->getService('Management_Service_DepotSetting');
        $mainAccountingDepotId = $depotSettingService->getMainAccountingDepot($workorder->getDepotId());

        try {
            $workOrderContact = $contactService->find($workorder->getContactId());
        } catch (BAS_Shared_NotFoundException $e) {
            $workOrderContact = new Contacts_Library_ContactDecorator(new BAS_Shared_Model_Contact());
        }

        $workOrderPerson = new BAS_Shared_Model_Contact();
        try {
            if ($workorder->getPersonId() !== null) {
                $workOrderPerson = $contactService->findById($workorder->getPersonId());
            }
        } catch (BAS_Shared_NotFoundException $e) {}

        $companyIndustry = $contactService->findCompanyIndustry($workorder->getContactId(), $mainAccountingDepotId);
        $priorityCode = $contactService->getPriorityCode($workorder->getContactId());

        $customerInformation = [
            'name' => $workOrderContact->getName(),
            'industry' => $companyIndustry->name,
            'customerPriority' => $priorityCode,
            'contactPerson' => $workOrderPerson->getName(),
            'invoiceAddressCompany' => '',
            'debtorNumber' => $mainAccountingDepotId,
            'creditLimit' => '',
            'sumOpenAmount' => '',
            'contactAddressContactId' => '',
        ];

        if ($workorder->getInvoiceAddressId() > 0) {
            try {
                $invoiceAddress = $addressService->find($workorder->getInvoiceAddressId());
                $customerInformation['invoiceAddressCompany'] = $invoiceAddress->getCompany();
                $customerInformation['contactAddressContactId'] = $invoiceAddress->getContactId();
            } catch (BAS_Shared_NotFoundException $e) {}
        }

        $resultCustomerInformation = $orderWorkshopMapper->findCustomerInformation($workorder->getWorkorderId(), $mainAccountingDepotId);

        return array_merge($customerInformation, $resultCustomerInformation);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return array
     */
    public function getVehicleInformation(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        /** @var Vehicles_Service_Vehicle $vehicleService */
        $vehicleService = $this->getService('Vehicles_Service_Vehicle');

        $resultVehicleInformation = [];
        $vehicleInformation = [
            'make' => '',
            'type' => '',
            'configuration' => '',
            'frameNumber' => '',
            'licencePlate' => '',
            'driver' => '',
            'truckId' => '',
            'vehicleId' => '',
            'registrationDate' => '',
            'location' => '',
            'mileage' => $workorder->getMileage(),
        ];

        $vehicleSource = $workorder->getVehicleSource();

        if ($vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_FLEET) {

            $resultVehicleInformation = $this->findContactVehicleInformation($workorder->getVehicleId());

        } elseif (
            $vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_STOCK ||
            $vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_ORDER_VEHICLE
        ) {
            $vehicleId = $vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_STOCK ?
                $workorder->getVehicleId() :
                $this->getWorkorderLegacyVehicleId($workorder)
            ;

            $vehicle = $vehicleService->findByVehicleId($vehicleId);
            $vehicleInformation['location'] = $this->getTranslate()->translate('delivered');

            if ($vehicle->getTagId() !== BAS_Shared_Model_Vehicle::TAG_DELIVERED) {
                $vehicleInformation['location'] = $vehicle->getLocation();

                $tag = $vehicleService->getLocation($vehicleId);
                if (!empty($tag['Zone'])) {
                    $vehicleInformation['location'] = $tag['Zone'];
                }
            }

            $resultVehicleInformation = [
                'make'          => $vehicle->getBrand(),
                'type'          => $vehicle->getType(),
                'configuration' => $vehicle->getConfiguration(),
                'frameNumber'   => $vehicle->getChassisNumber(),
                'licencePlate'  => $vehicle->getlicensePlate(),
                'driver'        => $vehicle->getSupplierName(),
                'truckId'       => $vehicle->getTruckId(),
                'vehicleId'       => $vehicleId,
                'registrationDate' => $vehicle->getFirstRegistration(),
            ];
        }

        return array_merge($vehicleInformation, $resultVehicleInformation);
    }

    /**
     * @param int $contactVehicleId
     * @return array
     */
    public function findContactVehicleInformation($contactVehicleId)
    {
        if (!$contactVehicleId) return [];

        /** @var BAS_Shared_Model_ContactVehicleMapper $contactVehicleMapper */
        $contactVehicleMapper = $this->getMapper('ContactVehicle');

        try {
            $contactVehicle = $contactVehicleMapper->getContactVehicleOverviewById($contactVehicleId);
        } catch (BAS_Shared_NotFoundException $e) {
            return [];
        }

        return [
            'make' => $contactVehicle['brandTitle'],
            'type' => $contactVehicle['typeTitle'],
            'configuration' => $contactVehicle['configurationTitle'],
            'frameNumber' => $contactVehicle['vin'],
            'licencePlate' => $contactVehicle['license_plate'],
            'driver' => $contactVehicle['driver'],
            'truckId' => $contactVehicle['vehicleId'],
            'vehicleId' => $contactVehicle['vehicleId'],
            'registrationDate' => $contactVehicle['registration'],
        ];
    }

    /**
     * @param int $depotId
     * @return array
     */
    public function getUserOptions($depotId)
    {
        /** @var BAS_Shared_Model_PersonMapper $personMapper */
        $personMapper = $this->getMapper('Person');
        $personsData = $personMapper->getPersonsByDepotAndDepartment($depotId, BAS_Shared_Model_Department::DEPARTMENT_ID_WORKSHOP);
        return BAS_Shared_Utils_Array::listData($personsData, 'id', 'name');
    }

    /**
     * @param $vehicleId
     * @return BAS_Shared_Model_OrderWorkshop[]
     */
    public function getOpenWorkordersForVehicle($vehicleId)
    {
        $vehicleId = (int)$vehicleId;

        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        return $orderWorkshopMapper->findAllByCondition([
            $orderWorkshopMapper->mapFieldToDb('vehicleId') . ' = ?' => $vehicleId,
            $orderWorkshopMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_OrderWorkshop::NOT_ARCHIVED,
            '(status IN (' . implode(',', [
                BAS_Shared_Model_OrderWorkshop::WORKORDER_START,
                BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED,
            ]) . ') OR status IS NULL)',
        ]);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param int $userId
     * @return bool
     */
    public function isWorkorderReadonly(BAS_Shared_Model_OrderWorkshop $workorder, $userId)
    {
        /** @var Payment_Service_LockingWorkorder $workorderLockService */
        $workorderLockService = $this->getService('Payment_Service_LockingWorkorder');

        return in_array($workorder->getStatus(), [
            BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED,
            BAS_Shared_Model_OrderWorkshop::WORKORDER_ARCHIVED,
            BAS_Shared_Model_OrderWorkshop::WORKORDER_INVOICED,
        ]) || $workorderLockService->isLockedForUser($workorder->getWorkorderId(), $userId);
    }

    /**
     * @param int $workorderId
     * @param array $detailsData
     * @param int $userId
     * @throws Exception
     */
    public function saveWorkorderDetails($workorderId, array $detailsData, $userId)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        if ($detailsData === []) {
            return;
        }

        $db = $this->getDb();

        try {
            $db->beginTransaction();

            $workorder = $this->findById($workorderId);

            if ($this->isWorkorderReadonly($workorder, $userId)) {
                throw new BAS_Shared_Exception('Cannot update details due to workorder status: ' . BAS_Shared_Model_OrderWorkshop::$statusLabel[$workorder->getStatus()]);
            }

            foreach ($detailsData as $detailData) {
                $detailData = array_intersect_key($detailData, $orderWorkshopDetailMapper->getMap());
                $updatedDetail = new BAS_Shared_Model_Workshop_OrderWorkshopDetail($detailData);
                $originalDetail = $workorderDetailService->findById($updatedDetail->getId());
                $this->saveWorkorderDetail($workorder, $updatedDetail, $originalDetail, $userId);
            }

            $workorderDetailService->updateActualPriceOfWorkorderComposedTasks($workorderId);
            $this->updateWorkorderPricesAndDiscounts($workorderId, $userId);

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        $this->getEventManager()->trigger(
            BAS_Shared_Model_MapperEvent::EVENT_WORKORDER_UPDATE_TOTAL_INVOICE_PRICE,
            $this,
            ['workorderId' => $workorderId]
        );
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $updatedDetail
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @param int $userId
     * @return bool
     * @throws BAS_Shared_Exception
     */
    private function saveWorkorderDetail(
        BAS_Shared_Model_OrderWorkshop $workorder,
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $updatedDetail,
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail,
        $userId
    ) {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');
        /** @var Warehouse_Service_ItemStock $itemStockService */
        $itemStockService = $this->getService('Warehouse_Service_ItemStock');
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $detailUpdateData = $this->getDetailUpdateData(
            $updatedDetail,
            $originalDetail,
            $userId
        );

        if ($detailUpdateData === []) {
            return false;
        }

        $orderWorkshopDetailMapper->updateFieldsByCondition(
            $orderWorkshopDetailMapper->mapToDb($detailUpdateData),
            ['id = ?' => $updatedDetail->getId()]
        );

        $isQuantityChanged =
            array_key_exists('quantity', $detailUpdateData)
            && BAS_Shared_Utils_Math::compareFloat(
                $detailUpdateData['quantity'],
                $originalDetail->getQuantity(),
                BAS_Shared_Utils_Math::SCALE_2
            ) !== 0;

        if ($originalDetail->isItem()) {
            // update stock

            if ($isQuantityChanged) {
                $quantityDelta = BAS_Shared_Utils_Math::sub(
                    $detailUpdateData['quantity'],
                    $originalDetail->getQuantity(),
                    BAS_Shared_Utils_Math::SCALE_2
                );

                $itemStockService->updateQuantityByWorkorderDetailId(
                    $originalDetail->getId(),
                    $quantityDelta,
                    $userId
                );
            }

            $isOrderPriceChanged =
                array_key_exists('priceOrder', $detailUpdateData)
                && BAS_Shared_Utils_Math::compareFloat(
                    $detailUpdateData['priceOrder'],
                    $originalDetail->getPriceOrder(),
                    BAS_Shared_Utils_Math::SCALE_4
                ) !== 0;

            $isOriginalPriceChanged =
                array_key_exists('priceOriginal', $detailUpdateData)
                && BAS_Shared_Utils_Math::compareFloat(
                    $detailUpdateData['priceOriginal'],
                    $originalDetail->getPriceOriginal(),
                    BAS_Shared_Utils_Math::SCALE_4
                ) !== 0;

            if ($isOrderPriceChanged || $isOriginalPriceChanged) {
                $itemStockService->updatePriceByWorkorderDetailId(
                    $originalDetail->getId(),
                    $userId
                );
            }
        }

        if ($originalDetail->isTask() && $originalDetail->isMainDetail() && $isQuantityChanged) {
            $workorderDetailService->updateQuantityOfSubDetails($originalDetail, $detailUpdateData['quantity'], $userId);
        }

        return true;
    }

    /**
     * @param int $detailId
     * @param int $userId
     * @return true
     * @throws Exception
     */
    public function deleteWorkorderDetail($detailId, $userId)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $db = $this->getDb();

        try {
            $db->beginTransaction();

            $workorderDetail = $workorderDetailService->findById($detailId);
            $workorder = $this->findById($workorderDetail->getOrderWorkshopId());

            if ($this->isWorkorderReadonly($workorder, $userId)) {
                throw new BAS_Shared_Exception('Cannot delete detail due to workorder status: ' . BAS_Shared_Model_OrderWorkshop::$statusLabel[$workorder->getStatus()]);
            }

            $this->returnDetailToStock($workorderDetail, $userId);
            $workorderDetailService->delete($workorderDetail->getId(), $userId);
            $this->updateWorkorderPricesAndDiscounts($workorderDetail->getOrderWorkshopId(), $userId);

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        return true;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $workorderDetail
     * @param int $userId
     * @throws BAS_Shared_Exception
     */
    private function returnDetailToStock(BAS_Shared_Model_Workshop_OrderWorkshopDetail $workorderDetail, $userId)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');
        /** @var Warehouse_Service_ItemStock $itemStockService */
        $itemStockService = $this->getService('Warehouse_Service_ItemStock');

        $subItems = $workorderDetail->isMainDetail() ? $workorderDetailService->findSubItems($workorderDetail->getId()) : [];

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetail[] $itemsToDelete */
        $itemsToDelete = $workorderDetail->isItem() ? [$workorderDetail] : [];
        $itemsToDelete = array_merge($itemsToDelete, $subItems);

        foreach ($itemsToDelete as $itemToDelete) {
            $quantityDelta = BAS_Shared_Utils_Math::neg($itemToDelete->getQuantity(), BAS_Shared_Utils_Math::SCALE_2);

            $itemStockService->updateQuantityByWorkorderDetailId(
                $itemToDelete->getId(),
                $quantityDelta,
                $userId
            );
        }
    }

    /**
     * @param int $workorderId
     * @param int $userId
     * @throws BAS_Shared_Exception
     */
    public function updateWorkorderPricesAndDiscounts($workorderId, $userId)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        $workorder = $this->findById($workorderId);
        $workorderDetailsInfo = $workorderDetailService->getDetailsInfoByWorkorderId($workorderId);

        $workorderTotals = $this->calculateWorkorderTotalsByDetailsInfo($workorderDetailsInfo);
        $workorder->populate($workorderTotals);
        $workorderSurcharges = $this->calculateSurcharges($workorder);
        
        $updateData = array_merge($workorderTotals, $workorderSurcharges, [
            'updatedBy' => $userId,
            'updatedAt' => $this->getCurrentDate(),
        ]);
        
        $orderWorkshopMapper->updateFieldsByCondition(
            $orderWorkshopMapper->mapToDb($updateData),
            [$orderWorkshopMapper->mapFieldToDb('workorderId') . ' = ?' => (int)$workorderId]
        );

        $this->getEventManager()->trigger(
            BAS_Shared_Model_MapperEvent::EVENT_WORKORDER_UPDATE_TOTAL_INVOICE_PRICE,
            $this,
            ['workorderId' => $workorderId]
        );
    }

    /**
     * @param int $workorderId
     * @param int $vatType
     * @param string $reasonText
     * @return bool|int
     */
    public function changeVatType($workorderId, $vatType, $reasonText)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        return $orderWorkshopMapper->updateFieldsByCondition([
            $orderWorkshopMapper->mapFieldToDb('invoiceSelectedVatType') => (int)$vatType,
            $orderWorkshopMapper->mapFieldToDb('invoiceVatTypeChangeReason') => (string)$reasonText,
        ], [
            $orderWorkshopMapper->mapFieldToDb('workorderId') . ' = ?' => (int)$workorderId
        ]);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return int|null
     */
    public function getDebtorNumberByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        /** @var Management_Service_DepotSetting $depotSettingsService */
        $depotSettingsService = $this->getService('Management_Service_DepotSetting');
        /** @var Contacts_Service_ContactTypeDebtor $contactTypeDebtorService */
        $contactTypeDebtorService = $this->getService('Contacts_Service_ContactTypeDebtor');

        if ($this->isInternalWorkorder($workorder->getType()) && $workorder->getContactId() === null) {
            return (int)$depotSettingsService->getSettingValue($workorder->getDepotId(), BAS_Shared_Model_Setting::DEBTOR_DEFAULT_NUMBER);
        }

        $mainAccountingDepotId = $depotSettingsService->getMainAccountingDepot($workorder->getDepotId());
        $contactTypeDebtor = $contactTypeDebtorService->getContactTypeDebtor($workorder->getInvoiceAddressId(), $mainAccountingDepotId, null);

        if ($contactTypeDebtor === false) {
            return null;
        }

        return $contactTypeDebtor->getDebtorId();
    }

    /**
     * @param int $workorderId
     * @return string
     */
    public function getInvoiceLanguage($workorderId)
    {
        /** @var Contacts_Service_Contact $contactService */
        $contactService = $this->getService('Contacts_Service_Contact');

        $workorder = $this->findById($workorderId);

        if ($workorder->getPersonId() === null) {
            return self::DEFAULT_INVOICE_LANGUAGE;
        }

        try {
            $person = $contactService->findById($workorder->getPersonId());
        } catch (BAS_Shared_NotFoundException $e) {
            return self::DEFAULT_INVOICE_LANGUAGE;
        }

        return strtolower($person->getLanguageCode());
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[] $detailsInfo
     * @return array
     */
    public function calculateWorkorderTotalsByDetailsInfo(array $detailsInfo)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $totals = [
            'priceExternalItem' => null,
            'priceInternalItem' => null,
            'priceGuaranteeItem' => null,
            'priceContraItem' => null,

            'priceExternalTask' => null,
            'priceInternalTask' => null,
            'priceGuaranteeTask' => null,
            'priceContraTask' => null,

            'priceExternalWorkExternal' => null,
            'priceInternalWorkExternal' => null,
            'priceGuaranteeWorkExternal' => null,
            'priceContraWorkExternal' => null,

            'discountExternal' => null,
            'discountInternal' => null,
            'discountGuarantee' => null,
            'discountContra' => null,
        ];

        foreach ($detailsInfo as $detailInfo) {

            $priceFieldName = 'price';
            $discountFieldName = 'discount';

            switch ($detailInfo->getBookingCodeType()) {
                case BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_INTERNAL:
                    $priceFieldName .= 'Internal';
                    $discountFieldName .= 'Internal';
                    break;
                case BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_EXTERNAL:
                    $priceFieldName .= 'External';
                    $discountFieldName .= 'External';
                    break;
                case BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_GUARANTEE:
                    $priceFieldName .= 'Guarantee';
                    $discountFieldName .= 'Guarantee';
                    break;
                case BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_CONTRA_BOOKING:
                    $priceFieldName .= 'Contra';
                    $discountFieldName .= 'Contra';
                    break;
                default: break;
            }

            if ($detailInfo->getType() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM) {
                $priceFieldName .= 'Item';
            } else {
                if ($detailInfo->getWorkshopTaskType() === BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL) {
                    $priceFieldName .= 'WorkExternal';
                } else {
                    $priceFieldName .= 'Task';
                }
            }

            if ($detailInfo->isComposedSubDetail() &&
                $detailInfo->getBookingCodeType() !== BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_CONTRA_BOOKING
            ) {
                // totals will be calculated based on main detail
                continue;
            }

            if (!array_key_exists($priceFieldName, $totals)) {
                continue;
            }

            $detailOriginalPrice = $detailInfo->getPriceOriginal() === null
                ? $detailInfo->getPriceOrder()
                : $detailInfo->getPriceOriginal();

            $detailAmount = BAS_Shared_Utils_Math::mul(
                $detailInfo->getQuantity(),
                $detailOriginalPrice,
                BAS_Shared_Utils_Math::SCALE_4
            );

            $totals[$priceFieldName] = BAS_Shared_Utils_Math::add(
                $totals[$priceFieldName],
                $detailAmount,
                BAS_Shared_Utils_Math::SCALE_4
            );

            $totals[$discountFieldName] = BAS_Shared_Utils_Math::add(
                $totals[$discountFieldName],
                round($workorderDetailService->calculateDetailDiscountAmount($detailInfo), BAS_Shared_Utils_Math::SCALE_2),
                BAS_Shared_Utils_Math::SCALE_4
            );
        }

        return $totals;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $updatedDetail
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @param int $userId
     * @return array
     * @throws BAS_Shared_Exception
     */
    public function getDetailUpdateData(
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $updatedDetail,
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail,
        $userId
    ) {
        $updateData = $this->getUpdateValues($updatedDetail, $originalDetail);
        $readOnlyFields = $this->getReadonlyFields($originalDetail, $userId);
        $nullFields = $this->getNullFields($originalDetail);

        $updateData = array_diff_key($updateData, array_flip($readOnlyFields));

        $spentTime = array_key_exists('spentTime', $updateData) ?
            $updateData['spentTime'] :
            $updatedDetail->getSpentTime()
        ;

        $invoiceTime = array_key_exists('invoiceTime', $updateData) ?
            $updateData['invoiceTime'] :
            $updatedDetail->getInvoiceTime()
        ;

        if ((float) $spentTime === (float) $originalDetail->getInvoiceTime()) {
            $updateData['spentTime'] = $invoiceTime;
        }

        foreach ($updateData as $updateField => $updateValue) {
            if ($updateValue === '') {
                $updateData[$updateField] = null;
            }
        }

        foreach ($nullFields as $nullField) {
            $updateData[$nullField] = null;
        }

        if ($updateData !== []) {
            $updateData['updatedAt'] = date('Y-m-d H:i:s');
            $updateData['updatedBy'] = $userId;
        }

        return $updateData;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $updatedDetail
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @return array
     * @throws BAS_Shared_Exception
     */
    private function getUpdateValues(
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $updatedDetail,
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
    ) {
        $updateData = [
            'sequence' => $updatedDetail->getSequence(),
            'invoiceDescription' => $updatedDetail->getInvoiceDescription(),
            'quantity' => $updatedDetail->getQuantity(),
            'priceOrder' => $updatedDetail->getPriceOrder(),
            'priceOriginal' => $updatedDetail->getPriceOriginal(),
            'depotBookingCodeId' => $updatedDetail->getDepotBookingCodeId(),
            'onInvoice' => $updatedDetail->getOnInvoice(),
            'labourRateId' => $updatedDetail->getLabourRateId(),
            'invoiceTime' => $updatedDetail->getInvoiceTime(),
            'spentTime' => $updatedDetail->getSpentTime(),
            'costPrice' => $updatedDetail->getCostPrice(),
            'supplierId' => $updatedDetail->getSupplierId(),
        ];

        if ($originalDetail->isItem()) {
            $updateData = $this->getItemUpdateData($updateData, $updatedDetail, $originalDetail);
        } elseif ($originalDetail->isTask()) {
            $updateData = $this->getTaskUpdateData($updateData, $updatedDetail, $originalDetail);
        }

        return $updateData;
    }

    /**
     * @param array $updateData
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $updatedDetail
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @return array
     * @throws BAS_Shared_Exception
     */
    private function getItemUpdateData(
        array $updateData,
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $updatedDetail,
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
    ) {
        /** @var Warehouse_Service_Item $itemService */
        $itemService = $this->getService('Warehouse_Service_Item');

        $item = $itemService->findById($originalDetail->getReferenceId());

        $updateData['priceOriginal'] = $item->isNoStockItem()
            ? $updatedDetail->getPriceOrder()
            : $originalDetail->getPriceOriginal();

        return $updateData;
    }

    /**
     * @param array $updateData
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $updatedDetail
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @return array
     * @throws BAS_Shared_Exception
     */
    private function getTaskUpdateData(
        array $updateData,
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $updatedDetail,
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
    ) {
        /** @var Order_Service_Product $productService */
        $productService = $this->getService('Order_Service_Product');
        /** @var Workshop_Service_OrderWorkshopDetail $orderWorkshopDetailService */
        $orderWorkshopDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $product = $productService->findByIdAndDepotId($originalDetail->getReferenceId(), $originalDetail->getDepotId());

        switch ($product->getWorkshopTaskType()) {
            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_HOUR_BASED:

                if ((int)$product->getWorkshopTimeType() === BAS_Shared_Model_Product::WORKSHOP_TIME_TYPE_ACTUAL) {

                    $price = $orderWorkshopDetailService->calculateTimeBasedPrice(
                        $updatedDetail->getInvoiceTime(),
                        $updatedDetail->getLabourRateId()
                    );

                    $updateData['priceOrder'] = $price;
                    $updateData['priceOriginal'] = $price;
                }

                break;

            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_FIXED_PRICE:

                if ($product->getFixedPriceOptional() === BAS_Shared_Model_Product::FIXED_PRICE_OPTIONAL_YES) {

                    $time = $orderWorkshopDetailService->calculatePriceBasedTime(
                        $updatedDetail->getPriceOrder(),
                        $updatedDetail->getLabourRateId()
                    );

                    $updateData['invoiceTime'] = $time;
                    $updateData['priceOriginal'] = $updatedDetail->getPriceOrder();
                }

                break;

            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL:

                $updateData['priceOriginal'] = $updateData['priceOrder'];

                break;

            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_COMPOSED:

                break;

            default: break;
        }

        return $updateData;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @param int $userId
     * @return array
     * @throws BAS_Shared_Exception
     */
    private function getReadonlyFields(BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail, $userId)
    {
        /** @var Order_Service_Product $productService */
        $productService = $this->getService('Order_Service_Product');
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $readOnlyFields = [];

        if ($originalDetail->isItem()) {
            $readOnlyFields = $this->getItemReadonlyFields($originalDetail, $userId);
        } elseif ($originalDetail->isTask()) {
            $readOnlyFields = $this->getTaskReadonlyFields($originalDetail);
        }

        if ($originalDetail->isSubDetail()) {
            $mainDetail = $workorderDetailService->findById($originalDetail->getMainOrderWorkshopDetailId());

            if ($mainDetail->isTask()) {
                $readOnlyFields[] = 'sequence';
                $readOnlyFields[] = 'depotBookingCodeId';

                $mainTask = $productService->findByIdAndDepotId($mainDetail->getReferenceId(), $mainDetail->getDepotId());

                if ($mainTask->getCompositionPriceType() === BAS_Shared_Model_Product::COMPOSITION_PRICE_TYPE_FIXED) {
                    $readOnlyFields = array_merge($readOnlyFields, [
                        'invoiceDescription',
                        'quantity',
                        'priceOriginal',
                        'priceOrder',
                        'depotBookingCodeId',
                        'onInvoice',
                        'labourRateId',
                        'invoiceTime',
                        'spentTime',
                    ]);
                }
            }
        }

        return $readOnlyFields;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @param int $userId
     * @return array
     * @throws BAS_Shared_Exception
     */
    private function getItemReadonlyFields(BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail, $userId)
    {
        /** @var Warehouse_Service_Item $itemService */
        $itemService = $this->getService('Warehouse_Service_Item');

        $item = $itemService->findById($originalDetail->getReferenceId());

        $readOnlyFields = [];

        $readOnlyFields[] = 'labourRateId';
        $readOnlyFields[] = 'invoiceTime';

        if ($originalDetail->isTradeIn() || $item->isStockItem()) {
            $readOnlyFields[] = 'costPrice';
        }

        if ($originalDetail->isTradeIn()) {
            $readOnlyFields[] = 'priceOrder';
            $readOnlyFields[] = 'priceOriginal';
        }

        $isAllowedDiscount = $this->hasUserAccess($userId, 'workshop.workorder.discount');

        if (!$isAllowedDiscount) {
            $readOnlyFields[] = 'priceOriginal';
            $readOnlyFields[] = 'priceOrder';
        }

        return $readOnlyFields;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @return array
     * @throws BAS_Shared_Exception
     */
    private function getTaskReadonlyFields(BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail)
    {
        /** @var Order_Service_Product $productService */
        $productService = $this->getService('Order_Service_Product');
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $product = $productService->findByIdAndDepotId($originalDetail->getReferenceId(), $originalDetail->getDepotId());
        $readOnlyFields = [];

        if ($product->getWorkshopTaskType() !== BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL) {
            $readOnlyFields[] = 'costPrice';
        }

        switch ($product->getWorkshopTaskType()) {
            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_HOUR_BASED:

                if ($product->getWorkshopTimeType() === BAS_Shared_Model_Product::WORKSHOP_TIME_TYPE_FIXED) {

                    $readOnlyFields[] = 'quantity';
                    $readOnlyFields[] = 'priceOriginal';
                    $readOnlyFields[] = 'priceOrder';
                    $readOnlyFields[] = 'labourRateId';
                    $readOnlyFields[] = 'invoiceTime';
                }

                break;

            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_FIXED_PRICE:

                if ($product->getFixedPriceOptional() !== BAS_Shared_Model_Product::FIXED_PRICE_OPTIONAL_YES) {

                    $readOnlyFields[] = 'priceOriginal';
                    $readOnlyFields[] = 'priceOrder';
                    $readOnlyFields[] = 'labourRateId';
                    $readOnlyFields[] = 'invoiceTime';
                }

                break;

            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL:

                $readOnlyFields[] = 'labourRateId';
                $readOnlyFields[] = 'invoiceTime';

                break;

            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_COMPOSED:

                $readOnlyFields[] = 'priceOriginal';
                $readOnlyFields[] = 'priceOrder';
                $readOnlyFields[] = 'labourRateId';
                $readOnlyFields[] = 'invoiceTime';

                if ($product->getCompositionPriceType() === BAS_Shared_Model_Product::COMPOSITION_PRICE_TYPE_COMPOSED) {

                    $readOnlyFields[] = 'quantity';
                }

                break;

            default: break;
        }

        return $readOnlyFields;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @return array
     */
    private function getNullFields(BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail)
    {
        $nullFields = [];

        if ($originalDetail->isItem()) {
            $nullFields = $this->getItemNullFields($originalDetail);
        } elseif ($originalDetail->isTask()) {
            $nullFields = $this->getTaskNullFields($originalDetail);
        }

        return $nullFields;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @return array
     */
    private function getItemNullFields(BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail)
    {
        /** @var Warehouse_Service_Item $itemService */
        $itemService = $this->getService('Warehouse_Service_Item');

        $item = $itemService->findById($originalDetail->getReferenceId());

        $nullFields = [
            'labourRateId',
            'invoiceTime'
        ];

        if ($item->isStockItem()) {
            $nullFields[] = 'supplierId';
        }

        return $nullFields;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail
     * @return array
     * @throws BAS_Shared_Exception
     */
    private function getTaskNullFields(BAS_Shared_Model_Workshop_OrderWorkshopDetail $originalDetail)
    {
        /** @var Order_Service_Product $productService */
        $productService = $this->getService('Order_Service_Product');

        $product = $productService->findByIdAndDepotId($originalDetail->getReferenceId(), $originalDetail->getDepotId());
        $nullFields = [];

        switch ($product->getWorkshopTaskType()) {

            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_FIXED_PRICE:
                $nullFields[] = 'supplierId';
                break;

            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_HOUR_BASED:
                $nullFields[] = 'supplierId';
                break;

            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL:
                $nullFields[] = 'labourRateId';
                $nullFields[] = 'invoiceTime';
                break;

            case BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_COMPOSED:
                $nullFields[] = 'labourRateId';
                $nullFields[] = 'invoiceTime';
                $nullFields[] = 'supplierId';
                break;

            default: break;
        }

        return $nullFields;
    }

    /**
     * @param int $workorderId
     * @param array $data
     * @param int $userId
     * @return BAS_Shared_Model_OrderWorkshop
     */
    public function saveWorkorderGeneralTabData($workorderId, array $data, $userId)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $originalWorkorder = $this->findById($workorderId);

        // prevent updates when order status is 'closed'
        if ($this->isWorkorderReadonly($originalWorkorder, $userId)) {
            return $originalWorkorder;
        }

        $data = $this->_normalizeEditGeneralTabSectionsByRequestData($data);

        $generalData = $this->_getWorkorderGeneralDbDataByRequest($originalWorkorder, $data[Workshop_Form_WorkorderGeneralTab::SECTION_GENERAL], $userId);
        $updateDetailsLabourRate = (bool)$data['general']['updateDetailsLabourRate'];
        $workorderType = array_key_exists('type', $generalData) ? $generalData['type'] : $originalWorkorder->getType();
        $customerData = $this->_getWorkorderCustomerDbDataByRequest($data[Workshop_Form_WorkorderGeneralTab::SECTION_CUSTOMER], $workorderType);
        $addressData = $this->_getWorkorderAddressDbDataByRequest($data[Workshop_Form_WorkorderGeneralTab::SECTION_ADDRESS], $customerData);

        $vehicleData = $this->_getWorkorderVehicleDbDataByRequest(
            $data[Workshop_Form_WorkorderGeneralTab::SECTION_VEHICLE],
            $data[Workshop_Form_WorkorderGeneralTab::SECTION_UNKNOWN_VEHICLE],
            $workorderType,
            $customerData
        );

        $updateData = array_merge($generalData, $customerData, $addressData, $vehicleData);

        if (!$this->isCustomerApplicableByWorkorderType($workorderType)
            && !($this->isInternalWorkorder($workorderType) && !empty($updateData['contactId']))) {
            $updateData['contactId'] = null;
            $updateData['personId'] = null;
            $updateData['invoiceAddressId'] = null;
        }

        if ($originalWorkorder->getType() === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER) {
            unset($updateData['type']);
            unset($updateData['vehicleId']);
            unset($updateData['vehicleSource']);
        }

        // for newly created sales preparation workorders set sales preparation status to HOLD
        if ((int)$updateData['type'] !== $originalWorkorder->getType() &&
            (int)$updateData['type'] === BAS_Shared_Model_OrderWorkshop::TYPE_SALES_PREPARATION
        ) {
            $updateData['salesPreparationStatus'] = BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_HOLD;
        }

        $updateData = $this->applyWorkorderVat($originalWorkorder, $updateData);

        $updateData[$workorderMapper->mapFieldToDb('updatedBy')] = $userId;
        $updateData[$workorderMapper->mapFieldToDb('updatedAt')] = new DateTime();
        
        $workorderMapper->updateFieldsByCondition(
            $workorderMapper->mapToDb($updateData),
            [$workorderMapper->mapFieldToDb('workorderId') . ' = ?' => $workorderId]
        );

        /** @var BAS_Shared_Model_OrderWorkshop $updatedWorkorder */
        $updatedWorkorder = $workorderMapper->findOneByCondition([
            $workorderMapper->mapFieldToDb('workorderId') . ' = ?' => $workorderId
        ]);

        if ((int)$originalWorkorder->getDefaultDepotBookingCodeId() !== (int)$updatedWorkorder->getDefaultDepotBookingCodeId()
            && (int)$updatedWorkorder->getDefaultDepotBookingCodeId() !== BAS_Shared_Model_Workshop_OrderWorkshopDetail::EMPTY_BOOKING_CODE
        ) {
            $this->updateEmptyDetailBookingCodes($workorderId, $updatedWorkorder->getDefaultDepotBookingCodeId());
        }

        if ($updateDetailsLabourRate) {
            $workorderDetailService->updateDetailsLabourRate($workorderId, $updatedWorkorder->getDefaultLabourRateId(), $userId);
        }

        $this->getEventManager()->trigger(
            BAS_Shared_Model_MapperEvent::EVENT_WORKORDER_UPDATE_TOTAL_INVOICE_PRICE,
            $this,
            ['workorderId' => $workorderId]
        );

        return $updatedWorkorder;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $originalWorkorder
     * @param array $updateData
     * @return array
     * @throws BAS_Shared_Exception
     */
    public function applyWorkorderVat(BAS_Shared_Model_OrderWorkshop $originalWorkorder, array $updateData)
    {
        /** @var Vatcheck_Service_VatCheck $vatCheckService */
        $vatCheckService = $this->getService('Vatcheck_Service_VatCheck');
        /** @var Management_Service_DepotSetting $depotSettingService */
        $depotSettingService = $this->getService('Management_Service_DepotSetting');

        $type = array_key_exists('type', $updateData) ? $updateData['type'] : $originalWorkorder->getType();
        $contactId = array_key_exists('contactId', $updateData) ? $updateData['contactId'] : $originalWorkorder->getContactId();
        $depotId = $originalWorkorder->getDepotId();

        //first we get the suggested vat type from vat service
        if ($this->isInternalWorkorder($type) && $contactId === null) {
            $defaultDebtorId = (int)$depotSettingService->getSettingValue($depotId, BAS_Shared_Model_Setting::DEBTOR_DEFAULT_NUMBER);
            $suggestedVatType = $vatCheckService->getVatTypeByDebtorId($defaultDebtorId, $depotId);
        } else {
            $suggestedVatType = $vatCheckService->getVatTypeByContactAddressAndDepotId(
                $updateData['invoiceAddressId'],
                $originalWorkorder->getDepotId()
            );
        }

        //now we check which fields we need to update
        //we always update the suggested vat
        $updateData['invoiceSuggestedVatType'] = $suggestedVatType;

        // if the user did not change the vat type (selected == suggested) we update the selected vat type too
        if ($originalWorkorder->getInvoiceSelectedVatType() == $originalWorkorder->getInvoiceSuggestedVatType()) {
            $updateData['invoiceSelectedVatType'] = $suggestedVatType;
        }

        return $updateData;
    }

    /**
     * @param int $workorderId
     * @return bool
     */
    public function isDepotAccountGroupMissedForWorkorderDetails($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        try {
            $detailWithEmptyAccountGroup = $orderWorkshopDetailMapper->findOneByCondition([
                $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
                $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
                implode(' OR ', [
                    $orderWorkshopDetailMapper->mapFieldToDb('depotAccountGroupId') . ' IS NULL',
                    $orderWorkshopDetailMapper->mapFieldToDb('depotAccountGroupId') . ' = ""',
                    $orderWorkshopDetailMapper->mapFieldToDb('depotAccountGroupId') . ' = 0',
                ]),
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            $detailWithEmptyAccountGroup = null;
        }

        return $detailWithEmptyAccountGroup !== null;
    }

    /**
     * @param int $workorderId
     * @return bool
     */
    public function isBookingCodeMissedForWorkorderDetails($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        try {
            $detailWithEmptyBookingCode = $orderWorkshopDetailMapper->findOneByCondition([
                $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
                $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
                $orderWorkshopDetailMapper->mapFieldToDb('depotBookingCodeId') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::EMPTY_BOOKING_CODE,
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            $detailWithEmptyBookingCode = null;
        }

        return $detailWithEmptyBookingCode !== null;
    }

    /**
     * @param int $workorderId
     * @param int $bookingCode
     * @return bool|int
     */
    public function updateEmptyDetailBookingCodes($workorderId, $bookingCode)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->updateFieldsByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('depotBookingCodeId') => (int)$bookingCode,
        ],[
            $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
            $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
            $orderWorkshopDetailMapper->mapFieldToDb('depotBookingCodeId') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::EMPTY_BOOKING_CODE,
        ]);
    }

    /**
     * @param int $workorderId
     * @param int $invoiceBookingCodeType
     * @return bool
     */
    public function containsDetailsWithInvoiceBookingType($workorderId, $invoiceBookingCodeType)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');
        return $workorderMapper->containsDetailsWithInvoiceBookingType($workorderId, $invoiceBookingCodeType);
    }

    /**
     * @param array $data
     * @return array
     */
    private function _normalizeEditGeneralTabSectionsByRequestData(array $data)
    {
        $sections = [
            Workshop_Form_WorkorderGeneralTab::SECTION_GENERAL,
            Workshop_Form_WorkorderGeneralTab::SECTION_CUSTOMER,
            Workshop_Form_WorkorderGeneralTab::SECTION_ADDRESS,
            Workshop_Form_WorkorderGeneralTab::SECTION_VEHICLE,
            Workshop_Form_WorkorderGeneralTab::SECTION_UNKNOWN_VEHICLE,
        ];

        foreach ($sections as $section) {
            if (!isset($data[$section])) {
                $data[$section] = [];
            }
        }

        return $data;
    }

    /**
     * @param array $data
     * @param int $userId
     * @param BAS_Shared_Model_OrderWorkshop $originalWorkorder
     * @return array
     */
    private function _getWorkorderGeneralDbDataByRequest(BAS_Shared_Model_OrderWorkshop $originalWorkorder, array $data, $userId)
    {
        if (empty($data)) {
            return [];
        }

        $dateFilter = new Lease_Form_Filter_DateFormat('Y-m-d');

        $generalSectionAvailableFields = [
            'type',
            'defaultDepotBookingCodeId',
            'workshopId',
            'warehouseId',
            'defaultLabourActivityId',
            'defaultLabourRateId',
            'mileage',
            'userId',
            'deadlineDate',
            'startDate',
            'finishedDate',
            'customerReferenceNo',
            'locked',
        ];

        $dateFields = [
            'deadlineDate',
            'startDate',
            'finishedDate',
        ];

        $generalSection = array_intersect_key($data, array_flip($generalSectionAvailableFields));

        foreach ($generalSection as $field => $value) {
            if ($value === '') {
                $value = null;
            }
            if (in_array($field, $dateFields)) {
                $value = $dateFilter->filter($value);
            }

            $generalSection[$field] = $value;
        }

        if (array_key_exists('locked', $generalSection)) {
            switch ((int)$generalSection['locked']) {
                case BAS_Shared_Model_OrderWorkshop::NOT_LOCKED:
                    $generalSection['lockedAt'] = null;
                    $generalSection['lockedBy'] = null;
                    break;
                case BAS_Shared_Model_OrderWorkshop::LOCKED:
                    $generalSection['lockedAt'] = new DateTime();
                    $generalSection['lockedBy'] = $userId;
                    break;
                default:
                    unset($generalSection['locked']);
            }
        }

        return $generalSection;
    }

    /**
     * @param array $data
     * @param array $unknownVehicle
     * @param int $workorderType
     * @param array $customerData
     * @return array
     */
    private function _getWorkorderVehicleDbDataByRequest(array $data, array $unknownVehicle, $workorderType, array $customerData)
    {
        if (empty($data)) {
            return [];
        }

        $vehicleId = $data['vehicleId'];
        $workorderType = (int)$workorderType;
        $contactId = isset($customerData['contactId']) ? $customerData['contactId'] : null;
        $vehicleSource = $this->getVehicleSource($workorderType, $contactId);

        if ($vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_FLEET) {
            if ($vehicleId === 'unknown' && !empty($unknownVehicle)) {
                $vehicleId = $this->saveWorkorderUnknownVehicle($unknownVehicle + ['contact_id' => $contactId]);
            }
        }

        if (empty($vehicleId)) {
            $vehicleSource = null;
        }

        return [
            'vehicleId' => (int)$vehicleId,
            'vehicleSource' => $vehicleSource,
        ];
    }

    /**
     * @param array $data
     * @param array $data
     * @return int|null
     */
    public function saveWorkorderUnknownVehicle(array $data)
    {
        if (empty($data)) {
            return null;
        }

        /** @var Contacts_Service_ContactVehicleOverview $contactVehicleService */
        $contactVehicleService = $this->getService('Contacts_Service_ContactVehicleOverview');
        $newContactVehicleModel = $contactVehicleService->addUnknownVehicle($data);

        return $newContactVehicleModel->id;
    }

    /**
     * @param array $data
     * @param array $customerData
     * @return array
     */
    private function _getWorkorderAddressDbDataByRequest(array $data, array $customerData)
    {
        if (empty($data) || empty($data['addressId'])) {
            return [];
        }

        if (!isset($customerData['contactId']) || empty($customerData['contactId'])) {
            $data['addressId'] = null;
        }

        return ['invoiceAddressId' => $data['addressId']];
    }

    /**
     * @param array $data
     * @param int $workorderType
     * @return array
     */
    private function _getWorkorderCustomerDbDataByRequest(array $data, $workorderType)
    {
        if (empty($data)) {
            return [];
        }

        $customerAvailableFields = [
            'contactId',
            'personId',
        ];

        if ($this->isInternalWorkorder($workorderType) && (!isset($data['contactId']) || empty($data['contactId']))) {
            $data['contactId'] = null;
            $data['personId'] = null;
        }

        $defaultValues = array_fill_keys($customerAvailableFields, null);

        $customerSectionValues = array_intersect_key($data, array_flip($customerAvailableFields));
        $customerSectionData = array_merge($defaultValues, $customerSectionValues);

        return $customerSectionData;
    }

    /**
     * @return Zend_Db_Select
     */
    public function getOpenWorkorderListSelect()
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');
        return $workorderMapper->getOpenWorkorderListSelect();
    }

    /**
     * @param int $vehicleId
     * @param int $userId
     * @throws Ibuildings_Exception_NotFound
     * @return BAS_Shared_Model_OrderWorkshop
     */
    public function createWorkorderOnStockVehicle($vehicleId, $userId)
    {
        /** @var Vehicles_Service_Vehicle $vehicleService */
        $vehicleService = $this->getService('Vehicles_Service_Vehicle');
        /** @var Management_Service_DepotSetting $settingService */
        $settingService = $this->getService('Management_Service_DepotSetting');
        /** @var Stock_Service_StockDefects $stockDefectsService */
        $stockDefectsService = $this->getService('Stock_Service_StockDefects');
        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');
        /** @var Order_Service_OrderWorkshop $orderWorkshopService */
        $orderWorkshopService = $this->getService('Order_Service_OrderWorkshop');

        try {
            /** @var BAS_Shared_Model_OrderWorkshop $existingWorkorder */
            $existingWorkorder = $workorderMapper->findOneByCondition([
                $workorderMapper->mapFieldToDb('vehicleId') . ' = ?' => $vehicleId,
                $workorderMapper->mapFieldToDb('vehicleSource') . ' = ?' => BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_STOCK,
                $workorderMapper->mapFieldToDb('type') . ' = ?' => BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT,
                $workorderMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_OrderWorkshop::NOT_ARCHIVED,
                $workorderMapper->mapFieldToDb('status') . ' NOT IN (?)' => [
                    BAS_Shared_Model_OrderWorkshop::WORKORDER_INVOICED,
                    BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED,
                ],
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            $existingWorkorder = null;
        }

        if ($existingWorkorder !== null) {
            return $existingWorkorder;
        }

        $vehicle = $vehicleService->findByVehicleId($vehicleId);
        $vehicleDepot = $vehicle->getDepotIdOwner();

        $workorder = new BAS_Shared_Model_OrderWorkshop();
        $workorder
            ->setDepotId($vehicleDepot)
            ->setWorkshopId($workorder->getDepotId())
            ->setCreatedBy($userId)
            ->setCreatedAt(new DateTime())
            ->setStatus(BAS_Shared_Model_OrderWorkshop::WORKORDER_START)
            ->setType(BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT)
            ->setVehicleSource(BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_STOCK)
            ->setArchived(BAS_Shared_Model_OrderWorkshop::NOT_ARCHIVED)
            ->setLocked(BAS_Shared_Model_OrderWorkshop::NOT_LOCKED)
            ->setVehicleId($vehicle->getVehicleId());

        $defaultDepotBookingCodeId = $settingService->getSettingValue($vehicleDepot, 'default_internal_stock_depot_booking_code_id');
        $defaultLabourRateId = $orderWorkshopService->getDefaultLabourRateId($vehicleDepot, $workorder->getType(), null);
        $defaultLabourActivityId = $settingService->getSettingValue($vehicleDepot, 'default_labour_activity_id');
        $defaultWarehouseId = $settingService->getSettingValue($vehicleDepot, 'workshop_default_warehouse');

        $workorder
            ->setDefaultLabourRateId($defaultLabourRateId)
            ->setDefaultLabourActivityId($defaultLabourActivityId)
            ->setDefaultDepotBookingCodeId($defaultDepotBookingCodeId)
            ->setWarehouseId($defaultWarehouseId === false ? null : (int)$defaultWarehouseId);

        /** @var BAS_Shared_Model_OrderWorkshop $workorder */
        $workorder = $workorderMapper->saveModel($workorder);
        $workorder->setAs400Id($workorder->getWorkorderId());

        $workorderMapper->updateFieldsByCondition(
            [$workorderMapper->mapFieldToDb('as400Id') => $workorder->getAs400Id()],
            [$workorderMapper->mapFieldToDb('workorderId') . ' = ?' => $workorder->getWorkorderId()]
        );

        $stockDefectsService->createWorkorderDefect($workorder, $userId);

        return $workorder;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return bool
     */
    public function isWorkorderStatusLocked(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        return $workorder->getLocked() === BAS_Shared_Model_OrderWorkshop::LOCKED;
    }

    /**
     * @param int $workorderId
     * @return bool
     */
    public function containsOnlyInternalDetails($workorderId)
    {
        return !$this->containsDetailsWithInvoiceBookingType($workorderId, BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL)
            && $this->containsDetailsWithInvoiceBookingType($workorderId, BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL)
        ;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return int|null
     */
    public function getCustomerNumber(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        /** @var Management_Service_DepotSetting $depotSettingsService */
        $depotSettingsService = $this->getService('Management_Service_DepotSetting');
        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');

        $mainAccountingDepotId = $depotSettingsService->getMainAccountingDepot($workorder->getDepotId());
        $customerNumber = $workorderMapper->getCustomerNumber($workorder->getWorkorderId(), $mainAccountingDepotId);

        return $customerNumber;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return int|null
     */
    public function getDefaultDebtorNumber(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        if (!$this->isInternalWorkorder($workorder->getType())) {
            return null;
        }

        /** @var Management_Service_DepotSetting $depotSettingService */
        $depotSettingService = $this->getService('Management_Service_DepotSetting');
        $defaultDebtorNumber = $depotSettingService->getSettingValue($workorder->getDepotId(), BAS_Shared_Model_Setting::DEBTOR_DEFAULT_NUMBER);

        if ($defaultDebtorNumber === false) {
            return null;
        }

        return (int)$defaultDebtorNumber;
    }

    /**
     * @param $newDeadlineDate
     * @param $workorderId
     * @param $userId
     * @param null $oldDateToCheck
     * @return array
     */
    public function updateDeadlineDate($newDeadlineDate, $workorderId, $userId, $oldDateToCheck = null)
    {
        $gridDateFormat = 'd-m-Y';
        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');
        /** @var BAS_Shared_Model_OrderWorkshop $workorderModel */
        $workorderModel = $workorderMapper->find($workorderId);

        $newDeadlineDateDT = DateTime::createFromFormat($gridDateFormat, $newDeadlineDate);
        $startDateDT = new DateTime($workorderModel->startDate);

        $validatorFinishedAndDeadline = new BAS_Shared_Form_Validate_FinishedAndDeadlineDateValidator();
        if(!$validatorFinishedAndDeadline->isValid($newDeadlineDate, ['startDate' => $startDateDT->format($gridDateFormat)])){
            return [
                'status' => false,
                'message' => $validatorFinishedAndDeadline->getMessages()['invalidValue']
            ];
        };

        $validatorAlreadyModified = new BAS_Shared_Form_Validate_ChangedDateValidator();
        if(!$validatorAlreadyModified->isValid($oldDateToCheck, ['workorderId' => $workorderId])){
            return [
                'status' => false,
                'message' => $validatorAlreadyModified->getMessages()['alreadyModified'],
            ];
        }

        $dataToUpdate = ['deadlineDate' => $newDeadlineDateDT,];
        $this->update($dataToUpdate, $workorderId, $userId);

        return ['status' => true];
    }

    /*
     * Used for workorder history widget
     * @param int $contactId
     * @return Zend_Db_Select
     */
    public function getWorkorderHistoryGridSelect($contactId)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');
        return $orderWorkshopMapper->getWorkorderHistoryByContactId($contactId);
    }

    /**
     * Used for workorder truckId history widget
     * @param $truckId
     * @return Zend_Db_Select
     */
    public function getWorkorderTruckIdHistoryGridSelect($truckId)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');
        return $orderWorkshopMapper->getWorkorderHistoryByTruckId($truckId);
    }

    /**
     * Returns true if workorder contains details with missing cost price.
     * Otherwise returns false
     *
     * @param int $workorderId
     * @return bool
     */
    public function workorderContainsDetailsWithMissingCostPrice($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->workorderContainsNoStockItemsWithEmptyCostPrice($workorderId) ||
               $orderWorkshopDetailMapper->workorderContainsExternalTasksWithEmptyCostPrice($workorderId);
    }

    /**
     * Returns true if workorder contains details with missing supplier id.
     * Otherwise returns false
     *
     * @param int $workorderId
     * @return bool
     */
    public function workorderContainsDetailsWithMissingSupplier($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->workorderContainsNoStockItemsWithEmptySupplier($workorderId) ||
               $orderWorkshopDetailMapper->workorderContainsExternalTasksWithEmptySupplier($workorderId);
    }

    /**
     * @return BAS_Shared_Model_OrderWorkshop[]
     */
    public function getWorkordersForSalesPreparationMails()
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        return $orderWorkshopMapper->findAllByCondition([
            $orderWorkshopMapper->mapFieldToDb('salesPreparationStatus') . ' = ?' => BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_WAITING,
            $orderWorkshopMapper->mapFieldToDb('salesPreparationProposal') . ' IS NULL',
            $orderWorkshopMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_OrderWorkshop::NOT_ARCHIVED,
        ]);
    }

    /**
     * @param int $defectId
     * @param array $salesPreparationStatus
     * @return BAS_Shared_Model_OrderWorkshop
     * @throws BAS_Shared_NotFoundException
     */
    public function findByDefectIdAndSalesPreparationStatus($defectId, array $salesPreparationStatus)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');
        return $orderWorkshopMapper->findByDefectIdAndSalesPreparationStatus($defectId, $salesPreparationStatus);
    }

    /**
     * @param int $workorderId
     * @param int $personId
     * @return int|bool
     */
    public function changePersonByWorkorderId($workorderId, $personId)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');

        return $orderWorkshopMapper->updateFieldsByCondition([
            $orderWorkshopMapper->mapFieldToDb('person_id') => (int) $personId,
        ], [
            $orderWorkshopMapper->mapFieldToDb('workorder_id') . '=?' => $workorderId,
            $orderWorkshopMapper->mapFieldToDb('archived') . '=?' => BAS_Shared_Model_AbstractModel::NOT_ARCHIVED,
        ]);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return array
     */
    public function getAvailableChangePersonsByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');
        $personList = $orderWorkshopMapper->getAvailableChangePersonsByContactId($workorder->getContactId());

        return BAS_Shared_Utils_Array::listData($personList, 'id', 'title');
    }

    /*
     * Get total invoice price by order id
     * 
     * @param integer $workOrderId
     * @return array
     */
    public function getTotalInvoicePriceByOrderId($workOrderId)
    {
        if (0 >= (int)$workOrderId) {
            return [];
        }

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');
        
        return $orderWorkshopDetailMapper->getTotalInvoicePriceByOrderId($workOrderId);
    }
    
    /**
     * Update total invoice price by order id
     * 
     * @param integer $workOrderId
     * @return integr|boolean
     */
    public function updateTotalInvoicePriceByOrderId($workOrderId)
    {
        if (0 >= (int)$workOrderId) {
            return false;
        }
        
        $invoicePriceResult = $this->getTotalInvoicePriceByOrderId($workOrderId);

        if ([] === $invoicePriceResult) {
            $invoicePriceResult = [
                'totalInvoicePrice' => 0,
                'orderId' => (int)$workOrderId
            ];
        }

        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopDetailMapper */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');
        
        return $orderWorkshopMapper->updateFieldsByCondition(
            ['total_invoice_price' => $invoicePriceResult['totalInvoicePrice']],
            ['workorder_id = ?' => (int)$workOrderId]);
    }
}
