<?php

class Workshop_Service_ClosedWorkorderListGrid extends Workshop_Service_BaseVehicleStockTable
{
    /** @var \Order_Service_OrderWorkshop */
    protected $_service;

    /** @var Bvb_BasGrid */
    protected $_grid;

    protected $_view;
    protected $_config;

    /** @var  int $_depotId */
    protected $_depotId;

    /**
     * ordered columns in grid
     *
     * @var array
     */
    protected $_columnOrderInGrid = array(
        'workorderId',
        'workorderType',
        'bookingCode',
        'contactName',
        'debtorNumber',
        'userId',
        'createdAt',
        'updatedAt',
        'spendTime',
        'lastDateTimeSpend',
        'truckId',
        'licencePlate',
        'startDate',
        'deadlineDate',
        'totalAmount',
        'workshopStatus'
    );

    /**
     * Workshop_Service_ClosedWorkorderListGrid constructor.
     * @param Order_Service_OrderWorkshop $service
     * @param Zend_View $view
     * @param Zend_Controller_Request $request
     */
    public function __construct(Order_Service_OrderWorkshop $service, Zend_View $view, Zend_Controller_Request_Http $request)
    {
        parent::__construct();

        $this->setService($service);
        $this->_view = $view;
        $this->_config = Zend_Registry::get('config');
        $this->_request = $request;
    }

    /**
     * @param \Order_Service_OrderWorkshop $service
     */
    public function setService($service)
    {
        $this->_service = $service;
    }

    /**
     * @return \Order_Service_OrderWorkshop
     */
    public function getService()
    {
        return $this->_service;
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * @param int $depotId
     */
    public function setDepotId($depotId)
    {
        $this->_depotId = $depotId;
    }

    /**
     * @return int
     */
    public function getDepotId()
    {
        return $this->_depotId;
    }

    public function getWorkshopGridOptions()
    {
        $orderWorkshopService = new Order_Service_OrderWorkshop();
        $sourceSelect = $orderWorkshopService->getInvoicedWorkorders($this->getDepotId());

        $gridId = $this->getRequest()->getParam('_zfgid');
        $columns = $this->getColumns();
        $columnKeys = array_keys($columns);
        $requestParams = $this->getRequest()->getUserParams();

        /**
         * trick to show empty grid in case if no filters defined (to make fleters behave as searh form)
         */
        if (null === $gridId || $this->isEmptyFilters($columnKeys, $requestParams, $gridId)) {
            $sourceSelect->where('true = false');
        }

        $options = [
            'sourceObject' => $sourceSelect,
            'columns' => $columns,
            'filters' => $this->getGridFilters(),
            'setKeyEventsOnFilters' => true,
            'recordPerPage' => '200',
            'setShowOrderImage' => false,
            'setAjax' => true,
            'paginationInterval' => ['200' => '200', '400' => '400', '600' => '600'],
        ];

        $options = $this->orderColumns($options);

        return $options;
    }

    /**
     * @return Bvb_Grid
     */
    public function getGrid()
    {
        if ($this->_grid) {
            return $this->_grid;
        }

        $gridConfig = Zend_Registry::get('gridConfig');

        $this->_grid = new Bvb_BasGrid();

        // lets get our own grid object so we can modify it later
        $this->_grid->setGrid(Bvb_Grid::factory('Table', $gridConfig, ''));
        $this->_grid->getGrid()->addFormatterDir('application/modules/workshop/formatter', 'Workshop_Formatter');
        $this->_grid->getGrid()->addFiltersRenderDir('application/modules/workshop/filter', 'Workshop_Filter');

        return $this->_grid->getGridObject($this->getWorkshopGridOptions(), $gridConfig, $this->getRowConditions());
    }

    /**
     * row condition
     *
     * @return array
     */
    protected function getRowConditions()
    {
        return array(
            'condition' => "'{{deadlineDate}}' > 0",
            'ifClass' => 'defect-row-blue',
            'elseClass' => '',
        );
    }

    /**
     * sets column ordering
     *
     * @param $options
     * @return mixed
     */
    protected function orderColumns($options)
    {
        for ($i = 0; $i < count($this->_columnOrderInGrid); $i++) {
            $column = $this->_columnOrderInGrid[$i];
            $options['columns'][$column]['position'] = $i + 1;
        }

        return $options;
    }

    protected function getGridFilters()
    {
        return [
            'workorderId' => ['searchType' => '=', 'class' => 'filter-medium-number'],
            'debtorNumber' => ['searchType' => '=', 'class' => 'filter-medium-number'],
            'truckId' => ['searchType' => '='],
            'contactName' => ['searchType' => 'like', 'class' => 'filter-medium-number'],
            'licencePlate' => ['searchType' => 'like', 'class' => 'filter-medium-number'],
            'createdAt' => ['render' => 'dateFilter', 'class' => 'date-filter'],
            'updatedAt' => ['render' => 'dateFilter', 'class' => 'date-filter'],
            'lastDateTimeSpend' => ['render' => 'dateFilter', 'class' => 'date-filter'],
        ];
    }

    protected function getStatusDecorator()
    {
        return '<div data-id="{{workorderId}}" class="status">{{workshopStatus}}</div>';
    }

    protected function getWorkorderIdDecorator()
    {
        $depotSettingService = new Management_Service_DepotSetting();
        if ($depotSettingService->depotHasExtranetWorkorderSetting($this->getDepotId())) {
            $workorderIdTpl = '<a href="/vbd/public/workshop/workorder/edit/id/{{workorderId}}" title="Open Workorder">{{workorderId}}</a>';
        } else {
            $workorderIdTpl = '{{workorderId}}';
        }

        return sprintf('<div data-id="{{workorderId}}" class="filter-medium-number">%s</div>', $workorderIdTpl);
    }

    /**
     * @return Zend_Controller_Request|Zend_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->_request;
    }

    /**
     * Check is request have empty filter params
     * @param $columnKeys
     * @param $requestParams
     * @param string $gridId
     * @return bool
     */
    private function isEmptyFilters($columnKeys, $requestParams, $gridId)
    {
        if (empty($gridId)) {
            return true;
        }

        foreach ($columnKeys as $key) {
            if (array_key_exists($key . $gridId, $requestParams)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Columns options array
     * @return array
     */
    private function getColumns()
    {
        return [
            'workorderId' => ['title' => 'Workorder', 'order' => true, 'decorator' => $this->getWorkorderIdDecorator()],
            'contactName' => ['title' => 'customer', 'order' => true],
            'debtorNumber' => ['title' => 'debtor_number', 'class' => 'filter-medium-number'],
            'userId' => [
                'title' => 'receptionist',
                'decorator' => ['function' => function($rowData) {
                    return $this->getView()->userAbbreviation($rowData['userId']);
                }]
            ],
            'createdAt' => [
                'title' => 'created_at',
                'searchType'   => 'sqlExp',
                'searchSqlExp' => "DATE_FORMAT(ow.created_at, '%d-%m-%Y') = '{{value}}'",
                'decorator' => ['function' => function ($rowData) {
                    return (isset($rowData['createdAt']) ? (new Default_Formatter_Date)->format($rowData['createdAt']): '');
                }],
            ],
            'updatedAt' => [
                'title' => 'updated_at',
                'searchType'   => 'sqlExp',
                'searchSqlExp' => "DATE_FORMAT(ow.updated_at, '%d-%m-%Y') = '{{value}}'",
                'decorator' => ['function' => function ($rowData) {
                    return (isset($rowData['updatedAt']) ? (new Default_Formatter_Date)->format($rowData['updatedAt']): '');
                }],
            ],
            'bookingCode' => ['title' => 'booking_code'],
            'spendTime' => ['title' => 'spend_time'],
            'lastDateTimeSpend' => [
                'title' => 'last_date_time_spend',
                'searchType'   => 'sqlExp',
                'searchSqlExp' => "DATE_FORMAT(lastDateTimeSpend, '%d-%m-%Y') = '{{value}}'",
                'decorator' => ['function' => function ($rowData) {
                    return (isset($rowData['lastDateTimeSpend']) ? (new Default_Formatter_Date)->format($rowData['lastDateTimeSpend']): '');
                }],
            ],
            'truckId' => ['title' => 'truck_id'],
            'licencePlate' => ['title' => 'licence_plate'],
            'workorderType' => [
                'title' => 'workorder_type',
                'decorator' => ['function' => function ($rowData) {
                    return $this->getView()->translate(BAS_Shared_Model_OrderWorkshop::$typeLabel[$rowData['workorderType']]);
                }],
            ],
            'totalAmount' => [
                'title' => 'total_amount',
                'decorator' => ['function' => function ($rowData) {
                    $filter = new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => 2]);
                    return $filter->filter($rowData['totalAmount']);
                }],
            ],
            'startDate' => [
                'title' => 'start',
                'decorator' => ['function' => function ($rowData) {
                    return (isset($rowData['startDate']) ? (new Default_Formatter_Date)->format($rowData['startDate']): '');
                }],
            ],
            'deadlineDate' => [
                'title' => 'deadline',
                'decorator' => ['function' => function ($rowData) {
                    return (isset($rowData['deadlineDate']) ? (new Default_Formatter_Date)->format($rowData['deadlineDate']): '');
                }],
            ],
        ];
    }

}
