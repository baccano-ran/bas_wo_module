<?php

/**
 * Class Workshop_Service_LabourRateGrid
 */
class Workshop_Service_LabourRateGrid extends BAS_Shared_Service_Abstract
{

    /**
     * @var Zend_View_Interface
     */
    protected $_view;

    /**
     * @param Zend_View_Interface $view
     */
    public function __construct(Zend_View_Interface $view)
    {
        $this->_view = $view;
    }

    /**
     * @param string $ymdDate
     * @return string
     */
    private function formatDate($ymdDate)
    {
        $returnDate = '';
        if ($ymdDate !== '0000-00-00') {
            $date = DateTime::createFromFormat('Y-m-d', $ymdDate);
            if ($date !== false) {
                $returnDate = $date->format('d-m-Y');
            }
        }
        return $returnDate;
    }

    /**
     * @param Zend_Db_Select $source
     * @return Bvb_Grid
     * @throws Bvb_Grid_Exception
     * @throws Zend_Exception
     */
    public function getGrid(Zend_Db_Select $source)
    {
        $columns = [
            'rate_id' => ['title' => 'Id'],
            'department_id' => ['hidden' => true],
            'department_name' => ['searchField' => 'department_id', 'title' => 'department'],
            'rate_name' => ['title' => 'name'],
            'rate' => ['decorator' => [
                'function' => function ($rawData) {
                    return number_format($rawData['rate'], 2, ',', ' ') . ' €';
                }
            ]],
            'cost_price' => [
                'decorator' => [
                    'function' => function ($rawData) {
                        return number_format($rawData['cost_price'], 2, ',', ' ') . ' €';
                    }
                ],
                'title' => 'cost_price',
            ],
            'end_date' => [
                'title' => 'end_date',
                'searchType' => 'sqlExp',
                'searchSqlExp' => "IF(end_date = '0000-00-00' OR end_date > NOW(), 1, 2) = {{value}}",
                'decorator' => [
                    'function' => function ($rawData) {
                        return $this->formatDate($rawData['end_date']);
                    }
                ],
            ],
        ];
        $filters = [
            'department_id' => [
                'searchType' => '=',
            ],
            'department_name' => [
                'distinct' => [
                    'field' => 'department_id',
                    'name' => 'department_name',
                    'order' => 'department_name ASC',
                ]
            ],
            'end_date' => [
                'showAllOption' => true,
                'values' => [
                    1 => 'not_expired',
                    2 => 'expired',
                ],
            ],
        ];

        $actions = $this->_view->render('labour-rate/_grid-actions.phtml');
        $actionColumn = new Bvb_Grid_Extra_Column();
        $actionColumn->position('right')->name('actions')->title('')->decorator($actions);

        $gridConfig = Zend_Registry::get('gridConfig');
        $grid = Bvb_Grid::factory('Table', $gridConfig);
        $grid->setAjax(true);
        $grid->setShowOrderImages(false);
        $grid->addExtraColumns($actionColumn);
        $grid->setRecordsPerPage(20);
        $grid->setPaginationInterval([20 => 20, 50 => 50, 100 => 100]);

        $basGrid = new Bvb_BasGrid();
        $basGrid->setGrid($grid);
        $basGrid->setSource($grid, 0, $source);
        $basGrid->setFilters($grid, [], $filters, true);
        $basGrid->setColumns($grid, $columns);

        $grid->deploy();
        return $grid;
    }

}
