<?php

/**
 * Class Workshop_Service_WorkorderTruckIdHistoryGrid
 */
class Workshop_Service_WorkorderTruckIdHistoryGrid extends Workshop_Service_HistoryBaseGrid
{
    protected $_ajax = 1;

    protected $_recordsPerPage = 10;

    public $depotId;

    /**
     * Workshop_Service_WorkorderTruckIdHistoryGrid constructor.
     * @param Zend_Db_Select $source
     * @param null|Zend_View_Interface $view
     * @param array $actionUrls
     * @param int $depotId
     */
    public function __construct(Zend_Db_Select $source, $view, array $actionUrls = [], $depotId = 0)
    {
        $this->depotId = $depotId;
        parent::__construct($source, $view, $actionUrls);
    }

    protected function _setColumns()
    {
        $dateFilter = new Lease_Form_Filter_DateFormat('d-m-Y');

        $depotSettingService = new Management_Service_DepotSetting();
        if ($depotSettingService->depotHasExtranetWorkorderSetting($this->depotId)) {
            $workorderIdTpl = '<a href="/vbd/public/workshop/workorder/edit/id/{{workorderId}}" target="_blank" class="">{{workorderId}}</a>';
        } else {
            $workorderIdTpl = '{{workorderId}}';
        }

        $this->_columns = [
            'orderId' => [
                'title' => 'workorder',
                'decorator' => [
                    'function' => function($rowData) use ($workorderIdTpl) {
                        return strtr($workorderIdTpl, [
                            '{{workorderId}}' => $rowData['__value'],
                        ]);
                    },
                ]
            ],
            'TruckId' => [
                'hidden' => true,
            ],
            'fileToken' => [
                'hidden' => true,
            ],
            'total' => [
                'title' => 'total',
                'decorator' => [
                    'function' => function($rowData) {
                        return '&euro;&nbsp;' . $this->getView()->normalizedToLocalized($rowData['total']);
                    },
                ],
            ],
            'orderDate' => [
                'title' => 'oc_order_date',
                'decorator' => $this->_getDecoratorWithFilter($dateFilter),
            ],
            'fileName' => [
                'hidden' => true,
            ],
            'fileType' => [
                'hidden' => true,
            ],
            'fileId' => [
                'hidden' => true,
            ],
            'fileToken' => [
                'hidden' => true,
            ],
            'invoice' => [
                'title' => 'invoice',
                'class' => 'fileLink',
                'decorator' => [
                    'function' => function ($rowData) {
                        $adapter = new Order_Library_DocumentWidget_OrderWorkshopAdapter($this->depotId);
                        return isset($rowData['invoice']) ? $this->getView()->partial('document-widget/_partial/document-link.phtml', 'order', [
                            'adapter'    => $adapter,
                            'rowData'    => $rowData,
                            'documentId' => $rowData['fileId'],
                            'fileName' => $rowData['fileName'],
                            'fileToken' => $rowData['fileToken'],
                            'documentType' => $rowData['fileType'],
                            'orderId' => $rowData['orderId'],
                            'documentTitle' => $rowData['invoice'],
                            'sourceType' => Order_Library_DocumentWidget_OrderWorkshopAdapter::SOURCE_TYPE,
                        ]) : '';
                    },
                ],
            ],
            'invoiceDate' => [
                'title' => 'invoice_date',
                'decorator' => $this->_getDecoratorWithFilter($dateFilter),
            ],
            'status' => [
                'title' => 'status',
                'decorator' => [
                    'function' => function($rowData) {
                        return $this->getView()->translate(BAS_Shared_Model_OrderWorkshop::$statusLabel[$rowData['status']]);
                    },
                ]
            ],
        ];
    }

    protected function _setFilters()
    {
        $onChangeFilterJsFunction = '_' . $this->_ajax . 'gridChangeFilters(1);';
        $this->_filters = [
            'orderId' => [
                'searchType' => '=',
                'class' => 'width-full',
            ],
            'orderDate' => [
                'searchType' => '=',
                'class' => 'js-date_picker width80',
            ],
            'invoice' => [
                'searchType' => '=',
                'class' => 'width60',
            ],
            'invoiceDate' => [
                'searchType' => '=',
                'attributes' => [
                    'onchange' => $onChangeFilterJsFunction,
                ],
                'class' => 'js-date_picker width80',
            ],
            'status' => [
                'searchType' => 'sqlExpr',
                'searchSqlExp' => 'COALESCE(ow.status, \'__OFFER__\')={{value}}',
                'attributes' => [
                    'onchange' => '_' . $this->_ajax . 'gridChangeFilters(1);'
                ],
                'values' => $this->_getWorkshopStatusOptions(),
            ],
        ];
    }
}