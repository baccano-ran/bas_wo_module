<?php

class Workshop_Service_TradeInPrices extends BAS_Shared_Service_Abstract
{

    /**
     * @param array $itemsData
     * @param array $taskIds
     * @param int $workorderDepotId
     * @return array
     */
    public function getTradeInItemsData(array $itemsData, array $taskIds, $workorderDepotId)
    {
        /** @var Order_Service_ProductComposition $productCompositionService */
        $productCompositionService = $this->getService('Order_Service_ProductComposition');
        /** @var Warehouse_Service_ItemRequired $itemRequiredService */
        $itemRequiredService = $this->getService('Warehouse_Service_ItemRequired');

        $tradeInConfirmationData = [];
        $formattedItemsData = [];
        $tradeInPossibleData = [];

        foreach ($itemsData as $itemData) {
            $itemId = $itemData['itemId'];
            $depotId = $itemData['depotId'];
            $quantity = $itemData['quantity'];

            $tradeInPossibleData[$itemId . '-' . $depotId] = $this->checkTradeInPossible($itemId, $depotId, $quantity);
            $formattedItemsData[$itemId . '-' . $depotId] = [$itemId, $depotId];

            $requiredItems = $itemRequiredService->findByMainItemId($itemId);
            foreach ($requiredItems as $requiredItem) {
                $tradeInPossible = $this->checkTradeInPossible($requiredItem->getRequiredItemId(), $depotId, $quantity);
                $tradeInPossibleData[$requiredItem->getRequiredItemId() . '-' . $depotId] = $tradeInPossible;
                $formattedItemsData[$requiredItem->getRequiredItemId() . '-' . $depotId] = [$requiredItem->getRequiredItemId(), $depotId];
            }
        }

        foreach ($taskIds as $taskId) {
            $subItems = $productCompositionService->findSubItems($taskId);
            foreach ($subItems as $subItem) {
                $formattedItemsData[$subItem->getReferenceId() . '-' . $workorderDepotId] = [$subItem->getReferenceId(), $workorderDepotId];

                $requiredItems = $itemRequiredService->findByMainItemId($subItem->getReferenceId());
                foreach ($requiredItems as $requiredItem) {
                    $formattedItemsData[$requiredItem->getRequiredItemId() . '-' . $workorderDepotId] = [$requiredItem->getRequiredItemId(), $workorderDepotId];
                }
            }
        }

        foreach ($formattedItemsData as $formattedItemData) {
            list($itemId, $depotId) = $formattedItemData;
            $tradeInConfirmationData[] = $this->getTradeInItemData($itemId, $depotId);
        }
        $tradeInConfirmationData['tradeInPossible'] = (in_array(false, $tradeInPossibleData))? false: true;

        return $tradeInConfirmationData;
    }


    /**
     * @param int $itemId
     * @param int $depotId
     * @param int $quantity
     * @return bool
     */
    public function checkTradeInPossible($itemId, $depotId, $quantity)
    {
        if (0 <= (int)$quantity) {
            return true;
        }

        /** @var BAS_Shared_Model_ItemMapper $itemMapper */
        $itemMapper = $this->getMapper('Item');
        try {
            $itemDetails = $itemMapper->findByIdAndDepot($itemId, $depotId);
        } catch (Exception $e) {
            return true;
        }

        if (BAS_shared_Model_Item::NO_STOCK_ITEM_NO === (int)$itemDetails->getNoStockItem()) {
            return true;
        }

        return false;
    }

    /**
     * @param int $itemId
     * @param int $depotId
     * @return array
     * @throws BAS_Shared_Exception
     */
    private function getTradeInItemData($itemId, $depotId)
    {
        /** @var Warehouse_Service_ItemName $itemNameService */
        $itemNameService = $this->getService('Warehouse_Service_ItemName');
        /** @var Warehouse_Service_ItemPrice $itemPriceService */
        $itemPriceService = $this->getService('Warehouse_Service_ItemPrice');

        $itemName = $itemNameService->getItemName($itemId);
        $itemCostPrice = $itemPriceService->getItemCostPrice($itemId, $depotId);

        return [
            'itemId' => $itemId,
            'itemName' => $itemName,
            'depotId' => $depotId,
            'tradeInPrice' => $itemCostPrice === null ? 0 : $itemCostPrice->getPrice(),
        ];
    }

}
