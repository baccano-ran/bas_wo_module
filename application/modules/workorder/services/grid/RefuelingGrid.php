<?php

/**
 * Class Workshop_Service_RefuelingGrid
 */
class Workshop_Service_RefuelingGrid extends Lease_Service_BaseGrid
{
    public $_ajax = 'bas-grid-rg';

    protected function _init()
    {
        parent::_init();
        $this->_tableFooterHtml = $this->getView()->partial('refueling/_grid_footer.phtml');
    }

    protected function _setColumns()
    {
        $decimalFilter = new Zend_Filter_NormalizedToLocalized([
            'locale' => 'nl_NL',
            'precision' => '2',
        ]);

        $decimalDecorator = function ($rowData) use ($decimalFilter) {
            return $decimalFilter->filter($rowData['__value']);
        };

        $this->_columns = [
            'id' => [
                'title' => 'product_id',
                'class' => 'js-refueling_id'
            ],

            '_date_time' => [
                'title' => 'date_time',
            ],

            'quantity' => [
                'title' => 'quantity',
                'decorator' => [ $decimalDecorator ],
            ],

            'amount' => [
                'title' => 'amount',
                'decorator' => [ $decimalDecorator ],
            ],

            'card_number' => [
                'title' => 'card_number',
            ],

            'manual_number' => [
                'title' => 'manual_number',
            ],

            'vehicle_license_plate' => [
                'title' => 'license_plate',
            ],

            'mileage' => [
                'title' => 'mileage',
            ],

            'username' => [
                'title' => 'employee',
            ],
        ];
    }

    protected function _setFilters()
    {
        $this->_filters = [
            'id' => [
                'searchType' => '=',
            ],

            '_date_time' => [
                'searchType' => 'like',
                'attributes' => [
                    'placeholder' => $this->getView()->translate('date'),
                    'class' => 'js-date_picker',
                ]
            ],

            'card_number' => [
                'searchType' => 'like',
                'class' => 'width-full',
            ],

            'manual_number' => [
                'searchType' => 'like',
                'class' => 'width-full',
            ],

            'vehicle_license_plate' => [
                'searchType' => 'like',
                'class' => 'width-full',
            ],

            'username' => [
                'searchType' => 'like',
                'class' => 'width-full',
            ],
        ];
    }

    /**
     * @return Bvb_Grid_Extra_Column
     */
    protected function _getActionsColumn()
    {
        $extraColumn = new Bvb_Grid_Extra_Column();
        $extraColumn
            ->position('left')
            ->name('actions')
            ->title('')
            ->decorator([
                'function' => function ($rowData) {
                    $html = $this->getView()->partial('refueling/_grid_actions.phtml', [

                        'refuelingId' => $rowData['id'],

                        'editUrl' => $this->getView()->url([
                            'module' => 'workshop',
                            'controller' => 'refueling',
                            'action' => 'open-external-workorder-list',
                        ], null, true),
                    ]);

                    return $html;
                },
            ])
        ;

        return $extraColumn;
    }
}