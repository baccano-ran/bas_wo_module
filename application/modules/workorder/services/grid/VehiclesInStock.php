<?php
/**
 * Created by PhpStorm.
 * User: Ashutosh
 * Date: 30-9-14
 * Time: 15:15
 */

class Workshop_Service_VehiclesInStock extends Workshop_Service_BaseVehicleStockTable
{
    /** @var \Order_Service_OrderWorkshop */
    protected $_service;

    /** @var Bvb_BasGrid */
    protected $_grid;

    protected $_view;
    protected $_config;

    /** @var  int $_depotId */
    protected $_depotId;


    protected static $WORKSHOP_STATUS_DROPDOWN = array(
        BAS_Shared_Model_OrderWorkshop::WORKORDER_START => 'defects_preparation_workshop_start',
        BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED => 'defects_preparation_workshop_workorder_printed'
    );

    protected static $STATUS_DROPDOWN = array(
        BAS_Shared_Model_Vehicle::STATUS_IN_STOCK => 'voorraad',
        BAS_Shared_Model_Vehicle::STATUS_RESERVED => 'gereserveerd',
        BAS_Shared_Model_Vehicle::STATUS_BILLED => 'gefactureerd',
        BAS_Shared_Model_Vehicle::STATUS_ARCHIVED => 'archief',
        BAS_Shared_Model_Vehicle::STATUS_FICTIVE => 'fictieve voorraad',
        BAS_Shared_Model_Vehicle::STATUS_NEW => 'nieuw',
        BAS_Shared_Model_Vehicle::STATUS_ARCHIVE => 'archief',
        BAS_Shared_Model_Vehicle::STATUS_AS400 => 'AS400',
        BAS_Shared_Model_Vehicle::STATUS_WITHIN => 'binnen',
        BAS_Shared_Model_Vehicle::STATUS_APPROVED => 'goedgekeurd',
        BAS_Shared_Model_Vehicle::STATUS_CHECKIN => 'inchecken',
        BAS_Shared_Model_Vehicle::STATUS_INTRODUCED => 'ingevoerd',
        BAS_Shared_Model_Vehicle::STATUS_RECHECK => 'opnieuw inchecken',
        BAS_Shared_Model_Vehicle::STATUS_ERROR => 'error'
    );
    
    protected static $WORKSHOP_TYPE_DROPDOWN = array(
        BAS_Shared_Model_OrderWorkshop::TYPE_INCOMING_VEHICLES => 'type_incoming_vehicles',
        BAS_Shared_Model_OrderWorkshop::TYPE_STOCK => 'stock',
        BAS_Shared_Model_OrderWorkshop::TYPE_SALES_PREPARATION => 'sales_preparation',
    );
        
    /**
     * ordered columns in grid
     *
     * @var array
     */
    protected $_columnOrderInGrid = array(
        'workorder_id',
        'workorderType',
        'TruckID',
        'vehicle_status',
        'description',
        'Location',
        'Afkorting',
        'tabvehicles_vehicle_productiondate',
        'chassis_number',
        'Inkoper',
        'Supplier',
        'purchase_comment_cnt',
        'entry_date',
        'days_from_entry',
        'sales_preparation_status',
        'workshop_status',
        'as400_id',
        'totalAmount',
        'startDate',
        'deadlineDate'
    );

    protected $_hiddenColumns = array(
        'VoertuigID', 'defect_id', 'contact_type_supplier_fee_contact_id', 'supplier_id', 'purchase_id'
    );

    public function __construct(Order_Service_OrderWorkshop $service, Zend_View $view)
    {
        parent::__construct();

        $this->setService($service);
        $this->_view = $view;
        $this->_config = Zend_Registry::get('config');
    }

    /**
     * @param \Order_Service_OrderWorkshop $service
     */
    public function setService($service)
    {
        $this->_service = $service;
    }

    /**
     * @return \Order_Service_OrderWorkshop
     */
    public function getService()
    {
        return $this->_service;
    }

    /**
     * @return Zend_View
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * @param int $depotId
     */
    public function setDepotId($depotId)
    {
        $this->_depotId = $depotId;
    }

    /**
     * @return int
     */
    public function getDepotId()
    {
        return $this->_depotId;
    }

    public function getWorkshopGridOptions()
    {
        $orderWorkshopService = new Order_Service_OrderWorkshop();
        $options = array(
                'sourceObject' => $orderWorkshopService->getWorkOrdersForStock($this->getDepotId()),
                'columns' => array(
                    'workorder_id' => array('title' => 'Workorder', 'order'=>true, 'decorator' => $this->getWorkorderIdDecorator()),
                    'workorderType' => [
                        'title' => 'workorder_type',
                        'decorator' => ['function' => function ($rowData) {
                            return $this->getView()->translate(BAS_Shared_Model_OrderWorkshop::$typeLabel[$rowData['workorderType']]);
                        }],
                    ],
                    'TruckID'=> array('title' => 'TR', 'order'=>true),
                    'description'=> array('title' => 'Description','class' => 'filter-medium-number'),
                    'Afkorting' => array('title' => 'S'),
                    'tabvehicles_vehicle_productiondate'=> array(
                        'format' => 'Year',
                        'decorator' => $this->getYearDecorator(),
                        'title' => 'tabvehicles_vehicle_productiondate'
                    ),
                    'Location'=> array('decorator' => $this->getLocationDecorator()),
                    'entry_date'=> array(
                        'format' => 'Date',
                        'decorator' => $this->getDateDecorator(),
                        'title' => 'entryexpected'
                    ),
                    'Supplier' =>  array ('format' => 'TruncateText', 'class' => 'filter-medium-number' ),
                    'days_from_entry' => array('class' => 'filter-small-number'),
                    'as400_id' => array('title' => 'AS 400 ID'),
                    'totalAmount' => [
                        'title' => 'total_amount',
                        'decorator' => ['function' => function ($rowData) {
                            $filter = new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => 2]);
                            return $filter->filter($rowData['totalAmount']);
                        }],
                    ],
                    'vehicle_status' => array(
                        'title' => 'Status',
                    ),
                    'sales_preparation_status' => [
                        'title' => 'sales_preparation_status',
                        'decorator' => [
                            'function' => function ($rowData) {
                                return '<img src="' . $this->_view->getSalesPreparationStatusImageUrl($rowData['sales_preparation_status']) . '">';
                            }
                        ],
                    ],
                    'workshop_status' => [
                        'class' => 'filter-small-number',
                        'decorator'    => [
                                'function' => function ($rowData) {
                                    return $this->_view->partial('workorder/partial/workshop-status.phtml', [
                                        'rowData' => $rowData,
                                    ]);
                                },
                            ],
                    ],
                    'purchase_comment_cnt' => [
                        'title' => 'purchase_comment',
                        'class' => 'purchase-comment-col width80',
                        'decorator'    => [
                            'function' => function ($rowData) {
                                return $this->_view->partial('workorder/partial/purchase-comment.phtml', [
                                    'rowData' => $rowData,
                                ]);
                            },
                        ],
                    ],
                    'startDate'=> array(
                        'format' => 'Date',
                        'title' => 'start'
                    ),
                    'deadlineDate'=> array(
                        'format' => 'Date',
                        'decorator' => ['function' => function($rowData){
                            return $this->getView()->partial('partial/deadline-date.phtml', [
                                'deadlineDate' => [
                                    'date' => $rowData['deadlineDate'],
                                    'workorderId' => $rowData['workorder_id']
                                ],
                            ]);
                        }],
                        'title' => 'deadline'
                    ),
                ),
                'filters' => $this->getGridFilters(),
                'recordPerPage' => '200',
                'setShowOrderImage' => false,
                'setAjax' => false,
                'paginationInterval' => array('200' => '200', '400' => '400', '600' => '600'),
            );

        // lets add hidden columns
        $options['columns'] += $this->getHiddenColumns();
        $options = $this->orderColumns($options);

        return $options;
    }


    /**
     * @return Bvb_Grid
     */
    public function getGrid()
    {
        if ($this->_grid) {
            return $this->_grid;
        }

        $gridConfig = Zend_Registry::get('gridConfig');

        $this->_grid = new Bvb_BasGrid();

        // lets get our own grid object so we can modify it later
        $this->_grid->setGrid(Bvb_Grid::factory('Table', $gridConfig, ''));
        $this->_grid->getGrid()->addFormatterDir('application/modules/workshop/formatter', 'Workshop_Formatter');
        $this->_grid->getGrid()->addFiltersRenderDir('application/modules/workshop/filter', 'Workshop_Filter');
        $this->_grid->getGrid()->addClassCellCondition('actions', '{{defect_id}}  >  0', '', 'noDefects');

        $this->_grid->getGrid()->addExtraColumns($this->getActionColumn());

        return $this->_grid->getGridObject($this->getWorkshopGridOptions(), $gridConfig, $this->getRowConditions());
    }

    /**
     * row condition
     *
     * @return array
     */
    protected function getRowConditions()
    {
        $currentDate = date('d-m-Y');
        return [
            [
                'condition' => "'$currentDate' === date( 'd-m-Y', strtotime('{{deadlineDate}}') )",
                'ifClass' => 'defect-row-orange',
                'elseClass' => '',
            ],
            [
                'condition' => "'{{deadlineDate}}' != '' && strtotime('$currentDate') > strtotime( date( 'd-m-Y', strtotime('{{deadlineDate}}') ) )",
                'ifClass' => 'defect-row-red',
                'elseClass' => '',
            ],
        ];
    }

    /**
     * sets column ordering
     *
     * @param $options
     * @return mixed
     */
    protected function orderColumns($options)
    {
        $position = 1;

        foreach($this->_columnOrderInGrid as $column)
        {
            if (isset($options['columns'][$column])) {
                $options['columns'][$column]['position'] = $position;
            } else {
                $options['columns'][$column] = array('position' => $position);
            }
            $position += 1;
        }

        return $options;
    }

    protected function getHiddenColumns()
    {
        $returnArray = array();

        foreach($this->_hiddenColumns as $column) {
            $returnArray[$column] = array('hidden' => true);
        }

        return $returnArray;
    }

    protected function getGridFilters()
    {
        return array(
            'workorder_id' => array('searchType' => '=', 'class' => 'filter-small-number'),
            'workorderType' => [
                'values' => $this->getWorkshopTypeDropDown(),
                'searchType' => '=',
                'class' => 'filter-dropdown-medium',
            ],
            'TruckID' => array('searchType' => '=', 'class' => 'filter-small-number'),
            'Afkorting' => array('class' => 'filter-dropdown-xsmall',
                'distinct' => array(
                    'field' => 'Afkorting',
                    'name' => 'Afkorting',
                )
            ),
            'Inkoper' => array('class' => 'filter-dropdown-xsmall',
                'distinct' => array(
                    'field' => 'Inkoper',
                    'name' => 'Inkoper',
                )
            ),
            'year' => array('render' => 'dateFilter', 'class' => 'filter-small-number'),
            'entry_date' => array('render' => 'dateFilter', 'class' => 'date-filter'),
            'chassis_number' => array('searchType' => 'like', 'class' => 'filter-medium-number'),
            'vehicle_status' => array( 'values' => $this->getStatusDropDown(),
                'searchType' => '=',
                'class' => 'filter-dropdown-small',
            ),
            'workshop_status' => array(
                'values' => $this->getWorkshopStatusDropDown(),
                'searchType' => '=',
                'class' => 'filter-dropdown-small',
            ),
            'as400_id' => array('searchType' => '=', 'class' => 'filter-small-number'),
        );
    }

    protected function getActionColumn()
    {
        $column = new Bvb_Grid_Extra_Column();

        $column
            ->position('left')
            ->name('actions')
            ->title($this->getView()->translate('tab_actions'))
            ->decorator(['function' => function ($rowData) {
                return $this->getView()->partial('partial/vehiclesInGridActionColumn.phtml', [
                    'rowData' => $rowData,
                ]);
            }])
        ;

        return $column;
    }

    /**
     * @return array
     */
    protected function getPartnerPortalData(){
        /** @var BAS_Shared_View_View $view */
        $view = $this->_view;
        return [
            'portalIconRight' => $view->isAllowed('default.partner-portal.portal-vehicle-icon'),
            'interSupplierIds' => BAS_Shared_Model_ContactTypeSupplier::getInternalSupplierContactIds(),
            'supplierFeeUnknown' => BAS_Shared_Model_ContactTypeSupplier::SUPPLIER_FEE_UNKNOWN,
        ];
    }

    protected function getDateDecorator()
    {
        return '<div data-id={{workorder_id}} class="date">{{entry_date}}</div>';
    }

    protected function getYearDecorator()
    {
        return '<div data-id={{workorder_id}} class="date">{{tabvehicles_vehicle_productiondate}}</div>';
    }

    protected function getLocationDecorator()
    {
        return "<div data-id={{workorder_id}} class='filter-medium-number'><a href='javascript:void(0);' onclick='javascript:popupWindow(\" /vbd/public/map/index/index?z={{Location}}\", 1035, 620);' title='show on map'>{{Location}}</a></div>";
    }

    protected function getWorkorderIdDecorator()
    {
        $depotSettingService = new Management_Service_DepotSetting();
        if ($depotSettingService->depotHasExtranetWorkorderSetting($this->getDepotId())) {
            $workorderIdTpl = '<a href="/vbd/public/workshop/workorder/edit/id/{{workorder_id}}" title="Open Workorder">{{workorder_id}}</a>';
        } else {
            $workorderIdTpl = '{{workorder_id}}';
        }

        return sprintf('<div data-id="{{workorder_id}}" class="filter-medium-number">%s</div>', $workorderIdTpl);
    }
    
    protected function getWorkshopStatusDropDown()
    {
        $t = Zend_Registry::get('Zend_Translate');
        $values = self::$WORKSHOP_STATUS_DROPDOWN;
        foreach($values as $k => $v) {
            $values[$k] = $t->translate($v);
        }
        return $values;
    }

    protected function getStatusDropDown()
    {
        $t = Zend_Registry::get('Zend_Translate');
        $values = self::$STATUS_DROPDOWN;
        foreach($values as $k => $v) {
            $values[$k] = $t->translate($v);
        }
        return $values;
    }
    
    protected function getWorkshopTypeDropDown()
    {
        $t = Zend_Registry::get('Zend_Translate');
        $values = self::$WORKSHOP_TYPE_DROPDOWN;
        foreach($values as $k => $v) {
            $values[$k] = $t->translate($v);
        }
        return $values;
    }
}
