<?php

class Workshop_Service_InternalWorkorderListGrid extends Workshop_Service_BaseVehicleStockTable
{
    /** @var \Order_Service_OrderWorkshop */
    protected $_service;

    /** @var Bvb_BasGrid */
    protected $_grid;

    protected $_view;
    protected $_config;

    /** @var  int $_depotId */
    protected $_depotId;

    /**
     * ordered columns in grid
     *
     * @var array
     */
    protected $_columnOrderInGrid = array(
        'workorderId',
        'activity',
        'bookingCode',
        'contactName',
        'debtorNumber',
        'createdAt',
        'updatedAt',
        'spentTimeThisMonth',
        'spendTime',
        'lastDateTimeSpend',
        'licencePlate',
    );

    protected $_hiddenColumns = array(
        'vehicleId',
    );

    public function __construct(Order_Service_OrderWorkshop $service, Zend_View $view)
    {
        parent::__construct();

        $this->setService($service);
        $this->_view = $view;
        $this->_config = Zend_Registry::get('config');
    }

    /**
     * @param \Order_Service_OrderWorkshop $service
     */
    public function setService($service)
    {
        $this->_service = $service;
    }

    /**
     * @return \Order_Service_OrderWorkshop
     */
    public function getService()
    {
        return $this->_service;
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * @param int $depotId
     */
    public function setDepotId($depotId)
    {
        $this->_depotId = $depotId;
    }

    /**
     * @return int
     */
    public function getDepotId()
    {
        return $this->_depotId;
    }

    public function getWorkshopGridOptions()
    {
        $orderWorkshopService = new Order_Service_OrderWorkshop();
        $options = array(
                'sourceObject' =>  $orderWorkshopService->getWorkOrdersForInternalWorkorder($this->getDepotId()),
                'columns' => array(
                    'workorderId' => array('title' => 'Workorder', 'order'=>true, 'decorator' => $this->getWorkorderIdDecorator()),
                    'activity' => array('title' => 'activity'),
                    'contactName'=> array('title' => 'customer', 'order'=>true),
                    'debtorNumber'=> array('title' => 'debtor_number','class' => 'filter-medium-number'),
                    'createdAt'=> array(
                        'format' => 'Date',
                        'title' => 'created_at'
                    ),
                    'updatedAt'=> array(
                        'format' => 'Date',
                        'title' => 'updated_at'
                    ),
                    'bookingCode' =>  array('title' => 'booking_code'),
                    'spentTimeThisMonth' => array('title' => 'spent_time_this_month'),
                    'spendTime' => array('title' => 'spend_time'),
                    'lastDateTimeSpend'=> array(
                        'format' => 'Date',
                        'title' => 'last_date_time_spend'
                    ),
                    'licencePlate' => array(
                        'title' => 'licence_plate',
                    )
                ),
                'filters' => $this->getGridFilters(),
                'setKeyEventsOnFilters' => true,
                'recordPerPage' => '200',
                'setShowOrderImage' => false,
                'setAjax' => false,
                'paginationInterval' => array('200' => '200', '400' => '400', '600' => '600'),
            );

        // lets add hidden columns
        $options['columns'] += $this->getHiddenColumns();
        $options = $this->orderColumns($options);

        return $options;
    }


    /**
     * @return Bvb_Grid
     */
    public function getGrid()
    {
        if ($this->_grid) {
            return $this->_grid;
        }

        $gridConfig = Zend_Registry::get('gridConfig');

        $this->_grid = new Bvb_BasGrid();

        // lets get our own grid object so we can modify it later
        $this->_grid->setGrid(Bvb_Grid::factory('Table', $gridConfig, ''));
        $this->_grid->getGrid()->addFormatterDir('application/modules/workshop/formatter', 'Workshop_Formatter');
        $this->_grid->getGrid()->addFiltersRenderDir('application/modules/workshop/filter', 'Workshop_Filter');
        $this->_grid->getGrid()->addExtraColumns($this->_getActionColumn());

        return $this->_grid->getGridObject($this->getWorkshopGridOptions(), $gridConfig, $this->getRowConditions());
    }

    /**
     * row condition
     *
     * @return array
     */
    protected function getRowConditions()
    {
        return array();
    }

    /**
     * sets column ordering
     *
     * @param $options
     * @return mixed
     */
    protected function orderColumns($options)
    {
        $position = 1;

        foreach($this->_columnOrderInGrid as $column)
        {
            if (isset($options['columns'][$column])) {
                $options['columns'][$column]['position'] = $position;
            } else {
                $options['columns'][$column] = array('position' => $position);
            }
            $position += 1;
        }

        return $options;
    }

    protected function _getActionColumn()
    {
        $column = new Bvb_Grid_Extra_Column();
        $column
            ->position('left')
            ->name('actions')
            ->title('')
            ->class('widthColumn')
            ->decorator(['function' => function($rowData) {
                return $this->getView()->vehicleMemo($rowData['vehicleId']);
            }])
        ;

        return  $column;
    }

    protected function getHiddenColumns()
    {
        $returnArray = array();

        foreach($this->_hiddenColumns as $column) {
            $returnArray[$column] = array('hidden' => true);
        }

        return $returnArray;
    }

    protected function getGridFilters()
    {
        return array(
            'workorderId' => array('searchType' => '=', 'class' => 'filter-medium-number'),
            'bookingCode' => array('searchType' => 'like', 'class' => 'filter-medium-number'),
            'debtorNumber' => array('searchType' => '=', 'class' => 'filter-medium-number'),
            'contactName' => array('searchType' => 'like', 'class' => 'filter-medium-number'),
            'licencePlate' => array('searchType' => 'like', 'class' => 'filter-medium-number'),
            'createdAt' => array('render' => 'dateFilter', 'class' => 'date-filter'),
            'updatedAt' => array('render' => 'dateFilter', 'class' => 'date-filter'),
            'lastDateTimeSpend' => array('render' => 'dateFilter', 'class' => 'date-filter')
        );
    }

    protected function getWorkorderIdDecorator()
    {
        $depotSettingService = new Management_Service_DepotSetting();
        if ($depotSettingService->depotHasExtranetWorkorderSetting($this->getDepotId())) {
            $workorderIdTpl = '<a href="/vbd/public/workshop/workorder/edit/id/{{workorderId}}" title="Open Workorder">{{workorderId}}</a>';
        } else {
            $workorderIdTpl = '{{workorderId}}';
        }

        return sprintf('<div data-id="{{workorderId}}" class="filter-medium-number">%s</div>', $workorderIdTpl);
    }
}
