<?php

/**
 * Class Workshop_Service_LabourActivityGrid
 */
class Workshop_Service_LabourActivityGrid extends BAS_Shared_Service_Abstract
{

    /**
     * @var Zend_View_Interface
     */
    protected $_view;

    /**
     * @param Zend_View_Interface $view
     */
    public function __construct(Zend_View_Interface $view)
    {
        $this->_view = $view;
    }

    /**
     * @param Zend_Db_Select $source
     * @return Bvb_Grid
     * @throws Bvb_Grid_Exception
     * @throws Zend_Exception
     */
    public function getGrid(Zend_Db_Select $source)
    {
        $columns = [
            'activity_id' => [
                'class' => 'width70',
                'title' => 'Id',
            ],
            'department_id' => ['hidden' => true],
            'department_name' => [
                'searchField' => 'department_id',
                'title' => 'department',
            ],
            'activity_name' => [
                'title' => 'name',
            ],
        ];
        $filters = [
            'activity_name' => [
                'searchType' => 'like',
            ],
            'department_id' => [
                'searchType' => '=',
            ],
            'department_name' => [
                'distinct' => [
                    'field' => 'department_id',
                    'name' => 'department_name',
                    'order' => 'department_name ASC',
                ]
            ],
        ];

        $actions = $this->_view->render('labour-activity/_grid-actions.phtml');
        $actionColumn = new Bvb_Grid_Extra_Column();
        $actionColumn->position('right')->name('actions')->title('')->decorator($actions);

        $gridConfig = Zend_Registry::get('gridConfig');
        $grid = Bvb_Grid::factory('Table', $gridConfig);
        $grid->setAjax(true);
        $grid->setShowOrderImages(false);
        $grid->addExtraColumns($actionColumn);
        $grid->setRecordsPerPage(20);
        $grid->setPaginationInterval([20 => 20, 50 => 50, 100 => 100]);

        $basGrid = new Bvb_BasGrid();
        $basGrid->setGrid($grid);
        $basGrid->setSource($grid, 0, $source);
        $basGrid->setFilters($grid, [], $filters, true);
        $basGrid->setColumns($grid, $columns);

        $grid->deploy();
        return $grid;
    }

}
