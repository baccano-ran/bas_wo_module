<?php

/**
 * Class Workshop_Service_TimeTrackGrid
 */
class Workshop_Service_TimeTrackGrid extends BAS_Shared_Service_Abstract
{

    /**
     * @var Zend_View_Interface
     */
    protected $_view;


    /**
     * @var Zend_Db_Select
     */
    protected $sourceSelect;

    /**
     * @var int
     */
    protected $activeDepotId;

    /**
     * @var int
     */
    protected $workorderId;


    /**
     * @param Zend_View_Interface $view
     * @param int $activeDepotId
     * @param int $workorderId
     */
    public function __construct(Zend_View_Interface $view, $activeDepotId, $workorderId)
    {
        $this->_view = $view;
        $this->activeDepotId = $activeDepotId;
        $this->workorderId = $workorderId;
    }

    /**
     * @param Zend_Db_Select $source
     * @return Bvb_Grid
     * @throws Bvb_Grid_Exception
     * @throws Zend_Exception
     */
    public function getGrid(Zend_Db_Select $source)
    {
        $this->sourceSelect = $source;

        $columns = $this->getGridColumns();
        $filters = $this->getGridFilters();

        $gridConfig = Zend_Registry::get('gridConfig');
        $gridConfig['grid'] = [
            'baseUrl' => str_replace($this->getView()->baseUrl() . '/', '', $this->getView()->url([
                'module'=>'workshop',
                'controller'=>'workorder-details-tab',
                'action'=>'time-track-grid',
                'workorderId'=>$this->workorderId,
            ],null, true ))
        ];

        /** @var Bvb_Grid_Deploy_Table $grid */
        $grid = Bvb_Grid::factory('Table', $gridConfig);

        $gridId = 'timetrack-grid';
        $grid
            ->setGridId($gridId)
            ->setAjax($gridId)
            ->setShowOrderImages(false)
            ->setAdvanceSearchLink('')
            ->addFormatterDir('application/modules/workshop/formatter', 'Workshop_Formatter')
            ->addExtraColumns(
                $this->getSearchEmptyColumn(),
                $this->getActionColumn()
            )
            ->setRecordsPerPage(50)
            ->setPaginationInterval([
                20  => 20,
                50  => 50,
                100 => 100,
            ])
        ;


        $basGrid = new Bvb_BasGrid();
        $basGrid
            ->setGrid($grid)
            ->setSource($grid, 0, $source)
            ->setFilters($grid, [], $filters)
            ->setColumns($grid, $columns);

        $grid->deploy();

        return $grid;
    }

    /**
     * @return text
     */
    private function getGridFooter()
    {
        $workorderTimeService = new Workshop_Service_WorkorderTime();
        return $this->getView()->partial('workorder-details-tab/partial/time-track-grid-footer.phtml',
            ['totalSpentTime' => $workorderTimeService->getWorkorderSpentTime($this->workorderId)]);
    }

    /**
     * @return Bvb_Grid_Extra_Column
     */
    private function getSearchEmptyColumn()
    {
        $actionColumn = new Bvb_Grid_Extra_Column();
        $actionColumn
            ->position('left')
            ->name('actions')
            ->title('')
            ->class('col-actions');

        return $actionColumn;
    }

    /**
     * @return array
     */
    private function getGridColumns()
    {
        $gridObject = $this;
        $priceFilter = new Zend_Filter_NormalizedToLocalized([
            'locale' => 'nl_NL',
            'precision' => 2,
        ]);
        return [
            'id' => [
                'hidden' => true,
            ],
            'employee_number' => [
                'title' => 'employee_number',
            ],
            'employee_name'   => [
                'title' => 'name',
            ],
            'date'              => [
                'title'        => 'Date',
                'class'        => 'col-date',
                'searchType'   => 'sqlExp',
                'searchSqlExp' => "DATE_FORMAT(owt.date, '%d-%m-%Y') = '{{value}}'",
                'decorator'    => [
                    'function' => function ($rowData) {
                        return isset($rowData['date']) ? $this->getView()->formatDate($rowData['date'], 'd-m-Y') : '';
                    },
                ],
            ],
            'spent_time'        => [
                'title' => 'spent_time',
                'class' => 'col-spen-time',
                'decorator'    => [
                    'function' => function ($rowData) use (&$gridObject, $priceFilter) {
                        $spentTime = isset($rowData['spent_time']) ? $priceFilter->filter($rowData['spent_time']) : '';
                        return $spentTime;
                    },
                ],
            ],
            'activity_name' => [
                'title' => 'activity',
                'class'        => 'col-activity',
                'decorator'    => [
                    'function' => function ($rowData) {
                        return isset($rowData['activity_name']) ? $this->getView()->translate($rowData['activity_name']) : '';
                    },
                ],

            ],
        ];
    }

    /**
     * @return array
     */
    private function getGridFilters()
    {
        return [
            'employee_number'           => [
                'searchType' => '=',
            ],
            'employee_name'           => [
                'searchType' => 'like'
            ],
            'date'         => [
                'class' => 'js-date-filter',
            ],
            'activity_name'           => [
                'values' => $this->getLabourActivityOptions(),
            ],

        ];
    }

    /**
     * @return array
     */
    private function getLabourActivityOptions()
    {
        $labourActivityService = new Workshop_Service_LabourActivity();

        $labourActivities = $labourActivityService->findByDepartmentId(
            BAS_Shared_Model_Department::DEPARTMENT_ID_WORKSHOP,
            $this->activeDepotId
        );

        $options = array_map(function($activityName){
            return $this->getView()->translate($activityName);
        }, BAS_Shared_Utils_Array::listData($labourActivities, 'id', 'name'));

        return $options;
    }


    private function getActionColumn()
    {
        $column = new Bvb_Grid_Extra_Column();
        $column
            ->position('right')
            ->name('actions')
            ->title('')
            ->class('col-actions')
            ->decorator(['function' => function($rowData) {
                return $this->getView()->partial('workorder-details-tab/partial/time-track-grid-actions.phtml', [
                    'rowData' => $rowData,
                ]);
            }])
        ;

        return  $column;
    }
}
