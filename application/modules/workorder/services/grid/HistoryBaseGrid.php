<?php

/**
 * Class Workshop_Service_HistoryBaseGrid
 *
 * Used as base grid class for order/workorder history widgets
 */
class Workshop_Service_HistoryBaseGrid extends Lease_Service_BaseGrid
{
    protected $_recordsPerPage = 10;
    protected $_paginationInterval = [];

    protected function _getActionsColumn() { }

    protected function _getDateDecorator()
    {
        return $this->_getDecoratorWithFilter(new Lease_Form_Filter_DateFormat('d-m-Y'));
    }

    /**
     * @return array
     */
    protected function _getWorkshopStatusOptions()
    {
        return [
            '__OFFER__' => $this->_view->translate(BAS_Shared_Model_OrderWorkshop::LABEL_WORKORDER_OFFER),
            BAS_Shared_Model_OrderWorkshop::WORKORDER_START => $this->_view->translate(BAS_Shared_Model_OrderWorkshop::LABEL_WORKORDER_START),
            BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED => $this->_view->translate(BAS_Shared_Model_OrderWorkshop::LABEL_WORKORDER_PRINTED),
            BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED => $this->_view->translate(BAS_Shared_Model_OrderWorkshop::LABEL_WORKORDER_CLOSED),
            BAS_Shared_Model_OrderWorkshop::WORKORDER_ARCHIVED => $this->_view->translate(BAS_Shared_Model_OrderWorkshop::LABEL_WORKORDER_ARCHIVED),
            BAS_Shared_Model_OrderWorkshop::WORKORDER_INVOICED => $this->_view->translate(BAS_Shared_Model_OrderWorkshop::LABEL_WORKORDER_INVOICED),
        ];
    }
}

