<?php

/**
 * Workshop service
 *
 * @category   Workshop
 * @package    Service
 * @subpackage workshop
 *
 * @author Jimit Shah
 * @version SVN: $Id: $
 */
class Workshop_Service_Workshop extends Default_Service_Grid
{
    /** @var Bvb_BasGrid */
    protected $_planningGrid;

    /** @var Order_Service_OrderWorkshop */
    protected $_service;

    /** @var Management_Service_Depot */
    protected $_depotService;

    /** @var Contacts_Service_ContactCompanyRoles */
    protected $_contactCompanyRoleService;

    /** @var Zend_View */
    protected $_view;
    
    /** @var array */
    protected $_paginationOption = [
            '50' => '50',
            '100' => '100',
            '200' => '200',
        ];

    const GRID_RECORD_LIMIT = 50;

    // const for grid
    const GRID_PLANNING = 'plannning';
    
    /**
     * maximum length of text which will be after that truncated
     */
    const MAX_TEXT_LENGTH = 40;
    
    /** @var array */
    protected $_contactList;
    
    /**
     * @var array
     */
    protected $_status = [];
    
    /**
     * To assign the values to variables.
     *
     * @param array $options
     */
    function __construct($options = [])
    {
        $this->setBasePath('default');

        parent::__construct($options);
    }

    /**
     * Set order workshop service
     *
     * @param Order_Service_OrderWorkshop $service 
     * @return Workshop_Service_Workshop
     */
    public function setWorkshopService(Order_Service_OrderWorkshop $service)
    {
        $this->_service = $service;

        return $this;
    }

    /**
     * Get order workshop service
     *
     * @return Order_Service_OrderWorkshop
     */
    public function getWorkshopService()
    {
        if (null === $this->_service) {
            $this->_service = new Order_Service_OrderWorkshop();
        }

        return $this->_service;
    }

    /**
     * Set depot service
     *
     * @param Management_Service_Depot $service
     * @return Workshop_Service_Workshop
     */
    public function setDepotService(Management_Service_Depot $service)
    {
        $this->_depotService = $service;

        return $this;
    }

    /**
     * Get depot service
     *
     * @return Management_Service_Depot
     */
    public function getDepotService()
    {
        if (null === $this->_depotService) {
            $this->_depotService = new Management_Service_Depot();
        }
        return $this->_depotService;
    }

    /**
     * Set contact role service
     *
     * @param Contacts_Service_ContactCompanyRoles $service
     * @return Workshop_Service_Workshop
     */
    public function setContactCompanyRoleService(Contacts_Service_ContactCompanyRoles $service)
    {
        $this->_contactCompanyRoleService = $service;

        return $this;
    }

    /**
     * Get contact role service
     *
     * @return Contacts_Service_ContactCompanyRoles
     */
    public function getContactCompanyRoleService()
    {
        if (null === $this->_contactCompanyRoleService) {
            $this->_contactCompanyRoleService = new Contacts_Service_ContactCompanyRoles();
        }
        return $this->_contactCompanyRoleService;
    }

    /**
     * Return view object
     * 
     * @return \Zend_View
     */
    public function getView()
    {
        if (null !== $this->_view) {
            return $this->_view;
        }
        $this->_view = new Zend_View();
        $this->_view->addBasePath(__DIR__ . '/../views');
        $this->_view->addHelperPath(__DIR__ . '/../../default/views/helpers', 'Default_View_Helper_');
        return $this->_view;
    }
    
    
    /**
     * Get planning grid data
     * 
     * @param int $depotId
     * @param array $params
     * @param array $depotList
     * @param int $mainAccountingDepotId
     * @return Bvb_BasGrid|NULL
     */
    public function getPlanningGrid($depotId, $params, $depotList, $mainAccountingDepotId) {
        
        if (NULL !== $this->_planningGrid) {
            return $this->_planningGrid;
        }
        
        $gridConfig = Zend_Registry::get('gridConfig');
        $this->_planningGrid = new Bvb_BasGrid();
        $this->_planningGrid->setGrid(Bvb_Grid::factory('Table', $gridConfig, ''));
        $this->_planningGrid->getGrid()->addFormatterDir('application/modules/default/formatter', 'Default_Formatter');
        $this->_planningGrid->getGrid()->addFormatterDir('application/modules/defect/formatter', 'Defect_Formatter');
        $this->_planningGrid->getGrid()->addFormatterDir('application/modules/workshop/formatter', 'Workshop_Formatter');
        $this->_planningGrid->getGrid()->addFormatterDir('application/modules/purchase/formatter', 'Purchase_Formatter');
        $this->_planningGrid->getGrid()->addExtraColumns($this->_getActionColumn());
        
        $planningGridOptions = $this->getPlanningGridOptions(
            $depotId,
            $params,
            $depotList,
            $mainAccountingDepotId
        );
        $grid = null;
        
        if ([] !== $planningGridOptions) {
            $grid = $this->_planningGrid->getGridObject(
                        $planningGridOptions, 
                        $gridConfig);
        }
        
        return $grid;
    }
    
    /**
     * @return \Bvb_Grid_Extra_Column
     */
    protected function _getActionColumn()
    {
        $translator = $this->getTranslate();
        $column = new Bvb_Grid_Extra_Column();
        
        $actionHtml = $this->getView()->render('partial/planning-actions.phtml');
        $column->position('left')
                ->name('actions')
                ->title($translator->translate('tab_actions'))
                ->decorator($actionHtml);
        return $column;
    }
    
    /**
     * @return string
     */
    protected function _getLocationDecorator()
    {
        return $this->getView()->render('partial/planning-location.phtml');
    }
    

    /**
     * Returns hidden columns
     * @return array
     */
    protected function _getPlanningGridHidenColumns()
    {
        return [
            'orderVehicleId',
            'vehicleId',
            'customerLanguage',
            'customerRemark',
            'customerName',
            'tagId',
            'orderId',
            'isCreatedManually',
            'vehicleTypeId',
            'refNo',
            'defectId',
            'depotId',
            'workorderType',
            'id',
            'supplierReference',
            'commentId',
            'depotIdLocation',
            'expectedDeliveryDateStatus',
        ];
    }
    
    protected function _getPlanningGridColumns()
    {
        return [
            'truckId',
            'workorderId',
            'as400Id',
            'contactName',
            'type',
            'workshopId',
            'vehicleType',
            'description',
            'chassisNumber',
            'licensePlate',
            'year',
            'location',
            'wplVoca',
            'vehicleStatus',
            'startDate',
            'deadlineDate',
            'expectedDeliveryDate',
            'workorderStatus'
        ];
    }
    
    /**
     * Get planning grid option
     * 
     * @param int $depotId
     * @param array $params
     * @param array $depotList
     * @param int $mainAccountingDepotId
     * @return array
     */
    public function getPlanningGridOptions($depotId, $params, $depotList, $mainAccountingDepotId)
    {
        $nacaIncludePriceOriginal = (int)$this->getView()->depotSetting('naca_include_price_original');
        $orderWorkshopService = $this->getWorkshopService();
        $result = $orderWorkshopService->getPlanningGridData(
            $depotId,
            $params,
            $nacaIncludePriceOriginal,
            array_keys($depotList),
            $mainAccountingDepotId
        );
        
        if ($params['isExternal']) {
            $contactCompanyRoleService = $this->getContactCompanyRoleService();
            $depotList = $contactCompanyRoleService->findDepotContactsByRole(
                $depotId,
                BAS_Shared_Model_ContactCompanyRole::WORKSHOP,
                true
            );
        }

        $this->setPersonPageSettingService(new Default_Service_PersonPageSetting());
        $pageSettings = $this->getPersonPageSettings($this->_options['user']->getId(),
            $this->_options['activeDepotId'],
            $params
        );
        $title = $this->getTitleList($pageSettings);
        $this->_sourceType = Bvb_BasGrid::SOURCE_TYPE_ARRAY_OBJECT;
        $options = $this->getGridOptions(
            new Default_Source_Array($result, array_keys($title)),
            $this->getColumns($depotId),
            $this->getPlanningGridFilters($depotList),
            [],
            self::GRID_RECORD_LIMIT,
            self::GRID_PLANNING,
            [],
            true
        );

        $options = $this->setColumnStatusHidden($options, $params['node'], $params['action'], $pageSettings['columnStatus']);
        $options = $this->setColumnLabel($options, $pageSettings['columnLabels']);
        $options['columns'] = array_merge($options['columns'], $this->getHiddenColumns($this->_getPlanningGridHidenColumns()));
        $options = $this->orderColumns($options, $pageSettings['columnSequence']);

        return $options;
    }

    /**
     * returns filters configuration for grid
     * @param $depotList
     * @return array
     */
    protected function getPlanningGridFilters($depotList)
    {
        $translator = $this->getTranslate();
        return [
            'vehicleType' => array(
                'distinct' => array(
                    'field' => 'vehicleType',
                    'name' => 'vehicleType'
                ),
                'class' => 'filter-very-small'
            ),
            'truckId' => [
                'searchType' => '=' ,
                'class' => 'filter-small-number'
            ],
            'chassisNumber' => [
                'searchType' => 'like',
                'class' => 'width115'
            ],
            'licensePlate' => [
                'searchType' => '=',
                'class' => 'width70'
            ],
            'year' => [
                'searchType' => '=',
                'class' => 'width-30',
            ],
            'workorderId' => [
                'searchType' => '=' ,
                'class' => 'filter-small-number'
            ],
            'as400Id' => [
                'searchType' => '=' ,
                'class' => 'filter-small'
            ],
            'vehicleStatus' => [
                'class' => 'width50',
                'distinct' => [
                    'field' => 'vehicleStatus',
                    'name' => 'vehicleStatus'
                ]
            ],
            'startDate' => [
                'searchType' => '>=',
                'class' => 'width70'
            ],
            'deadlineDate' => [
                'searchType' => '<=',
                'class' => 'width70'
            ],
            'type' => [
                'values' => [
                    BAS_Shared_Model_OrderWorkshop::TYPE_ORDER => $translator->translate('order'),
                    BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT => $translator->translate('defects'),
                    BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK => $translator->translate('external'),
                ],
                'searchType' => '=',
                'class' => 'filter-small'
            ],
            'workorderStatus' => [
                'values' => [
                    BAS_Shared_Model_OrderWorkshop::WORKORDER_START => $translator->translate('not_started'),
                    BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED => $translator->translate('started'),
                    BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED => $translator->translate('complete')
                ],
                'searchType' => '=',
                'class' => 'width60'
            ],
            'workshopId' => [
                'values' => $depotList,
                'class' => 'width60'
            ],
            'contactName' => [
                'searchType' => 'like',
            ],
            'expectedDeliveryDate' => [
                'searchType' => '=',
                'class' => 'width70'
            ],
        ];
    }

    /**
     * Get columns for planning grid
     *
     * @param integer $depotId
     * @return array $columns
     * @throws BAS_Shared_Exception
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function getColumns($depotId)
    {
        $as400Id = 'Enter as400';
        $config = $this->getConfig();
        $serviceUrl = $config->api->internal_base_url;
        $depotService = $this->getDepotService();
        $canAssignWorkManually = $depotService->isDepotAllowedToAssignWork($depotId);
        $depotSettingService = new Management_Service_DepotSetting();
        $depotHasExtranetWorkorderSetting = $depotSettingService->depotHasExtranetWorkorderSetting($depotId);
        $this->_status = [
            BAS_Shared_Model_OrderWorkshop::TYPE_ORDER => $this->getView()->translate('order'),
            BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT => $this->getView()->translate('defects'),
            BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK => $this->getView()->translate('external'),
        ];
        // get contacts with workshop role
        $contactList = $this->getContactList();
        // get depots
        $depotList = $this->getDepots();
        $externalWork = BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK;
        $columns = [
            'truckId' => [
                'format' => [
                    'WorkorderDetails',
                    [
                        'workorderId' => '{{workorderId}}',
                        'orderId' => '{{orderId}}',
                        'orderVehicleId' => '{{orderVehicleId}}',
                        'vehicleId' => '{{vehicleId}}',
                        'truckId' => '{{refNo}}',
                        'type' => '{{workorderType}}',
                        'defectId' => '{{defectId}}',
                        'as400Id' => '{{as400Id}}',
                        'supplierReference' => '{{supplierReference}}',
                        'isSupplierReferenceVisible' => $this->isAllowed('stock.vehicle.supplier-reference'),
                        'workOrderLabel' => $this->getView()->translate('work_orders'),
                        'detailLabel' => $this->getView()->translate('details'),
                    ]
                ]
            ],
            'workorderId' => [
                'format' => [
                    'PrintWorkorder',
                    [
                        'orderId' => '{{orderId}}',
                        'defectId' => '{{defectId}}',
                        'as400Id' => '{{as400Id}}',
                        'orderVehicleId' => '{{orderVehicleId}}',
                        'vehicleId' => '{{vehicleId}}',
                        'truckId' => '{{refNo}}',
                        'serviceUrl' => $serviceUrl,
                        'workorderStatus' => '{{workorderStatus}}',
                        'canAssignWorkManually' => $canAssignWorkManually,
                        'changeWorkorderStatusOnPrint' => $this->getView()->depotSetting('change_workorder_status_on_print'),
                        'depotHasExtranetWorkorderSetting' => $depotHasExtranetWorkorderSetting,
                        'type' => '{{workorderType}}'
                    ]
                ]
            ],
            'as400Id' => [
                'class' => 'updateManualField',
                'decorator' => [
                    'function' => function($data)  use($as400Id) {
                        return $this->_getManualFieldDecorator(
                            $data['as400Id'],
                            $data['workorderId'],
                            $as400Id
                        );
                    }]
            ],
            'workshopId' => [
                'decorator' => [
                    'function' => function ($workshop) use($contactList, $depotList, $externalWork) {
                        $workshopId = $workshop['workshopId'];
                        
                        if ($externalWork === (int)$workshop['type']) {
                            return (isset($contactList[$workshopId])) ? $contactList[$workshopId] : $workshopId;
                        } else {
                            return (isset($depotList[$workshopId])) ? $depotList[$workshopId] : $workshopId;
                        }
                    },
                ]
            ],
            'contactName' => [
                'decorator' => [
                    'function' => function ($workshop) {
                        $contactName = $workshop['contactName'];
                        
                        if (strlen($contactName) > self::MAX_TEXT_LENGTH) {
                            $chunks = str_split($contactName, self::MAX_TEXT_LENGTH);
                            
                            return implode("<br/>", $chunks);
                        }

                        return $contactName;
                    }
                ]
            ],
            'type' => [
                'decorator' => [
                    'function' => function ($workshop) {
                        $typeText = '';
                        
                        if (array_key_exists($workshop['type'], $this->_status)) {
                            $typeText = $this->_status[$workshop['type']];
                        }

                        return $typeText;
                    },
                ]
            ],
            'vehicleType' => [
                'class' => 'width25'
            ],
            'description' => [
                'decorator' => [
                    'function' => function ($workshop) {
                        $description = $workshop['description'];
                        
                        if (strlen($description) > self::MAX_TEXT_LENGTH) {
                            $chunks = str_split($description, self::MAX_TEXT_LENGTH);
                            
                            return implode("<br/>", $chunks);
                        }

                        return $description;
                    }
                ]
            ],
            'chassisNumber' => [],
            'licensePlate' => [],
            'year' => [],
            'location' => [
                'decorator' => $this->_getLocationDecorator(),
            ],
            'wplVoca' => [
                'format' => [
                    'currency', 
                    'nl_NL'
                ],
            ],
            'vehicleStatus' => [],
            'startDate' => ['date' => true],
            'deadlineDate' => ['date' => true],
            'expectedDeliveryDate' => [
                'format' => [
                    'DatePicker',
                    [
                        'isAllowed' => true,
                        'expectedDeliveryDateStatus' => '{{expectedDeliveryDateStatus}}',
                        'fieldName' => '',
                        'id' =>  '',
                    ]
                ],
                'date' => true
            ],
            'workorderStatus' => [
                'format' => [
                    'WorkorderStatus',
                    [
                        'workorderId' => '{{id}}',
                        'workorderType' => '{{workorderType}}',
                        'orderVehicleId' => '{{orderVehicleId}}',
                        'orderId' => '{{orderId}}',
                        'vehicleId' => '{{vehicleId}}',
                        'commentId' => '{{commentId}}',
                        'changeWorkorderStatusOnPrint' => $this->getView()->depotSetting('change_workorder_status_on_print'),
                        'canCompleteInternalWorkorder' => $this->isAllowed('workshop.workorder.complete-internal-workorder')
                    ]
                ],
            ],
        ];

        if (!$this->hasUserAccess(BAS_Shared_Auth_Service::getIdentity()->id,
            'workshop.index.update-order-workshop-as400')) {
            $columns['as400Id'] = [];
        }

        return $columns;
    }

    /**
     * Manual field decorator
     *
     * @param integer $value
     * @param $workOrderId
     * @param string $message
     * @return string
     */
    protected function _getManualFieldDecorator($value, $workOrderId, $message)
    {
        return sprintf('<input type="text" workOrderId="%s"'
            . 'class="manualField width50 borderNoneWithBackground" readonly value="%s" title="%s"/>',
            $workOrderId,
            $value,
            $message
        );
    }
    
    /**
     * Get contact list
     * 
     * @return array
     */
    public function getContactList()
    {
        if (null === $this->_contactList) {
            $this->_contactList = $this->getContactCompanyRoleService()->findDepotContactsByRole(
                $this->_options['activeDepotId'],
                BAS_Shared_Model_ContactCompanyRole::WORKSHOP,
                true
            );
        }
        
        return $this->_contactList;
    }
}