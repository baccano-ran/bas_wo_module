<?php
/**
 * Order Workshop service for display Workshop agenda grid
 */
class Workshop_Service_OrderWorkshop extends Default_Service_Grid
{
    /**
     * Records per page value
     *
     * @var integer
     */
    const RECORDS_PER_PAGE_DEFAULT = 50;
    const MIN_WPL_VOCA = 750;

    /**
     * Grid ID
     *
     * @var string
     */
    const ORDER_WORKSHOP_AGENDA_GRID = 'orderWorkshopAgenda';

    /** Scheduled deadline date diff count */
    const MIN_DAY_DIFF = 0;
    const MAX_DAY_DIFF = 3;

    /** @var string image url path */
    protected $_imageUrl;
    
    /** @var string base path of application */
    protected $_baseUrl;
    
    /** @var  Contacts_Service_TypeSupplier */
    protected $_contactTypeSupplierService;

    /** @var  array */
    protected $_supplierList;

    /**
     * Hidden columns
     * @var array
     */
    protected $_hiddenColumns = [
        'orderVehicleId',
        'priceParts',
        'priceTransportTotal',
        'priceOther',
        'priceBodyWork',
        'priceExternalWork',
        'priceDocuments',
        'priceDiesel',
        'priceVehicle',
        'productDiscount',
        'preparationDownpayment',
        'vehicleId',
        'saleType',
        'daysToSchedule',
        'expectedDeliveryDateBy',
        'expectedDeliveryDateAt',
        'preparationApprovedBy',
        'preparationApprovedAt',
        'status',
        'exchangeRate',
        'tagId',
        'vehicleStatus',
        'depotIdLocation',
        'entryExpected',
        'contactTypeSupplierFeeContactId',
        'supplierReference',
        'vehicleType',
        'transportId',
        'workorderId',
        'commentId',
        'contactId',
        'languageCode',
        'remarks',
        'preparationRemark',
        'loadedOnVehicle',
        'countryName',
        'transportName',
        'workshopStatus',
        'commentPersonAbbreviation',
        'workorderId',
        'workorderType',
        'workshopDeadline',
        'expectedDate',
        'receivedPaymentDollar',
        'expectedDeliveryDateStatus',
    ];

    /**
     * Vehicle status delivered
     *
     * @var integer
     */
    protected $_vehicleStatusDelivered;

    /**
     * To assign the values to variables.
     *
     * @param array $options
     */
    function __construct($options = [])
    {
        $this->_vehicleStatusDelivered = BAS_Shared_Model_Vehicle::VEHICLE_STATUS_DELIVERED;

        $this->setBasePath('workshop');
        $this->setBasePath('default');

        parent::__construct($options);
    }

    /**
     * Get order workshop agenda grid
     *
     * @param Zend_Db_Select $orderWorkshopSelect
     * @param array $orderWorkshopParams
     * @return Bvb_BasGrid|Bvb_Grid
     */
    public function getOrderWorkshopGrid($orderWorkshopSelect, $orderWorkshopParams)
    {
        $this->setPersonPageSettingService(new Default_Service_PersonPageSetting());
        $pageSettings = $this->getPersonPageSettings($this->_options['user']->getId(),
            $this->_options['activeDepotId'],
            $orderWorkshopParams
        );

        $this->_sourceType = Bvb_BasGrid::SOURCE_TYPE_SELECT;
        $gridOptions = $this->getGridOptions(
            $orderWorkshopSelect,
            $this->getColumns(),
            $this->getFilters(),
            [],
            self::RECORDS_PER_PAGE_DEFAULT,
            self::ORDER_WORKSHOP_AGENDA_GRID,
            [],
            true
        );

        $gridOptions = $this->setColumnStatusHidden($gridOptions, $orderWorkshopParams['node'], $orderWorkshopParams['action'], $pageSettings['columnStatus']);
        $gridOptions = $this->setColumnLabel($gridOptions, $pageSettings['columnLabels']);
        $gridOptions['columns'] = array_merge($gridOptions['columns'], $this->getHiddenColumns($this->_hiddenColumns));
        $gridOptions = $this->orderColumns($gridOptions, $pageSettings['columnSequence']);
        
        return $this->prepareGrid($gridOptions, $orderWorkshopParams);
    }

    /**
     * Prepare grid for workshop
     *
     * @param array $gridOptions
     * @param array $orderWorkshopParams
     * @return Bvb_BasGrid|Bvb_Grid
     */
    public function prepareGrid($gridOptions, $orderWorkshopParams)
    {
        if ($this->_grid) {
            return $this->_grid;
        }

        $this->_grid = new Bvb_BasGrid();
        $this->_grid->setGrid(Bvb_Grid::factory('Table', []));
        $this->_grid->getGrid()->addFormatterDir('application/modules/workshop/formatter', 'Workshop_Formatter');
        $this->_grid->getGrid()->addFormatterDir('application/modules/default/formatter', 'Default_Formatter');
        $this->_grid->getGrid()->addFiltersRenderDir('application/modules/default/filter', 'Default_Filter_');
        $this->_grid->getGrid()->addExtraColumns($this->getActionColumnLeft());
        $rowCondition = $this->getRowCondition($orderWorkshopParams['highlightWorkOrderSetting'],
            $orderWorkshopParams['redBackgroundWorkorder']);
        if (!array_key_exists('deliveredAtorderWorkshopAgenda', $orderWorkshopParams)) {
            $this->_grid->getGrid()->setDefaultFiltersValues(['deliveredAt' => 'all']);
        }

        $url = $this->getView()->urlMap('basePath') . '/workshop/index/advance-search';
        $advancedSearchTag = sprintf('<br />(<a href="%s">%s</a>)', $url,
            $this->getView()->translate('advance_search'));
        $this->_grid->getGrid()->setAdvanceSearchLink($advancedSearchTag);

        return $this->_grid->getGridObject($gridOptions, [], $rowCondition, []);
    }

    /**
     * Get columns shown for grid
     */
    public function getColumns()
    {
        $imageUrl = $this->getConfig()->extranet->image_url;
        $baseUrl = $this->getConfig()->extranet->base_path;
        $commentedAt = $this->getView()->translate('tabtransport_commentat');
        $commentedBy = $this->getView()->translate('tabtransport_commentby');
        $vehicleTypeAbbreviationList = $this->getVehiclesTypeAbbreviation();
        $customerInfoRight = $this->getView()->isAllowed('order.customer.customerinfo');
        $contactDetailsLabel = $this->getView()->translate('contact_details');
        $deliveredLabel = $this->getView()->translate('Delivered');
        $extranetUrl = $this->getConfig()->extranet->base_url;
        $emptyDateTime = BAS_Shared_Model_Vehicle::DEFAULT_EMPTY_DATE_TIME;
        $nacaIncludePriceOriginal = (int)$this->getView()->depotSetting('naca_include_price_original');

        $marginRound = [
            'Amount',
            [
                'numberFormat' => 'marginround',
                'currencyClass' => 'currency{{currency}}',
            ]
        ];
        $transportMethodList = $this->getTransportMethodFilterValues();
        $depotList = $this->getDepotList();

        return [
            'orderId' => [
                'format' => [
                    'OrderWorkshopDetails',
                    [
                        'contactTypeSupplierFeeContactId' => '{{contactTypeSupplierFeeContactId}}',
                        'orderId' => '{{orderId}}',
                        'vehicleId' => '{{vehicleId}}',
                        'truckId' => '{{truckId}}',
                        'orderVehicleId' => '{{orderVehicleId}}',
                        'baseUrl' => $baseUrl,
                        'workOrderLabel' => $this->getView()->translate('work_orders'),
                        'portalIconRight' => $this->isAllowed('default.partner-portal.portal-vehicle-icon'),
                        'interSupplierIds' => serialize($this->_getInterSupplierIds()),
                        'supplierList' => serialize($this->_getSupplierList()),
                        'supplierFeeUnknown' => BAS_Shared_Model_ContactTypeSupplier::SUPPLIER_FEE_UNKNOWN,
                        'unknownLabel' => $this->getView()->translate('unknown'),
                        'imageUrl' => $imageUrl,
                    ]
                ]
            ],
            'truckId' => [
                'format' => [
                    'TruckDetails',
                    [
                        'vehicleId' => '{{vehicleId}}',
                        'truckId' => '{{truckId}}',
                        'supplierReference' => '{{supplierReference}}',
                        'preparationApproved' => '{{preparationApproved}}',
                        'vehicleId' => '{{vehicleId}}',
                        'orderId' => '{{orderId}}',
                        'orderVehicleId' => '{{orderVehicleId}}',
                        'truckId' => '{{truckId}}',
                        'baseUrl' => $baseUrl,
                        'supplierReferenceAllowed' => $this->isAllowed('stock.vehicle.supplier-reference'),
                        'workshopAdminAllowed' => $this->isAllowed('workshop.workshop.admin'),
                        'changeWorkorderStatusOnPrint' => $this->getView()->depotSetting('change_workorder_status_on_print'),
                        'imageUrl' => $imageUrl,
                        'preparationApprovedValue' => BAS_Shared_Model_OrderVehicle::PREPARATION_APPROVED_APPROVED,
                    ]
                ]
            ],
            'vehicleTypeId' => [
                'orderQuery' => function($direction) {
                    return new Zend_Db_Expr('s.Afkorting ' . $direction);
                },
                'decorator' => [
                    'function' => function ($row) use($vehicleTypeAbbreviationList){
                        return (!empty($vehicleTypeAbbreviationList[$row['vehicleTypeId']])) ?
                            $vehicleTypeAbbreviationList[$row['vehicleTypeId']] : '';
                    }
                ]
            ],
            'description' => [],
            'chassisNumber' => [],
            'licensePlate' => [],
            'year' => [],
            'location' => [
                'format' => [
                    'LocationDetails',
                    [
                        'tagId' => '{{tagId}}',
                        'depotIdLocation' => '{{depotIdLocation}}',
                        'location' => '{{location}}',
                        'deliverLabel' => $deliveredLabel,
                        'extranetUrl' => $extranetUrl . $baseUrl,
                        'basDepot' => BAS_Shared_Model_Depot::BAS_DEPOT,
                    ]
                ]
            ],
            'personAbbreviation' => [],
            'customer' => [
                'orderQuery' => function($direction) {
                    return new Zend_Db_Expr('cust.name ' . $direction);
                },
                'format' => [
                    'CustomerDetails',
                    [
                        'contactId' => '{{contactId}}',
                        'client' => '{{customer}}',
                        'customerInfoAllowed' => $customerInfoRight,
                        'imageUrl' => $imageUrl,
                        'contactDetailLabel' => $contactDetailsLabel,
                        'baseUrl' => $baseUrl,
                    ]
                ]
            ],
            'country' => [],
            'incoterm' => [],
            'priceWorkshop' => [
                'class' => 'whiteSpaceNoWrap',
                'decorator' => [
                    'function' => function($row) use($nacaIncludePriceOriginal) {
                        $priceWorkshop = BAS_Shared_Utils_OrderVehicle::getWorkshopVocaFromArray($row);
                        if (BAS_Shared_Model_DepotSetting::MARGIN_NACA_INCLUDE_PRICE_ORIGINAL === $nacaIncludePriceOriginal) {
                            $priceWorkshop = BAS_Shared_Utils_Math::add($priceWorkshop,
                                BAS_Shared_Utils_Math::mul($row['productDiscount'], -1)
                            );
                        }
                        $formattedPrice = BAS_Shared_Formula_PriceFormat::priceFormat($priceWorkshop, 'marginround');

                        return ($priceWorkshop > self::MIN_WPL_VOCA) ? '<b>' . $formattedPrice . '</b>' : $formattedPrice;
                    }
                ]
            ],
            'createdAt' => [
                'class' => 'right',
                'format' => ['Date'],
            ],
            'daysFromOrder' => [],
            'expectedArrival' => [
                'format' => [
                    'ExpectedDate',
                    [
                        'expectedDate' => '{{expectedDate}}',
                        'entryExpected' => '{{entryExpected}}',
                        'workorderId' => '{{workorderId}}',
                        'expectedArrival' => '{{expectedArrival}}'
                    ]
                ],
                'class' => 'center'
            ],
            'scheduledDeadline' => [
                'format' => [
                    'DeadlineDate',
                    [
                        'expectedDeliveryDateAt' => '{{expectedDeliveryDateAt}}',
                        'expectedDeliveryDateBy' => '{{expectedDeliveryDateBy}}',
                        'truckId' => '{{truckId}}',
                        'deadline' => '{{scheduledDeadline}}',
                        'workorderId' => '{{workorderId}}',
                        'activeDepotId' => $this->_options['activeDepotId'],
                        'commentByLabel' => $this->getView()->translate('tabtransport_commentby'),
                    ]
                ],
            ],
            'preparationApproved' => [
                'class' => 'filter-very-small center',
                'format' => [
                    'Approved',
                    [
                        'vehicleId' => '{{vehicleId}}',
                        'commentBy' => $commentedBy,
                        'commentAt' => $commentedAt,
                        'preparationApprovedAt' => '{{preparationApprovedAt}}',
                        'preparationRemark' => '{{preparationRemark}}',
                        'preparationApprovedBy' => '{{preparationApprovedBy}}',
                        'activeDepotId' => $this->getActiveDepotId(),
                        'imagePath' => $imageUrl,
                        'unknownLabel' => BAS_Shared_Model_OrderVehicle::PREPARATION_APPROVED_UNKNOWN_STRING,
                    ]
                ],
            ],
            'preparationWorkshop' => [
                'format' => [
                    'OrderWorkshopStatus',
                    [
                        'vehicleId' => '{{vehicleId}}',
                        'orderId' => '{{orderId}}',
                        'commentId' => '{{commentId}}',
                        'orderVehicleId' => '{{orderVehicleId}}',
                        'preparationWorkshop' => '{{preparationWorkshop}}',
                        'imageUrl' => $imageUrl,
                        'workshopCommentType' => BAS_Shared_Model_OrderComment::COMMENT_TYPE_WORKSHOP,
                    ]
                ],
            ],
            'deliveredAt' => [
                'class' => 'center',
                'decorator' => [
                    'function' => function ($row) use($imageUrl, $emptyDateTime) {
                        $icon = (null == $row['deliveredAt'] || $emptyDateTime == $row['deliveredAt']) ? '0' : '3';

                        return sprintf('<img class="imageWidthHeight16" src="%s/action_%s.png">', $imageUrl, $icon);
                    }
                ]
            ],
            'vehiclePrice' => [
                'class' => 'whiteSpaceNoWrap',
                'format' => $marginRound,
            ],
            'transportMethod' => [
                'decorator' => [
                    'function' => function ($row) use($transportMethodList) {
                        if (!empty($row['transportMethod'])
                        && array_key_exists($row['transportMethod'], $transportMethodList)) {
                            return $transportMethodList[$row['transportMethod']];
                        }
                        return null;
                    }
                ],
            ],
            'receivedPayment' => [
                'decorator' => [
                    'function' => function($row) {
                        $value = (float)$row['exchangeRate'] === 0.0
                            ? $row['receivedPayment']
                            : $row['receivedPayment'] + ($row['receivedPaymentDollar'] / $row['exchangeRate']);
                        return BAS_Shared_Formula_PriceFormat::priceFormat($value, 'marginround');
                    }
                ]
            ],
            'serviceDepotId' => [
                'decorator' => [
                    'function' => function($row) use($depotList){
                        if (empty($row['serviceDepotId'])) {
                            return null;
                        }
                        if (array_key_exists($row['serviceDepotId'], $depotList)) {
                            return $depotList[$row['serviceDepotId']]->name;
                        }
                    }
                ]
            ],
            'expectedDeliveryDate' => [
                'format' => [
                    'FinalStatusDatePicker',
                    [
                        'fieldName' => 'datepickerReserved',
                        'id' =>  'datepickerReserved{{vehicleId}}',
                        'datePicketId' => 'expectedDeliveryDate',
                        'vehicleId' => '{{vehicleId}}',
                        'isAllowed' => true,
                        'hiddenValue' => 'delete_delivery_date',
                        'hiddenFieldName' => 'deleteDeliveryDateMessage',
                        'expectedDeliveryDateStatus' => '{{expectedDeliveryDateStatus}}',
                        'expectedDeliveryDateChange' => true,
                        'imageUrl' => $imageUrl,
                        'orderVehicleId' => '{{orderVehicleId}}',
                        'expectedDeliveryDateStatusActive' => BAS_Shared_Model_OrderVehicle::EXPECTED_DELIVERY_DATE_STATUS_ACTIVE,
                    ]
                ],
            ],
            'deliveryMethod' => [
                'decorator' => [
                    'function' => function($row) {
                        $row['deliveryMethod'] = (int)$row['deliveryMethod'];
                        $value = '';
                        if (BAS_Shared_Model_Order::TRANSPORT_METHOD_PICKUP === $row['deliveryMethod'])  {
                            $value = 'pickup';
                        } else {
                            if (BAS_Shared_Model_Order::TRANSPORT_METHOD_DELIVERY === $row['deliveryMethod'])  {
                                $value = 'delivery';
                            }
                        }

                        return $this->getView()->translate($value);
                    }
                ]
            ],
            'obuStatus' => [
                'format' => [
                    'ObuStatus',
                    [
                        'vehicleId' => '{{vehicleId}}',
                        'orderId' => '{{orderId}}',
                        'orderVehicleId' => '{{orderVehicleId}}',
                        'obuStatus' => '{{obuStatus}}',
                        'imageUrl' => $imageUrl,
                    ]
                ],
                'searchType' => 'sqlExp',
                'searchSqlExp' => "IF(ov.obu_status IS NULL, 'NULL', ov.obu_status)={{value}}",
            ],
        ];
    }

    /**
     * Get action column
     *
     * @return Bvb_Grid_Extra_Column
     */
    public function getActionColumnLeft()
    {
        $column = new Bvb_Grid_Extra_Column();

        return $column->position('left')
            ->name('selection')
            ->title('')
            ->decorator(['function' => function($row) {
                return sprintf('<a href="%s/stock/vehicle/detail/vehicleid/%s">
                        <img src="%s" alt="%s" class="width100"/>
                    </a>',
                    $this->getView()->urlMap('extranetFullUrl'),
                    $row['vehicleId'],
                    $this->getView()->vehicleImage($row['vehicleId']),
                    $row['vehicleId']);
            }]);
    }

    /**
     * Get filters for order workshop grid
     *
     * @return array
     */
    public function getFilters()
    {
        $dateFilter = [
            'render' => 'dateFilter',
            'class' => ['width40'],
            'searchType' => '=',
            'dateFilter' => ['showOn' => 'both'],
        ];
        $workshopOptions = [
            BAS_Shared_Model_OrderVehicle::PREPARATION_WORKSHOP_START => $this->getView()->translate('action_required'),
            BAS_Shared_Model_OrderVehicle::PREPARATION_WORKSHOP_IN_PROCESS => $this->getView()->translate('in_progress'),
            BAS_Shared_Model_OrderVehicle::PREPARATION_WORKSHOP_ADDITIONAL_WORK => $this->getView()->translate('additional_work_required'),
            BAS_Shared_Model_OrderVehicle::PREPARATION_WORKSHOP_COMPLETED => $this->getView()->translate('label_completed'),
            BAS_Shared_Model_OrderVehicle::PREPARATION_WORKSHOP_WORK_PLANNED => $this->getView()->translate('work_planned'),
            'NULL' => $this->getView()->translate('no_action'),
        ];
        $deliveredStatusList = [
            'all' => '--' . ucfirst($this->getView()->translate('all')) . '--',
            BAS_Shared_Model_Vehicle::VEHICLE_STATUS_DELIVERED => $this->getView()->translate('delivered'),
            BAS_Shared_Model_Vehicle::VEHICLE_STATUS_NOT_DELIVERED => $this->getView()->translate('not_delivered'),
            BAS_Shared_Model_Vehicle::VEHICLE_STATUS_ALL => $this->getView()->translate('all_vehicles'),
        ];
        $preparationApprovedStatus = [
            BAS_Shared_Model_OrderVehicle::PREPARATION_APPROVED_DISAPPROVED => $this->getView()->translate('label_no'),
            BAS_Shared_Model_OrderVehicle::PREPARATION_APPROVED_NOT_YET_APPROVED => $this->getView()->translate('approval_downpayment_received'),
            BAS_Shared_Model_OrderVehicle::PREPARATION_APPROVED_APPROVED => $this->getView()->translate('label_yes'),
            BAS_Shared_Model_OrderVehicle::PREPARATION_APPROVED_ON_HOLD => $this->getView()->translate('on_hold'),
            BAS_Shared_Model_OrderVehicle::PREPARATION_APPROVED_READY_TO_BE_SEND => $this->getView()->translate('ready_to_be_send'),
            'NULL' => $this->getView()->translate('unknown'),
        ];

        $obuStatus = [
            '' => '--' . ucfirst($this->getView()->translate('all')) . '--',
            'NULL' => $this->getView()->translate('obu_status_not_applicable'),
            BAS_Shared_Model_OrderVehicle::OBU_STATUS_ACTION_REQUIRED => $this->getView()->translate('obu_status_action_required'),
            BAS_Shared_Model_OrderVehicle::OBU_STATUS_IN_PROCESS => $this->getView()->translate('obu_status_in_progress'),
            BAS_Shared_Model_OrderVehicle::OBU_STATUS_COMPLETED => $this->getView()->translate('obu_status_completed'),
        ];
        
        return [
            'orderId' => ['class' => 'width50'],
            'truckId' => ['class' => 'width50'],
            'customer' => ['searchType' => 'like'],
            'licensePlate' => ['class' => 'width65px'],
            'vehicleTypeId' => [
                'values' => $this->getVehiclesTypeAbbreviation(),
                'class' => 'filter-very-small',
            ],
            'country' => ['values' => $this->getCountryList()],
            'incoterm' => ['values' => $this->getListIncoterm()],
            'personAbbreviation' => [
                'values' => $this->getUserAbbreviationList(true),
                'class' => 'filter-very-small',
            ],
            'createdAt' => $dateFilter,
            'scheduledDeadline' => $dateFilter,
            'preparationApproved' => [
                'values' => $preparationApprovedStatus,
                'searchType' => '=',
                'class' => 'filter-very-small',
            ],
            'preparationWorkshop' => [
                'values' => $workshopOptions,
                'searchType' => '=',
                'class' => 'filter-very-small',
            ],
            'deliveredAt' => [
                'values' => $deliveredStatusList,
                'class' => 'width50',
                'callback' => [
                    'function' => function($row) {
                        if ('0' == $row['value']) {
                            $emptyDateTimeCondition = '(ov.delivered_at IS NULL OR ov.delivered_at = ?)';
                            $row['select']->where($emptyDateTimeCondition, BAS_Shared_Model_Vehicle::DEFAULT_EMPTY_DATE_TIME);
                        }
                        if ('1' == $row['value']) {
                            $row['select']->where('(ov.delivered_at is NOT NULL OR ov.delivered_at <> ?)', BAS_Shared_Model_Vehicle::DEFAULT_EMPTY_DATE_TIME);
                        }
                        $matches = array_filter($row['select']->getPart(Zend_Db_Select::SQL_WHERE), function ($haystack) {
                            return(strpos($haystack, 'cust.name'));
                        });

                        if ([] != $matches && 'all' == $row['value']) {
                            $emptyDateTimeCondition = '(ov.delivered_at IS NULL OR ov.delivered_at = ?)';
                            $row['select']->where($emptyDateTimeCondition, BAS_Shared_Model_Vehicle::DEFAULT_EMPTY_DATE_TIME);
                        }
                    }

                ],
                'showAllOption' => 'NO',
            ],
            'expectedArrival' => $dateFilter,
            'transportMethod' => [
                'class' => 'filter-very-small',
                'values' => $this->getTransportMethodFilterValues(),
            ],
            'chassisNumber' => ['searchType' => 'like'],
            'serviceDepotId' => [
                'searchType' => '=',
                'values' => $this->getDepots(),
                'class' => 'width80px',
            ],
            'obuStatus' => [
                'values' => $obuStatus,
                'class' => 'filter-very-small',
            ],
        ];
    }

    /**
     * Get row condition for workshop agenda grid
     *
     * @param string $highlightWorkOrderSetting
     * @param array $redBackgroundWorkOrder
     * @return array
     */
    public function getRowCondition($highlightWorkOrderSetting, $redBackgroundWorkOrder)
    {
        if (BAS_Shared_Model_DepotSetting::WORKSHOP_DEADLINE_IN_ORDER_VEHICLE_SETTING == (int)$highlightWorkOrderSetting) {
            return [
                [
                    'condition' => '"{{saleType}}" == ' . BAS_Shared_Model_OrderVehicle::ORDER_SALE_TYPE_LEASE,
                    'ifClass' => 'table_tr_yellow',
                    'elseClass' => '',
                ],
                [
                    'condition' => '"{{saleType}}" != ' . BAS_Shared_Model_OrderVehicle::ORDER_SALE_TYPE_LEASE
                        . ' && abs("{{daysToSchedule}}") > ' . SELF::MIN_DAY_DIFF
                        . ' && abs("{{daysToSchedule}}") <= ' . SELF::MAX_DAY_DIFF
                        . ' && "{{preparationWorkshop}}" < ' . BAS_Shared_Model_OrderWorkshop::WORKORDER_ARCHIVED,
                    'ifClass' => 'table_tr_red',
                    'elseClass' => '',
                ],
                [
                    'condition' => "strtotime('{{scheduledDeadline}}') < strtotime('+3 days')"
                        . ' && (!in_array(date("Y", strtotime("{{scheduledDeadline}}")), ['
                        . (int)BAS_Shared_Model_ContactCheckin::UNIX_TIME_YEAR . ', '
                        . BAS_Shared_Model_Vehicle::YEAR_9999 . ', 1111]))'
                        . ' && "{{preparationWorkshop}}" < ' . BAS_Shared_Model_OrderWorkshop::WORKORDER_ARCHIVED,
                    'ifClass' => 'table_tr_red',
                    'elseClass' => '',
                ],
            ];
        }
        if (BAS_Shared_Model_DepotSetting::START_DATE_AND_STATUS_IN_ORDER_WORKSHOP_SETTING == (int)$highlightWorkOrderSetting) {
            return [
                [
                    'condition' => 'in_array("{{orderVehicleId}}", [' . implode(", ", $redBackgroundWorkOrder) . '] )',
                    'ifClass' => 'table_tr_red',
                    'elseClass' => '',
                ]
            ];
        }
    }

    /**
     * Get transport method list for filters
     *
     * @return array $filterValues
     */
    public function getTransportMethodFilterValues()
    {
        $cache = $this->getCache();

        if (!$filterValues = $cache->load('transportMethod')) {
            $transportMethodList = $this->getTransportMethodList();
            if (!empty($transportMethodList)) {
                foreach ($transportMethodList as $transportMethod) {
                    $method = BAS_Shared_Model_TransportMethod::FILTER_LABEL . $transportMethod->id;
                    $filterValues[$method] = $method;
                }
            }
            $cache->save($filterValues, 'transportMethod');
        }

        foreach ($filterValues as $k => $v) {
            $filterValues[$k] = $this->getView()->translate($v);
        }

        return $filterValues;
    }
    
    /**
     * Get internal supplier ids
     *
     * @return array
     */
    protected function _getInterSupplierIds()
    {
        return BAS_Shared_Model_ContactTypeSupplier::getInternalSupplierContactIds();
    }

    /**
     * @param integer $depotId
     * @return array
     */
    protected function _getSupplierList($depotId = null)
    {
        if (null === $this->_supplierList) {
            $contactTypeSupplierService = $this->_getContactTypeSupplierService();
            $this->_supplierList = $contactTypeSupplierService->getSupplierListByDepotId($depotId);
        }
        
        return $this->_supplierList;
    }

    /**
     * @return Contacts_Service_TypeSupplier
     */
    protected function _getContactTypeSupplierService()
    {
        if (null === $this->_contactTypeSupplierService) {
            $this->_contactTypeSupplierService = new Contacts_Service_TypeSupplier();
        }
        
        return $this->_contactTypeSupplierService;
    }
}