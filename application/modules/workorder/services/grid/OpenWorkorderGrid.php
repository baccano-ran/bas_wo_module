<?php

/**
 * Class Workshop_Service_OpenWorkorderGrid
 */
class Workshop_Service_OpenWorkorderGrid extends Lease_Service_BaseGrid
{
    protected $_ajax = 'bas-grid-owg';

    protected function _init()
    {
        parent::_init();
        $this->setRecordsPerPage(100);
    }

    protected function _getActionsColumn()
    {
    }

    protected function _setColumns()
    {
        $this->_columns = [
            'workorder_id' => [
                'title' => 'workorder_id',
                'class' => 'js-workorder_id column-workorder_id',
            ],
            'customer' => [
                'title' => 'customer',
            ],
            'vehicle_type' => [
                'title' => 'vehicle_type',
                'decorator' => [
                    function($rowData) {
                        $type = $rowData['__value'];

                        if (!$type) {
                            return '';
                        }

                        $label = BAS_Shared_Model_OrderWorkshop::$sourceLabels[$type];
                        return $this->getView()->translate($label);
                    }
                ],
            ],
            'reference' => [
                'title' => 'reference',
            ],
        ];
    }

    protected function _setFilters()
    {
        $this->_filters = [
            'workorder_id' => [
                'searchType' => 'like',
            ],
            'reference' => [
                'searchType' => 'like',
            ],
        ];
    }
}