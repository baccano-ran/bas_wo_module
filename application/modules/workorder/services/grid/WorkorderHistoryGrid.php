<?php

/**
 * Class Workshop_Service_WorkorderHistoryGrid
 */
class Workshop_Service_WorkorderHistoryGrid extends Workshop_Service_HistoryBaseGrid
{
    protected $_ajax = 'workorder_history';

    public $depotId;

    /**
     * Workshop_Service_WorkorderHistoryGrid constructor.
     * @param Zend_Db_Select $source
     * @param null|Zend_View_Interface $view
     * @param array $actionUrls
     * @param $depotId
     */
    public function __construct(Zend_Db_Select $source, $view, array $actionUrls, $depotId)
    {
        $this->depotId = $depotId;
        parent::__construct($source, $view, $actionUrls);
    }

    protected function _setColumns()
    {
        $depotSettingService = new Management_Service_DepotSetting();
        if ($depotSettingService->depotHasExtranetWorkorderSetting($this->depotId)) {
            $workorderIdTpl = '<a href="/vbd/public/workshop/workorder/edit/id/{{workorderId}}" target="_blank" class="noAjax">{{workorderId}}</a>';
        } else {
            $workorderIdTpl = '{{workorderId}}';
        }

        $this->_columns = [
            'workorder_id' => [
                'title' => 'workorder',
                'class' => 'width-full',
                'sortAttr' => [
                    'class' => 'noAjax',
                ],
                'decorator' => [
                    'function' => function($rowData) use ($workorderIdTpl) {
                        return strtr($workorderIdTpl, [
                            '{{workorderId}}' => $rowData['__value'],
                        ]);
                    },
                ],
            ],
            'contact_id' => [
                'hidden' => true,
            ],
            'fileType' => [
                'hidden' => true,
            ],
            'fileId' => [
                'hidden' => true,
            ],
            'fileToken' => [
                'hidden' => true,
            ],
            'vehicle' => [
                'title' => 'vehicle',
                'sortAttr' => [
                    'class' => 'noAjax',
                ],
            ],
            'total' => [
                'title' => 'total',
                'sortAttr' => [
                    'class' => 'noAjax',
                ],
                'decorator' => [
                    'function' => function($rowData) {
                        return '&euro;&nbsp;' . $this->getView()->normalizedToLocalized($rowData['total']);
                    },
                ]
            ],
            'orderDate' => [
                'title' => 'oc_order_date',
                'sortAttr' => [
                    'class' => 'noAjax',
                ],
                'decorator' => $this->_getDateDecorator(),
            ],
            'fileName' => [
                'hidden' => true,
            ],
            'invoice' => [
                'title' => 'invoice',
                'sortAttr' => [
                    'class' => 'noAjax',
                ],
                'class' => 'fileLink',
                'decorator' => [
                    'function' => function ($rowData) {
                        $adapter = new Order_Library_DocumentWidget_OrderWorkshopAdapter($this->depotId);
                        return isset($rowData['invoice']) ? $this->getView()->partial('document-widget/_partial/document-link.phtml', 'order', [
                            'adapter'    => $adapter,
                            'rowData'    => $rowData,
                            'documentId' => $rowData['fileId'],
                            'fileName' => $rowData['fileName'],
                            'fileToken' => $rowData['fileToken'],
                            'documentType' => $rowData['fileType'],
                            'orderId' => $rowData['workorder_id'],
                            'documentTitle' => $rowData['invoice'],
                            'sourceType' => Order_Library_DocumentWidget_OrderWorkshopAdapter::SOURCE_TYPE,
                        ]) : '';
                    },
                ],
            ],
            'invoiceDate' => [
                'title' => 'invoice_date',
                'sortAttr' => [
                    'class' => 'noAjax',
                ],
                'decorator' => $this->_getDateDecorator(),
            ],
            'status' => [
                'title' => 'status',
                'sortAttr' => [
                    'class' => 'noAjax',
                ],
                'decorator' => [
                    'function' => function($rowData) {
                        return $this->getView()->translate(BAS_Shared_Model_OrderWorkshop::$statusLabel[$rowData['status']]);
                    },
                ]
            ],
        ];
    }

    protected function _setFilters()
    {
        $onChangeFilterJsFunction = '_' . $this->_ajax . 'gridChangeFilters(1);';
        $this->_filters = [
            'workorder_id' => [
                'searchType' => '=',
                'class' => 'width50',
            ],
            'vehicle' => [
                'distinct' => [
                    'field' => 'vehicle',
                    'name' => 'vehicle',
                ],
                'class' => 'width75',
            ],
            'orderDate' => [
                'searchType' => '=',
                'attributes' => [
                    'onchange' => $onChangeFilterJsFunction,
                ],
                'class' => 'js-date_picker datepicker-input-bg width75',
            ],
            'invoice' => [
                'searchType' => '=',
                'class' => 'width75',
            ],
            'invoiceDate' => [
                'searchType' => '=',
                'attributes' => [
                    'onchange' => $onChangeFilterJsFunction,
                ],
                'class' => 'js-date_picker datepicker-input-bg width75',
            ],
            'status' => [
                'searchType' => 'sqlExpr',
                'searchSqlExp' => 'COALESCE(ow.status, \'__OFFER__\')={{value}}',
                'values' => $this->_getWorkshopStatusOptions(),
                'attributes' => [
                    'onchange' => $onChangeFilterJsFunction,
                ],
                'class' => 'width75',
            ],
        ];
    }


}