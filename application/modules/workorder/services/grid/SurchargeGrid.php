<?php

/**
 * Class Workshop_Service_SurchargeGrid
 */
class Workshop_Service_SurchargeGrid extends BAS_Shared_Service_Abstract
{

    /**
     * @var Zend_View_Interface
     */
    protected $_view;

    /**
     * @param Zend_View_Interface $view
     */
    public function __construct(Zend_View_Interface $view)
    {
        $this->_view = $view;
    }

    /**
     * @param Zend_Db_Select $source
     * @return Bvb_Grid
     * @throws Bvb_Grid_Exception
     * @throws Zend_Exception
     */
    public function getGrid(Zend_Db_Select $source)
    {
        $columns = [
            'id' => ['title' => 'Id'],
            'type' => [
                'decorator' => [
                    'function' => function ($rawData) {
                        switch ($rawData['type']) {
                            case BAS_Shared_Model_Workshop_WorkshopSurcharge::TYPE_CONSUMABLES: return $this->_view->translate('consumables');
                            case BAS_Shared_Model_Workshop_WorkshopSurcharge::TYPE_ENVIRONMENT: return $this->_view->translate('environment');
                            default: return '';
                        }
                    },
                ],
                'title' => 'type',
            ],
            'rate' => ['decorator' => [
                'function' => function ($rawData) {
                    return number_format($rawData['rate'], 4, ',', '.') . ' %';
                }
            ]],
            'calculation_type' => ['decorator' => [
                'function' => function ($rawData) {
                    switch ($rawData['calculation_type']) {
                        case BAS_Shared_Model_Workshop_WorkshopSurcharge::CALCULATION_TYPE_OVER_ITEMS: return $this->_view->translate('over_items');
                        case BAS_Shared_Model_Workshop_WorkshopSurcharge::CALCULATION_TYPE_OVER_TASKS: return $this->_view->translate('over_tasks');
                        case BAS_Shared_Model_Workshop_WorkshopSurcharge::CALCULATION_TYPE_OVER_TASKS_AND_ITEMS: return $this->_view->translate('over_tasks_and_items');
                        default: return '';
                    }
                }
            ]],
            'max_amount' => [],
            'end_date' => [
                'title' => 'label_end_date',
                'decorator' => [
                    'function' => function ($rawData) {
                        $date = DateTime::createFromFormat('Y-m-d H:i:s', $rawData['end_date']);
                        return $date === false ? '' : $date->format('d-m-Y');
                    }
                ],
            ],
        ];

        $filters = [];

        $gridConfig = Zend_Registry::get('gridConfig');
        $grid = Bvb_Grid::factory('Table', $gridConfig);
        $grid->setAjax(true);
        $grid->setShowOrderImages(false);
        $grid->setRecordsPerPage(20);
        $grid->setPaginationInterval([20 => 20, 50 => 50, 100 => 100]);

        $basGrid = new Bvb_BasGrid();
        $basGrid->setGrid($grid);
        $basGrid->setSource($grid, 0, $source);
        $basGrid->setFilters($grid, [], $filters, true);
        $basGrid->setColumns($grid, $columns);

        $grid->deploy();
        return $grid;
    }

}
