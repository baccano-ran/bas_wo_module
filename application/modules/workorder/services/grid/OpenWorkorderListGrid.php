<?php

class Workshop_Service_OpenWorkorderListGrid extends Workshop_Service_BaseVehicleStockTable
{
    /** @var \Order_Service_OrderWorkshop */
    protected $_service;

    /** @var Bvb_BasGrid */
    protected $_grid;

    protected $_view;
    protected $_config;

    /** @var  int $_depotId */
    protected $_depotId;

    /**
     * ordered columns in grid
     *
     * @var array
     */
    protected $_columnOrderInGrid = array(
        'workorderId',
        'contactTypeSupplierFeeContactId',
        'workorderDepotId',
        'workorderType',
        'bookingCode',
        'contactName',
        'debtorNumber',
        'userId',
        'createdAt',
        'updatedAt',
        'spendTime',
        'lastDateTimeSpend',
        'truckId',
        'licencePlate',
        'startDate',
        'deadlineDate',
        'totalAmount',
        'workshopStatus'
    );

    public function __construct(Order_Service_OrderWorkshop $service, $view)
    {
        parent::__construct();

        $this->setService($service);
        $this->_view = $view;
        $this->_config = Zend_Registry::get('config');
    }

    /**
     * @param \Order_Service_OrderWorkshop $service
     */
    public function setService($service)
    {
        $this->_service = $service;
    }

    /**
     * @return \Order_Service_OrderWorkshop
     */
    public function getService()
    {
        return $this->_service;
    }

    /**
     * @return mixed
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * @param int $depotId
     */
    public function setDepotId($depotId)
    {
        $this->_depotId = $depotId;
    }

    /**
     * @return int
     */
    public function getDepotId()
    {
        return $this->_depotId;
    }

    public function getWorkshopGridOptions()
    {
        $orderWorkshopService = new Order_Service_OrderWorkshop();
        $options = [
            'sourceObject' => $orderWorkshopService->getWorkOrdersForOpenWorkorder($this->getDepotId()),
            'columns' => [
                'vehicleId' => ['hidden' => true],
                'contactTypeSupplierFeeContactId' => ['hidden' => true],
                'workorderId' => ['title' => 'Workorder', 'order' => true, 'decorator' => $this->getWorkorderIdDecorator()],
                'workorderDepotId' => ['title' => 'depot'],
                'contactName' => ['title' => 'customer', 'order' => true],
                'debtorNumber' => ['title' => 'debtor_number', 'class' => 'filter-medium-number'],
                'userId' => [
                    'title' => 'receptionist',
                    'decorator' => ['function' => function($rowData) {
                        return $this->getView()->userAbbreviation($rowData['userId']);
                    }]
                ],
                'createdAt' => [
                    'title' => 'created_at',
                    'searchType'   => 'sqlExp',
                    'searchSqlExp' => "DATE_FORMAT(ow.created_at, '%d-%m-%Y') = '{{value}}'",
                    'decorator' => ['function' => function ($rowData) {
                        return (isset($rowData['createdAt']) ? (new Default_Formatter_Date)->format($rowData['createdAt']): '');
                    }],
                ],
                'updatedAt' => [
                    'title' => 'updated_at',
                    'searchType'   => 'sqlExp',
                    'searchSqlExp' => "DATE_FORMAT(ow.updated_at, '%d-%m-%Y') = '{{value}}'",
                    'decorator' => ['function' => function ($rowData) {
                        return (isset($rowData['updatedAt']) ? (new Default_Formatter_Date)->format($rowData['updatedAt']): '');
                    }],
                ],
                'bookingCode' => ['title' => 'booking_code'],
                'spendTime' => ['title' => 'spend_time'],
                'lastDateTimeSpend' => [
                    'title' => 'last_date_time_spend',
                    'searchType'   => 'sqlExp',
                    'searchSqlExp' => "DATE_FORMAT(lastDateTimeSpend, '%d-%m-%Y') = '{{value}}'",
                    'decorator' => ['function' => function ($rowData) {
                        return (isset($rowData['lastDateTimeSpend']) ? (new Default_Formatter_Date)->format($rowData['lastDateTimeSpend']): '');
                    }],
                ],
                'truckId' => ['title' => 'truck_id'],
                'licencePlate' => ['title' => 'licence_plate'],
                'workorderType' => [
                    'title' => 'workorder_type',
                    'decorator' => ['function' => function ($rowData) {
                        return $this->getView()->translate(BAS_Shared_Model_OrderWorkshop::$typeLabel[$rowData['workorderType']]);
                    }],
                ],
                'workshopStatus' => [
                    'format' => 'workshopStatus',
                    'class' => 'filter-small-number',
                    'searchType' => 'sqlExp',
                    'searchSqlExp' => 'COALESCE(ow.status, \'__OFFER__\')={{value}}',
                ],
                'totalAmount' => [
                    'title' => 'total_amount',
                    'decorator' => ['function' => function ($rowData) {
                        $filter = new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => 2]);
                        return $filter->filter($rowData['totalAmount']);
                    }],
                ],
                'startDate' => [
                    'title' => 'start',
                    'decorator' => ['function' => function ($rowData) {
                        return (isset($rowData['startDate']) ? (new Default_Formatter_Date)->format($rowData['startDate']): '');
                    }],
                ],
                'deadlineDate' => [
                    'title' => 'deadline',
                    'decorator' => ['function' => function ($rowData) {
                        return (isset($rowData['deadlineDate']) ? (new Default_Formatter_Date)->format($rowData['deadlineDate']): '');
                    }],
                ],
            ],
            'filters' => $this->getGridFilters(),
            'setKeyEventsOnFilters' => true,
            'recordPerPage' => '200',
            'setShowOrderImage' => false,
            'setAjax' => true,
            'paginationInterval' => ['200' => '200', '400' => '400', '600' => '600'],
        ];

        $options = $this->orderColumns($options);

        return $options;
    }

    /**
     * @return Bvb_Grid
     */
    public function getGrid()
    {
        if ($this->_grid) {
            return $this->_grid;
        }

        $gridConfig = Zend_Registry::get('gridConfig');

        $this->_grid = new Bvb_BasGrid();

        // lets get our own grid object so we can modify it later
        $this->_grid->setGrid(Bvb_Grid::factory('Table', $gridConfig, ''));
        $this->_grid->getGrid()->addFormatterDir('application/modules/workshop/formatter', 'Workshop_Formatter');
        $this->_grid->getGrid()->addFiltersRenderDir('application/modules/workshop/filter', 'Workshop_Filter');
        $this->_grid->getGrid()->addExtraColumns($this->_getActionColumn());

        return $this->_grid->getGridObject($this->getWorkshopGridOptions(), $gridConfig, $this->getRowConditions());
    }

    /**
     * row condition
     *
     * @return array
     */
    protected function getRowConditions()
    {
        $currentDate = date('d-m-Y');
        return [
            [
                'condition' => "'$currentDate' === date( 'd-m-Y', strtotime('{{deadlineDate}}') )",
                'ifClass' => 'defect-row-orange',
                'elseClass' => '',
            ],
            [
                'condition' => "'{{deadlineDate}}' != '' && strtotime('$currentDate') > strtotime( date( 'd-m-Y', strtotime('{{deadlineDate}}') ) )",
                'ifClass' => 'defect-row-red',
                'elseClass' => '',
            ],
        ];
    }

    /**
     * sets column ordering
     *
     * @param $options
     * @return mixed
     */
    protected function orderColumns($options)
    {
        for ($i = 0; $i < count($this->_columnOrderInGrid); $i++) {
            $column = $this->_columnOrderInGrid[$i];
            $options['columns'][$column]['position'] = $i + 1;
        }

        return $options;
    }

    protected function _getActionColumn()
    {
        $column = new Bvb_Grid_Extra_Column();
        $column
            ->position('left')
            ->name('actions')
            ->title('')
            ->class('widthColumn')
            ->decorator(['function' => function($rowData) {
                return $this->getView()->partial('workorder/partial/openWorkorderActionColumn.phtml', [
                    'rowData' => $rowData,
                ]);
            }])
        ;

        return  $column;
    }

    protected function getGridFilters()
    {       return [
            'workorderId' => ['searchType' => '=', 'class' => 'filter-medium-number'],
            'workorderType' => [
                'values' => $this->getWorkorderTypeOptions(),
            ],
            'bookingCode' => ['searchType' => 'like', 'class' => 'filter-medium-number'],
            'workorderDepotId' => ['searchType' => 'like', 'class' => 'filter-medium-number'],
            'debtorNumber' => ['searchType' => '=', 'class' => 'filter-medium-number'],
            'truckId' => ['searchType' => '='],
            'contactName' => ['searchType' => 'like', 'class' => 'filter-medium-number'],
            'licencePlate' => ['searchType' => 'like', 'class' => 'filter-medium-number'],
            'createdAt' => ['render' => 'dateFilter', 'class' => 'date-filter'],
            'updatedAt' => ['render' => 'dateFilter', 'class' => 'date-filter'],
            'lastDateTimeSpend' => ['render' => 'dateFilter', 'class' => 'date-filter'],
            'workshopStatus' => [
                'values' => $this->getWorkshopStatusDropDown(),
            ],
        ];
    }

    protected function getStatusDecorator()
    {
        return '<div data-id="{{workorderId}}" class="status">{{workshopStatus}}</div>';
    }

    protected function getWorkorderIdDecorator()
    {
        $depotSettingService = new Management_Service_DepotSetting();
        if ($depotSettingService->depotHasExtranetWorkorderSetting($this->getDepotId())) {
            $workorderIdTpl = '<a href="/vbd/public/workshop/workorder/edit/id/{{workorderId}}" title="Open Workorder">{{workorderId}}</a>';
        } else {
            $workorderIdTpl = '{{workorderId}}';
        }

        return sprintf('<div data-id="{{workorderId}}" class="filter-medium-number">%s</div>', $workorderIdTpl);
    }

    protected function getWorkshopStatusDropDown()
    {
        return [
            '__OFFER__' => $this->_view->translate(BAS_Shared_Model_OrderWorkshop::LABEL_WORKORDER_OFFER),
            BAS_Shared_Model_OrderWorkshop::WORKORDER_START => $this->_view->translate(BAS_Shared_Model_OrderWorkshop::LABEL_WORKORDER_START),
            BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED => $this->_view->translate(BAS_Shared_Model_OrderWorkshop::LABEL_WORKORDER_PRINTED),
            BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED => $this->_view->translate(BAS_Shared_Model_OrderWorkshop::LABEL_WORKORDER_CLOSED),
        ];
    }

    protected function getWorkorderTypeOptions()
    {
        return array_map(function($label) {
            return $this->getView()->translate($label);
        }, BAS_Shared_Model_OrderWorkshop::$typeLabel);
    }
}
