<?php

/**
 * Class Workshop_Service_WorkorderTime
 */
class Workshop_Service_WorkorderTime extends BAS_Shared_Service_Abstract
{

    /**
     * @param string $xmlFilename
     * @return bool
     */
    public function isTimexXmlAlreadyProcessed($xmlFilename)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopTimeMapper $mapper */
        $mapper = $this->getMapper('Workshop_OrderWorkshopTime');

        return $mapper->isTimexXmlAlreadyProcessed($xmlFilename);
    }

    /**
     * @param int $workorderId
     * @return float
     */
    public function getWorkorderSpentTime($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopTimeMapper $mapper */
        $mapper = $this->getMapper('Workshop_OrderWorkshopTime');

        return $mapper->getWorkorderSpentTime($workorderId);
    }

    /**
     * @param int $workorderId
     * @return Zend_Db_Select
     */
    public function getTimeTrackGridSource($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopTimeMapper $mapper */
        $mapper = $this->getMapper('Workshop_OrderWorkshopTime');

        return $mapper->getTimeTrackGridSource($workorderId);
    }

    /**
     * @param int $orderWorkshopTimeId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopTime
     */
    public function getById($orderWorkshopTimeId)
    {
        if (0 === (int)$orderWorkshopTimeId) {
            throw new BAS_Shared_InvalidArgumentException('Invalid orderWorkshopTimeId');
        }

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopTimeMapper $mapper */
        $mapper = $this->getMapper('Workshop_OrderWorkshopTime');

        return $mapper->findOneByCondition(['id = ?' => (int)$orderWorkshopTimeId]);
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopTime $workorderTime
     * @param string $type
     * @return BAS_Shared_Model_Workshop_OrderWorkshopTime
     */
    public function saveModel(BAS_Shared_Model_Workshop_OrderWorkshopTime $workorderTime, $type = BAS_Shared_Model_AbstractMapper::SAVE_TYPE_AUTO)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopTimeMapper $mapper */
        $mapper = $this->getMapper('Workshop_OrderWorkshopTime');

        return $mapper->saveModel($workorderTime, $type);
    }

    /**
     * @param int $workorderId
     * @return bool
     */
    public function isTimeSpentAttachedToWorkorder($workorderId)
    {
        $workorderId = (int)$workorderId;
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopTimeMapper $mapper */
        $mapper = $this->getMapper('Workshop_OrderWorkshopTime');

        try {
            $workorderTime = $mapper->findOneByCondition([
                $mapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
                $mapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopTime::NOT_ARCHIVED,
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            $workorderTime = null;
        }

        return $workorderTime !== null;
    }
}