<?php

/**
 * Class Workshop_Service_TimexMailLogger
 */
class Workshop_Service_TimexMailLogger extends BAS_Shared_Service_Abstract
{
    const TYPE_GENERAL = 'general';
    const TYPE_WORKORDER_ID_UNKNOWN = 'err_workorder_id';
    const TYPE_EMPLOYEE_NUMBER_UNKNOWN = 'err_employee_nr';

    /** @var array */
    private $messages = [];

    /**
     * @param string $message
     * @param int $type
     */
    public function addMessage($message, $type = self::TYPE_GENERAL)
    {
        $this->messages[$type] = isset($this->messages[$type]) ? $this->messages[$type]: [];
        $this->messages[$type][] = $message;

        return $this;
    }

    /**
     * @return $this
     */
    public function flushMessages()
    {
        $this->setMessages([]);

        return $this;
    }

    /**
     * @return bool
     */
    public function hasMessages()
    {
        return [] !== $this->getMessages();
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @param array $messages
     * @return Workshop_Service_TimexMailLogger
     */
    public function setMessages($messages)
    {
        $this->messages = $messages;

        return $this;
    }

}