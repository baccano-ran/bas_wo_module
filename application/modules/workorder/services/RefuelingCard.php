<?php

/**
 * Class Workshop_Service_Refueling
 */
class Workshop_Service_RefuelingCard extends BAS_Shared_Service_Abstract
{
    /**
     * @param int $contactVehicleId
     * @return BAS_Shared_Model_Workshop_WorkshopRefuelingCard
     */
    public function getCardByContactVehicleId($contactVehicleId)
    {
        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingCardMapper $refuelingCardMapper */
        $refuelingCardMapper = $this->getMapper('Workshop_WorkshopRefuelingCard');
        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingCard $cardModel */
        $cardModel = $refuelingCardMapper->findOneByCondition(
            ['contact_vehicle_id = ?' => (int)$contactVehicleId]
        );

        return $cardModel;
    }
}