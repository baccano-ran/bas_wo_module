<?php

/**
 * Class Workshop_Service_Surcharge
 */
class Workshop_Service_Surcharge extends BAS_Shared_Service_Abstract
{

    /**
     * @param int $depotId
     * @return Zend_Db_Select
     */
    public function getGridSource($depotId)
    {
        /** @var BAS_Shared_Model_Workshop_WorkshopSurchargeMapper $mapper */
        $mapper = $this->getMapper('Workshop_WorkshopSurcharge');

        return $mapper->getGridSource($depotId);
    }

}