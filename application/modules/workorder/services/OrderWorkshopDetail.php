<?php

/**
 * Class Workshop_Service_OrderWorkshopDetail
 */
class Workshop_Service_OrderWorkshopDetail extends BAS_Shared_Service_Abstract
{

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail
     * @param $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail
     */
    public function saveModel(BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail, $userId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        if ($detail->getId()) {
            $detail->setUpdatedBy($userId);
            $detail->setUpdatedAt($this->getCurrentDate());
        } else {
            $detail->setCreatedBy($userId);
            $detail->setCreatedAt($this->getCurrentDate());
        }

        return $orderWorkshopDetailMapper->saveModel($detail);
    }

    /**
     * @param int $workorderId
     * @return float
     */
    public function getWorkorderInvoicedTime($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');
        return $orderWorkshopDetailMapper->getWorkorderInvoicedTime($workorderId);
    }

    /**
     * @param int $workorderId
     * @return float
     */
    public function getWorkorderSpentTime($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');
        return $orderWorkshopDetailMapper->getWorkorderSpentTime($workorderId);
    }

    /**
     * @param int $detailId
     * @param string $spentTime
     * @param int $userId
     * @return bool|int
     */
    public function saveSpentTimeByDetailId($detailId, $spentTime, $userId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $normalizedFilter = new Zend_Filter_LocalizedToNormalized([
            'locale' => 'nl_NL',
            'precision' => 2,
        ]);

        $spentTime = (float) $normalizedFilter->filter($spentTime);

        return $orderWorkshopDetailMapper->updateFieldsByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('spent_time') => $spentTime,
            $orderWorkshopDetailMapper->mapFieldToDb('updatedBy') => (int) $userId,
            $orderWorkshopDetailMapper->mapFieldToDb('updatedAt') => date('Y-m-d H:i:s'),
        ], [
            $orderWorkshopDetailMapper->mapFieldToDb('id') . ' = ?' => (int) $detailId,
        ]);
    }

    /**
     * @param int $detailId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail
     * @throws BAS_Shared_NotFoundException
     */
    public function findById($detailId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->findOneByCondition([
            'id = ?' => (int)$detailId,
        ]);
    }

    /**
     * @param int $workorderId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail[]
     * @throws BAS_Shared_NotFoundException
     */
    public function findByWorkorderId($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->findAllByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
            $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
        ]);
    }

    /**
     * @param int $workorderId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getWorkorderItemsToPick($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->getWorkorderItemsToPick($workorderId);
    }

    /**
     * @param int $workorderId
     * @param int $type
     * @param bool $orderByReturnDetails
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail[]
     */
    public function findByWorkorderIdAndType($workorderId, $type, $orderByReturnDetails = false)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $orderBySign = $orderByReturnDetails ? '<' : '>';
        $orderBy = new Zend_Db_Expr($orderWorkshopDetailMapper->mapFieldToDb('quantity') . $orderBySign . '0 DESC');

        return $orderWorkshopDetailMapper->findAllByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
            $orderWorkshopDetailMapper->mapFieldToDb('type') . ' = ?' => (int)$type,
            $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
        ], $orderBy);
    }

    /**
     * @param int $workorderId
     * @param int $status
     * @param int $userId
     * @param int|null $fromStatus
     * @return int
     */
    public function changeWorkorderItemsStatus($workorderId, $status, $userId, $fromStatus = null)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $where = [
            $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
            $orderWorkshopDetailMapper->mapFieldToDb('type') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM,
            $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
        ];

        if ($fromStatus !== null) {
            $where[$orderWorkshopDetailMapper->mapFieldToDb('itemPicked') . ' = ?'] = (int)$fromStatus;
        }

        return $orderWorkshopDetailMapper->updateFieldsByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('itemPicked') => $status,
            $orderWorkshopDetailMapper->mapFieldToDb('updatedBy') => $userId,
            $orderWorkshopDetailMapper->mapFieldToDb('updatedAt') => date('Y-m-d H:i:s'),
        ], $where);
    }

    /**
     * @param int $detailId
     * @param int $userId
     * @return bool|int
     */
    public function delete($detailId, $userId)
    {
        $detailId = (int)$detailId;
        $userId = (int)$userId;

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->updateFieldsByCondition(
            [
                $orderWorkshopDetailMapper->mapFieldToDb('archived') => BAS_Shared_Model_Workshop_OrderWorkshopDetail::ARCHIVED,
                $orderWorkshopDetailMapper->mapFieldToDb('updatedAt') => $this->getCurrentDate(),
                $orderWorkshopDetailMapper->mapFieldToDb('updatedBy') => $userId,
            ],
            [implode(' OR ', [
                $orderWorkshopDetailMapper->mapFieldToDb('id') . '=' . $detailId,
                $orderWorkshopDetailMapper->mapFieldToDb('mainOrderWorkshopDetailId') . '=' . $detailId,
            ])]
        );
    }

    /**
     * @param int $workorderId
     * @param array $detailsData
     * @param array $tradeInPrices
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail[]
     * @throws Exception
     */
    public function addDetails($workorderId, array $detailsData, array $tradeInPrices, $userId)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        $newWorkorderDetails = [];
        $db = $this->getDb();

        try {
            $db->beginTransaction();

            foreach ($detailsData as $detailData) {

                $referenceId = (int)$detailData['id'];
                $referenceType = (int)$detailData['type'];
                $quantity = empty($detailData['quantity']) ? null : (int)$detailData['quantity'];
                $tradeIn = array_key_exists('tradeIn', $detailData) && is_numeric($detailData['tradeIn']) ? (int)$detailData['tradeIn'] : null;
                $detailDepotId = $referenceType === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM ? (int)$detailData['depotId'] : 0;

                $addDetailRequest = new BAS_Shared_Model_Workshop_AddDetailRequest([
                    'workorderId' => $workorderId,
                    'referenceId' => $referenceId,
                    'referenceType' => $referenceType,
                    'detailDepotId' => $detailDepotId,
                    'tradeIn' => $tradeIn,
                    'quantity' => $quantity,
                    'tradeInPrices' => $tradeInPrices,
                ]);

                $newWorkorderDetails[] = $this->addDetail($addDetailRequest, $userId);
            }

            $workorderService->updateWorkorderPricesAndDiscounts($workorderId, $userId);

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        return $newWorkorderDetails;
    }

    /**
     * @param BAS_Shared_Model_Workshop_AddDetailRequest $addDetailRequest
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail
     * @throws BAS_Shared_Exception
     */
    public function addDetail(BAS_Shared_Model_Workshop_AddDetailRequest $addDetailRequest, $userId)
    {
        /** @var Order_Service_Product $productService */
        $productService = $this->getService('Order_Service_Product');
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');
        /** @var Warehouse_Service_ItemStock $itemStockService */
        $itemStockService = $this->getService('Warehouse_Service_ItemStock');
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $workorderId = $addDetailRequest->getWorkorderId();
        $referenceId = $addDetailRequest->getReferenceId();
        $referenceType = $addDetailRequest->getReferenceType();
        $mainDetail = $addDetailRequest->getMainDetail();

        $workorder = $workorderService->findById($workorderId);

        if ($workorderService->isWorkorderReadonly($workorder, $userId)) {
            throw new BAS_Shared_Exception('Cannot add detail due to workorder status: ' . $workorder->getStatusLabel());
        }

        $workorderDetail = $referenceType === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM
            ? $this->createItemDetail($addDetailRequest, $workorder)
            : $this->createTaskDetail($addDetailRequest, $workorder);

        $depotBookingCodeId = $workorder->getDefaultDepotBookingCodeId() === null
            ? BAS_Shared_Model_Workshop_OrderWorkshopDetail::EMPTY_BOOKING_CODE
            : $workorder->getDefaultDepotBookingCodeId();

        $workorderDetail
            ->setOrderWorkshopId($workorder->getWorkorderId())
            ->setReferenceId($referenceId)
            ->setType($referenceType)
            ->setOnInvoice(BAS_Shared_Model_Workshop_OrderWorkshopDetail::ON_INVOICE_YES)
            ->setSpentTime($workorderDetail->getInvoiceTime())
            ->setDepotBookingCodeId($depotBookingCodeId)
            ->setMainOrderWorkshopDetailId($mainDetail === null ? null : $mainDetail->getId())
            ->setArchived(BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->setCreatedBy($userId)
            ->setCreatedAt($this->getCurrentDate());

        if ($mainDetail !== null && $mainDetail->isTask()) {
            $mainProduct = $productService->findByIdAndDepotId($mainDetail->getReferenceId(), $mainDetail->getDepotId());

            $workorderDetail
                ->setSequence(0)
                ->setDepotBookingCodeId($mainProduct->getCompositionInternalBookingCodeId());

            if ($mainProduct->getCompositionExplode() === BAS_Shared_Model_Product::COMPOSITION_EXPLODE_NO) {
                $workorderDetail->setOnInvoice(BAS_Shared_Model_Workshop_OrderWorkshopDetail::ON_INVOICE_NO);
            }
        } else {
            $lastSequenceNumber = $this->getLastSequenceNumber($workorder->getWorkorderId());
            $workorderDetail->setSequence($lastSequenceNumber + 1);
        }

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetail $workorderDetail */
        $workorderDetail = $orderWorkshopDetailMapper->saveModel($workorderDetail);

        if ($workorderDetail->isItem()) {
            $itemStockService->updateQuantityByWorkorderDetailId(
                $workorderDetail->getId(),
                $workorderDetail->getQuantity(),
                $userId
            );
        }

        if ($mainDetail === null) { // prevent recursive composition relations
            $this->addComposedDetails($workorderDetail, $addDetailRequest, $userId);
        }

        return $workorderDetail;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $mainDetail
     * @param BAS_Shared_Model_Workshop_AddDetailRequest $addMainDetailRequest
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail[]
     * @throws BAS_Shared_Exception
     */
    public function addComposedDetails(
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $mainDetail,
        BAS_Shared_Model_Workshop_AddDetailRequest $addMainDetailRequest,
        $userId
    ) {
        if ($mainDetail->isItem()) {
            $composedDetails = $this->addRequiredItems($mainDetail, $addMainDetailRequest, $userId);
        } elseif ($mainDetail->isTask()) {
            $composedDetails = $this->addProductComposition($mainDetail, $addMainDetailRequest, $userId);
        } else {
            throw new BAS_Shared_InvalidArgumentException('Invalid detail type');
        }

        return $composedDetails;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $mainDetail
     * @param BAS_Shared_Model_Workshop_AddDetailRequest $addMainDetailRequest
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail[]
     * @throws BAS_Shared_Exception
     */
    private function addRequiredItems(
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $mainDetail,
        BAS_Shared_Model_Workshop_AddDetailRequest $addMainDetailRequest,
        $userId
    ) {
        /** @var Warehouse_Service_ItemRequired $itemRequiredService */
        $itemRequiredService = $this->getService('Warehouse_Service_ItemRequired');

        $requiredItems = $itemRequiredService->findByMainItemId($mainDetail->getReferenceId());
        $subDetails = [];
        $quantity = $mainDetail->getQuantity();

        if ($mainDetail->getMainOrderWorkshopDetailId() > 0) {
            // if the main item is a sub detail of a composition we add required items directly to the composed task detail
            $mainDetail = $addMainDetailRequest->getMainDetail();
        }

        foreach ($requiredItems as $requiredItem) {
            $addDetailRequest = new BAS_Shared_Model_Workshop_AddDetailRequest([
                'workorderId' => $mainDetail->getOrderWorkshopId(),
                'referenceId' => $requiredItem->getRequiredItemId(),
                'referenceType' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM,
                'detailDepotId' => $mainDetail->getDepotId(),
                'tradeIn' => $addMainDetailRequest->getTradeIn(),
                'mainDetail' => $mainDetail,
                'tradeInPrices' => $addMainDetailRequest->getTradeInPrices(),
                'quantity' => $quantity,
            ]);

            $subDetails[] = $this->addDetail($addDetailRequest, $userId);
        }

        return $subDetails;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $mainDetail
     * @param BAS_Shared_Model_Workshop_AddDetailRequest $addMainDetailRequest
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail[]
     * @throws BAS_Shared_Exception
     */
    private function addProductComposition(
        BAS_Shared_Model_Workshop_OrderWorkshopDetail $mainDetail,
        BAS_Shared_Model_Workshop_AddDetailRequest $addMainDetailRequest,
        $userId
    ) {
        /** @var Order_Service_Product $productService */
        $productService = $this->getService('Order_Service_Product');
        /** @var Order_Service_ProductComposition $productCompositionService */
        $productCompositionService = $this->getService('Order_Service_ProductComposition');

        $product = $productService->findByIdAndDepotId($mainDetail->getReferenceId(), $mainDetail->getDepotId());

        if ($product->getWorkshopTaskType() !== BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_COMPOSED) {
            return [];
        }

        $subDetails = [];
        $productCompositions = $productCompositionService->findByProductId($product->getId());

        foreach ($productCompositions as $productComposition) {
            $addDetailRequest = new BAS_Shared_Model_Workshop_AddDetailRequest([
                'workorderId' => $mainDetail->getOrderWorkshopId(),
                'referenceId' => $productComposition->getReferenceId(),
                'referenceType' => $productComposition->getType(),
                'detailDepotId' => $productComposition->getDepotId() > 0
                    ? (int)$productComposition->getDepotId()
                    : $mainDetail->getDepotId(),
                'tradeIn' => $addMainDetailRequest->getTradeIn(),
                'mainDetail' => $mainDetail,
                'tradeInPrices' => $addMainDetailRequest->getTradeInPrices(),
                'quantity' => BAS_Shared_Utils_Math::mul(
                    $productComposition->getQuantity(),
                    $mainDetail->getQuantity(),
                    BAS_Shared_Utils_Math::SCALE_2
                ),
            ]);

            $subDetail = $this->addDetail($addDetailRequest, $userId);
            $subDetails[] = $subDetail;

            if ($subDetail->isItem()) {
                $subDetails = array_merge($subDetails, $this->addRequiredItems($subDetail, $addDetailRequest, $userId));
            }
        }

        $this->updateActualPriceOfComposedTask($mainDetail->getId());

        return $subDetails;
    }

    /**
     * @param BAS_Shared_Model_Workshop_AddDetailRequest $addDetailRequest
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail
     * @throws BAS_Shared_Exception
     * @throws BAS_Shared_NotFoundException
     */
    private function createItemDetail(
        BAS_Shared_Model_Workshop_AddDetailRequest $addDetailRequest,
        BAS_Shared_Model_OrderWorkshop $workorder
    ) {
        /** @var Warehouse_Service_Item $itemService */
        $itemService = $this->getService('Warehouse_Service_Item');
        /** @var Warehouse_Service_ItemName $itemNameService */
        $itemNameService = $this->getService('Warehouse_Service_ItemName');
        /** @var Warehouse_Service_ItemPrice $itemPriceService */
        $itemPriceService = $this->getService('Warehouse_Service_ItemPrice');
        /** @var Warehouse_Service_ItemDepot $itemDepotService */
        $itemDepotService = $this->getService('Warehouse_Service_ItemDepot');
        /** @var Management_Service_DepotSetting $depotSettingService */
        $depotSettingService = $this->getService('Management_Service_DepotSetting');
        /** @var Workshop_Service_Refueling $refuelingService */
        $refuelingService = $this->getService('Workshop_Service_Refueling');

        $itemId = $addDetailRequest->getReferenceId();
        $depotId = $addDetailRequest->getDetailDepotId() > 0
            ? (int)$addDetailRequest->getDetailDepotId()
            : $workorder->getDepotId();

        $tradeIn = (int)$addDetailRequest->getTradeIn();

        $defaultItemDeliveredStatus = $depotSettingService->getDefaultItemDeliveredStatus($workorder->getDepotId());

        if ($defaultItemDeliveredStatus) {
            $itemPicked = BAS_Shared_Model_Workshop_OrderWorkshopDetail::ITEM_PICKED_DELIVERED;
        } else {
            $itemPicked = $workorder->getStatus() === BAS_Shared_Model_OrderWorkshop::WORKORDER_OFFER
                ? BAS_Shared_Model_Workshop_OrderWorkshopDetail::ITEM_PICKED_OFFER
                : BAS_Shared_Model_Workshop_OrderWorkshopDetail::ITEM_PICKED_ORDER;
        }

        $item = $itemService->findById($itemId);
        $itemName = $itemNameService->findById($item->getItemNameId())->getName();
        $itemPrice = $itemPriceService->getLastItemPrice($itemId, $depotId);
        $costPrice = $itemPriceService->getItemCostPrice($itemId, $depotId);
        $itemDepot = $itemDepotService->findByItemIdAndDepotId($itemId, $depotId);
        $isStockItem = $item->getNoStockItem() === BAS_Shared_Model_Item::NO_STOCK_ITEM_NO;

        $defaultQuantity = $item->getUnit() === BAS_Shared_Model_Item::UNIT_FLUID
            ? BAS_Shared_Model_Workshop_OrderWorkshopDetail::DEFAULT_FLUID_QUANTITY
            : BAS_Shared_Model_Workshop_OrderWorkshopDetail::DEFAULT_QUANTITY;

        $quantity = $addDetailRequest->getQuantity() === null ? $defaultQuantity : $addDetailRequest->getQuantity();

        if ($itemDepot === null) {
            throw new BAS_Shared_NotFoundException('Item ' . $itemId . ' not found in depot' . $depotId);
        }

        $priceOrder = $itemPrice ? $itemPrice->getPrice() : null;
        $costPrice = $costPrice ? $costPrice->getPrice() : null;


        if ($tradeIn === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TRADE_IN_YES) {
            $tradeInPrice = $addDetailRequest->getTradeInPrice($itemId, $depotId);

            if ($tradeInPrice !== false) {
                $priceOrder = $tradeInPrice;
                $costPrice = $tradeInPrice;
            }
        }

        /**
         * Add markup to refueling item for any internal_fixed order
         */
        $mainAccountingDepotId = $depotSettingService->getMainAccountingDepot($depotId);

        if ((int)$itemId === $this->getDefaultRefuelingItemId($mainAccountingDepotId)
            && $refuelingRecord = $refuelingService->findById($addDetailRequest->getRefuelingId())) {

            $itemName = sprintf('Diesel geboekt op %s bonnr. %s Liter: %s', date('d-m-Y', strtotime($refuelingRecord->getRefuelingTimestamp())), $refuelingRecord->getReceiptNumber(), $refuelingRecord->getQuantity());
            $quantity = $refuelingRecord->getQuantity();

            if ($workorder->getType() == BAS_Shared_Model_OrderWorkshop::TYPE_INTERNAL_FIXED) {
                $internalDieselTransactionMarkup = $depotSettingService->getInternalDieselTransactionMarkup($mainAccountingDepotId);
                $priceOrder = BAS_Shared_Utils_Math::add((float)$costPrice, $internalDieselTransactionMarkup, BAS_Shared_Utils_Math::SCALE_4);
            }
        }

        $priceOriginal = $priceOrder;

        if ($isStockItem && !$tradeIn) {
            $costPrice = null;
        }

        $workorderDetail = new BAS_Shared_Model_Workshop_OrderWorkshopDetail();
        $workorderDetail
            ->setTradeIn($tradeIn)
            ->setItemPicked($itemPicked)
            ->setPriceOrder($priceOrder)
            ->setPriceOriginal($priceOriginal)
            ->setCostPrice($costPrice)
            ->setDepotAccountGroupId($itemDepot->getDepotAccountGroupId())
            ->setInvoiceDescription($itemName)
            ->setQuantity($quantity)
            ->setDepotId($depotId)
        ;

        return $workorderDetail;
    }

    /**
     * @param int $workorderId
     * @param int $refuelingId
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail
     * @throws BAS_Shared_Exception
     */
    public function addWorkshopRefuelingDetail($workorderId, $refuelingId, $userId)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');
        /** @var Management_Service_DepotSetting $depotSettingsService */
        $depotSettingsService = $this->getService('Management_Service_DepotSetting');
        /** @var Warehouse_Service_ItemDepot $itemDepotService */
        $itemDepotService = $this->getService('Warehouse_Service_ItemDepot');

        $workorder = $workorderService->findById($workorderId);

        if (!$workorder) {
            throw new BAS_Shared_Exception(sprintf('Workorder %s not found', $workorderId));
        }

        $mainAccountingDepotId = $depotSettingsService->getMainAccountingDepot($workorder->getDepotId());

        $refuelingItemId = $this->getDefaultRefuelingItemId($mainAccountingDepotId);

        if (!$refuelingItemId) {
            throw new BAS_Shared_Exception(sprintf('Default Diesel/refueling item_id not found. Workorder id: %d. Depot id: %d', $workorder->getWorkorderId(), $workorder->getDepotId()));
        }

        $itemDepot = $itemDepotService->findByItemIdAndDepotId($refuelingItemId, $mainAccountingDepotId);
        if (null === $itemDepot) {
            throw new BAS_Shared_Exception(sprintf('Refueling item %d not found in depot %d', $refuelingItemId, $mainAccountingDepotId));
        }

        $addDetailRequest = new BAS_Shared_Model_Workshop_AddDetailRequest([
            'workorderId' => $workorderId,
            'referenceId' => $refuelingItemId,
            'referenceType' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM,
            'detailDepotId' => $mainAccountingDepotId,
            'refuelingId' => $refuelingId,
        ]);

        return $this->addDetail($addDetailRequest, $userId);
    }

    /**
     * @param int $depotId
     * @return int
     */
    public function getDefaultRefuelingItemId($depotId)
    {
        /** @var Management_Service_DepotSetting $depotSettingsService */
        $depotSettingsService = $this->getService('Management_Service_DepotSetting');

        return (int)$depotSettingsService->getSettingValue($depotId, BAS_Shared_Model_Setting::DIESEL_DEFAULT_ITEM_ID);
    }

    /**
     * @param BAS_Shared_Model_Workshop_AddDetailRequest $addDetailRequest
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail
     * @throws BAS_Shared_Exception
     */
    private function createTaskDetail(
        BAS_Shared_Model_Workshop_AddDetailRequest $addDetailRequest,
        BAS_Shared_Model_OrderWorkshop $workorder
    ) {
        /** @var Order_Service_Product $productService */
        $productService = $this->getService('Order_Service_Product');

        $taskId = $addDetailRequest->getReferenceId();
        $depotId = $addDetailRequest->getDetailDepotId() > 0
            ? (int)$addDetailRequest->getDetailDepotId()
            : $workorder->getDepotId();

        $task = $productService->findByIdAndDepotId($taskId, $depotId);

        $quantity = $addDetailRequest->getQuantity() === null
            ? BAS_Shared_Model_Workshop_OrderWorkshopDetail::DEFAULT_QUANTITY
            : $addDetailRequest->getQuantity();

        $workorderDetail = new BAS_Shared_Model_Workshop_OrderWorkshopDetail();
        $workorderDetail
            ->setReferenceId($task->getId())
            ->setPriceOrder($task->getPrice())
            ->setPriceOriginal($task->getPrice())
            ->setDepotAccountGroupId($task->getDepotAccountGroupId())
            ->setInvoiceDescription($task->getTitle())
            ->setQuantity($quantity)
            ->setTradeIn(BAS_Shared_Model_Workshop_OrderWorkshopDetail::TRADE_IN_NO)
            ->setDepotId($depotId)
        ;

        if (in_array($task->getWorkshopTaskType(), [
            BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_HOUR_BASED,
            BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_FIXED_PRICE,
        ])) {
            $workorderDetail
                ->setLabourRateId($task->getLabourRateId())
                ->setPlannedTime($task->getWorkshopTimePlanned())
                ->setInvoiceTime($task->getWorkshopTimeDefault())
            ;

            if ($workorder->getDefaultLabourRateId() > 0) {
                $this->setDetailLabourRate($workorderDetail, $workorder->getDefaultLabourRateId());
            }
        }

        if ($task->getWorkshopTaskType() === BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL) {
            $workorderDetail->setCostPrice($task->getCost());
        }

        return $workorderDetail;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail
     * @param int $labourRateId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail
     * @throws BAS_Shared_Exception
     */
    public function setDetailLabourRate(BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail, $labourRateId)
    {
        /** @var Order_Service_Product $productService */
        $productService = $this->getService('Order_Service_Product');

        $task = $productService->findByIdAndDepotId($detail->getReferenceId(), $detail->getDepotId());

        if ($task->getWorkshopTaskType() === BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_HOUR_BASED) {
            $price = $this->calculateTimeBasedPrice($detail->getInvoiceTime(), $labourRateId);
            $detail
                ->setLabourRateId($labourRateId)
                ->setPriceOrder($price)
                ->setPriceOriginal($price);

        } elseif ($task->getWorkshopTaskType() === BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_FIXED_PRICE) {
            $invoiceTime = $this->calculatePriceBasedTime($task->getPrice(), $labourRateId);
            $detail
                ->setLabourRateId($labourRateId)
                ->setInvoiceTime($invoiceTime);
        }

        return $detail;
    }

    /**
     * @param int $workorderId
     * @return int
     */
    public function getLastSequenceNumber($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        try {
            /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetail $workorderDetail */
            $workorderDetail = $orderWorkshopDetailMapper->findOneByCondition([
                $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
            ], 'sequence DESC');

            $lastSequenceNumber = $workorderDetail->getSequence();
        } catch (BAS_Shared_NotFoundException $e) {
            $lastSequenceNumber = 0;
        }

        return $lastSequenceNumber;
    }

    /**
     * @param int $workorderId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getDetailsInfoByWorkorderId($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');
        /** @var Warehouse_Service_OrderItems $orderItemService */
        $orderItemService = $this->getService('Warehouse_Service_OrderItems');

        $detailsInfo = $orderWorkshopDetailMapper->getDetailsInfoByWorkorderId($workorderId);

        /**
         * @var int $id
         * @var BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo $detailInfo
         */
        foreach ($detailsInfo as $id => $detailInfo) {
            $detailInfo->setDiscount($this->calculateDetailDiscount($detailInfo));

            if ($detailInfo->getType() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM) {
                $stockQuantity = $orderItemService->getItemStockQuantityInDepots($detailInfo->getReferenceId(), [$detailInfo->getDepotId()]);
                $detailInfo->setStockQuantity($stockQuantity);
            }

            if ($detailInfo->getUnit() === BAS_Shared_Model_Item::UNIT_PRODUCT) {
                $detailInfo->setQuantity(round($detailInfo->getQuantity(), 0));
            }

            if ($detailInfo->isComposedSubDetail()) {
                if (array_key_exists($detailInfo->getMainOrderWorkshopDetailId(), $detailsInfo)) {
                    $detailsInfo[$detailInfo->getMainOrderWorkshopDetailId()]->addSubDetailInfo($detailInfo);
                }
            }
        }

        return $detailsInfo;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail
     * @return float|int
     */
    public function calculateDetailDiscount(BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail)
    {
        if ((float)$detail->getPriceOriginal() === 0.0) {
            return 0;
        }

        // 1 - price_order / price_original

        $discount = BAS_Shared_Utils_Math::sub(
            1,
            BAS_Shared_Utils_Math::div(
                (float)$detail->getPriceOrder(),
                (float)$detail->getPriceOriginal(),
                BAS_Shared_Utils_Math::SCALE_4
            ),
            BAS_Shared_Utils_Math::SCALE_4
        );

        return $discount;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail
     * @return float|int
     */
    public function calculatePriceTotal(BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail)
    {
        // quantity * price_order

        $priceTotal = BAS_Shared_Utils_Math::mul(
            (float)$detail->getQuantity(),
            (float)$detail->getPriceOrder(),
            BAS_Shared_Utils_Math::SCALE_4
        );

        return $priceTotal;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail
     * @return float|int
     */
    public function calculateDetailDiscountAmount(BAS_Shared_Model_Workshop_OrderWorkshopDetail $detail)
    {
        // quantity * price_original - quantity * price_order

        if ((float)$detail->getPriceOriginal() === 0.0) {
            return 0;
        }

        $discountAmount = BAS_Shared_Utils_Math::sub(
            BAS_Shared_Utils_Math::mul(
                (float)$detail->getQuantity(),
                (float)$detail->getPriceOriginal(),
                BAS_Shared_Utils_Math::SCALE_4
            ),
            BAS_Shared_Utils_Math::mul(
                (float)$detail->getQuantity(),
                (float)$detail->getPriceOrder(),
                BAS_Shared_Utils_Math::SCALE_4
            ),
            BAS_Shared_Utils_Math::SCALE_4
        );

        return $discountAmount;
    }

    /**
     * @param float $originalPrice
     * @param float $discount
     * @return float|int
     */
    public function calculateOrderPrice($originalPrice, $discount)
    {
        return BAS_Shared_Utils_Math::mul(
            $originalPrice,
            1 - $discount,
            BAS_Shared_Utils_Math::SCALE_4
        );
    }

    /**
     * @param float $time
     * @param int $labourRateId
     * @return float|int
     * @throws BAS_Shared_Exception
     */
    public function calculateTimeBasedPrice($time, $labourRateId)
    {
        /** @var Workshop_Service_LabourRate $labourRateService */
        $labourRateService = $this->getService('Workshop_Service_LabourRate');

        $labourRate = $labourRateService->findById($labourRateId);

        $price = BAS_Shared_Utils_Math::mul(
            $time,
            $labourRate === null ? 0 : $labourRate->getRate(),
            BAS_Shared_Utils_Math::SCALE_4
        );

        return $price;
    }

    /**
     * @param float $price
     * @param int $labourRateId
     * @return float|int
     * @throws BAS_Shared_Exception
     */
    public function calculatePriceBasedTime($price, $labourRateId)
    {
        /** @var Workshop_Service_LabourRate $labourRateService */
        $labourRateService = $this->getService('Workshop_Service_LabourRate');

        $labourRate = $labourRateService->findById($labourRateId);

        if ($labourRate === null || (float)$labourRate->getRate() === 0.0) {
            return 0;
        }

        $time = BAS_Shared_Utils_Math::div(
            $price,
            $labourRate->getRate(),
            BAS_Shared_Utils_Math::SCALE_4
        );

        return $time;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail[] $details
     * @return string
     */
    public function calculateTotalAmountByDetails(array $details)
    {
        $totalAmount = 0;

        foreach ($details as $detail) {
            $detailAmount = BAS_Shared_Utils_Math::mul(
                (float)$detail->getQuantity(),
                (float)$detail->getPriceOrder(),
                BAS_Shared_Utils_Math::SCALE_4
            );

            $totalAmount = BAS_Shared_Utils_Math::add(
                $totalAmount,
                $detailAmount,
                BAS_Shared_Utils_Math::SCALE_4
            );
        }

        return $totalAmount;
    }

    /**
     * @param int $workorderId
     * @param int $detailId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getSubDetailsInfo($workorderId, $detailId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $subDetailsInfo = [];

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetail[] $subDetails */
        $subDetails = $orderWorkshopDetailMapper->findAllByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => $workorderId,
            $orderWorkshopDetailMapper->mapFieldToDb('mainOrderWorkshopDetailId') . ' = ?' => $detailId,
        ]);

        foreach ($subDetails as $subDetail) {
            $subDetailsInfo[$subDetail->getId()] = $orderWorkshopDetailMapper->getDetailInfoByWorkorderIdAndDetailId(
                $workorderId,
                $subDetail->getId()
            );
        }

        return $subDetailsInfo;
    }

    /**
     * @param int $detailId
     * @return int
     * @throws BAS_Shared_Exception
     */
    public function syncAccountGroupId($detailId)
    {
        /** @var Order_Service_Product $productService */
        $productService = $this->getService('Order_Service_Product');
        /** @var Warehouse_Service_ItemDepot $itemDepotService */
        $itemDepotService = $this->getService('Warehouse_Service_ItemDepot');
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $detail = $this->findById($detailId);
        $actualAccountGroupId = null;

        if ($detail->getType() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM) {

            $itemDepot = $itemDepotService->findByItemIdAndDepotId($detail->getReferenceId(), $detail->getDepotId());
            $actualAccountGroupId = $itemDepot === null ? null : $itemDepot->getDepotAccountGroupId();

        } elseif ($detail->getType() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK) {

            $product = $productService->findByIdAndDepotId($detail->getReferenceId(), $detail->getDepotId());
            $actualAccountGroupId = $product->getDepotAccountGroupId();
        }

        if ($detail->getDepotAccountGroupId() !== $actualAccountGroupId) {
            $orderWorkshopDetailMapper->updateFieldsByCondition(
                [$orderWorkshopDetailMapper->mapFieldToDb('depotAccountGroupId') => $actualAccountGroupId],
                [$orderWorkshopDetailMapper->mapFieldToDb('id') . ' = ?' => $detail->getId()]
            );
        }

        return $actualAccountGroupId;
    }

    /**
     * @param int $workorderId
     * @return array
     */
    public function getDetailsPostingSetups($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->getDetailsPostingSetups($workorderId);
    }

    /**
     * @param int $workorderId
     */
    public function markWorkorderDetailsAsDelivered($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $orderWorkshopDetailMapper->updateFieldsByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('itemPicked') => BAS_Shared_Model_Workshop_OrderWorkshopDetail::ITEM_PICKED_DELIVERED,
        ], [
            $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
        ]);
    }

    /**
     * @param int $workorderId
     * @return bool
     */
    public function workorderContainsDetailsWithLabourRate($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        try {
            $result = $orderWorkshopDetailMapper->findOneByCondition([
                $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
                $orderWorkshopDetailMapper->mapFieldToDb('labourRateId') . ' > 0',
                $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            $result = null;
        }

        return $result !== null;
    }

    /**
     * @param int $workorderId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail[]
     */
    private function getDetailsWithLabourRate($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->findAllByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId,
            $orderWorkshopDetailMapper->mapFieldToDb('labourRateId') . ' > 0',
            $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
        ]);
    }

    /**
     * @param int $workorderId
     * @param int $labourRateId
     * @param int $userId
     * @throws BAS_Shared_Exception
     * @throws Exception
     */
    public function updateDetailsLabourRate($workorderId, $labourRateId, $userId)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $details = $this->getDetailsWithLabourRate($workorderId);

        foreach ($details as $detail) {
            $detail = $this->setDetailLabourRate($detail, $labourRateId);
            $detail->setUpdatedBy($userId);
            $detail->setUpdatedAt($this->getCurrentDate());
            $orderWorkshopDetailMapper->saveModel($detail);
        }

        $this->updateActualPriceOfWorkorderComposedTasks($workorderId);
        $workorderService->updateWorkorderPricesAndDiscounts($workorderId, $userId);
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetail $mainDetail
     * @param float $mainQuantity
     * @param int $userId
     * @throws BAS_Shared_Exception
     */
    public function updateQuantityOfSubDetails(BAS_Shared_Model_Workshop_OrderWorkshopDetail $mainDetail, $mainQuantity, $userId)
    {
        /** @var Order_Service_Product $productService */
        $productService = $this->getService('Order_Service_Product');
        /** @var Order_Service_ProductComposition $productCompositionService */
        $productCompositionService = $this->getService('Order_Service_ProductComposition');
        /** @var Warehouse_Service_ItemStock $itemStockService */
        $itemStockService = $this->getService('Warehouse_Service_ItemStock');
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        if ($mainDetail->isItem()) {
            return;
        }

        $task = $productService->findByIdAndDepotId($mainDetail->getReferenceId(), $mainDetail->getDepotId());

        if ($task->getWorkshopTaskType() !== BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_COMPOSED) {
            return;
        }

        if ($task->getCompositionPriceType() !== BAS_Shared_Model_Product::COMPOSITION_PRICE_TYPE_FIXED) {
            return;
        }

        $subDetails = $this->findSubDetails($mainDetail->getId());

        foreach ($subDetails as $subDetail) {
            $compositionDetail = $productCompositionService->findByProductAndReferenceId(
                $task->getId(),
                $subDetail->getReferenceId()
            );

            $quantity = BAS_Shared_Utils_Math::mul(
                $mainQuantity,
                $compositionDetail->getQuantity(),
                BAS_Shared_Utils_Math::SCALE_2
            );

            $orderWorkshopDetailMapper->updateFieldsByCondition(
                ['quantity' => $quantity],
                ['id = ?' => $subDetail->getId()]
            );

            $quantityDelta = BAS_Shared_Utils_Math::sub(
                $quantity,
                $subDetail->getQuantity(),
                BAS_Shared_Utils_Math::SCALE_2
            );

            $itemStockService->updateQuantityByWorkorderDetailId(
                $subDetail->getId(),
                $quantityDelta,
                $userId
            );
        }
    }

    /**
     * @param int $detailId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail[]
     */
    public function findSubDetails($detailId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->findAllByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('mainOrderWorkshopDetailId') . ' = ?' => (int)$detailId,
            $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
        ]);
    }

    /**
     * @param int $detailId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail[]
     */
    public function findSubItems($detailId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        return $orderWorkshopDetailMapper->findAllByCondition([
            $orderWorkshopDetailMapper->mapFieldToDb('mainOrderWorkshopDetailId') . ' = ?' => (int)$detailId,
            $orderWorkshopDetailMapper->mapFieldToDb('type') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM,
            $orderWorkshopDetailMapper->mapFieldToDb('archived') . ' = ?' => BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED,
        ]);
    }

    /**
     * @param int $workorderId
     */
    public function updateActualPriceOfWorkorderComposedTasks($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $orderWorkshopDetailMapper->updateActualPriceOfWorkorderComposedTasks($workorderId);
    }

    /**
     * @param int $detailId
     */
    public function updateActualPriceOfComposedTask($detailId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $orderWorkshopDetailMapper->updateActualPriceOfComposedTask($detailId);
    }

    /**
     * Update total invoice price per order
     * 
     * return boolean
     */
    public function updateTotalInvoicePrice()
    {
        $logger = $this->getLogger();
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $orderWorkshopDetailMapper */
        $orderWorkshopDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');
        $result =  $orderWorkshopDetailMapper->getTotalInvoicePrice(); 
        
        if ([] === $result) {
            $logger->log("\033[32m>>> no records found", Zend_Log::INFO);

            return true;
        }
        
        /** @var BAS_Shared_Model_OrderWorkshopMapper $orderWorkshopMapper  */
        $orderWorkshopMapper = $this->getMapper('OrderWorkshop');
        $totalRecordsUpdated = 0;
        foreach ($result as $orderPrice) {
            // Update order total invoice price
            if (empty($orderPrice['totalInvoicePrice'])) {
                continue;
            }
            $orderWorkshopMapper->updateFieldsByCondition(
                ['total_invoice_price' => $orderPrice['totalInvoicePrice']],
                ['workorder_id = ?' => (int)$orderPrice['orderId']]);
            $totalRecordsUpdated ++;

            $logger->log("\033[32m>>> update order {$orderPrice['orderId']} total invoice price ", Zend_Log::INFO);
        }
        
        $logger->log("\033[32m>>> updated total records {$totalRecordsUpdated}\033[37m", Zend_Log::INFO);
        
        return true;
    }
}
