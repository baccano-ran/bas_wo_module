<?php

/**
 * Class Workshop_Service_Dieselpump
 */
class Workshop_Service_Dieselpump extends BAS_Shared_Service_Abstract
{
    const DOM_VERSION = '1.0';
    const DOM_ENCODING = 'ISO-8859-1';
    const XML_NOT_PROCESSED = 'not_processed';
    const XML_PROCESSED = 'processed';

    /**
     * @param int $userId
     * @return bool
     */
    public function processWorkshopRefuelingData($userId)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        /** @var Workshop_Service_Refueling $workshopRefuelingService */
        $workshopRefuelingService = $this->getService('Workshop_Service_Refueling');

        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingMapper $refuelingMapper */
        $refuelingMapper = $this->getMapper('Workshop_WorkshopRefueling');

        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingCardMapper $refuelingCardMapper */
        $refuelingCardMapper = $this->getMapper('Workshop_WorkshopRefuelingCard');

        /** @var array|BAS_Shared_Model_Workshop_WorkshopRefueling[] $refuelingRecords */
        $refuelingRecordsToProcess = $refuelingMapper->findAllByCondition(['status = ?' => BAS_Shared_Model_Workshop_WorkshopRefueling::STATUS_NEW]);

        if ([] === $refuelingRecordsToProcess) {
            $this->getLogger()->log('* No WorkshopRefueling records found to process', Zend_Log::DEBUG);

            return true;
        }

        $refuelingCards = $refuelingCardMapper->findAllAvailablePerCardNumber();

        /** @var BAS_Shared_Model_Workshop_WorkshopRefueling $refuelingRecord */
        foreach ($refuelingRecordsToProcess as $refuelingRecord) {
            try {
                $cardNumberExists = array_key_exists($refuelingRecord->getCardNumber(), $refuelingCards);

                /**
                 * Case if card number exists, and orderWorkshopId defined
                 * => the diesel can be booked on the attached workorder
                 */
                if ($cardNumberExists && 0 !== (int)$refuelingCards[ $refuelingRecord->getCardNumber() ]->getOrderWorkshopId()) {
                    $workorderId = (int)$refuelingCards[ $refuelingRecord->getCardNumber() ]->getOrderWorkshopId();

                    $this->getLogger()->log(sprintf('* Workorder was identified by Card Number "%s". Workorder id: %d',
                        $refuelingRecord->getCardNumber(), $workorderId), Zend_Log::DEBUG);

                    $this->attachRefuelingItemToWorkorder($workorderId, $refuelingRecord->getId(), $userId);

                    $workshopRefuelingService->changeRefuelingRecordStatus($refuelingRecord->getId(),
                        BAS_Shared_Model_Workshop_WorkshopRefueling::STATUS_PROCESSED,
                        BAS_Shared_Model_Person::SYSTEM_USER_ID);

                    continue;
                }

                /**
                 * Case if card number exists and no orderWorkshopId defined
                 * => try to find workorder by vehicle licence plate number
                 */
                if ($cardNumberExists && null === $refuelingCards[ $refuelingRecord->getCardNumber() ]->getOrderWorkshopId()
                    && null !== $refuelingRecord->getVehicleLicensePlate()
                    && $workorder = $workorderService->findByContactVehicleLicencePlate($refuelingRecord->getVehicleLicensePlate())
                ) {
                    $this->getLogger()->log(sprintf('* Workorder was identified by vehicle Licence Plate "%s". Workorder id: %d',
                        $refuelingRecord->getVehicleLicensePlate(), $workorder->getWorkorderId()), Zend_Log::DEBUG);

                    $this->attachRefuelingItemToWorkorder($workorder->getWorkorderId(), $refuelingRecord->getId(), $userId);

                    $workshopRefuelingService->changeRefuelingRecordStatus($refuelingRecord->getId(),
                        BAS_Shared_Model_Workshop_WorkshopRefueling::STATUS_PROCESSED,
                        BAS_Shared_Model_Person::SYSTEM_USER_ID);

                    continue;

                }

                /**
                 * Case if no card number exists/defined, but manual card number is defined
                 * => if present we find workorder by TruckID and book the diesel on that workorder
                 * Manual number "1" is excluded,b ecause we dont want to match manual number = 1 to truckid 1 (which is used on several internal workorders)
                 */
                if (!$cardNumberExists && null !== $refuelingRecord->getManualNumber()
                    && 1 !== (int)$refuelingRecord->getManualNumber()
                    && $workorder = $workorderService->findByTruckId($refuelingRecord->getManualNumber())
                ) {

                    $this->getLogger()->log(sprintf('* Workorder was identified by TruckId "%s". Workorder id: %d',
                        $refuelingRecord->getManualNumber(), $workorder->getWorkorderId()), Zend_Log::DEBUG);

                    $this->attachRefuelingItemToWorkorder($workorder->getWorkorderId(), $refuelingRecord->getId(), $userId);

                    $workshopRefuelingService->changeRefuelingRecordStatus($refuelingRecord->getId(),
                        BAS_Shared_Model_Workshop_WorkshopRefueling::STATUS_PROCESSED,
                        BAS_Shared_Model_Person::SYSTEM_USER_ID);

                    continue;
                }

            } catch (Exception $e) {
                $errorMessage = sprintf('Error processing WorkshopRefueling record with id: %d. Error: %s. Detailed trace: %s', $refuelingRecord->getId(), $e->getMessage(), $e->getTraceAsString());
                $this->getLogger()->log($errorMessage, Zend_Log::ALERT);
            }

            /**
             * In case we cant identify workorde => set status to be manually processed
             */
            $workshopRefuelingService->changeRefuelingRecordStatus($refuelingRecord->getId(),
                BAS_Shared_Model_Workshop_WorkshopRefueling::STATUS_TO_BE_PROCESSED_MANUAL,
                BAS_Shared_Model_Person::SYSTEM_USER_ID);
            $this->getLogger()->log('* Workorder was not identified. Setting status "to be processed manually"', Zend_Log::DEBUG);
        }

        return true;

    }

    /**
     * @param int $workorderId
     * @param int $refuelingId
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail
     * @throws BAS_Shared_Exception
     */
    public function attachRefuelingItemToWorkorder($workorderId, $refuelingId, $userId)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');
        $this->getDb()->beginTransaction();
        try {
            $workshopRefuelingDetail = $workorderDetailService->addWorkshopRefuelingDetail($workorderId, $refuelingId, $userId);
            $this->getDb()->commit();

            return $workshopRefuelingDetail;
        } catch (Exception $e) {
            $this->getDb()->rollBack();
            $this->getLogger()->log(sprintf('* Failed attaching refueling item to workorder id: %d. No default item found or out of stock. Details: %s',
                $workorderId, $e->getMessage()), Zend_Log::DEBUG);

            throw $e;
        }
    }

    /**
     * @return bool
     */
    public function processDieselpumpXml()
    {
        $incomingXmlPath = $this->getConfig()->get('workshop')->get('dieselpump_incoming_xml_path');

        $this->getLogger()->log(sprintf('* Start processing Dieselpump/Hecpoll XML files in %s', $incomingXmlPath), Zend_Log::DEBUG);
        $xmlFilesFound = $this->scanDieselpumpXmlFiles($incomingXmlPath);

        if ([] === $xmlFilesFound) {
            $this->getLogger()->log('* No XML files found to process', Zend_Log::DEBUG);

            return true;
        }
        foreach ($xmlFilesFound as $xmlFilePath) {
            $db = $this->getDb();
            try {
                $db->beginTransaction();

                $xmlFileName = basename($xmlFilePath);

                if (true === $this->checkXmlProcessed($xmlFileName)) {
                    $newXmlFilePath = $this->moveDieselpumpXml($xmlFilePath, self::XML_NOT_PROCESSED);
                    $this->getLogger()->log(sprintf('* XML file "%s" already processed. Moving to %s', $xmlFileName, $newXmlFilePath), Zend_Log::DEBUG);
                    continue;
                }

                $workshopRefuelingRecords = $this->readDieselpumpXmlIntoRefuelingRecords($xmlFilePath);

                $this->processRefuelingRecords($workshopRefuelingRecords);
                $this->moveDieselpumpXml($xmlFilePath, self::XML_PROCESSED);
                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                $errorMessage = sprintf('Error processing Dieselpump/Hecpoll XML workorder records: %s. Moving file to not processed folder. Detailed trace: %s', $e->getMessage(), $e->getTraceAsString());
                $this->getLogger()->log($errorMessage, Zend_Log::ALERT);
                $newXmlFilePath = $this->moveDieselpumpXml($xmlFilePath, self::XML_NOT_PROCESSED);
                continue;
            }
        }
        $this->getLogger()->log(sprintf('* Completed processing Dieselpump/Hecpoll XML files in %s', $incomingXmlPath), Zend_Log::DEBUG);

        return true;
    }

    /**
     * @param string $xmlFilePath
     * @return array|BAS_Shared_Model_Workshop_WorkshopRefueling[]
     */
    public function readDieselpumpXmlIntoRefuelingRecords($xmlFilePath)
    {

        $xmlDoc = new DOMDocument(self::DOM_VERSION);
        $xmlDoc->validateOnParse = false;
        $xmlDoc->preserveWhiteSpace = false;
        if (!$xmlDoc->load($xmlFilePath)) {
            throw new BAS_Shared_Exception(sprintf('Cannot read/load XML file: %s', $xmlFilePath));
        }

        $xmlFileName = basename($xmlFilePath);
        $this->getLogger()->log(sprintf('* Processing XML file: %s', $xmlFileName), Zend_Log::DEBUG);

        $refuelingRecords = [];
        $xpath = new DOMXPath($xmlDoc);

        // refueling date and time
        $createDateElem = $xpath->query("//Transactions/DocumentInfo/CreateDate")->item(0);
        $createTimeElem = $xpath->query("//Transactions/DocumentInfo/CreateTime")->item(0);
        $refuelingTimestamp = sprintf('%04d-%02d-%02d %02d:%02d:%02d',
            $createDateElem->getAttribute('Year'),
            $createDateElem->getAttribute('Month'),
            $createDateElem->getAttribute('Day'),
            $createTimeElem->getAttribute('Hour'),
            $createTimeElem->getAttribute('Minute'),
            $createTimeElem->getAttribute('Second')
        );

        // transactions
        $transactionRecords = $xpath->query("//Transactions/DocumentData/Sale");
        foreach ($transactionRecords as $transactionRecordElem) {
            // receipt number
            $receiptNumber = $transactionRecordElem->getAttribute('RcpNum');

            /**
             * ArticleSale section
             */
            $articleSaleElem = $xpath->query("ArticleSale", $transactionRecordElem)->item(0);
            $quantity = null;
            $price = null;
            $amount = null;
            if(null !== $articleSaleElem) {
                // Quantity
                $quantity = $articleSaleElem->getAttribute('Quantity') ?: null;
                // Price
                $price = $articleSaleElem->getAttribute('SglPrice') ?: null;
                // Amount
                $amount = $articleSaleElem->getAttribute('Amount') ?: null;
            }
            /**
             * Payment section
             */
            $paymentElem = $xpath->query("Payment", $transactionRecordElem)->item(0);
            $cardNumber = null;
            $manualNumber = null;
            $mileage = null;
            if (null !== $paymentElem) {
                // Card number
                $cardNumber = $paymentElem->getAttribute('PAN') ?: null;
                // Manual number
                $manualNumber = $paymentElem->getAttribute('Pan_twee') ?: null;
                // Mileage
                $mileage = $paymentElem->getAttribute('Odometer') ?: null;
            }

            /**
             * Employee section
             */
            $employeeElem = $xpath->query("Employee", $transactionRecordElem)->item(0);

            // Employee username
            $employeeUsername = null;
            if (null !== $employeeElem) {
                $employeeUsername = sprintf('%s %s', $employeeElem->getAttribute('Firstname'), $employeeElem->getAttribute('Lastname'));
                $employeeUsername = trim($employeeUsername) ?: null;
            }

            /**
             * Vehicle section
             */
            $vehicleElem = $xpath->query("Vehicle", $transactionRecordElem)->item(0);
            $vehicleLicensePlate = null;

            // Vehicle licence plate
            if (null !== $vehicleElem) {
                $vehicleLicensePlate = $vehicleElem->getAttribute('LicensePlate');
            }

            $workshopRefueling = new BAS_Shared_Model_Workshop_WorkshopRefueling();
            $workshopRefueling
                ->setReceiptNumber($receiptNumber)
                ->setRefuelingTimestamp($refuelingTimestamp)
                ->setQuantity($quantity)
                ->setPrice($price)
                ->setAmount($amount)
                ->setCardNumber($cardNumber)
                ->setManualNumber($manualNumber)
                ->setMileage($mileage)
                ->setUsername($employeeUsername)
                ->setVehicleLicensePlate($vehicleLicensePlate)
                ->setXmlName($xmlFileName)
                ->setCreatedBy(BAS_Shared_Model_Person::SYSTEM_USER_ID)
                ->setStatus(BAS_Shared_Model_Workshop_WorkshopRefueling::STATUS_NEW)
                ->setDepotId(BAS_Shared_Model_Depot::BAS_DEPOT);
            $refuelingRecords[] = $workshopRefueling;
        }

        return $refuelingRecords;
    }

    /**
     * @param array $refuelingRecords
     * @return bool
     * @throws BAS_Shared_Exception
     */
    public function processRefuelingRecords(array $refuelingRecords)
    {
        if ([] === $refuelingRecords) {
            return false;
        }

        /** @var Workshop_Service_Refueling $workshopRefuelingService */
        $workshopRefuelingService = $this->getService('Workshop_Service_Refueling');

        foreach ($refuelingRecords as $refuelingRecord) {
            $workshopRefuelingService->saveModel($refuelingRecord, BAS_Shared_Model_AbstractMapper::SAVE_TYPE_INSERT);
        }
    }

    public function checkXmlProcessed($xmlFileName)
    {
        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingMapper $refuelingMapper */
        $refuelingMapper = $this->getMapper('Workshop_WorkshopRefueling');

        return $refuelingMapper->checkXmlProcessed($xmlFileName);
    }


    /**
     * @param string $incomingXmlPath
     * @return array
     * @throws BAS_Shared_Exception
     */
    public function scanDieselpumpXmlFiles($incomingXmlPath)
    {
        if (!is_dir($incomingXmlPath)) {
            throw new BAS_Shared_Exception(sprintf('Directory %s does not exist', $incomingXmlPath));
        }

        return glob(sprintf('%s/*.xml', $incomingXmlPath));
    }

    /**
     * @param string $xmlFilePath
     * @param string $xmlProcessStatus
     * @return bool|string
     * @throws BAS_Shared_Exception
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function moveDieselpumpXml($xmlFilePath, $xmlProcessStatus)
    {
        $workshopConfig = $this->getConfig()->get('workshop');
        switch ($xmlProcessStatus) {
            case self::XML_PROCESSED:
                $destinationXmlBasePath = $workshopConfig->get('dieselpump_processed_xml_path');
                break;
            case self::XML_NOT_PROCESSED:
                $destinationXmlBasePath = $workshopConfig->get('dieselpump_not_processed_xml_path');
                break;
            default:
                throw new BAS_Shared_InvalidArgumentException('Unknown XML status passed');
        }
        if (!is_dir($destinationXmlBasePath)) {
            throw new BAS_Shared_Exception(sprintf('Dieselpump/Hecpoll XML directory does not exists at %s', $destinationXmlBasePath));

            return false;
        }
        $destinationFilePath = sprintf('%s/%s', $destinationXmlBasePath, basename($xmlFilePath));

        if (false === rename($xmlFilePath, $destinationFilePath)) {
            throw new BAS_Shared_Exception(sprintf('Cannot move file from %s to %s', $xmlFilePath, $destinationFilePath));
        }

        return $destinationFilePath;
    }

}