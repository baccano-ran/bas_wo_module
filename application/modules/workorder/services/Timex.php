<?php

/**
 * Class Workshop_Service_Timex
 */
class Workshop_Service_Timex extends BAS_Shared_Service_Abstract
{
    const DOM_VERSION = '1.0';
    const DOM_ENCODING = 'UTF-8';
    const TIMEX_XML_NOT_PROCESSED = 'not_processed';
    const TIMEX_XML_PROCESSED = 'processed';

    /**
     * @param Twig_Environment $twig
     * @return bool
     */
    public function writeWorkordersTimexXml(Twig_Environment $twig)
    {
        $this->getLogger()->log('* Start prepearing/writing Timex XML for available workorders', Zend_Log::DEBUG);

        $outgoingXmlBasePath = $this->getConfig()->get('workshop')->get('timex_outgoing_xml_path');
        $xmlFilePath = sprintf('%s/Timex-%d.xml', $outgoingXmlBasePath, time());
        $xmlContent = '';

        try {

            if (!$this->checkTimexXmlDirectoryExists($outgoingXmlBasePath)) {
                throw new BAS_Shared_Exception(sprintf('Timex outgoing XML directory does not exist: %s', $outgoingXmlBasePath));
            }

            $workorders = $this->getAvailableWorkorderListForTimex();
            if (empty($workorders)) {
                $this->getLogger()->log('* No available workorders found', Zend_Log::DEBUG);

                return true;
            }

            $this->getLogger()->log(sprintf('* Available workorders found: %d', count($workorders)), Zend_Log::DEBUG);

            $xmlContent = $this->buildWorkordersTimexXml($workorders);

            $bytesWritten = $this->saveTimexXml($xmlFilePath, $xmlContent);

            if (false === $bytesWritten) {
                $this->getLogger()->log('* Error writing xml file ', Zend_Log::DEBUG);
                throw new BAS_Shared_Exception(sprintf('Error writing xml file: %s', $xmlFilePath));
            }
        } catch (Exception $e) {

            $errorMessage = sprintf('Error creating Timex XML with workorder records: %s. Detailed trace: %s', $e->getMessage(), $e->getTraceAsString());
            $this->getLogger()->log($errorMessage, Zend_Log::ALERT);
            $this->sendCreateTimexXmlErrorReport($errorMessage, $twig, $xmlFilePath, $xmlContent);

            return false;
        }

        $this->getLogger()->log(sprintf('* Timex XML created successfully (%d bytes written)', $bytesWritten), Zend_Log::DEBUG);

        return true;
    }

    /**
     * @param string $xmlDirectoryPath
     * @return bool
     */
    public function checkTimexXmlDirectoryExists($xmlDirectoryPath)
    {
        return is_dir($xmlDirectoryPath);
    }

    /**
     * @param string $xmlFilePath
     * @param text $xmlContent
     * @return int|bool
     */
    public function saveTimexXml($xmlFilePath, $xmlContent)
    {
        return file_put_contents($xmlFilePath, $xmlContent);
    }

    /**
     * @param array $workorders
     * @return string
     */
    public function buildWorkordersTimexXml(array $workorders)
    {
        $xmlDoc = new DOMDocument(self::DOM_VERSION, self::DOM_ENCODING);
        $xmlDoc->preserveWhiteSpace = false;
        $xmlDoc->formatOutput = true;

        $documentContainer = $xmlDoc->createElement('Container');

        foreach ($workorders as $workorderData) {
            // <ProdOrderRoutingLine>
            $workorderContainer = $xmlDoc->createElement('ProdOrderRoutingLine');
            // <salesorderno> - n/a
            $salesOrderNumberElement = $xmlDoc->createElement('salesorderno');
            $workorderContainer->appendChild($salesOrderNumberElement);
            // <prodid> - workorderId
            $workorderId = str_pad($workorderData['workorder_id'], 6, '0', STR_PAD_LEFT);
            $workorderIdElement = $xmlDoc->createElement('prodid', $workorderId);
            $workorderContainer->appendChild($workorderIdElement);
            // <custname> - customer name
            $customerNameElement = $xmlDoc->createElement('custname');
            // createTextNode to escape customer_name text
            $customerNameElement->appendChild($xmlDoc->createTextNode($workorderData['customer_name']));
            $workorderContainer->appendChild($customerNameElement);
            // <cityname> - n/a
            $cityNameElement = $xmlDoc->createElement('cityname');
            $workorderContainer->appendChild($cityNameElement);
            // <carplate> -  car identifier: TruckID when stocked, and license_plate when from contact_vehicle
            $vehicleLicencePlateElement = $xmlDoc->createElement('carplate', $workorderData['car_identifier']);
            $workorderContainer->appendChild($vehicleLicencePlateElement);

            $documentContainer->appendChild($workorderContainer);
        }

        $xmlDoc->appendChild($documentContainer);

        return $xmlDoc->saveXML();
    }

    /**
     * @return array
     */
    public function getAvailableWorkorderListForTimex()
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');

        $mainDepotId = BAS_Shared_Model_Depot::BAS_DEPOT;

        /** @var Journal_Service_Setting $journalSettingService */
        $journalSettingService = $this->getService('Journal_Service_Setting');
        $affiliatedDepotIds = array_merge($journalSettingService->getAffiliatedDepotList($mainDepotId), [$mainDepotId]);

        return $workorderMapper->findAvailableWorkorderListForTimex($affiliatedDepotIds);
    }


    /**
     * @param Twig_Environment $twig
     * @return bool
     */
    public function processTimexXml(Twig_Environment $twig)
    {
        $incomingXmlPath = $this->getConfig()->get('workshop')->get('timex_incoming_xml_path');

        $this->getLogger()->log(sprintf('* Start processing Timex XML files in %s', $incomingXmlPath), Zend_Log::DEBUG);

        $xmlFilesFound = $this->scanTimexXmlFiles($incomingXmlPath);

        if ([] === $xmlFilesFound) {
            $this->getLogger()->log('* No XML files found to process', Zend_Log::DEBUG);
            return true;
        }

        /** @var Workshop_Service_TimexMailLogger $mailLoggerService */
        $mailLoggerService = $this->getService('Workshop_Service_TimexMailLogger');

        foreach ($xmlFilesFound as $xmlFilePath) {
            $xmlFilename = basename($xmlFilePath);
            if ($this->isXmlAlreadyProcessed($xmlFilename)) {
                $this->getLogger()->log(sprintf('* XML file "%s" already processed. Skipping', $xmlFilePath), Zend_Log::DEBUG);
                continue;
            }

            $mailLoggerService->flushMessages();

            $db = $this->getDb();

            try {
                $db->beginTransaction();

                $timexWorkorderRecords = $this->readTimexXmlIntoWorkorderRecords($xmlFilePath);

                $this->processTimexWorkorderRecords($xmlFilename, $timexWorkorderRecords);
                $newXmlFilePath =  $this->moveTimexXml($xmlFilePath, self::TIMEX_XML_PROCESSED);

                $db->commit();
            } catch (Exception $e) {
                $db->rollBack();
                $errorMessage = sprintf('Error processing Timex XML workorder records: %s. Detailed trace: %s', $e->getMessage(), $e->getTraceAsString());
                $this->getLogger()->log($errorMessage, Zend_Log::ALERT);
                $mailLoggerService->addMessage($errorMessage);
            }

            if ($mailLoggerService->hasMessages()) {
                $mailLoggerService->addMessage('Record is processed into order_workshop_time, a email will be generated to notify about this error.');
                $this->getLogger()->log('* Record is processed into order_workshop_time, a email will be generated to notify about this error.', Zend_Log::DEBUG);
                $this->sendTimexXmlProcessErrorReport($mailLoggerService->getMessages(), $twig, $xmlFilePath);
            }

        }

        $this->getLogger()->log(sprintf('* Completed processing Timex XML files in %s', $incomingXmlPath), Zend_Log::DEBUG);

        return true;
    }

    /**
     * @param string $xmlFilename
     * @return array
     */
    public function isXmlAlreadyProcessed($xmlFilename)
    {
        /** @var Workshop_Service_WorkorderTime $workorderTimeService */
        $workorderTimeService = $this->getService('Workshop_Service_WorkorderTime');

        return $workorderTimeService->isTimexXmlAlreadyProcessed($xmlFilename);
    }

    /**
     * @param string $xmlFilePath
     * @return array
     */
    public function readTimexXmlIntoWorkorderRecords($xmlFilePath)
    {
        $xmlDoc = new DOMDocument(self::DOM_VERSION);
        $xmlDoc->validateOnParse = false;
        $xmlDoc->preserveWhiteSpace = false;

        $tries = 0;
        $xmlLoaded = false;
        $attempts = 3;

        do {
            $xmlLoaded = $xmlDoc->load($xmlFilePath);
            $tries++;
            if (!$xmlLoaded) {
                $this->getLogger()->log(sprintf('* Failed to read XML file: %s. Try #%d', basename($xmlFilePath), $tries), Zend_Log::DEBUG);

                usleep(500000); // 0.5 seconds
            }
        } while (!$xmlLoaded && $tries < $attempts);

        if (!$xmlLoaded) {
            throw new BAS_Shared_Exception(sprintf('Cannot read/load XML file: %s', $xmlFilePath));
        }

        $this->getLogger()->log(sprintf('* Processing XML file: %s', basename($xmlFilePath)), Zend_Log::DEBUG);

        $messages = $xmlDoc->getElementsByTagName('message');

        $filterFieldsMap = [
            'messageType' => 'MESSAGE_TYPE',
            'messageDt' => 'DATETIME',
            'workorderId' => 'PONUMBER',
            'employeeNumber' => 'RESOURCENO',
            'batchDate' => 'DATUM',
            'spentTime' => 'AANTAL',
        ];

        $timexWorkorderRecords = [];
        /** @var DOMElement $messageNode */
        foreach ($messages as $messageNode) {

            $xml = $xmlDoc->saveXML($messageNode);
            $parser = xml_parser_create();
            xml_parse_into_struct($parser, $xml, $fieldValues, $fieldIndexes);
            xml_parser_free($parser);

            $fieldIndexes = array_intersect_key($fieldIndexes, array_flip($filterFieldsMap));

            $messageData = array_map(function($fieldIndex) use ($fieldValues) {
                return isset($fieldValues[$fieldIndex[0]]['value']) ? trim($fieldValues[$fieldIndex[0]]['value']) : null;
            }, $fieldIndexes);

            $timexWorkorderRecords[] = array_combine(array_keys($filterFieldsMap), $messageData);

        }

        return $timexWorkorderRecords;
    }

    /**
     * @param string $incomingXmlPath
     * @return array
     */
    public function scanTimexXmlFiles($incomingXmlPath)
    {
        return glob(sprintf('%s/*.xml', $incomingXmlPath));
    }

    /**
     * @param array $errorMessages
     * @param Twig_Environment $twig
     * @param string $xmlFilePath
     * @return bool
     */
    public function sendTimexXmlProcessErrorReport(array $errorMessages, Twig_Environment $twig, $xmlFilePath)
    {
        $errorMessage = '';
        foreach ([Workshop_Service_TimexMailLogger::TYPE_GENERAL,
                     Workshop_Service_TimexMailLogger::TYPE_WORKORDER_ID_UNKNOWN,
                     Workshop_Service_TimexMailLogger::TYPE_EMPLOYEE_NUMBER_UNKNOWN] as $messageType) {
            $errorMessage .= array_key_exists($messageType, $errorMessages)
                ? implode("\r\n", $errorMessages[$messageType]). "\r\n" : '';
        }

        $reportEmailTo = $this->getConfig()->get('workshop')->get('timex_report_email_to');
        $reportEmailFrom = $this->getConfig()->get('workshop')->get('timex_report_email_from');

        $template = $twig->loadTemplate('timex_xml_process_error_report.twig');

        /** @var Default_Service_MailQueue $mailQueueService */
        $mailQueueService = $this->getService('Default_Service_MailQueue');

        $subject = 'Timex spent time XML cannot be processed';
        switch (true) {
            case (array_key_exists(Workshop_Service_TimexMailLogger::TYPE_WORKORDER_ID_UNKNOWN, $errorMessages)):
                    $subject .= ' <workorderId unknown>';
                break;
            case (array_key_exists(Workshop_Service_TimexMailLogger::TYPE_EMPLOYEE_NUMBER_UNKNOWN, $errorMessages)):
                    $subject .= ' <employee number unknown>';
                break;
        }

        $xmlContent = $this->getXmlFileContent($xmlFilePath);

        $mailBody = $template->render(
            [
                'xmlFilePath'    => $xmlFilePath,
                'xmlContent'    => $xmlContent,
                'errorMessage'    => $errorMessage,
                'reportDate'    => date('Y-m-d H:i'),
            ]);

        $xmlFileName = basename($xmlFilePath);

        /** @var BAS_Shared_Service_FileService $fileService */
        $fileService = $this->getService('BAS_Shared_Service_FileService');

        $xmlFileToken =  $fileService->sendFile($xmlFileName, $xmlContent);

        $xData['attachments'] = [
            $xmlFileName => [
                'token' => $xmlFileToken,
                'mime' => 'application/xml',
                'originalName' => $xmlFileName,
            ],
        ];

        if (false === $mailQueueService->addMailQueue($reportEmailFrom, $reportEmailTo, $subject, $mailBody, $xData)) {
            throw new BAS_Shared_Exception('Error sending Timex XML process error report');
        }
    }

    /**
     * @param string $xmlFilePath
     * @return string
     */
    public function getXmlFileContent($xmlFilePath)
    {
        return file_get_contents($xmlFilePath);
    }

    /**
     * @param string $errorMessage
     * @param Twig_Environment $twig
     * @param string $xmlFilePath
     * @param string $xmlContent
     * @return bool
     * @throws BAS_Shared_Exception
     */
    public function sendCreateTimexXmlErrorReport($errorMessage, Twig_Environment $twig, $xmlFilePath, $xmlContent)
    {
        $reportEmailTo = $this->getConfig()->get('workshop')->get('timex_report_email_to');
        $reportEmailFrom = $this->getConfig()->get('workshop')->get('timex_report_email_from');

        $template = $twig->loadTemplate('timex_create_xml_error_report.twig');

        /** @var Default_Service_MailQueue $mailQueueService */
        $mailQueueService = $this->getService('Default_Service_MailQueue');

        $subject = '[Workshop/Timex waarschuwing] Fout bij het maken van Timex XML!';
        $mailBody = $template->render(
            [
                'xmlFilePath'    => $xmlFilePath,
                'xmlContent'    => $xmlContent,
                'errorMessage'    => $errorMessage,
                'reportDate'    => date('Y-m-d H:i'),
            ]);

        if (false === $mailQueueService->addMailQueue($reportEmailFrom, $reportEmailTo, $subject, $mailBody)) {
            throw new BAS_Shared_Exception('Error sending Timex XML creation error report');
        }
    }

    /**
     * @param string $xmlFilename
     * @param array $timexWorkorderRecords
     * @return bool
     * @throws BAS_Shared_Exception
     */
    public function processTimexWorkorderRecords($xmlFilename, array $timexWorkorderRecords)
    {
        if ([] === $timexWorkorderRecords) {
            return false;
        }

        /** @var Workshop_Service_TimexMailLogger $mailLoggerService */
        $mailLoggerService = $this->getService('Workshop_Service_TimexMailLogger');

        /** @var Workshop_Service_WorkorderTime $workorderTimeService */
        $workorderTimeService = $this->getService('Workshop_Service_WorkorderTime');

        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        /** @var Contacts_Service_Person $personService */
        $personService = $this->getService('Contacts_Service_Person');

        /** @var Management_Service_Setting $settingService */
        $settingService = $this->getService('Management_Service_Setting');

        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');

        foreach ($timexWorkorderRecords as $timexWorkorderRecord) {

            $workorderId = null;
            $labourActivityId = null;
            $personId = null;
            $person = null;

            $plainWorkorderId = $timexWorkorderRecord['workorderId'];
            $workorderId = 0 !== (int)$plainWorkorderId
                ? (int)$plainWorkorderId : null;

            $workorder = $workorderService->findById($workorderId);

            if (!$workorder) {
                $errorMsg = sprintf('Workorder was not found. Workorder id: %d', $plainWorkorderId);
                $this->getLogger()->log('* '.$errorMsg, Zend_Log::DEBUG);
                $mailLoggerService->addMessage($errorMsg, Workshop_Service_TimexMailLogger::TYPE_WORKORDER_ID_UNKNOWN);
                $workorderId = null;
            }

            // check if the workorder is available and fetch the default_labour_activity_id
            if (!$workorder || !$labourActivityId = $workorderMapper->getWorkorderDefaultLabourActivityId($workorderId)) {
                $errorMsg = sprintf('Workorder is not available or default labour activity ID is not set. Workorder id: %d. ', $plainWorkorderId);
                $this->getLogger()->log('* ' . $errorMsg, Zend_Log::DEBUG);
                $mailLoggerService->addMessage($errorMsg, Workshop_Service_TimexMailLogger::TYPE_WORKORDER_ID_UNKNOWN);
            }

            // extract '069-' from number (where 069 stands for the company)
            $employeeNumber = preg_replace('/^069-/', '', $timexWorkorderRecord['employeeNumber']);

            if ($workorder) {
                $mainAccountingDepot = $settingService->getMainAccountingDepot($workorder->getDepotId());
                $person = $personService->findByEmployeeNumberInDepot($mainAccountingDepot, $employeeNumber, false);
            }

            // check if we know the employee
            if (!$workorder || !$person) {
                $errorMsg = sprintf('Employee was not found. Employee number: %s. Workorder id: %d. ', $timexWorkorderRecord['employeeNumber'], $plainWorkorderId);
                $this->getLogger()->log('* '.$errorMsg, Zend_Log::DEBUG);
                $mailLoggerService->addMessage($errorMsg, Workshop_Service_TimexMailLogger::TYPE_EMPLOYEE_NUMBER_UNKNOWN);
            }
            $personId = $person ? $person->getId() : null;


            $batchDate = DateTime::createFromFormat('Ymd', $timexWorkorderRecord['batchDate'])->format('Y-m-d');

            $workorderTimeRecord = new BAS_Shared_Model_Workshop_OrderWorkshopTime();
            $workorderTimeRecord
                ->setOrderWorkshopId($workorderId)
                ->setPersonId($personId)
                ->setSpentTime((float)$timexWorkorderRecord['spentTime'])
                ->setLabourActivityId($labourActivityId)
                ->setXmlFilename($xmlFilename)
                ->setManual(BAS_Shared_Model_Workshop_OrderWorkshopTime::NOT_MANUAl)
                ->setArchived(BAS_Shared_Model_Workshop_OrderWorkshopTime::NOT_ARCHIVED)
                ->setCreatedBy(BAS_Shared_Model_User::SYSTEM_USER_ID)
                ->setDate($batchDate)
            ;

            $workorderTimeService->saveModel($workorderTimeRecord, BAS_Shared_Model_AbstractMapper::SAVE_TYPE_INSERT);

        }
    }

    /**
     * @param string $xmlFilePath
     * @param string $xmlProcessStatus
     * @return bool|string
     * @throws BAS_Shared_Exception
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function moveTimexXml($xmlFilePath, $xmlProcessStatus)
    {
        $workshopConfig = $this->getConfig()->get('workshop');
        switch ($xmlProcessStatus) {
            case self::TIMEX_XML_PROCESSED:
                $destinationXmlBasePath = $workshopConfig->get('timex_processed_xml_path');
                break;
            case self::TIMEX_XML_NOT_PROCESSED:
                $destinationXmlBasePath = $workshopConfig->get('timex_not_processed_xml_path');
                break;
            default:
                throw new BAS_Shared_InvalidArgumentException('Unknown XML status passed');
        }

        if (!is_dir($destinationXmlBasePath)) {
            throw new BAS_Shared_Exception(sprintf('Incoming XML directory does not exists at %s', $destinationXmlBasePath));
            return false;
        }

        $destinationFilePath = sprintf('%s/%s', $destinationXmlBasePath, basename($xmlFilePath));

        if (false === rename($xmlFilePath, $destinationFilePath)) {
            throw new BAS_Shared_Exception(sprintf('Cannot move file from %s to %s', $xmlFilePath, $destinationFilePath));
        }

        return $destinationFilePath;
    }

}