<?php

/**
 * Class Workshop_Service_LabourActivity
 */
class Workshop_Service_LabourActivity extends BAS_Shared_Service_Abstract
{

    /**
     * @param int $id
     * @param int $depotId
     * @return BAS_Shared_Model_Workshop_LabourActivity|null
     */
    public function findByIdAndDepotId($id, $depotId)
    {
        $id = (int)$id;
        $depotId = (int)$depotId;

        /** @var BAS_Shared_Model_Workshop_LabourActivityMapper $mapper */
        $mapper = $this->getMapper('Workshop_LabourActivity');

        $labourActivity = null;

        try {
            $labourActivity = $mapper->findOneByCondition([
                $mapper->mapFieldToDb('id') . ' = ?' => $id,
                $mapper->mapFieldToDb('depotId') . ' = ?' => $depotId,
            ]);
        } catch (BAS_Shared_NotFoundException $e) {}

        return $labourActivity;
    }

    /**
     * @param int $departmentId
     * @param int $depotId
     * @return BAS_Shared_Model_Workshop_LabourActivity[]
     */
    public function findByDepartmentId($departmentId, $depotId)
    {
        /** @var BAS_Shared_Model_Workshop_LabourActivityMapper $mapper */
        $labourActivityMapper = $this->getMapper('Workshop_LabourActivity');

        return $labourActivityMapper->findAllByCondition([
            $labourActivityMapper->mapFieldToDb('depotId') . ' = ?' => (int)$depotId,
            $labourActivityMapper->mapFieldToDb('departmentId') . ' = ?' => $departmentId,
        ]);
    }

    /**
     * @param array $data
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_LabourActivity
     */
    public function createModelByData(array $data, $userId)
    {
        /** @var BAS_Shared_Model_Workshop_LabourActivityMapper $mapper */
        $mapper = $this->getMapper('Workshop_LabourActivity');

        $data = array_intersect_key($data, $mapper->getMap());
        $labourActivity = new BAS_Shared_Model_Workshop_LabourActivity($data);

        if ((int)$labourActivity->getId() > 0) {
            $labourActivity->setUpdatedBy($userId);
            $labourActivity->setUpdatedAt(date('Y-m-d H:i:s'));
        } else {
            $labourActivity->setCreatedBy($userId);
        }

        return $labourActivity;
    }

    /**
     * @param BAS_Shared_Model_Workshop_LabourActivity $labourActivity
     * @return BAS_Shared_Model_Workshop_LabourActivity
     */
    public function saveModel(BAS_Shared_Model_Workshop_LabourActivity $labourActivity)
    {
        /** @var BAS_Shared_Model_Workshop_LabourActivityMapper $mapper */
        $mapper = $this->getMapper('Workshop_LabourActivity');

        return $mapper->saveModel($labourActivity);
    }

    /**
     * @param int $depotId
     * @return Zend_Db_Select
     */
    public function getGridSource($depotId)
    {
        /** @var BAS_Shared_Model_Workshop_LabourActivityMapper $mapper */
        $mapper = $this->getMapper('Workshop_LabourActivity');

        return $mapper->getGridSource($depotId);
    }

}