<?php

/**
 * Class Workshop_Service_Refueling
 */
class Workshop_Service_Refueling extends BAS_Shared_Service_Abstract
{

    /**
     * @param int $id
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling|bool
     */
    public function findById($id)
    {
        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingMapper $refuelingMapper */
        $refuelingMapper = $this->getMapper('Workshop_WorkshopRefueling');

        try {
            return $refuelingMapper->findOneByCondition([
                $refuelingMapper->mapFieldToDb('id') . ' = ?' => (int)$id,
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            return false;
        }
    }

    /**
     * @param int $depotId
     * @return Zend_Db_Select
     */
    public function getGridSelect($depotId)
    {
        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingMapper $refuelingMapper */
        $refuelingMapper = $this->getMapper('Workshop_WorkshopRefueling');

        return $refuelingMapper->getGridSelectByDepotAndStatus(
            $depotId,
            BAS_Shared_Model_Workshop_WorkshopRefueling::STATUS_TO_BE_PROCESSED_MANUAL
        );
    }

    /**
     * @param BAS_Shared_Model_Workshop_WorkshopRefueling $workshopRefuelingRecord
     * @param string $type
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function saveModel(BAS_Shared_Model_Workshop_WorkshopRefueling $workshopRefuelingRecord, $type = BAS_Shared_Model_AbstractMapper::SAVE_TYPE_AUTO)
    {
        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingMapper $refuelingMapper */
        $refuelingMapper = $this->getMapper('Workshop_WorkshopRefueling');

        return $refuelingMapper->saveModel($workshopRefuelingRecord, $type);
    }

    /**
     * @param int $workshopRefuelingId
     * @param int $status
     * @param int $userId
     * @return bool|int
     */
    public function changeRefuelingRecordStatus($workshopRefuelingId, $status, $userId)
    {
        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingMapper $refuelingMapper */
        $refuelingMapper = $this->getMapper('Workshop_WorkshopRefueling');

        return $refuelingMapper->updateFieldsByCondition([
            $refuelingMapper->mapFieldToDb('status')    => (int)$status,
            $refuelingMapper->mapFieldToDb('updatedBy') => (int)$userId,
            $refuelingMapper->mapFieldToDb('updatedAt') => date('Y-m-d H:i:s'),
        ], [
            $refuelingMapper->mapFieldToDb('id') . ' = ?' => (int)$workshopRefuelingId,
        ]);
    }

    /**
     * @param array $refuelingIdList
     * @param int $workorderId
     * @param int $userId
     * @throws BAS_Shared_Exception
     * @return true
     */
    public function attachRefuelingToWorkorder(array $refuelingIdList, $workorderId, $userId)
    {
        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingMapper $refuelingMapper */
        $refuelingMapper = $this->getMapper('Workshop_WorkshopRefueling');

        /** @var BAS_Shared_Model_Workshop_WorkshopRefueling[] $refuelingModels */
        $refuelingModels = $refuelingMapper->findAllByCondition([
            $refuelingMapper->mapFieldToDb('id') . ' IN (?)' => $refuelingIdList,
        ]);

        /** @var Workshop_Service_Dieselpump $dieselpumpService */
        $dieselpumpService = $this->getService('Workshop_Service_Dieselpump');

        foreach ($refuelingModels as $refuelingModel) {
            $dieselpumpService->attachRefuelingItemToWorkorder($workorderId, $refuelingModel->getId(), $userId);
            $refuelingModel->status = BAS_Shared_Model_Workshop_WorkshopRefueling::STATUS_PROCESSED;
            $refuelingMapper->saveModel($refuelingModel);
        }

        return true;
    }
}