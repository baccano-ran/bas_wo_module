<?php

/**
 * Class Workshop_Service_WorkorderInvoice
 */
class Workshop_Service_WorkorderInvoice extends BAS_Shared_Service_Abstract
{

    /**
     * @param int $invoiceId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoice|bool
     */
    public function getInternalInvoiceById($invoiceId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceMapper $workorderInvoiceMapper */
        $workorderInvoiceMapper = $this->getMapper('Workshop_OrderWorkshopInternalInvoice');

        try {
            return $workorderInvoiceMapper->findOneByCondition(['invoice_id = ?' => (int)$invoiceId]);
        } catch (BAS_Shared_NotFoundException $e) {
            return false;
        }
    }

    /**
     * @param int $invoiceBookingType
     * @return BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceMapper|BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceMapper
     * @throws BAS_Shared_InvalidArgumentException
     */
    private function getWorkorderInvoiceMapper($invoiceBookingType)
    {
        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL) {
            $mapper = $this->getMapper('Workshop_OrderWorkshopInternalInvoice');
        } elseif ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL) {
            $mapper = $this->getMapper('Workshop_OrderWorkshopExternalInvoice');
        } else {
            throw new BAS_Shared_InvalidArgumentException('Invalid invoice booking type given');
        }

        return $mapper;
    }

    /**
     * @param int $invoiceBookingType
     * @return BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceLogMapper|BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceLogMapper
     * @throws BAS_Shared_InvalidArgumentException
     */
    private function getWorkorderInvoiceLogMapper($invoiceBookingType)
    {
        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL) {
            $mapper = $this->getMapper('Workshop_OrderWorkshopInternalInvoiceLog');
        } elseif ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL) {
            $mapper = $this->getMapper('Workshop_OrderWorkshopExternalInvoiceLog');
        } else {
            throw new BAS_Shared_InvalidArgumentException('Invalid invoice booking type given');
        }

        return $mapper;
    }

    /**
     * @param int $workorderId
     * @param int $invoiceType
     * @param int $invoiceFileId
     * @param string $languageCode
     * @param int $invoiceBookingType
     * @param int $depotId
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract
     * @throws BAS_Shared_Exception
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function createInvoice(
        $workorderId,
        $invoiceType,
        $invoiceFileId,
        $languageCode,
        $invoiceBookingType,
        $depotId,
        $userId
    ) {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');
        /** @var Management_Service_DepotSetting $depotSettingService */
        $depotSettingService = $this->getService('Management_Service_DepotSetting');
        $workorderInvoiceMapper = $this->getWorkorderInvoiceMapper($invoiceBookingType);

        $workorder = $workorderService->findById($workorderId);

        $invoiceBuilder = BAS_Shared_Model_Workshop_OrderWorkshopInvoiceBuilderFactory::create(
            $workorderService,
            $depotSettingService,
            $this,
            $invoiceBookingType
        );

        $invoice = $invoiceBuilder->build(
            $workorder,
            $invoiceType,
            $invoiceFileId,
            $languageCode,
            $invoiceBookingType,
            $depotId,
            $userId
        );

        return $workorderInvoiceMapper->saveModel($invoice);
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract $invoice
     * @param int $workorderId
     * @param int $invoiceBookingType
     * @return BAS_Shared_Model_Workshop_OrderWorkshopInvoiceLogAbstract[]
     * @throws BAS_Shared_Exception
     */
    public function createInvoiceLogs($invoice, $workorderId, $invoiceBookingType)
    {
        /** @var Workshop_Service_WorkorderReport $workorderReportService */
        $workorderReportService = $this->getService('Workshop_Service_WorkorderReport');

        $detailInfoList = $workorderReportService->getInvoiceLogDetailInfoList(
            $workorderId,
            $invoiceBookingType,
            BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::DEFAULT_EXCHANGE_RATE
        );

        $invoiceLogs = [];

        foreach ($detailInfoList as $detailInfo) {
            $invoiceLogs[] = $this->createInvoiceLog($detailInfo, $invoice, $workorderId, $invoiceBookingType);
        }

        return $invoiceLogs;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo $detailInfo
     * @param BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract $invoice
     * @param int $workorderId
     * @param int $invoiceBookingType
     * @return BAS_Shared_Model_Workshop_OrderWorkshopInvoiceLogAbstract
     * @throws BAS_Shared_Exception
     */
    public function createInvoiceLog(
        BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo $detailInfo,
        BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract $invoice,
        $workorderId,
        $invoiceBookingType
    ) {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');
        $workorderInvoiceLogMapper = $this->getWorkorderInvoiceLogMapper($invoiceBookingType);

        $quantity = $detailInfo->getQuantity();
        $price = $detailInfo->getPriceOrder();
        $amount = $detailInfo->getPriceTotal();

        $discountAmount = BAS_Shared_Utils_Math::neg(
            $workorderDetailService->calculateDetailDiscountAmount($detailInfo),
            BAS_Shared_Utils_Math::SCALE_4
        );

        $stockPriceStrategy = new BAS_Shared_Model_Workshop_InvoiceLogStockPriceStrategy(
            $this->getService('Warehouse_Service_ItemStockTransaction'),
            $this->getService('Warehouse_Service_ItemStock'),
            $this->getService('Warehouse_Service_Item'),
            $this->getService('Order_Service_Product')
        );

        $stockPrice = $stockPriceStrategy->getPrice(
            BAS_Shared_Service_ItemStock_ReferenceLine_AdapterFactory::create($detailInfo),
            $invoice->getType() === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::TYPE_CREDIT_INVOICE,
            $detailInfo->getDepotId()
        );

        $stockAmount = BAS_Shared_Utils_Math::neg(BAS_Shared_Utils_Math::mul(
            $stockPrice,
            $detailInfo->getQuantity(),
            BAS_Shared_Utils_Math::SCALE_4
        ), BAS_Shared_Utils_Math::SCALE_4);

        $workorderDetailInvoiceLog = BAS_Shared_Model_Workshop_OrderWorkshopInvoiceLogFactory::create($invoiceBookingType);
        $workorderDetailInvoiceLog
            ->setInvoiceId($invoice->getInvoiceId())
            ->setOrderWorkshopId($workorderId)
            ->setOrderWorkshopDetailId($detailInfo->getId())
            ->setType($detailInfo->getType())
            ->setReferenceId($detailInfo->getReferenceId())
            ->setDepotBookingCodeId($detailInfo->getDepotBookingCodeId())
            ->setDepotAccountGroupId($detailInfo->getDepotAccountGroupId())
            ->setQuantity($quantity)
            ->setPrice($price)
            ->setAmount($amount)
            ->setStockPrice($stockPrice)
            ->setStockAmount($stockAmount)
            ->setDiscountAmount($discountAmount)
        ;

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopInvoiceLogAbstract $workorderDetailInvoiceLog */
        $workorderDetailInvoiceLog = $workorderInvoiceLogMapper->saveModel($workorderDetailInvoiceLog);

        return $workorderDetailInvoiceLog;
    }

    /**
     * @param int $invoiceBookingType
     * @param int $depotId
     * @return int
     */
    public function getNextInvoiceId($invoiceBookingType, $depotId)
    {
        /** @var Management_Service_DepotSetting $depotSettingsService */
        $depotSettingsService = $this->getService('Management_Service_DepotSetting');
        $workorderInvoiceMapper = $this->getWorkorderInvoiceMapper($invoiceBookingType);

        $mainAccountingDepotId = $depotSettingsService->getMainAccountingDepot($depotId);
        $lastInvoiceId = $workorderInvoiceMapper->getLastInvoiceId($mainAccountingDepotId);
        $nextInvoiceId = $lastInvoiceId + 1;

        return $nextInvoiceId;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoice $invoice
     * @param array|BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceLog[] $invoiceLogs
     * @param int $workorderType
     * @param int $vehicleId
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return BAS_Shared_Model_StockTransaction
     */
    public function insertWorkOrdersAmountIntoStockTransaction($invoice, $invoiceLogs, $workorder)
    {
        /** @var Management_Service_DepotPostingSetup $postingSetupService */
        $postingSetupService = $this->getService('Management_Service_DepotPostingSetup');
        /** @var BAS_Shared_Model_StockTransactionRevaluationMapper $stockTransactionRevaluationMapper */
        $stockTransactionRevaluationMapper = $this->getMapper('StockTransactionRevaluation');
        /** @var BAS_Shared_Model_StockTransactionMapper $stockTransactionMapper */
        $stockTransactionMapper = $this->getMapper('StockTransaction');
        /** @var BAS_Shared_Model_OrderVehicleMapper $orderVehicleMapper */
        $orderVehicleMapper = $this->getMapper('OrderVehicle');
        /** @var BAS_Shared_Model_OrderVehicleProductMapper $orderVehicleProductMapper */
        $orderVehicleProductMapper = $this->getMapper('OrderVehicleProduct');
        /** @var BAS_Shared_Model_VehicleMapper $vehicleMapper */
        $vehicleMapper = $this->getMapper('vehicle');
        /** @var BAS_Shared_Model_PurchaseVehicleMapper $purchaseVehicleMapper */
        $purchaseVehicleMapper = $this->getMapper('PurchaseVehicle');
        /** @var BAS_Shared_Model_PurchaseVehicleProductMapper $purchaseVehicleProductMapper */
        $purchaseVehicleProductMapper = $this->getMapper('PurchaseVehicleProduct');
        /** @var Stock_Service_PurchaseVehicle $purchaseVehicleService */
        $purchaseVehicleService = $this->getService('Stock_Service_PurchaseVehicle');
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $workorderType = $workorder->getType();
        $vehicleId = $workorder->getVehicleId();
        $legacyVoertuigId = $vehicleId;

        if ($workorderType === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER) { 
            $orderVehicle = $orderVehicleMapper->getVehicleDetailsByOrderVehicleId($vehicleId);
            $legacyVoertuigId = $orderVehicle[$vehicleId]['vehicleId'];
        } 
        if (!isset($legacyVoertuigId)){
            return false;
        }
        $totalRowPrice = 0;
        foreach($invoiceLogs as $invoiceLog) {
            $bookingCode = $postingSetupService->findBookingCodeById($invoiceLog->getDepotBookingCodeId());
            if ($bookingCode->getType() !== BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_GUARANTEE &&
                    $bookingCode->getType() !== BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_CONTRA_BOOKING) {
                $totalRowPrice += $invoiceLog->getAmount();
            }
        }

        // here we do rounding first because we wants to match this amount with amount on
        // invoice where rounding is done before total
        $totalPrice = BAS_Shared_Utils_Math::add(round($totalRowPrice, 2),
            BAS_Shared_Utils_Math::add(round($invoice->getPriceConsumables(), 2),
                round($invoice->getPriceEnvironment(), 2),
                BAS_Shared_Utils_Math::SCALE_2),
            BAS_Shared_Utils_Math::SCALE_2);
        
        $stockTransaction = new BAS_Shared_Model_StockTransaction();
        $stockTransaction->setLegacyVehicleId($legacyVoertuigId);
        $stockTransaction->setDate($invoice->getInvoiceDate());
        $stockTransaction->setTypeId($workorderType === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER ? BAS_Shared_Model_StockTransaction::TYPE_ORDER : BAS_Shared_Model_StockTransaction::TYPE_COST_PRICE);
        $stockTransaction->setAmount($totalPrice);
        $stockTransaction->setDepartmentId(BAS_Shared_Model_Department::DEPARTMENT_ID_WORKSHOP);
        $stockTransaction->setOrderId($invoice->getOrderWorkshopId());
        $stockTransaction->setAs400Id(NULL);
        $stockTransaction->setDocumentType(BAS_Shared_Model_StockTransaction::DOCUMENT_TYPE_WORKSHOP);
        $stockTransaction->setDocumentId($invoice->getInvoiceId());
        $stockTransaction->setVehicleId($workorderType === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER ? $vehicleId : NULL);
        $stockTransaction->setCreatedBy($invoice->getCreatedBy());
        $stockTransaction->setCostMatched(BAS_Shared_Model_StockTransaction::COST_NOT_MATCHED);
        $stockTransaction->setCreatedAt(new Zend_Db_Expr("NOW()"));
        $stockTransaction = $stockTransactionMapper->save($stockTransaction);
        
        if (!is_null($stockTransaction->getId())) {
            // update appraisal
            $vehicleMapper->updateAppraisal($legacyVoertuigId);

            if ($workorderType === BAS_Shared_Model_OrderWorkshop::TYPE_INCOMING_VEHICLES) {
                $purchaseVehicle = $purchaseVehicleService->findVehicleById($legacyVoertuigId);
                $purchaseVehicleProductMapper->updateFieldsByCondition($purchaseVehicleProductMapper->mapToDb([
                    'costBooked' => BAS_Shared_Model_PurchaseVehicleProduct::COST_BOOKED
                ]), [
                    $purchaseVehicleProductMapper->mapFieldToDb('legacyVoertuigId') . '=?' => $legacyVoertuigId,
                    $purchaseVehicleProductMapper->mapFieldToDb('purchaseId') . '=?' => $purchaseVehicle->purchaseId,
                    $purchaseVehicleProductMapper->mapFieldToDb('archived') . '=?' => BAS_Shared_Model_PurchaseVehicleProduct::NOT_ARCHIVED,
                    $purchaseVehicleProductMapper->mapFieldToDb('referenceType') . '=?' => BAS_Shared_Model_PurchaseVehicleProduct::REFERENCE_TYPE_PRODUCT_TABLE,
                ]);

                $purchaseVehicleMapper->updatePurchaseTodoValue($legacyVoertuigId);
            }

            // make entry in stock_transaction_revaluation table for type 3 costs
            if(BAS_Shared_Model_StockTransaction::TYPE_VALUE_ADDED_COSTS === (int)$stockTransaction->getTypeId()) {
                $stockTransactionRevaluation = new BAS_Shared_Model_StockTransactionRevaluation();
                $stockTransactionRevaluation->setStockTransactionId($stockTransaction->getId());
                $stockTransactionRevaluation->setBooked(BAS_Shared_Model_StockTransactionRevaluation::VEHICLE_NOT_BOOKED);
                $stockTransactionRevaluation->setCreatedAt(new Zend_Db_Expr("NOW()"));
                $stockTransactionRevaluationMapper->save($stockTransactionRevaluation);
            }
        }
        
        if ($workorderType === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER) {
            // set cost_booked = 1 for products of this workorder in order_vehicle_product table
            $orderVehicleProductMapper->updateByConditionVehicleProduct(
                ['cost_booked' => BAS_Shared_Model_OrderVehicleProduct::COST_BOOKED],
                ['workorder_id = (?)' => $invoice->getOrderWorkshopId()]
            );
        }
        return $stockTransaction;
    }

    /**
     * Returns true if workorder is already invoiced, otherwise returns false
     *
     * @param int $workorderId
     * @return bool
     */
    public function isWorkorderInvoiced($workorderId)
    {
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceMapper $externalInvoiceMapper */
        $externalInvoiceMapper = $this->getMapper('Workshop_OrderWorkshopExternalInvoice');
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceMapper $externalInvoiceMapper */
        $internalInvoiceMapper = $this->getMapper('Workshop_OrderWorkshopInternalInvoice');

        try {
            $externalInvoice = $externalInvoiceMapper->findOneByCondition([
                $externalInvoiceMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            $externalInvoice = null;
        }

        if ($externalInvoice !== null) {
            return true;
        }

        try {
            $internalInvoice = $internalInvoiceMapper->findOneByCondition([
                $internalInvoiceMapper->mapFieldToDb('orderWorkshopId') . ' = ?' => (int)$workorderId
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            $internalInvoice = null;
        }

        if ($internalInvoice !== null) {
            return true;
        }

        return false;
    }


    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float
     * @throws BAS_Shared_Exception
     */
    public function calcInternalNetAmount($workorder)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        return BAS_Shared_Utils_Math::add(
            $workorderService->calcInternalNetAmount($workorder),
            $workorderService->calcGuaranteeNetAmount($workorder),
            BAS_Shared_Utils_Math::SCALE_4
        );
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return float
     * @throws BAS_Shared_Exception
     */
    public function calcExternalNetAmount($workorder)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        return $workorderService->calcExternalNetAmount($workorder);
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return string
     */
    public function calcInternalTotalExclVatByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        return BAS_Shared_Utils_Math::add(
            $workorderService->calcInternalTotalExclVatByWorkorder($workorder),
            $workorderService->calcGuaranteeTotalExclVatByWorkorder($workorder),
            BAS_Shared_Utils_Math::SCALE_4
        );
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return string
     */
    public function calcExternalTotalExclVatByWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        return $workorderService->calcExternalTotalExclVatByWorkorder($workorder);
    }

}