<?php

/**
 * Class Workshop_Service_LabourCost
 */
class Workshop_Service_LabourCost extends BAS_Shared_Service_Abstract
{

    /** @var BAS_Shared_Model_Workshop_LabourCostMapper */
    protected $mapper;

    public function __construct()
    {
        $this->setLabourCostMapper($this->getMapper('Workshop_LabourCost'));
    }

    /**
     * @param BAS_Shared_Model_Workshop_LabourCostMapper $mapper
     */
    public function setLabourCostMapper(BAS_Shared_Model_Workshop_LabourCostMapper $mapper)
    {
        $this->mapper = $mapper;
    }

    /**
     * @param array $data
     * @return array
     */
    public function validateArrayData(array $data)
    {
        $validationErrors = [];
        $endDateEmpty = false;

        foreach ($data as $labourCostData) {
            if ($errors = $this->validateData($labourCostData)) {
                $validationErrors = array_merge($validationErrors, $errors);
            }

            $endDate = array_key_exists('endDate', $labourCostData) ? $labourCostData['endDate'] : false;
            $endDate = DateTime::createFromFormat('d-m-Y', $endDate);

            if ($endDate === false) {
                if (!$endDateEmpty) {
                    $endDateEmpty = true;
                } else {
                    $validationErrors[] = [
                        'attr' => 'endDate',
                        'message' => $this->getTranslate()->translate('error_only_one_end_date_may_be_empty')
                    ];
                }
            }
        }
        return $validationErrors;
    }
    /**
     * @param array $data
     * @return array
     */
    public function validateData(array $data)
    {
        $errors = [];
        $rules = $this->getRules();
        foreach ($rules as $attr => $validators) {
            $value = array_key_exists($attr, $data) ? $data[$attr] : null;
            /** @var Zend_Validate_Abstract $validator */
            foreach ($validators as $validator) {
                if ($validator->isValid($value)) {
                    continue;
                }
                $messages = $validator->getMessages();
                foreach ($messages as $message) {
                    $errors[] = ['attr' => $attr, 'message' => $message];
                }
            }
        }
        return $errors;
    }

    /**
     * @return array
     */
    private function getRules()
    {
        return [
            'costPrice' => [
                new Zend_Validate_NotEmpty(),
                new BAS_Shared_Form_Validate_Numeric(),
            ],
        ];
    }

    /**
     * @param int $labourRateId
     * @return BAS_Shared_Model_Workshop_LabourCost[]
     */
    public function findAllByRateId($labourRateId)
    {
        $labourRateId = (int)$labourRateId;

        $costs = $this->mapper->findAllByCondition([
            $this->mapper->mapFieldToDb('labourRateId') . ' = ?' => $labourRateId,
        ]);

        return $costs;
    }
    /**
     * @param int $labourRateId
     * @return boolean
     */
    public function deleteByRateId($labourRateId)
    {
        $labourRateId = (int)$labourRateId;

        $costs = $this->mapper->deleteByCondition([
            $this->mapper->mapFieldToDb('labourRateId') . ' = ?' => $labourRateId,
        ]);

        return $costs;
    }
    /**
     * @param int $id
     * @return boolean
     */
    public function deleteById($id)
    {
        $id = (int)$id;

        $costs = $this->mapper->deleteByCondition([
            $this->mapper->mapFieldToDb('id') . ' = ?' => $id,
        ]);

        return $costs;
    }
    /**
     * @param array $ids
     * @param int $labourRateId
     * @return boolean
     */
    public function deleteByNotIds(array $ids, $labourRateId)
    {
        $labourRateId = (int)$labourRateId;
        if ($ids === []) {
            return $this->deleteByRateId($labourRateId);
        }

        return $this->mapper->deleteByCondition([
            $this->mapper->mapFieldToDb('labourRateId') . ' = ?' => $labourRateId,
            $this->mapper->mapFieldToDb('id') . ' NOT IN (?)' => $ids,
        ]);
    }

    /**
     * @param array $data
     * @param int $userId
     * @param int $timestamp
     * @return BAS_Shared_Model_Workshop_LabourCost
     */
    public function createModelByData(array $data, $userId, $timestamp = 0)
    {
        if (!$timestamp) {
            $timestamp = time();
        }
        $data = array_intersect_key($data, $this->mapper->getMap());
        $labourCost = new BAS_Shared_Model_Workshop_LabourCost($data);

        if ((int)$labourCost->getId() > 0) {
            $labourCost->setUpdatedBy($userId);
            $labourCost->setUpdatedAt(date('Y-m-d H:i:s', $timestamp));
        } else {
            $labourCost->setCreatedBy($userId);
            $labourCost->setCreatedAt(date('Y-m-d H:i:s', $timestamp));
        }

        $endDate = DateTime::createFromFormat('d-m-Y', $labourCost->getEndDate());
        if ($endDate === false) {
            $labourCost->setEndDate('0000-00-00');
        } else {
            $labourCost->setEndDate($endDate->format('Y-m-d'));
        }

        return $labourCost;
    }

    /**
     * @param BAS_Shared_Model_Workshop_LabourCost $labourCost
     * @return BAS_Shared_Model_Workshop_LabourCost
     */
    public function saveModel(BAS_Shared_Model_Workshop_LabourCost $labourCost)
    {
        return $this->mapper->saveModel($labourCost);
    }

    /**
     * @param int $rateId
     * @return BAS_Shared_Model_Workshop_LabourCost|null
     */
    public function findRateCost($rateId)
    {
        $rateId = (int)$rateId;

        try {
            $cost = $this->mapper->findOneByCondition([
                $this->mapper->mapFieldToDb('labourRateId') . ' = ?' => $rateId,
                $this->mapper->mapFieldToDb('endDate') . ' >= NOW() OR ' . $this->mapper->mapFieldToDb('endDate') . ' = \'0000-00-00\'',
            ]);
        } catch (BAS_Shared_NotFoundException $e) {
            $cost = null;
        }

        return $cost;
    }

}