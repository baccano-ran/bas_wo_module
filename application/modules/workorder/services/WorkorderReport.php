<?php

/**
 * Class Workshop_Service_WorkorderReport
 */
class Workshop_Service_WorkorderReport extends BAS_Shared_Service_Abstract
{
    const DEFAULT_PAYMENT_TERM_DAYS = 14;

    /**
     * @param int $orderId
     * @param int $languageCode
     * @return array
     * @throws BAS_Shared_NotFoundException
     */
    public function getWorkorderTradeCompanyData($orderId, $languageCode)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');
        return $workorderMapper->getWorkorderTradeCompanyData($orderId, $languageCode);
    }

    /**
     * @param int $workorderId
     * @param int $invoiceBookingType
     * @param float $currencyExchangeRate
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     * @throws BAS_Shared_Exception
     */
    public function getInvoiceDetailInfoList($workorderId, $invoiceBookingType, $currencyExchangeRate)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $workorderDetailMapper */
        $workorderDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $bookingCodeTypes = BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::$depotBookingCodeMap[$invoiceBookingType];
        $detailInfoList = $workorderDetailMapper->getInvoiceDetailInfoList($workorderId, $bookingCodeTypes);

        // group by main details
        foreach ($detailInfoList as $detailId => $detailInfo) {
            if ($detailInfo->isComposedSubDetail()) {
                // unset all sub details because they will be stored inside main details
                unset($detailInfoList[$detailId]);
                $detailInfoList[$detailInfo->getMainOrderWorkshopDetailId()]->addSubDetailInfo($detailInfo);
            }
        }

        // calculate total prices and convert currency
        foreach ($detailInfoList as $detailInfo) {
            $detailInfo->setPriceOrder($this->convertCurrencyByRate($detailInfo->getPriceOrder(), $currencyExchangeRate));
            $detailInfo->setPriceOriginal($this->convertCurrencyByRate($detailInfo->getPriceOriginal(), $currencyExchangeRate));
            $detailInfo->setPriceTotal($workorderDetailService->calculatePriceTotal($detailInfo));

            if ($detailInfo->getCompositionPriceType() === BAS_Shared_Model_Product::COMPOSITION_PRICE_TYPE_COMPOSED) {
                foreach ($detailInfo->getSubDetailsInfo() as $subDetailInfo) {
                    $subDetailInfo->setPriceOrder($this->convertCurrencyByRate($subDetailInfo->getPriceOrder(), $currencyExchangeRate));
                    $subDetailInfo->setPriceTotal($workorderDetailService->calculatePriceTotal($subDetailInfo));
                }
            }
        }

        return $detailInfoList;
    }

    /**
     * @param int $workorderId
     * @param int $invoiceBookingType
     * @param float $currencyExchangeRate
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     * @throws BAS_Shared_Exception
     */
    public function getInvoiceLogDetailInfoList($workorderId, $invoiceBookingType, $currencyExchangeRate)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper $workorderDetailMapper */
        $workorderDetailMapper = $this->getMapper('Workshop_OrderWorkshopDetail');

        $depotBookingCodes = BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::$depotBookingCodeMap[$invoiceBookingType];
        $detailInfoList = $workorderDetailMapper->getInvoiceLogDetailInfoList($workorderId, $depotBookingCodes);

        foreach ($detailInfoList as $detailInfo) {
            // convert prices by exchange rate
            $detailInfo->setPriceOriginal($this->convertCurrencyByRate($detailInfo->getPriceOriginal(), $currencyExchangeRate));
            $detailInfo->setPriceOrder($this->convertCurrencyByRate($detailInfo->getPriceOrder(), $currencyExchangeRate));
            $detailInfo->setPriceTotal($workorderDetailService->calculatePriceTotal($detailInfo));
        }

        return $detailInfoList;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[] $detailInfoList
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getInternalTaskInfoList(array $detailInfoList)
    {
        $tasks = [];

        foreach ($detailInfoList as $detailInfo) {
            // skip sub details
            if ($detailInfo->isSubDetail()) {
                continue;
            }

            if (in_array($detailInfo->getWorkshopTaskType(), [
                BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_HOUR_BASED,
                BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_FIXED_PRICE,
                BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_COMPOSED,
            ])) {
                $tasks[] = $detailInfo;
            }
        }

        return $tasks;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[] $detailInfoList
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getExternalTaskInfoList(array $detailInfoList)
    {
        $tasks = [];

        foreach ($detailInfoList as $detailInfo) {
            // skip sub details
            if ($detailInfo->isSubDetail()) {
                continue;
            }

            if ($detailInfo->getWorkshopTaskType() === BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL) {
                $tasks[] = $detailInfo;
            }
        }

        return $tasks;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[] $detailInfoList
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getItemInfoList(array $detailInfoList)
    {
        $items = [];

        foreach ($detailInfoList as $detailInfo) {
            // skip composed sub items
            if ($detailInfo->isComposedSubDetail()) {
                continue;
            }

            if ($detailInfo->getType() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM) {
                $items[] = $detailInfo;
            }
        }

        return $items;
    }

    /**
     * @param float $amount
     * @param float $currencyExchangeRate
     * @return float
     */
    public function convertCurrencyByRate($amount, $currencyExchangeRate)
    {
        return BAS_Shared_Utils_Math::mul($amount, $currencyExchangeRate, 4);
    }

    /**
     * @param int $workorderId
     * @return array
     * @throws BAS_Shared_Exception
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function getWorkorderReportGeneralData($workorderId)
    {
        /** @var BAS_Shared_Model_OrderWorkshopMapper $workorderMapper */
        $workorderMapper = $this->getMapper('OrderWorkshop');

        /** @var Management_Service_DepotSetting $depotSettingsService */
        $depotSettingsService = $this->getService('Management_Service_DepotSetting');

        /** @var BAS_Shared_Model_OrderWorkshop $workorder */
        $workorder = $workorderMapper->findByWorkorderId($workorderId);
        $mainAccountingDepotId = $depotSettingsService->getMainAccountingDepot($workorder->getDepotId());

        return $workorderMapper->getWorkorderReportGeneralData($workorderId, $mainAccountingDepotId);
    }

    /**
     * @param int $workorderId
     * @param int $userId
     * @throws BAS_Shared_Exception
     * @throws Exception
     */
    public function pickOrder($workorderId, $userId)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $db = $this->getDb();

        try {
            $db->beginTransaction();

            $workorderDetailService->changeWorkorderItemsStatus(
                $workorderId,
                BAS_Shared_Model_Workshop_OrderWorkshopDetail::ITEM_PICKED_TO_BE_PICKED,
                $userId,
                BAS_Shared_Model_Workshop_OrderWorkshopDetail::ITEM_PICKED_ORDER
            );

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }
    }

    /**
     * @param int $workorderId
     * @return array
     */
    public function getProformaInvoiceData($workorderId)
    {
        /** @var Order_Service_VatTypeList $orderVatTypeService */
        $orderVatTypeService = $this->getService('Order_Service_VatTypeList');
        /** @var Warehouse_Service_OrderItemInvoice $orderItemInvoiceService */
        $orderItemInvoiceService = $this->getService('Warehouse_Service_OrderItemInvoice');

        $workorderGeneralData = $this->getWorkorderReportGeneralData($workorderId);

        $depotId = (int)$workorderGeneralData['depotId'];
        $contactAddressId = (int)$workorderGeneralData['invoiceContactAddressId'];

        /** @var BAS_Shared_Model_ListVatTypeMapper $listVatTypeMapper */
        $listVatTypeMapper = $this->getMapper('ListVatType');

        $vatTypeList = $listVatTypeMapper->findAllByCondition([
            'id IN (?)' => [
                BAS_Shared_Model_ListVatType::VAT_TYPE_DOMESTIC_HIGH,
                BAS_Shared_Model_ListVatType::VAT_TYPE_INTRA_COMMUNITY,
                BAS_Shared_Model_ListVatType::VAT_TYPE_OUTSIDE_COMMUNITY,
                BAS_Shared_Model_ListVatType::VAT_TYPE_INTRA_COMPANY,
            ]
        ]);
        $vatTypeList = BAS_Shared_Utils_Array::listData($vatTypeList, 'id', 'name');

        $vatTypeSelected = 0 !== $contactAddressId
            ? $orderVatTypeService->getListVatTypeByContactAddressAndDepotId($contactAddressId, $depotId)
            : null;

        foreach ($vatTypeList as $invoiceVatTypeId => $vatTypeName) {
            $vatRate = (float)$orderItemInvoiceService->getVatRate($invoiceVatTypeId, $depotId, $workorderGeneralData['statusOrderDate']);
            $vatTypeList[$invoiceVatTypeId] = sprintf('%s %.1f%%', $vatTypeName, $vatRate);
        }
        return array_merge($workorderGeneralData, [
            'vatTypeList' => $vatTypeList,
            'vatTypeSelected' => $vatTypeSelected,
        ]);
    }

    /**
     * @param $workorderId
     * @return array|bool
     * @throws BAS_Shared_Exception
     */
    public function getFinalInvoiceData($workorderId)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');
        /** @var Order_Service_VatTypeList $vatTypeService */
        $vatTypeService = $this->getService('Order_Service_VatTypeList');

        $workorder = $workorderService->findById($workorderId);
        $workorderGeneralData = $this->getWorkorderReportGeneralData($workorder->getWorkorderId());

        $selectedVatType = $workorder->getInvoiceSelectedVatType() === null ? null :
            $vatTypeService->findById($workorder->getInvoiceSelectedVatType());

        $suggestedVatType = $workorder->getInvoiceSuggestedVatType() === null ? null :
            $vatTypeService->findById($workorder->getInvoiceSuggestedVatType());

        $workorderGeneralData = array_merge($workorderGeneralData, [
            'selectedVatType' => $selectedVatType,
            'suggestedVatType' => $suggestedVatType,
        ]);

        return $workorderGeneralData;
    }

    /**
     * @param int $workorderId
     * @param string $languageCode
     * @param bool $printHeaderFooter
     * @param string $extraInformation
     * @param integer $userId
     * @param integer $activeDepotId
     * @return string|false
     * @throws BAS_Shared_Exception
     */
    public function generateServiceOffers($workorderId, $languageCode, $printHeaderFooter, $extraInformation, $userId, $activeDepotId = null)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        $workorder = $workorderService->findById($workorderId);

        $externalOfferFile = $this->generateServiceOffer(
            $workorderId,
            BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL,
            $languageCode,
            $printHeaderFooter,
            $workorder->getDepotId(),
            $userId,
            $activeDepotId   
        );

        $internalOfferFile = $this->generateServiceOffer(
            $workorderId,
            BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL,
            $languageCode,
            $printHeaderFooter,
            $workorder->getDepotId(),
            $userId,
            $activeDepotId
        );

        $workorderService->update(['offerExtraInformation' => $extraInformation], $workorderId, $userId);

        return [
            'externalOfferFile' => $externalOfferFile,
            'internalOfferFile' => $internalOfferFile,
        ];
    }

    /**
     * @param int $workorderId
     * @param int $invoiceBookingType
     * @param string $languageCode
     * @param int $printHeaderFooter
     * @param int $depotId
     * @param int $userId
     * @param integer $activeDepotId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile|null
     * @throws BAS_Shared_Exception
     */
    public function generateServiceOffer($workorderId, $invoiceBookingType, $languageCode, $printHeaderFooter, $depotId, $userId, $activeDepotId = null)
    {
        /** @var Workshop_Service_OrderWorkshopFile $workorderFileService */
        $workorderFileService = $this->getService('Workshop_Service_OrderWorkshopFile');
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        if (!$workorderService->containsDetailsWithInvoiceBookingType($workorderId, $invoiceBookingType)) {
            return null;
        }

        $formatter = new BAS_Shared_Report_Workorder_Offer_Formatter();
        $dataRetriever = new BAS_Shared_Report_Workorder_Offer_DataRetriever($workorderId, $languageCode, $depotId, $invoiceBookingType, $activeDepotId);
        $templateProcessorWithHeaderAndFooter = $this->getOfferTemplateProcessor($invoiceBookingType, true, false, $languageCode);
        $reportService = $this->getReportService($dataRetriever, $templateProcessorWithHeaderAndFooter, $formatter);

        $report = $reportService->generate();
        $reportInfo = $this->sendReportFile($report, $this->getOfferFileName($invoiceBookingType), $workorderId);

        $reportFile = $workorderFileService->save(new BAS_Shared_Model_Workshop_OrderWorkshopFile([
            'workorderId' => $workorderId,
            'type' => BAS_Shared_Model_Workshop_OrderWorkshopFile::FILE_TYPE_SERVICE_OFFER,
            'fileName' => $reportInfo['fileName'],
            'title' => BAS_Shared_Model_Workshop_OrderWorkshopFile::FILE_TYPE_SERVICE_OFFER,
            'token' => $reportInfo['token'],
            'createdAt' => new Zend_Db_Expr('NOW()'),
            'createdBy' => $userId,
        ]));

        /**
         * We always store documents with header and footer, but if user picked printing without them, we need to generate new one
         * and return this newly generated doc. No need to store it in db. Only applicable for external invoice
         */
        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL && $printHeaderFooter === false) {
            $templateProcessorWithoutHeaderAndFooter = $this->getOfferTemplateProcessor($invoiceBookingType, false, false, $languageCode);
            $reportService->setTemplateProcessor($templateProcessorWithoutHeaderAndFooter);

            $previewReport = $reportService->generate();
            $previewReportInfo = $this->sendReportFile($previewReport, 'Offer_preview', $workorderId);
            $reportFile = new BAS_Shared_Model_Workshop_OrderWorkshopFile([
                'token' => $previewReportInfo['token'],
                'fileName' => $previewReportInfo['fileName'],
            ]);
        }

        return $reportFile;
    }

    /**
     * @param int $invoiceBookingType
     * @param bool $printHeaderFooter
     * @param bool $isPreview
     * @param string $languageCode
     * @return BAS_Shared_Report_TemplateProcessorInterface
     * @throws BAS_Shared_Exception
     */
    private function getOfferTemplateProcessor($invoiceBookingType, $printHeaderFooter, $isPreview, $languageCode)
    {
        switch ($invoiceBookingType) {
            case BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL:
                return new BAS_Shared_Report_Workorder_InternalOffer_TemplateProcessor($printHeaderFooter, $isPreview, $languageCode);
            case BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL:
                return new BAS_Shared_Report_Workorder_Offer_TemplateProcessor($printHeaderFooter, $isPreview, $languageCode);
            default: throw new BAS_Shared_Exception('Invalid depot booking code');
        }
    }

    /**
     * @param int $invoiceBookingType
     * @return string
     * @throws BAS_Shared_Exception
     */
    private function getOfferFileName($invoiceBookingType)
    {
        switch ($invoiceBookingType) {
            case BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL: $reportFileName = 'Internal'; break;
            case BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL: $reportFileName = 'External'; break;
            default: throw new BAS_Shared_Exception('Invalid depot booking code');
        }

        $reportFileName .= 'ServiceOffer' . date('His');

        return $reportFileName;
    }

    /**
     * @param int $workorderId
     * @param int $depotId
     * @param int $userId
     * @param string $languageCode
     * @return BAS_Shared_Model_OrderFile|bool
     */
    public function previewPickOrderReport($workorderId, $languageCode, $depotId, $userId)
    {
        $reportService = $this->getReportService(
            new BAS_Shared_Report_Workorder_PickOrder_DataRetriever($workorderId, $languageCode, $depotId, $userId),
            new BAS_Shared_Report_Workorder_PickOrder_TemplateProcessor(true, false, $languageCode),
            new BAS_Shared_Report_Workorder_PickOrder_Formatter($depotId)
        );

        $reportService->preview();
    }

    /**
     * @param int $workorderId
     * @param string $languageCode
     * @param int $vatType
     * @param bool $printHeaderFooter
     * @param string $extraInformation
     * @param int $userId
     * @param integer $activeDepotId
     * @return array
     * @throws BAS_Shared_Exception
     * @throws Exception
     */
    public function generateProformaInvoices($workorderId, $languageCode, $vatType, $printHeaderFooter, $extraInformation, $userId, $activeDepotId = null)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        $workorder = $workorderService->findById($workorderId);

        $externalProformaFile = $this->generateProformaInvoice(
            $workorderId,
            $languageCode,
            $vatType,
            BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL,
            $printHeaderFooter,
            $workorder->getDepotId(),
            $userId,
            $activeDepotId
        );

        $internalProformaFile = $this->generateProformaInvoice(
            $workorderId,
            $languageCode,
            $vatType,
            BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL,
            $printHeaderFooter,
            $workorder->getDepotId(),
            $userId,
            $activeDepotId
        );

        $workorderService->update([
            'offerExtraInformation' => $extraInformation,
            'vatType' => $vatType
        ], $workorderId, $userId);

        return [
            'externalProformaFile' => $externalProformaFile,
            'internalProformaFile' => $internalProformaFile,
        ];
    }

    /**
     * @param int $workorderId
     * @param string $languageCode
     * @param int $vatType
     * @param int $invoiceBookingType
     * @param bool $printHeaderFooter
     * @param int $depotId
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile|null
     */
    public function generateProformaInvoice($workorderId, $languageCode, $vatType, $invoiceBookingType, $printHeaderFooter, $depotId, $userId, $activeDepotId)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');
        /** @var Workshop_Service_OrderWorkshopFile $workorderFileService */
        $workorderFileService = $this->getService('Workshop_Service_OrderWorkshopFile');

        if (!$workorderService->containsDetailsWithInvoiceBookingType($workorderId, $invoiceBookingType)) {
            return null;
        }

        $formatter = new BAS_Shared_Report_Workorder_ProformaInvoice_Formatter();
        $dataRetriever = new BAS_Shared_Report_Workorder_ProformaInvoice_DataRetriever($workorderId, $languageCode, $depotId, $invoiceBookingType, $vatType, $activeDepotId);
        $templateProcessorWithHeaderAndFooter = $this->getProformaInvoiceTemplateProcessor($invoiceBookingType, true, false, $languageCode);

        $reportService = $this->getReportService($dataRetriever, $templateProcessorWithHeaderAndFooter, $formatter);

        $report = $reportService->generate();
        $reportInfo = $this->sendReportFile($report, $this->getProformaInvoiceFileName($invoiceBookingType), $workorderId);

        $reportFile = $workorderFileService->save(new BAS_Shared_Model_Workshop_OrderWorkshopFile([
            'workorderId' => $workorderId,
            'type' => BAS_Shared_Model_Workshop_OrderWorkshopFile::FILE_TYPE_INVOICE_PROFORMA,
            'fileName' => $reportInfo['fileName'],
            'title' => BAS_Shared_Model_Workshop_OrderWorkshopFile::FILE_TYPE_INVOICE_PROFORMA,
            'token' => $reportInfo['token'],
            'createdAt' => date('Y-m-d H:i:s'),
            'createdBy' => $userId,
        ]));

        /**
         * We always store documents with header and footer, but if user picked printing without them, we need to generate new one
         * and return this newly generated doc. No need to store it in db. Only applicable for external invoice
         */
        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL && $printHeaderFooter === false) {
            $templateProcessorWithoutHeaderAndFooter = $this->getProformaInvoiceTemplateProcessor($invoiceBookingType, false, false, $languageCode);
            $reportService->setTemplateProcessor($templateProcessorWithoutHeaderAndFooter);

            $previewReport = $reportService->generate();
            $previewReportInfo = $this->sendReportFile($previewReport, 'ProformaInvoice_preview', $workorderId);
            $reportFile = new BAS_Shared_Model_Workshop_OrderWorkshopFile([
                'token' => $previewReportInfo['token'],
                'fileName' => $previewReportInfo['fileName'],
            ]);
        }

        return $reportFile;
    }

    /**
     * @param int $invoiceBookingType
     * @param bool $printHeaderFooter
     * @param bool $isPreview
     * @param string $languageCode
     * @return BAS_Shared_Report_TemplateProcessorInterface
     * @throws BAS_Shared_Exception
     */
    private function getProformaInvoiceTemplateProcessor($invoiceBookingType, $printHeaderFooter, $isPreview, $languageCode)
    {
        switch ($invoiceBookingType) {
            case BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL:
                return new BAS_Shared_Report_Workorder_InternalProformaInvoice_TemplateProcessor($printHeaderFooter, $isPreview, $languageCode);
            case BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL:
                return new BAS_Shared_Report_Workorder_ProformaInvoice_TemplateProcessor($printHeaderFooter, $isPreview, $languageCode);
            default: throw new BAS_Shared_Exception('Invalid depot booking code');
        }
    }

    /**
     * @param int $invoiceBookingType
     * @return string
     * @throws BAS_Shared_Exception
     */
    private function getProformaInvoiceFileName($invoiceBookingType)
    {
        switch ($invoiceBookingType) {
            case BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL: $reportFileName = 'Internal'; break;
            case BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL: $reportFileName = 'External'; break;
            default: throw new BAS_Shared_Exception('Invalid depot booking code');
        }

        $reportFileName .= 'ProformaInvoice' . date('His');

        return $reportFileName;
    }

    /**
     * @param int $workorderId
     * @param string $extraInformation
     * @param string $languageCode
     * @param bool $printHeaderFooter
     * @param int $userId
     * @param integer $activeDepotId
     * @return array
     * @throws Exception
     */
    public function generateFinalInvoices(
        $workorderId,
        $extraInformation,
        $languageCode,
        $printHeaderFooter,
        $userId,
        $activeDepotId = null)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');
        /** @var Stock_Service_StockDefects $stockDefectsService */
        $stockDefectsService = $this->getService('Stock_Service_StockDefects');
        /** @var Order_Service_OrderVehicle $orderVehicleService */
        $orderVehicleService = $this->getService('Order_Service_OrderVehicle');
        /** @var BAS_Shared_Validate_WorkorderInvoiceValidator $workorderInvoiceValidator */
        $workorderInvoiceValidator = $this->getService('BAS_Shared_Validate_WorkorderInvoiceValidator');

        $workorder = $workorderService->findById($workorderId);
        $depotId = $workorder->getDepotId();
        $externalReportFile = null;
        $internalReportFile = null;

        if ($workorderInvoiceValidator->canBeInvoiced($workorder) === false) {
            throw new BAS_Shared_Exception($workorderInvoiceValidator->getComposedErrorMessage());
        }

        $externalReport = $this->generateFinalInvoiceReport(
            $workorder,
            $languageCode,
            $printHeaderFooter,
            BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL,
            $depotId,
            $activeDepotId
        );

        $internalReport = $this->generateFinalInvoiceReport(
            $workorder,
            $languageCode,
            $printHeaderFooter,
            BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL,
            $depotId,
            $activeDepotId
        );

        $db = $this->getDb();

        try {
            $db->beginTransaction();

            if ($workorderInvoiceValidator->canBeInvoiced($workorder) === false) {
                throw new BAS_Shared_Exception($workorderInvoiceValidator->getComposedErrorMessage());
            }

            $workorderService->update([
                'offerExtraInformation' => $extraInformation,
                'status' => BAS_Shared_Model_OrderWorkshop::WORKORDER_INVOICED,
            ], $workorderId, $userId);

            if ($externalReport !== null) {
                $externalReportFile = $this->saveFinalInvoiceFile(
                    $externalReport,
                    BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL,
                    $workorderId,
                    $userId
                );
                $externalInvoice = $this->createFinalInvoice(
                    $workorder,
                    $languageCode,
                    $externalReportFile->getId(),
                    BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL,
                    $depotId,
                    $userId
                );
            }

            if ($internalReport !== null) {
                $internalReportFile = $this->saveFinalInvoiceFile(
                    $internalReport,
                    BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL,
                    $workorderId,
                    $userId
                );
                $internalInvoice = $this->createFinalInvoice(
                    $workorder,
                    $languageCode,
                    $internalReportFile->getId(),
                    BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL,
                    $depotId,
                    $userId
                );
            }

            if ($workorder->getType() === BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT) {
                $stockDefectsService->updateWorkorderDefectPreparationWorkshop(
                    $workorder->getWorkorderId(),
                    $workorder->getType(),
                    BAS_Shared_Model_StockDefects::PREP_WORKSHOP_READY
                );
            }

            if ($workorder->getType() === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER && $workorder->getVehicleId()) {
                $orderVehicleService->updateOrderVehiclePreparationWorkshopStatus($workorder->getVehicleId());
                $orderVehicleService->updateOrderVehicleLocation($workorder->getVehicleId());
            }

            $db->commit();
        } catch (Exception $e) {
            $db->rollBack();
            throw $e;
        }

        return [
            'externalInvoiceFile' => $externalReportFile,
            'internalInvoiceFile' => $internalReportFile,
        ];
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param string $languageCode
     * @param bool $printHeaderFooter
     * @param int $invoiceBookingType
     * @param int $depotId
     * @param integer $activeDepotId
     * @return string|null
     * @throws BAS_Shared_Exception
     */
    public function generateFinalInvoiceReport(
        BAS_Shared_Model_OrderWorkshop $workorder,
        $languageCode,
        $printHeaderFooter,
        $invoiceBookingType,
        $depotId,
        $activeDepotId
    ) {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        if (!$workorderService->containsDetailsWithInvoiceBookingType($workorder->getWorkorderId(), $invoiceBookingType)) {
            return null;
        }

        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL) {
            $printHeaderFooter = false;
        }

        $dataRetriever = new BAS_Shared_Report_Workorder_Invoice_DataRetriever($workorder->getWorkorderId(), $languageCode, $depotId, $invoiceBookingType, $activeDepotId);
        $formatter = new BAS_Shared_Report_Workorder_BaseFormatter();
        $templateProcessor = $invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL
            ? new BAS_Shared_Report_Workorder_InternalInvoice_TemplateProcessor($printHeaderFooter, false, $languageCode)
            : new BAS_Shared_Report_Workorder_Invoice_TemplateProcessor($printHeaderFooter, false, $languageCode);

        $reportService = $this->getReportService(
            $dataRetriever,
            $templateProcessor,
            $formatter
        );

        $rawInvoice = $reportService->generate();

        return $rawInvoice;
    }

    /**
     * @param text $contentHtml
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param string $languageCode
     * @param bool $printHeaderFooter
     * @param int $invoiceBookingType
     * @param int $depotId
     * @return string|null
     * @throws BAS_Shared_Exception
     */
    public function generateFinalInvoiceReportFromContentHtml($contentHtml,
        BAS_Shared_Model_OrderWorkshop $workorder,
        $languageCode,
        $printHeaderFooter,
        $invoiceBookingType,
        $depotId
    ) {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        if (!$workorderService->containsDetailsWithInvoiceBookingType($workorder->getWorkorderId(), $invoiceBookingType)) {
            return null;
        }

        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL) {
            $printHeaderFooter = false;
        }

        $dataRetriever = new BAS_Shared_Report_Workorder_Invoice_DataRetriever($workorder->getWorkorderId(), $languageCode, $depotId, $invoiceBookingType);
        $formatter = new BAS_Shared_Report_Workorder_BaseFormatter();
        $templateProcessor = $invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL
            ? new BAS_Shared_Report_Workorder_InternalInvoice_TemplateProcessor($printHeaderFooter, false, $languageCode)
            : new BAS_Shared_Report_Workorder_Invoice_TemplateProcessor($printHeaderFooter, false, $languageCode);

        $reportService = $this->getReportService(
            $dataRetriever,
            $templateProcessor,
            $formatter
        );

        $rawInvoice = $reportService->generateFromContentHtml($contentHtml);

        return $rawInvoice;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param string $languageCode
     * @param bool $printHeaderFooter
     * @param int $invoiceBookingType
     * @param int $depotId
     * @return string|null
     * @throws BAS_Shared_Exception
     */
    public function generateFinalInvoiceReportHtml(
        BAS_Shared_Model_OrderWorkshop $workorder,
        $languageCode,
        $printHeaderFooter,
        $invoiceBookingType,
        $depotId
    ) {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        if (!$workorderService->containsDetailsWithInvoiceBookingType($workorder->getWorkorderId(), $invoiceBookingType)) {
            return null;
        }

        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL) {
            $printHeaderFooter = false;
        }

        $dataRetriever = new BAS_Shared_Report_Workorder_Invoice_DataRetriever($workorder->getWorkorderId(), $languageCode, $depotId, $invoiceBookingType);
        $formatter = new BAS_Shared_Report_Workorder_BaseFormatter();
        $templateProcessor = $invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL
            ? new BAS_Shared_Report_Workorder_InternalInvoice_TemplateProcessor($printHeaderFooter, false, $languageCode)
            : new BAS_Shared_Report_Workorder_Invoice_TemplateProcessor($printHeaderFooter, false, $languageCode);

        $reportService = $this->getReportService(
            $dataRetriever,
            $templateProcessor,
            $formatter
        );

        return $reportService->generateReportHtml();
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param string $languageCode
     * @param int $invoiceFileId
     * @param int $invoiceBookingType
     * @param int $depotId
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract
     * @throws BAS_Shared_Exception
     */
    public function createFinalInvoice(
        BAS_Shared_Model_OrderWorkshop $workorder,
        $languageCode,
        $invoiceFileId,
        $invoiceBookingType,
        $depotId,
        $userId
    ) {
        /** @var Workshop_Service_WorkorderInvoice $workorderInvoiceService */
        $workorderInvoiceService = $this->getService('Workshop_Service_WorkorderInvoice');
        /** @var Workshop_Service_OrderWorkshopDetail $workorderDetailService */
        $workorderDetailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $invoiceTotalAmount = $this->getTotalExclVat($workorder, $invoiceBookingType);
        $invoiceType = $this->getInvoiceType($invoiceTotalAmount);

        $invoice = $workorderInvoiceService->createInvoice(
            $workorder->getWorkorderId(),
            $invoiceType,
            $invoiceFileId,
            $languageCode,
            $invoiceBookingType,
            $depotId,
            $userId
        );

        $invoiceLogs = $workorderInvoiceService->createInvoiceLogs(
            $invoice,
            $workorder->getWorkorderId(),
            $invoiceBookingType
        );

        $workorderDetailService->markWorkorderDetailsAsDelivered($workorder->getWorkorderId());

        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL) {
            $workorderInvoiceService->insertWorkOrdersAmountIntoStockTransaction($invoice, $invoiceLogs, $workorder);
        }

        return $invoice;
    }

    /**
     * @param float $totalAmount
     * @return string
     */
    private function getInvoiceType($totalAmount)
    {
        return $totalAmount >= 0
            ? BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::TYPE_INVOICE
            : BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::TYPE_CREDIT_INVOICE;
    }

    /**
     * @param $workorder
     * @param $invoiceBookingType
     * @return float|int|string
     * @throws BAS_Shared_Exception
     */
    private function getTotalExclVat($workorder, $invoiceBookingType)
    {
        /** @var Workshop_Service_Workorder $workorderService */
        $workorderService = $this->getService('Workshop_Service_Workorder');

        $total = 0;

        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL) {
            $total = $workorderService->getTotalExclVat($workorder, BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_EXTERNAL);
        } elseif ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL) {
            $total = BAS_Shared_Utils_Math::add(
                $workorderService->getTotalExclVat($workorder, BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_INTERNAL),
                $workorderService->getTotalExclVat($workorder, BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_GUARANTEE),
                BAS_Shared_Utils_Math::SCALE_4
            );
        }

        return $total;
    }

    /**
     * @param BAS_Shared_Report_DataRetrieverInterface $dataRetriever
     * @param BAS_Shared_Report_TemplateProcessorInterface $templateProcessor
     * @param BAS_Shared_Report_FormatterInterface $formatter
     * @return BAS_Shared_Report_ReportService
     */
    protected function getReportService(
        BAS_Shared_Report_DataRetrieverInterface $dataRetriever,
        BAS_Shared_Report_TemplateProcessorInterface $templateProcessor,
        BAS_Shared_Report_FormatterInterface $formatter
    ) {
        return new BAS_Shared_Report_ReportService($dataRetriever, $templateProcessor, $formatter);
    }

    /**
     * @param string $report
     * @param string $fileName
     * @param int $workorderId
     * @return array
     * @throws BAS_Shared_Exception
     */
    public function sendReportFile($report, $fileName, $workorderId)
    {
        /** @var BAS_Shared_Service_FileService $fileService */
        $fileService = $this->getService('BAS_Shared_Service_FileService');

        $uploadFileName = sprintf('Workorder_%s_%d_%s.pdf', $fileName, $workorderId, date('Ymd'));
        $token = $fileService->sendFile($uploadFileName, $report);

        if ($token === null) {
            throw new BAS_Shared_Exception('FileService::sendFile failed');
        }

        return [
            'fileName' => $uploadFileName,
            'token' => $token,
            'url' => $fileService->getFileUrl($uploadFileName, $token),
        ];
    }

    /**
     * @param string $report
     * @param int $invoiceBookingType
     * @param int $workorderId
     * @param int $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     * @throws BAS_Shared_Exception
     */
    private function saveFinalInvoiceFile($report, $invoiceBookingType, $workorderId, $userId)
    {
        /** @var Workshop_Service_OrderWorkshopFile $workorderFileService */
        $workorderFileService = $this->getService('Workshop_Service_OrderWorkshopFile');

        $invoiceFileName = $invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL ? 'External' : 'Internal';
        $invoiceFileName = $invoiceFileName . '_Final_Invoice';
        $invoiceFileData = $this->sendReportFile($report, $invoiceFileName, $workorderId);

        $invoiceFile = $workorderFileService->save(new BAS_Shared_Model_Workshop_OrderWorkshopFile([
            'workorderId' => $workorderId,
            'type' => BAS_Shared_Model_Workshop_OrderWorkshopFile::FILE_TYPE_INVOICE,
            'fileName' => $invoiceFileData['fileName'],
            'title' => BAS_Shared_Model_Workshop_OrderWorkshopFile::FILE_TYPE_INVOICE,
            'token' => $invoiceFileData['token'],
            'createdAt' => date('Y-m-d H:i:s'),
            'createdBy' => $userId,
        ]));

        return $invoiceFile;
    }
}
