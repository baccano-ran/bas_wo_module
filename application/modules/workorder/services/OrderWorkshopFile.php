<?php

/**
 * Class Workshop_Service_OrderWorkshopFile
 */
class Workshop_Service_OrderWorkshopFile extends BAS_Shared_Service_Abstract
{
    /**
     * @param int $id
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     * @throws BAS_Shared_NotFoundException
     */
    public function findById($id)
    {
        $workorderFileMapper = $this->getOrderWorkshopFileMapper();
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopFile $workorderFile */
        $workorderFile = $workorderFileMapper->findOneByCondition([
            'id = ?' => $id,
        ]);

        return $workorderFile;
    }

    /**
     * @param int $orderWorkshopId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     * @throws BAS_Shared_NotFoundException
     */
    public function findByOrderWorkshopId($orderWorkshopId)
    {
        $workorderFileMapper = $this->getOrderWorkshopFileMapper();
        /** @var BAS_Shared_Model_Workshop_OrderWorkshopFile $workorderFile */
        $workorderFile = $workorderFileMapper->findOneByCondition([
            'workorder_id = ?' => $orderWorkshopId,
        ]);

        return $workorderFile;
    }
    /**
     * @param int $workorderId
     * @param int $type
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile|bool
     */
    public function findByWorkorderIdAndType($workorderId, $type)
    {
        $workorderFileMapper = $this->getOrderWorkshopFileMapper();

        try {
            /** @var BAS_Shared_Model_Workshop_OrderWorkshopFile $workorderFile */
            $workorderFile = $workorderFileMapper->findOneByCondition([
                'workorder_id = ?' => $workorderId,
                'type = ?' => $type,
            ]);

            return $workorderFile;
        } catch (BAS_Shared_NotFoundException $e) {
            return false;
        }
    }

    /**
     * @param int $workorderId
     * @param int $depotId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     * @throws BAS_Shared_NotFoundException
     */
    public function findByWorkorderIdAndDepot($workorderId, $depotId)
    {
        $workorderFileMapper = $this->getOrderWorkshopFileMapper();

        return $workorderFileMapper->findByWorkorderIdAndDepot($workorderId, $depotId);
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopFile $workorderFile
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function save(BAS_Shared_Model_Workshop_OrderWorkshopFile $workorderFile)
    {
        if (!$workorderFile->getWorkorderId()) {
            throw new InvalidArgumentException('The WorkorderId property must be set to save file');
        }

        $workorderFile->setArchived(BAS_Shared_Model_Workshop_OrderWorkshopFile::NOT_ARCHIVED);

        return $this->getOrderWorkshopFileMapper()->saveModel($workorderFile);
    }


    /**
     * @param int $orderWorkshopId
     * @param int $depotId
     * @return Zend_Db_Select
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function getGridSource($orderWorkshopId, $depotId = BAS_Shared_Model_Depot::BAS_DEPOT)
    {
        $filter = new Zend_Filter_Input([
        ], [
            'orderWorkshopId' => ['Int', new Zend_Validate_GreaterThan(0)],
            'depotId'         => ['Int', new Zend_Validate_GreaterThan(0)],
        ], [
            'orderWorkshopId' => $orderWorkshopId,
            'depotId'         => $depotId,
        ]);

        if ($filter->hasInvalid()) {
            $invalidParamList = $filter->getInvalid();
            foreach ($invalidParamList as $paramName => $paramErrorData) {
                throw new BAS_Shared_InvalidArgumentException(sprintf('Invalid "%s" specified (%s)', $paramName, implode(', ', array_values($paramErrorData))));
            }
        }

        $mapper = $this->getOrderWorkshopFileMapper();

        return $mapper->getGridSelect($orderWorkshopId, $depotId);
    }

    /**
     * @param int $orderWorkshopId
     * @param int $depotId
     * @return array
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function getCreatedByOptionsByOrderWorkshopId($orderWorkshopId, $depotId = BAS_Shared_Model_Depot::BAS_DEPOT)
    {
        $filter = new Zend_Filter_Input([
        ], [
            'orderWorkshopId' => ['Int', new Zend_Validate_GreaterThan(0)],
            'depotId'         => ['Int', new Zend_Validate_GreaterThan(0)],
        ], [
            'orderWorkshopId' => $orderWorkshopId,
            'depotId'         => $depotId,
        ]);

        if ($filter->hasInvalid()) {
            $invalidParamList = $filter->getInvalid();
            foreach ($invalidParamList as $paramName => $paramErrorData) {
                throw new BAS_Shared_InvalidArgumentException(sprintf('Invalid "%s" specified (%s)', $paramName, implode(', ', array_values($paramErrorData))));
            }
        }

        $mapper = $this->getOrderWorkshopFileMapper();

        return BAS_Shared_Utils_Array::listData($mapper->getCreatedByOptionsByorderWorkshopId($orderWorkshopId, $depotId), 'id', 'created_by_abbr');
    }

    /**
     * @param int $orderWorkshopId
     * @return array
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function getFileTypeOptionsByOrderWorkshopId($orderWorkshopId)
    {
        $filter = new Zend_Filter_Input([
        ], [
            'orderWorkshopId' => ['Int', new Zend_Validate_GreaterThan(0)],
        ], [
            'orderWorkshopId' => $orderWorkshopId,
        ]);

        if ($filter->hasInvalid()) {
            $invalidParamList = $filter->getInvalid();
            foreach ($invalidParamList as $paramName => $paramErrorData) {
                throw new BAS_Shared_InvalidArgumentException(sprintf('Invalid "%s" specified (%s)', $paramName, implode(', ', array_values($paramErrorData))));
            }
        }

        $mapper = $this->getOrderWorkshopFileMapper();

        return BAS_Shared_Utils_Array::listData($mapper->getFileTypeOptionsByOrderWorkshopId($orderWorkshopId), 'id', 'type');
    }

    /**
     * @param int $id
     * @return int
     */
    public function delete($id)
    {
        $orderFileMapper = $this->getOrderWorkshopFileMapper();

        return $orderFileMapper->deleteByCondition(['id = ?' => $id]);
    }

    /**
     * @param int $id
     * @return int
     */
    public function archiveFile($id)
    {
        return $this->getOrderWorkshopFileMapper()->updateFieldsByCondition(
            ['archived' => BAS_Shared_Model_Workshop_OrderWorkshopFile::ARCHIVED],
            [
                'id' => $id,
            ]
        );
    }

    /**
     * @param int $id
     * @return int
     */
    public function setFileIsRead($id)
    {
        return $this->getOrderWorkshopFileMapper()->updateFieldsByCondition(
            ['is_read' => BAS_Shared_Model_Workshop_OrderWorkshopFile::IS_READ],
            [
                'id' => $id,
            ]
        );
    }

    /**
     * Filtering and sanitation of filenames
     *
     * @param string $filename
     * @return string
     */
    public static function cleanFilename($filename)
    {
        $cleanFilename = $filename;
        $cleanFilename = strtolower($cleanFilename);
        $cleanFilename = str_replace(['%20', '+', '%2B', ' '], '_', $cleanFilename);
        $cleanFilename = str_replace(['"', '\'', '%', '-', '+', '|', '!', '?', '#'], '', $cleanFilename);
        $cleanFilename = filter_var($cleanFilename, FILTER_SANITIZE_STRING, [
            FILTER_FLAG_NO_ENCODE_QUOTES,
            FILTER_FLAG_STRIP_LOW,
            FILTER_FLAG_STRIP_HIGH,
            FILTER_FLAG_ENCODE_LOW,
            FILTER_FLAG_ENCODE_HIGH,
            FILTER_FLAG_ENCODE_AMP,
        ]);

        return $cleanFilename;
    }

    /**
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFileMapper
     */
    public function getOrderWorkshopFileMapper()
    {
        return $this->getMapper('Workshop_OrderWorkshopFile');
    }
}