<?php

/**
 * Class Workshop_Service_OrderWorkshopDetailImage
 */
class Workshop_Service_OrderWorkshopDetailImage extends BAS_Shared_Service_Abstract
{

    /**
     * @var StdClass
     */
    private $serviceConfig;

    /**
     * Workshop_Service_OrderWorkshopDetailImage constructor.
     * @param array $serviceConfig
     */
    public function __construct(array $serviceConfig = [])
    {
        $this->serviceConfig = (object)$serviceConfig;
    }

    /**
     * @param int $detailId
     * @return array
     */
    public function getImages($detailId)
    {
        /** @var Default_Service_Image $imageService */
        $imageService = $this->getService('Default_Service_Image');

        $imagesBaseName = $this->getImagesBaseName($detailId);
        $uploadPath = $this->getUploadPath($detailId);

        return $imageService->getReferenceImages($uploadPath, $imagesBaseName . '_*.jpg');
    }

    /**
     * @param int $detailId
     * @param int $width
     * @return array
     */
    public function getWebsiteImagesUrls($detailId, $width = 100)
    {
        $detailImages = $this->getImages($detailId);
        $detailImageUrls = [];

        foreach ($detailImages as $imageIndex => $detailImage) {
            $detailImageUrls[] = sprintf('%s/photos/%d/owd_%d_%d.jpg',
                $this->getSalesManagerBaseUrl(),
                $width,
                $detailId,
                $imageIndex + 1
            );
        }

        return $detailImageUrls;
    }

    /**
     * @param int $detailId
     * @param int $userId
     * @return int
     */
    public function updateDetailImageCount($detailId, $userId)
    {
        /** @var Workshop_Service_OrderWorkshopDetail $detailService */
        $detailService = $this->getService('Workshop_Service_OrderWorkshopDetail');

        $detailImages = $this->getImages($detailId);
        $detailImageCount = count($detailImages);

        $detail = $detailService->findById($detailId);
        $detail->setImageCount($detailImageCount);

        $detailService->saveModel($detail, $userId);

        return $detailImageCount;
    }

    /**
     * @param int $detailId
     * @return string
     */
    private function getUploadPath($detailId)
    {
        $uploadPath = $this->getPathToUploadFile() . DIRECTORY_SEPARATOR . $this->getUploadDirName($detailId);

        return $uploadPath;
    }

    /**
     * @param $detailId
     * @return string
     */
    private function getUploadDirName($detailId)
    {
        return substr($this->getImagesBaseName($detailId), 0, 3);
    }

    /**
     * @return string
     * @throws BAS_Shared_Exception
     */
    private function getThumbnailsPath()
    {
        if (!isset($this->serviceConfig->path_to_thumbnail)) {
            throw new BAS_Shared_Exception('Config error: path_to_thumbnail is not specified');
        }

        return $this->serviceConfig->path_to_thumbnail;
    }

    /**
     * @return mixed
     * @throws BAS_Shared_Exception
     */
    private function getPathToUploadFile()
    {
        if (!isset($this->serviceConfig->path_to_upload_file)) {
            throw new BAS_Shared_Exception('Config error: path_to_upload_file is not specified');
        }

        return $this->serviceConfig->path_to_upload_file;
    }

    /**
     * @return mixed
     * @throws BAS_Shared_Exception
     */
    private function getSalesManagerBaseUrl()
    {
        if (!isset($this->serviceConfig->sales_manager_base_url)) {
            throw new BAS_Shared_Exception('Config error: sales_manager_base_url is not specified');
        }

        return $this->serviceConfig->sales_manager_base_url;
    }

    /**
     * @param int $detailId
     * @return string
     */
    private function getImagesBaseName($detailId)
    {
        $imageName = 'D' . $detailId;

        return $imageName;
    }

    /**
     * @param int $detailId
     * @return array
     */
    public function uploadImages($detailId)
    {
        /** @var Default_Service_Image $imageService */
        $imageService = $this->getService('Default_Service_Image');

        $transferAdapter = $this->getFileTransferAdapter();
        $filesInfo = $transferAdapter->getFileInfo();
        $imagesBaseName = $this->getImagesBaseName($detailId);
        $uploadPath = $this->getUploadPath($detailId);
        $uploadedImages = [];

        $imageService->makeDir($uploadPath);

        foreach ($filesInfo as $fileId => $info) {
            if (strpos($fileId, 'detailImages') === false || !$transferAdapter->isValid($fileId)) {
                continue;
            }

            $uploadName = $imageService->getNewUploadFileName($uploadPath, $imagesBaseName . '_%d.jpg');

            $transferAdapter->addFilter('Rename', [
                'target' => $uploadPath . DIRECTORY_SEPARATOR . $uploadName,
                'overwrite' => true
            ], $fileId);

            if ($transferAdapter->isUploaded($fileId)) {
                if ($transferAdapter->receive($fileId)) {
                    $uid = str_replace('detailImages_', '', $fileId);
                    $uploadedImages[$uid] = $uploadName;
                }
            }
        }

        return $uploadedImages;
    }

    /**
     * @param $detailId
     * @param array $postData
     * @return bool
     * @throws BAS_Shared_Exception
     * @throws Zend_File_Transfer_Exception
     */
    public function saveImages($detailId, array $postData)
    {
        $detailImages = array_key_exists('detailImages', $postData) ? (array)$postData['detailImages'] : [];
        $removedImages = array_key_exists('removedImages', $postData) ? (array)$postData['removedImages'] : [];

        $this->deleteImages($detailId, BAS_Shared_Utils_Array::listData($removedImages, null, 'filename'));
        $uploadedImages = $this->uploadImages($detailId);

        foreach ($uploadedImages as $key => $uploadedImage) {
            if (array_key_exists($key, $detailImages)) {
                $detailImages[$key]['filename'] = $uploadedImage;
            }
        }

        $imagesSequence = BAS_Shared_Utils_Array::listData($detailImages, 'sequence', 'filename');
        $this->updateImagesSequence($detailId, $imagesSequence);

        return true;
    }

    /**
     * @param int $detailId
     * @param array $imageNames
     */
    public function updateImagesSequence($detailId, array $imageNames)
    {
        /** @var Default_Service_Image $imageService */
        $imageService = $this->getService('Default_Service_Image');

        $imagesBaseName = $this->getImagesBaseName($detailId);
        $uploadPath = $this->getUploadPath($detailId);

        $sequenceChanged = $imageService->changeSequence($imageNames, $imagesBaseName, $uploadPath);

        if ($sequenceChanged) {
            $thumbnailsNamePattern = $imagesBaseName . '_?.jpg';
            $imageService->removeThumbnails($this->getThumbnailsPath(), $thumbnailsNamePattern);
        }
    }

    /**
     * @param int $detailId
     * @param array $imageNames
     */
    public function deleteImages($detailId, array $imageNames)
    {
        /** @var Default_Service_Image $imageService */
        $imageService = $this->getService('Default_Service_Image');

        foreach ($imageNames as $imageName) {
            $imageService->removeImage($this->getUploadPath($detailId), $imageName);
            $imageService->removeThumbnails($this->getThumbnailsPath(), $imageName);
        }
    }

    /**
     * @return Zend_File_Transfer_Adapter_Http
     */
    protected function getFileTransferAdapter()
    {
        return new Zend_File_Transfer_Adapter_Http();
    }

    /**
     * @param array $serviceConfig
     * @return $this
     */
    public function setServiceConfig(array $serviceConfig)
    {
        $this->serviceConfig = (object)$serviceConfig;

        return $this;
    }

}
