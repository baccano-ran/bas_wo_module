<?php


class Workshop_LabourActivityController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'add' => 'json',
            'edit' => 'json',
            'save' => 'json',
            'delete' => 'json',
        ])->initContext();

        $this->jsEntryPointMap = [
            'add' => ['action' => 'edit']
        ];
    }

    public function indexAction()
    {
        $labourActivityService = new Workshop_Service_LabourActivity();
        $labourActivityGridService = new Workshop_Service_LabourActivityGrid($this->view);

        $this->view->grid = $labourActivityGridService->getGrid($labourActivityService->getGridSource($this->getLoggedInDepotId()));
    }

    public function addAction()
    {
        $activity = new BAS_Shared_Model_Workshop_LabourActivity();
        $activity->setDepotId($this->getLoggedInDepotId());

        $this->appendJsEntryPoint();
        $this->view->assign([
            'initialData' => [
                'labourActivity' => $activity,
                'departmentOptions' => $this->getDepartmentOptions(),
            ]
        ]);
    }

    public function editAction()
    {
        $id = (int)$this->getParam('id', 0);

        $labourActivityService = new Workshop_Service_LabourActivity();
        $activity = $labourActivityService->findByIdAndDepotId($id, $this->getLoggedInDepotId());

        $this->appendJsEntryPoint();
        $this->view->assign([
            'initialData' => [
                'labourActivity' => $activity,
                'departmentOptions' => $this->getDepartmentOptions(),
            ]
        ]);
    }

    public function saveAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest() || $this->getRequest()->isGet()) {
            return;
        }

        $labourActivityService = new Workshop_Service_LabourActivity();
        $form = new Workshop_Form_LabourActivityEdit();

        if (!$form->isValid($this->getAllParams())) {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::BAD_REQUEST);
            $this->view->assign(['errors' => $form->getElementErrors()]);
            return;
        }

        $activity = $labourActivityService->createModelByData($this->getAllParams(), $this->getUserInfo()->getId());
        $activity = $labourActivityService->saveModel($activity);

        $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::OK);
        $this->view->clearVars();
        $this->view->assign([
            'id' => $activity->getId(),
            'message' => $this->translate('record_successfully_saved'),
        ]);
    }

    /**
     * @return array
     */
    private function getDepartmentOptions()
    {
        $departmentService = new Order_Service_Department();
        $departments = $departmentService->findAllByDepotId($this->getLoggedInDepotId());

        $options = [['value' => null, 'name' => $this->translate('placeholder_select_department')]];

        foreach ($departments as $department) {
            $options[] = [
                'value' => $department['id'],
                'name' => $department['name'],
            ];
        }

        return $options;
    }

}