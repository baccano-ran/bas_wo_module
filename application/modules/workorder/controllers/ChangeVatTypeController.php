<?php

/**
 * Class Workshop_ChangeVatTypeController
 */
class Workshop_ChangeVatTypeController extends Zend_Controller_Action
{
    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'process' => 'json',
        ])->initContext();
    }

    public function getAction()
    {
        $listType = $this->getParam('listType', Order_Service_VatTypeList::LIST_TYPE_ALL);
        $workorderId = (int) $this->getParam('workorderId', 0);

        $workorderService = new Workshop_Service_Workorder();
        $workorder = $workorderService->findById($workorderId);

        $vatTypeService = new Order_Service_VatTypeList();

        $this->view->assign([
            'vatTypeList' => $vatTypeService->getVatTypeList($workorder->getDepotId(), 'workshop', $listType),
            'changeVatReasonText' => $workorder->getInvoiceVatTypeChangeReason(),
            'selectedVatType' => (int)$workorder->getInvoiceSelectedVatType(),
            'workorderId' => $workorderId,
        ]);
    }

    public function processAction()
    {
        $workorderId = (int) $this->getParam('workorderId', 0);
        $vatType = (int) $this->getParam('vatType', 0);
        $reason = (string) $this->getParam('reason');

        if (empty($reason)) {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
            return;
        }

        $workorderService = new Workshop_Service_Workorder();
        $workorderService->changeVatType($workorderId, $vatType, $reason);

        $this->view->clearVars();
        $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::OK);
    }
}
