<?php

/**
 * Workshop Workorder Controller Class
 *
 * Workshop Controller Class
 * 
 * @package workshop
 * @version 1.0
 * @copyright never
 */

/**
 * It is used to list work order , delete or set the workorder
 */
class Workshop_WorkorderController extends BAS_Shared_Controller_Action_Abstract
{
    protected $_logger;

    public function init()
    {
        parent::init();
        $this->_logger = Zend_Registry::get('workorderLogger');

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'save-offer' => 'json',
            'make-order' => 'json',
            'create-workorder-on-stock-vehicle' => 'json',
            'update-workorder-deadline-date' => 'json',
            'release-lock' => 'json',
            'confirm-lock' => 'json',
            'prepare-workorders-for-print' => 'json',
        ])->initContext();
    }

    /**
     * starting index action
     * @return view for grid
     */
    public function indexAction()
    {

        $config = (Zend_Registry::get('config'));
        $vehicleId = $this->getRequest()->getParam('vehicleId');
        $orderId = $this->getRequest()->getParam('orderId');
        $orderVehicleId = $this->getRequest()->getParam('orderVehicleId');

        $orderWorkshopService = new Order_Service_OrderWorkshop();
        $userService = new Order_Service_User();
        $orderService = new Order_Service_Order($config, '');
        $orderVehicleService = new Order_Service_OrderVehicle();
        $workorderList = array();
        $workorderList = $orderWorkshopService->findByVehicleId($orderVehicleId);
        $orderData = $orderService->getOrderModelById($orderId);
        $orderVehicleInfo = $orderVehicleService->findByOrderIdAndVehicleId($vehicleId, $orderId);
        $manuallyCreatedWorkorderIds = $orderWorkshopService->getManuallyCreatedWorkorder($orderVehicleId);

        $this->view->orderVehicleInfo = $orderVehicleInfo;
        $this->view->orderData = $orderData;
        $this->view->workorderList = $workorderList;
        $this->view->userModel = $userService;
        $this->view->orderId = $orderId;
        $this->view->vehicleId = $vehicleId;
        $this->view->orderVehicleId = $orderVehicleId;
        $this->view->serviceURL = $config->api->internal_base_url;
        $this->view->extranetURL = $config->extranet->base_url;
        $this->view->manuallyCreatedWorkorderIds = $manuallyCreatedWorkorderIds;

        $this->_helper->layout()->disableLayout();
    }

    /**
     * delete workorder from order workshop
     * @return object json
     */
    public function deleteAction()
    {

        $vehicleId = $this->getRequest()->getParam('vehicleId');
        $orderId = $this->getRequest()->getParam('orderId');
        $workorderId = $this->getRequest()->getParam('workorderId');
        $orderVehicleId = $this->getRequest()->getParam('orderVehicleId');
        $orderWorkshopService = new Order_Service_OrderWorkshop();

        $as400ids = array();
        $responseArray = array();
        $inprocessWorkorders = array();

        // get workorder 
        $workorder = $orderWorkshopService->find($workorderId);
        if ($workorder->cronStatus == 1) {
            $inprocessWorkorders[] = $workorderId;
            $responseArray[] = $as400ids;
            $responseArray[] = $inprocessWorkorders;
            echo json_encode($responseArray);
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
            return;
        }

        $deleteResponse = $orderWorkshopService->deleteWorkorderAndUpdateOrderVehicleStatus(
            $workorderId,
            $vehicleId,
            $orderId,
            $orderVehicleId
        );

        $responseArray[] = $deleteResponse['as400ids'];
        $responseArray[] = $deleteResponse['inprocessWorkorders'];
        $responseArray[] = $deleteResponse['status'];
        echo json_encode($responseArray);
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
    }
    

    public function getWorkOrdersAction(){

        $vehicleId = $this->getRequest()->getParam('orderVehicleId');
        $orderWorkshopService = new Order_Service_OrderWorkshop();
        $workOrders = $orderWorkshopService->findByVehicleId($vehicleId);

        $data = [];
        foreach($workOrders as $key => $workOrder){
            $data[$workOrder->workorderId] = $workOrder->status;
        }

        $this->view->data = $data;
        $this->_helper->layout()->disableLayout();
    }

    public function vehiclesInAction()
    {
        $service = new Order_Service_OrderWorkshop();
        $depotId = BAS_Shared_Auth_Service::getActiveDepotId();
        $workorderService = new Workshop_Service_VehiclesInStock($service, $this->view);
        $workorderService->setDepotId($depotId);
        $this->view->grid = $workorderService->getGrid();
    }

    public function internalWorkorderListAction()
    {
        $service = new Order_Service_OrderWorkshop();
        $depotId = BAS_Shared_Auth_Service::getActiveDepotId();
        $workorderService = new Workshop_Service_InternalWorkorderListGrid($service, $this->view);
        $workorderService->setDepotId($depotId);
        $this->view->grid = $workorderService->getGrid();
    }

    public function openWorkorderListAction()
    {
        $service = new Order_Service_OrderWorkshop();
        $depotId = BAS_Shared_Auth_Service::getActiveDepotId();
        $workorderService = new Workshop_Service_OpenWorkorderListGrid($service, $this->getView());
        $workorderService->setDepotId($depotId);
        $this->view->grid = $workorderService->getGrid();
    }

    public function closedWorkorderListAction()
    {
        $service = new Order_Service_OrderWorkshop();
        $depotId = BAS_Shared_Auth_Service::getActiveDepotId();
        $workorderService = new Workshop_Service_ClosedWorkorderListGrid($service, $this->getView(), $this->getRequest());
        $workorderService->setDepotId($depotId);
        $this->view->grid = $workorderService->getGrid();
    }

    public function changeWorkorderStatusAction()
    {
        $orderWorkshopService = new Order_Service_OrderWorkshop();

        $workOrderId = $this->getRequest()->getParam('workOrderId');
        $status = $this->getRequest()->getParam('status');

        $orderWorkshopService->updateWorkshopStatus($workOrderId, $status);

        $this->_helper->viewRenderer->setNoRender(true);
    }
    
    /**
     * Assign product to workshop manually
     */
    public function assignProductToWorkshopAction()
    {
        /** @var BAS_Shared_Model_User $user */
        $user = Zend_Auth::getInstance()->getIdentity();
        
        $orderId = $this->getParam('orderId');
        $orderVehicleId = $this->getParam('orderVehicleId');
        $params = $this->getAllParams();
        
        $service = new Order_Service_OrderWorkshop();
        $workorderId = $service->assignProductToWorkshop(
            $orderId,
            $orderVehicleId,
            BAS_Shared_Auth_Service::getActiveDepotId(),
            $params,
            $user
        );
        $this->_helper->json(['workorderId' => $workorderId]);
    }

    
    /**
     * This action updates workorder status to 1(printed)
     */
    public function updateWorkorderStatusAction()
    {
        $workorderId = $this->getParam('workorderId');
        $orderVehicleId = $this->getParam('orderVehicleId');
        $service = new Order_Service_OrderWorkshop();
        $workshopStatus = $service->updateWorkorderStatus(
                $orderVehicleId,
                $workorderId, 
                BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED
            );
        
        $result = array();
        $result['isProductAssigned'] = TRUE;
        $result['workshopStatus'] = $workshopStatus;
        $this->_helper->json($result);
    }
    
    /**
     * delete product from workorder
     */
    public function deleteProductAction()
    {
        $orderId = $this->getParam('orderId');
        $orderVehicleId = $this->getParam('orderVehicleId');
        $productId = $this->getParam('productId');
        $workorderId = $this->getParam('workorderId');
        $service = new Order_Service_OrderWorkshop();
        $deleteProduct = $service->deleteProduct(
                $orderId,
                $orderVehicleId,
                $productId,
                $workorderId 
            );
        $this->_helper->json($deleteProduct);
    }

    /**
     * update status of external workorder
     */
    public function updateExternalWorkorderStatusAction()
    {
        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        /** @var BAS_Shared_Controller_Action_Helper_Response $responseHelper */
        $responseHelper = $this->getHelper('response');

        $workorderId = (int)$this->getParam('workorderId', 0);
        $updateData = [];

        if (0 < (int)$workorderId) {
            $orderWorkshopService = new Order_Service_OrderWorkshop();
            $updateData = $orderWorkshopService->updateExternalWorkorderStatus($workorderId);
        }

        if ([] === $updateData) {
            $responseHelper
                ->setStatusCode(BAS_Shared_Http_StatusCode::INTERNAL_SERVER_ERROR)
                ->setErrors([['title' => 'Update failed']])
                ->json();
        }

        $responseHelper
            ->setStatusCode(BAS_Shared_Http_StatusCode::OK)
            ->setData(['status' => $updateData['status']])
            ->json();
    }

    /**
     * Update workorder
     * @throws BAS_Shared_Exception
     */
    public function updateAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);
        $updateData = $this->getParam('updateData', array());

        $details = array();
        if($workorderId > 0) {
            $orderWorkshopService = new Order_Service_OrderWorkshop();
            $details = $orderWorkshopService->updateUsingPostParams($workorderId, $updateData);
        }

        $this->_helper->json($details);
    }


    /**
     * Get valid workorders
     */
    public function getWorkordersWithProductsAction() {
        /** @var BAS_Shared_Controller_Action_Helper_Response $responseHelper */
        $responseHelper = $this->getHelper('response');
        $orderVehicleId = (int)$this->getParam('orderVehicleId', 0);

        if (0 >= $orderVehicleId) {
            $responseHelper
                ->setStatusCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY)
                ->setErrors([['title' => 'Invalid order vehicle id']])
                ->json();
        }

        /** @var Order_Service_OrderWorkshop $orderWorkshopService */
        $orderWorkshopService = new Order_Service_OrderWorkshop();
        $workorderIdArray = $orderWorkshopService->getValidWorkordersForPrinting(
            $orderVehicleId,
            BAS_Shared_Auth_Service::getActiveDepotId()
        );

        $responseHelper
            ->setStatusCode(BAS_Shared_Http_StatusCode::OK)
            ->setData(['workorderWithProducts' => $workorderIdArray])
            ->json();
    }


    /**
     * update status of internal workorder
     */
    public function completeInternalWorkorderAction()
    {
        /** @var BAS_Shared_Controller_Action_Helper_Response $responseHelper */
        $responseHelper = $this->getHelper('response');

        $workorderId = (int)$this->getParam('workorderId', 0);
        $orderVehicleId = (int)$this->getParam('orderVehicleId', 0);
        $orderId = (int)$this->getParam('orderId', 0);
        $updateSuccess = false;

        if (0 < $workorderId) {
            $orderWorkshopService = new Order_Service_OrderWorkshop();
            $updateSuccess = $orderWorkshopService->completeInternalWorkorder(
                $workorderId,
                $orderVehicleId,
                $orderId
            );
        }

        if (true !== $updateSuccess) {
            $responseHelper
                ->setStatusCode(BAS_Shared_Http_StatusCode::INTERNAL_SERVER_ERROR)
                ->setErrors([['title' => 'Failed to update workshop status']])
                ->json();
        }

        $responseHelper
            ->setStatusCode(BAS_Shared_Http_StatusCode::OK)
            ->setData(['status' => BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED])
            ->json();
    }
    
    /**
     * Update workshop workorder status
     */
    public function updateWorkshopWorkorderStatusAction()
    {
        $workorderId = $this->getParam('workorderId');
        $orderVehicleId = $this->getParam('orderVehicleId');
        $status = $this->getParam('status');
        $service = new Order_Service_OrderWorkshop();
        
        if (0 >= (int)$workorderId || 0 >= (int)$orderVehicleId || 0 > (int)$status) {
            $this->_helper->json(false);
        }
        
        $result = $service->updateWorkorderStatusManually($orderVehicleId, $workorderId, $status, true);
        $this->_helper->json($result);
    }

    public function createAction()
    {
        $depotId = $this->getLoggedInDepotId();
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $workorder = $workorderService->create($userId, $depotId);

        $this->redirect('workshop/workorder/edit/id/' . $workorder->getWorkorderId());
    }

    public function editAction()
    {
        $id = (int)$this->getParam('id', 0);
        $depotId = $this->getLoggedInDepotId();
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $workorderLockService = new Payment_Service_LockingWorkorder();

        $workorder = $workorderService->findByIdAndDepot($id, $depotId);
        $workorderLock = null;

        if ($workorder) {
            $workorderLock = $workorderLockService->getActualLockForUser($userId, $workorder->getWorkorderId());
            $workorderLockService->lock($userId, $workorder->getWorkorderId());
        }

        $this->appendJsEntryPoint();
        $this->view->assign([
            'workorder' => $workorder,
            'workorderId' => $id,
            'depotId' => $depotId,
            'workorderLock' => $workorderLock,
        ]);
    }

    public function releaseLockAction()
    {
        $id = (int)$this->getParam('id', 0);
        $userId = $this->getUserInfo()->getId();

        $lockService = new Payment_Service_LockingWorkorder();
        $result = $lockService->release($userId, $id);

        $this->view->clearVars();
        $this->view->assign([
            'result' => $result
        ]);
    }

    public function confirmLockAction()
    {
        $id = (int)$this->getParam('id', 0);
        $userId = $this->getUserInfo()->getId();

        $lockService = new Payment_Service_LockingWorkorder();
        $result = $lockService->lock($userId, $id);

        $this->view->clearVars();
        $this->view->assign([
            'result' => $result
        ]);
    }

    public function makeOrderAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);
        $errorFormat = $this->getParam('errorFormat');
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $itemService = new Warehouse_Service_Item();

        $this->view->clearVars();

        try {
            $workorderService->makeOrder($workorderId, $userId);
        } catch (BAS_Shared_Exception_ItemStockException $e) {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::BAD_REQUEST);
            $error = $e->getMessage();

            if ($errorFormat !== null) {
                $item = $itemService->findById($e->getDataValue('itemId'));
                $error = strtr($this->translate($errorFormat), [
                    '<itemId>' => $item->getId(),
                    '<as400Id>' => $item->getAs400Id(),
                ]);
            }

            $this->view->error = $error;
        }
    }

    public function createWorkorderOnStockVehicleAction()
    {
        $vehicleId = (int) $this->getParam('vehicleId');
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $workorder = $workorderService->createWorkorderOnStockVehicle($vehicleId, $userId);

        /** @var BAS_Shared_View_View $view */
        $view = $this->view;
        $view->clearVars();

        $view->assign('redirectUrl', $view->url([
            'module' => 'workshop',
            'controller' => 'workorder',
            'action' => 'edit',
            'id' => $workorder->workorderId,
        ], null, true));
    }


    public function openExternalWorkorderListAction()
    {
        $workorderService = new Workshop_Service_Workorder();
        $grid = new Workshop_Service_OpenWorkorderGrid($workorderService->getOpenWorkorderListSelect(), $this->getView());

        $this->view->assign([
            'grid' => $grid->getGrid(),
        ]);
    }

    public function updateWorkorderDeadlineDateAction()
    {
        $workorderService = new Workshop_Service_Workorder();
        $workorderId = $this->getRequest()->getParam('workorderId');
        $userId = $this->getUserInfo()->getId();
        $newDeadlineDate = $this->getRequest()->getParam('newDeadlineDate');
        $oldDeadlineDate = $this->getRequest()->getParam('oldDeadlineDate');

        $resp = $workorderService->updateDeadlineDate($newDeadlineDate, $workorderId, $userId, $oldDeadlineDate);

        $this->view->clearVars();
        $this->view->assign($resp);
    }

}

