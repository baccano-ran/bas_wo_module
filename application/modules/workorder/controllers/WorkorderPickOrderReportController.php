<?php

/**
 * Class Workshop_WorkorderPickOrderReportController
 */
class Workshop_WorkorderPickOrderReportController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'preview' => 'json',
        ])->initContext();
    }

    public function previewAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $this->view->clearVars();

        $workorderReportService = new Workshop_Service_WorkorderReport();

        $workorderReportService->pickOrder($workorderId, $this->getUserInfo()->getId());
        $workorderReportService->previewPickOrderReport(
            $workorderId,
            $this->getLang(),
            $this->getLoggedInDepotId(),
            $this->getUserInfo()->getId()
        );

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
    }

}