<?php

/**
 * Class Workshop_WorkorderDetailImagesController
 */
class Workshop_WorkorderDetailImagesController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'upload' => 'json',
            'save' => 'json',
        ])->initContext();
    }

    public function indexAction()
    {
        $detailId = (int)$this->getParam('detailId');

        $detailImagesService = new Workshop_Service_OrderWorkshopDetailImage([
            'path_to_upload_file' => $this->getConfig()->workorder->images->path_to_upload_file,
        ]);

        $this->view->assign([
            'detailImages' => $detailImagesService->getImages($detailId),
            'config' => $this->getConfig(),
        ]);
    }

    public function saveAction()
    {
        $postData = $this->getAllParams();
        $detailId = (int)$this->getParam('detailId');
        $userId = $this->getUserInfo()->getId();

        $detailImageService = new Workshop_Service_OrderWorkshopDetailImage([
            'path_to_upload_file' => $this->getConfig()->workorder->images->path_to_upload_file,
            'path_to_thumbnail' => $this->getConfig()->workorder->images->path_to_thumbnail,
        ]);

        $detailImageService->saveImages($detailId, $postData);
        $detailImageCount = $detailImageService->updateDetailImageCount($detailId, $userId);

        $this->view->clearVars();
        $this->view->assign([
            'detailId' => $detailId,
            'imagesCount' => $detailImageCount,
        ]);
    }

    public function uploadAction()
    {
        $detailId = (int)$this->getParam('detailId');
        $userId = $this->getUserInfo()->getId();

        $detailImageService = new Workshop_Service_OrderWorkshopDetailImage([
            'path_to_upload_file' => $this->getConfig()->workorder->images->path_to_upload_file,
            'path_to_thumbnail' => $this->getConfig()->workorder->images->path_to_thumbnail,
        ]);

        $detailImageService->uploadImages($detailId);
        $detailImageCount = $detailImageService->updateDetailImageCount($detailId, $userId);

        $this->view->clearVars();
        $this->view->assign([
            'detailId' => $detailId,
            'imagesCount' => $detailImageCount,
        ]);
    }

}