<?php

/**
 * Class Workshop_WorkorderCloseController
 */
class Workshop_WorkorderCloseController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'process' => 'json',
            'unlock-workorder' => 'json',
        ])->initContext();
    }

    public function getAction()
    {
        $workorderId = (int) $this->getParam('workorderId', 0);
        $workorderService = new Workshop_Service_Workorder();

        $workorder = $workorderService->findById($workorderId);
        $netAmount = $workorderService->calcNetAmountByWorkorder($workorder);

        $form = new Workshop_Form_CloseWorkorder(['workorderId' => $workorderId, 'netAmount' => $netAmount]);
        $form->populate($workorderService->getCloseWorkorderFormDataByWorkorder($workorder));

        $this->view->assign([
            'form' => $form,
            'workorder' => $workorder,
            'surcharges' => $workorderService->getSurchargesByWorkorder($workorder),
            'grossAmount' => $workorderService->calcGrossAmountByWorkorder($workorder),
            'netAmount' => $netAmount,
            'totalExtVat' => $workorderService->calcExternalTotalExclVatByWorkorder($workorder),
            'totalToPay' => $workorderService->calcExternalTotalInclVatByWorkorder($workorder),
        ]);
    }

    public function processAction()
    {
        $workorderId = (int) $this->getParam('workorderId', 0);
        $userId = $this->getUserInfo()->getId();

        $form = new Workshop_Form_CloseWorkorder([ 'workorderId' => $workorderId ]);

        $this->view->clearVars();

        if (!$form->isValid($this->getParam(Workshop_Form_CloseWorkorder::BELONG_ELEMENTS, []))) {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
            $this->view->assign('errors', $form->getErrorMessages());
            return;
        }

        $workorderService = new Workshop_Service_Workorder();
        $workorderService->saveCloseWorkorderFormByWorkorderId($form->getValues(), $workorderId, $userId);

        $this->view->assign('success', true);
    }

    public function unlockWorkorderAction()
    {
        $workorderId = (int) $this->getParam('workorderId', 0);
        $workorderService = new Workshop_Service_Workorder();

        $workorderService->unlockWorkorderById($workorderId);

        $this->view->clearVars();
        $this->view->assign('success', true);
    }


}