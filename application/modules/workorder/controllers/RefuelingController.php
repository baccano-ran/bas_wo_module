<?php

/**
 * Class Workshop_RefuelingController
 */
class Workshop_RefuelingController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'attach-refueling-to-workorder' => 'json',
        ])->initContext();
    }

    public function indexAction()
    {
        $refuelingService = new Workshop_Service_Refueling();
        $gridSelect = $refuelingService->getGridSelect($this->getLoggedInDepotId());

        $grid = new Workshop_Service_RefuelingGrid($gridSelect, $this->getView());

        $this->appendJsEntryPoint();
        $this->view->assign([
            'grid' => $grid->getGrid(),
        ]);
    }

    public function attachRefuelingToWorkorderAction()
    {
        $refuelingIdList = (array) $this->getParam('refuelingIdList', []);
        $workorderId = (int) $this->getParam('workorderId');
        $userId = $this->getUserInfo()->getId();

        $refuelingService = new Workshop_Service_Refueling();

        $this->view->clearVars();

        try {
            $refuelingService->attachRefuelingToWorkorder($refuelingIdList, $workorderId, $userId);
            $this->view->assign('success', true);
        } catch (Exception $e) {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
            $this->view->assign('error', $e->getMessage());
        }
    }

}

