<?php

/**
 * Workshop Index Contorller Class
 *
 * Indexworkshop Controller Class
 *
 * @package workshop
 * @version 1.0
 * @copyright never
 */

/**
 * It is used to display workshop grid and make advance search
 */
class Workshop_IndexController extends BAS_Shared_Controller_Action_Abstract
{
    /*
     * Set active depot id
     */
    protected $_activeDepotId;

    /**
     * @var Get all depot settings
     */
    protected $_settings;

    protected $_period;

    /**
     * @var Zend_Config_Ini 
     */
    protected $_config;
    
    /**
     * Initial function
     * @return string null
     */
    public function init()
    {
        parent::init();
        $this->_activeDepotId = $this->getLoggedInDepotId();
        $depotSettingService = new Management_Service_DepotSetting();
        $this->_settings = $depotSettingService->getDepotSettings($this->_activeDepotId, false);
        //Default Setting
        if (!isset($this->_settings['default_planning_type']['value'])) {
            $this->_settings['default_planning_type']['value'] = BAS_Shared_Model_DepotSetting::PLANNING_TYPE_DAY;
        }

        if (!isset($this->_settings['highlight_workorder']['value'])) {
            $this->_settings['highlight_workorder']['value'] = BAS_Shared_Model_DepotSetting::WORKSHOP_DEADLINE_IN_ORDER_VEHICLE_SETTING;
        }

        $this->_period = [
            BAS_Shared_Model_DepotSetting::PLANNING_TYPE_DAY => 'planningDays',
            BAS_Shared_Model_DepotSetting::PLANNING_TYPE_WEEK => 'planningWeeks',
        ];
        $this->_config = $this->getConfig();
    }

    /**
     * @return Default_Helper_Depot
     */
    public function getDepotHelper()
    {
        return $this->getHelper('Depot');
    }

    /**
     * Advance search form action
     */
    public function advanceSearchAction()
    {
        $vehicleTypeService = new Vehicles_Service_VehicleType();
        $vehicleBrandService = new Vehicles_Service_VehicleBrand();
        $incoTermService = new Order_Service_Incoterm();
        $transportMethodService = new Order_Service_TransportMethod();
        $countryService = new Order_Service_Country();
        $userService = new Order_Service_User();
        $searchForm = new Workshop_Form_AdvanceSearch();

        $searchForm->populate([
            'vehicleType' => $vehicleTypeService->getIdWithTitle(),
            'vehicleBrand' => $vehicleBrandService->getBrandList(),
            'incoTerm' => $incoTermService->getIncotermTitle(),
            'country' => $countryService->findIdWithNameByLanguage('EN'),
            'transportMethod' => $transportMethodService->findAll(),
            'user' => $userService->findAll()
        ]);

        $this->view->form = $searchForm;
        $this->view->user = BAS_Shared_Auth_Service::getAuthModelDecoratedObject($this->getUserInfo());
    }

    /**
     * Create send mail screen
     */
    
    public function prepareMailAction()
    {
        /** @var BAS_Shared_Model_User $user */
        $user = Zend_Auth::getInstance()->getIdentity();
        $parameter = $this->getRequest()->getParams();
        
        $vehicleService = new Vehicles_Service_Vehicle();
        $productService = new Order_Service_Product();
        $orderService = new Order_Service_Order();
        $userService = new Order_Service_User();
        $orderVehicleProductService = new Order_Service_OrderVehicleProduct();
        $mailService = new Mail_Service_Mail();

        $orderIdArray = explode(',',$parameter['orderId']);
        $orderData = $orderService->find($orderIdArray[0]); 
        $truckIdArray = explode(',',$parameter['id']);
        $orderVehicleIdArray = explode(',',$parameter['orderVehicleId']);
        $userData = $userService->find($orderData->userId);
		
        foreach($truckIdArray as $key=>$truckId) {
                if($orderVehicleProductService->isProductExist($orderVehicleIdArray[$key], $parameter['prodId']))
                $vehicleData[$key] = $vehicleService->find($truckId);
        }
        $productData = $productService->find($parameter['prodId']);

        $this->view->vehicleData = $vehicleData;
        $this->view->productData = $productData;
        $this->view->extranetURL = $this->_config->extranet->base_url;
        $this->view->imageURL = $this->_config->vbd->image_url;
        $this->view->orderId = $parameter['orderId'];
        $this->view->mailDecorator = $mailService->getMailSettings($this->_activeDepotId);
        $this->view->depotId = $this->_activeDepotId;
        $this->view->websiteBaseUrl = $this->_config->legacy->website_base_url;
        
        $this->view->productId=$parameter['prodId'];
        $this->view->orderVehicleId=$parameter['orderVehicleId'];
        $this->view->user = BAS_Shared_Auth_Service::getAuthModelDecoratedObject($user);
        $this->view->createdBy=$userData->Afkorting;
        $this->view->serviceURL=$this->_config->api->internal_base_url;
		
    }
    
    
    /**
     * It is used to send mail
     */
    public function sendMailAction(){
        $bootstrap = $this->getInvokeArg('bootstrap');
        $registry = $bootstrap->getResource('registry');
        $mail=$registry->workshopProductMailer;
        /** @var BAS_Shared_Model_User $user */
        $user = Zend_Auth::getInstance()->getIdentity();
        $senderName = $user->getName();
        $languageCode = $this->getRequest()->getParam('language');
        if ( $languageCode == 1 ) { 
             $workshop = 'Werkplaats';
         } elseif ( $languageCode == 2 ) {
             $workshop = 'Workshop';
         } elseif ( $languageCode == 3 ) {
             $workshop = 'Werkstatt';
         }
        $fromMailer = $this->_config->logmailer->from->workshopproductemail;
        $data=$this->getRequest()->getParam('maildata');
        
        $mailService = new Mail_Service_Mail();
        $depotSettingsService = new Management_Service_DepotSetting();
        $data['sender'] = $depotSettingsService->getSenderForWorkshopEmail($this->_activeDepotId, $fromMailer);
        $mailDecorator = $mailService->prepareMailData($this->_activeDepotId, $data);
        
        $mail->setHeaderEncoding(Zend_Mime::ENCODING_BASE64);
        $mail->setFrom($mailDecorator->getSenderEmail(), $mailDecorator->getSender() . ' - ' . $workshop . ' (' . $senderName .')');
        $mail->setBodyHtml($mailDecorator->getBody());
        $mails = json_decode($data['email']);
        $mail->clearRecipients();

        if (APPLICATION_ENV == 'production') {
            foreach($mails as $mailId){
                $mail->addTo($mailId);
            }
            $mail->addBcc($mailDecorator->getSenderEmail());
        } else {
            $mail->addTo($this->_config->workshop->notify->toArray());
        }

        $mail->setSubject($data['subject']);
        for($i=1; $i <= $data['appendix']; $i++){
            if($_FILES["appen$i"]['name'] != ""){ 
                $mail->createAttachment(file_get_contents($_FILES["appen$i"]['tmp_name']),mime_content_type($_FILES["appen$i"]['tmp_name']),Zend_Mime::DISPOSITION_INLINE,Zend_Mime::ENCODING_BASE64,$_FILES["appen$i"]['name']);
            }
        }
       
        try{
            $mail->send();
        }
        catch(Zend_Mail_Transport_Exception $e){
            $bootstrap = $this->getInvokeArg('bootstrap'); 
            $registry = $bootstrap->getResource('registry');
            $log = $registry->logger; 
            $log->crit($e->getMessage());
            $log->info($e->getTraceAsString()); 
        }
             
        $this->_redirect('workshop/index/index');
        $this->_helper->viewRenderer->setNoRender();
    }
    
    
    public function planningAction()
    {
        $depotId = $this->getParam('depot-id', $this->_activeDepotId);
        $params = $this->getAllParams();
        $params['isExternal'] = ('external' === $depotId) ? true : false;
        $depotList = $this->getHelper('depot')->getAffiliatedDepotMastersWithActiveDepot($depotId);
        $orderWorkshopService = new Order_Service_OrderWorkshop();

        // external work handling
        $gridType = 'planning_external';
        if (!$params['isExternal']) {
            $gridType = 'planning';
            $affiliatedDepotList = $workshopDepots = $this->getDepotHelper()->getAllowedWorkshopDepots($depotId);
            $orderWorkshopService->setWorkshopDepots(array_keys($workshopDepots));
        } else {
            $depotId = $this->_activeDepotId;
            $affiliatedDepotList = $this->getHelper('depot')->getAffiliatedDepotMastersWithActiveDepot($depotId);
        }

        // external work handling
        $this->view->selectedDepot = ($params['isExternal']) ? 'external' : $depotId;
        $this->view->planningType =  $this->_settings['default_planning_type']['value'];
        $this->view->period = $this->_period;
        
        if ($this->getRequest()->isXmlHttpRequest()) {
            $this->view->balloonStatus = $balloonStatus = $orderWorkshopService->getBalloonStatistics(
                $gridType,
                $this->_settings['default_planning_type']['value']
            );
            
            return $this->_helper->viewRenderer->setRender('index/workshop-planning-statistics', null, true);
        }
        
        $mainAccountingDepotId = $this->getHelper('depot')->getMainAccountingDepotId(
            $this->_activeDepotId
        );
        // Add lines for bvb grid
        $node = sprintf('%s.%s', $params['module'], $params['controller']);
        $this->view->node = $params['node'] = $node;
        $this->view->action = $params['action'];
        $gridParams = [
            'user' => $this->getUserInfo(),
            'activeDepotId' => $this->getLoggedInDepotId(),
            'config' => $this->getConfig(),
            'depots' => $depotList,
        ];
        $workshopService = new Workshop_Service_Workshop($gridParams);
        $grid = $workshopService->getPlanningGrid(
            $depotId,
            $params,
            $affiliatedDepotList,
            $mainAccountingDepotId
        );
        $this->view->grid = $grid;
        $this->view->depotList = $depotList;
    }

    public function updateOrderWorkshopAs400Action()
    {
        $workOrderId = $this->_request->getParam('workOrderId', 0);
        $as400Id = $this->_request->getParam('as400Id', null);
        $result = false;

        if ((int)$workOrderId > 0 && !empty($as400Id)) {
            $service = new Order_Service_OrderWorkshop();
            $result = $service->updateAs400Id($workOrderId, $as400Id);
        }

        return $this->_helper->json($result);

    }
}

