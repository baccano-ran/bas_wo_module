<?php


class Workshop_LabourRateController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'add' => 'json',
            'edit' => 'json',
            'save' => 'json',
            'delete' => 'json',
        ])->initContext();

        $this->jsEntryPointMap = [
            'add' => ['action' => 'edit']
        ];
    }

    public function indexAction()
    {
        $labourRateService = new Workshop_Service_LabourRate();
        $labourRateGridService = new Workshop_Service_LabourRateGrid($this->view);

        $this->view->grid = $labourRateGridService->getGrid($labourRateService->getGridSource($this->getLoggedInDepotId()));
    }

    public function addAction()
    {
        $labourRate = new BAS_Shared_Model_Workshop_LabourRate();
        $labourRate->setDepotId($this->getLoggedInDepotId());

        $this->appendJsEntryPoint();
        $this->view->assign([
            'initialData' => [
                'labourCosts' => [],
                'labourRate' => $labourRate,
                'departmentOptions' => $this->getDepartmentOptions(),
            ]
        ]);
    }

    public function editAction()
    {
        $labourRateId = (int)$this->getParam('id', 0);

        $labourRateService = new Workshop_Service_LabourRate();
        $labourCostService = new Workshop_Service_LabourCost();

        $labourRate = $labourRateService->findById($labourRateId);
        $labourRate->setEndDate($this->formatDate($labourRate->getEndDate()));

        $labourCosts = $labourCostService->findAllByRateId($labourRateId);
        foreach ($labourCosts as &$labourCost) {
            $labourCost->setEndDate($this->formatDate($labourCost->getEndDate()));
        }

        $this->appendJsEntryPoint();
        $this->view->assign([
            'initialData' => [
                'labourCosts' => $labourCosts,
                'labourRate' => $labourRate,
                'departmentOptions' => $this->getDepartmentOptions(),
            ]
        ]);
    }

    public function saveAction()
    {
        if (!$this->getRequest()->isXmlHttpRequest() || $this->getRequest()->isGet()) {
            return;
        }

        $labourRateService = new Workshop_Service_LabourRate();
        $labourCostService = new Workshop_Service_LabourCost();


        $data = $this->getAllParams();
        $validationErrors = [];

        if ($errors = $labourRateService->validateData($data['labourRate'])) {
            $validationErrors = array_merge($validationErrors, $errors);
        }
        if (isset($data['labourCosts']) && $errors = $labourCostService->validateArrayData($data['labourCosts'])) {
            $validationErrors = array_merge($validationErrors, $errors);
        }

        if ($validationErrors !== []) {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::BAD_REQUEST);
            $this->view->clearVars();
            $this->view->assign(['errors' => $validationErrors]);
            return;
        }

        $userId = $this->getUserInfo()->getId();
        $labourRate = $labourRateService->createModelByData($data['labourRate'], $userId);
        $labourRate = $labourRateService->saveModel($labourRate);

        $labourCostIds = [];

        if (isset($data['labourCosts'])) {
            foreach ($data['labourCosts'] as $labourCostData) {
                $labourCostData['labourRateId'] = $labourRate->getId();

                $labourCost = $labourCostService->createModelByData($labourCostData, $userId);
                $labourCost = $labourCostService->saveModel($labourCost);

                $labourCostIds[] = $labourCost->getId();
            }
        }

        $labourCostService->deleteByNotIds($labourCostIds, $labourRate->getId());

        $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::OK);
        $this->view->clearVars();
        $this->view->assign([
            'id' => $labourRate->getId(),
            'message' => $this->translate('record_successfully_saved'),
        ]);
    }

    /**
     * @return array
     */
    private function getDepartmentOptions()
    {
        $departmentService = new Order_Service_Department();
        $departments = $departmentService->findAllByDepotId($this->getLoggedInDepotId());

        $options = [['value' => null, 'name' => $this->translate('placeholder_select_department')]];

        foreach ($departments as $department) {
            $options[] = [
                'value' => $department['id'],
                'name' => $department['name'],
            ];
        }

        return $options;
    }

    /**
     * @param string $ymdDate
     * @return string
     */
    private function formatDate($ymdDate)
    {
        $returnDate = '';
        if ($ymdDate !== '0000-00-00') {
            $date = DateTime::createFromFormat('Y-m-d', $ymdDate);
            if ($date !== false) {
                $returnDate = $date->format('d-m-Y');
            }
        }
        return $returnDate;
    }

}