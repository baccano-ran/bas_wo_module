<?php

class Workshop_WorkorderHistoryWidgetController extends BAS_Shared_Controller_Action_Abstract
{
    public function indexAction()
    {
        $contactId = (int) $this->getParam('id');
        $depotId = (int) $this->getParam('depotId');

        $workorderService = new Workshop_Service_Workorder();

        /** @var  $gridSelect */
        $gridSelect = $workorderService->getWorkorderHistoryGridSelect($contactId);

        $workorderHistoryGrid = new Workshop_Service_WorkorderHistoryGrid(
            $gridSelect,
            $this->view,
            [],
            $depotId
        );

        $this->view->assign([
            'grid' => $workorderHistoryGrid->getGrid(),
        ]);
    }
}