<?php

/**
 * Class Workshop_TradeInPricesController
 */
class Workshop_TradeInPricesController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'post' => 'json',
        ])->initContext();
    }

    public function getAction()
    {
        $workorderDepotId = $this->getParam('workorderDepotId');
        $itemsData = (array)$this->getParam('itemsData', []);
        $composedTaskIds = (array)$this->getParam('composedTaskIds', []);

        $tradeInPricesService = new Workshop_Service_TradeInPrices();
        $tradeInItemsData = $tradeInPricesService->getTradeInItemsData($itemsData, $composedTaskIds, $workorderDepotId);

        $tradeInPricesForm = new Workshop_Form_TradeInPrices([
            'tradeInPrices' => $tradeInItemsData,
            'direction' => Workshop_Form_TradeInPrices::DIRECTION_OUTPUT,
        ]);

        $tradeInPricesForm->populate($tradeInItemsData);

        $this->view->assign([
            'tradeInPricesForm' => $tradeInPricesForm,
            'tradeInPossible' => $tradeInItemsData['tradeInPossible'],
        ]);
    }

    public function postAction()
    {
        $pricesData = (array)$this->getParam('prices', []);

        $tradeInPricesForm = new Workshop_Form_TradeInPrices([
            'tradeInPrices' => $pricesData,
            'direction' => Workshop_Form_TradeInPrices::DIRECTION_INPUT,
        ]);

        if ($tradeInPricesForm->isValid($this->getAllParams())) {

            $this->view->clearVars();
            $this->view->assign($tradeInPricesForm->getValues());

        } else {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::BAD_REQUEST);
            $messageFormatter = new BAS_Shared_Form_Formatter_Messages($tradeInPricesForm);

            $this->view->clearVars();
            $this->view->assign([
                'messages' => $messageFormatter->getMessages(),
            ]);
        }
    }
}