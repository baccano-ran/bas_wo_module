<?php

/**
 * Class Workshop_WorkorderProformaInvoiceReportController
 */
class Workshop_WorkorderProformaInvoiceReportController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'generate' => 'json',
        ])->initContext();
    }

    public function indexAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $workorderReportService = new Workshop_Service_WorkorderReport();
        $languageService = new Default_Service_Language();
        $workorderService = new Workshop_Service_Workorder();

        $workorder = $workorderService->findById($workorderId);
        $reportData = $workorderReportService->getProformaInvoiceData($workorderId);

        $languageOptions = $languageService->getListOfLanguages();
        $defaultLanguage = $workorderService->getInvoiceLanguageByOrderId($workorderId);

        $isInternalWorkorder = $workorderService->isInternalWorkorder($workorder->getType());
        $isCustomerApplicable = $workorderService->isCustomerApplicableByWorkorderType($workorder->getType());

        $defaultDebtorId = null;
        if ($isInternalWorkorder) {
            $depotSettingService = new Management_Service_DepotSetting();
            $defaultDebtorId = (int)$depotSettingService->getSettingValue($workorder->getDepotId(), BAS_Shared_Model_Setting::DEBTOR_DEFAULT_NUMBER);
        }
        $this->getView()->assign([
            'workorderId' => $workorderId,
            'workorder' => $workorder,
            'reportData' => $reportData,
            'defaultLanguage' => $defaultLanguage,
            'languageOptions' => $languageOptions,
            'isInternalWorkorder' => $isInternalWorkorder,
            'isCustomerApplicable' => $isCustomerApplicable,
            'defaultDebtorId' => $defaultDebtorId,
        ]);
    }

    public function generateAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);
        $printHeaderFooter = (bool)$this->getParam('printHeaderFooter', true);
        $extraInformation = $this->getParam('extraInformation', '');
        $languageCode = $this->getParam('language', strtoupper(BAS_Shared_Model_Order::DEFAULT_INVOICE_LANGUAGE_CODE));
        $vatType = $this->getParam('vatType');

        $workorderReportService = new Workshop_Service_WorkorderReport();
        $fileService = new BAS_Shared_Service_FileService();

        $reportFiles = $workorderReportService->generateProformaInvoices(
            $workorderId,
            $languageCode,
            $vatType,
            $printHeaderFooter,
            $extraInformation,
            $this->getUserInfo()->getId(),
            $this->getHelper('depot')->getActiveDepotId()
        );

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopFile $externalProformaFile */
        $externalProformaFile = $reportFiles['externalProformaFile'];
        $externalProformaFileUrl = $externalProformaFile === null ? null : $fileService->getFileUrl(
            $externalProformaFile->getFileName(),
            $externalProformaFile->getToken()
        );

        $this->view->clearVars();
        $this->view->data = ['reportUrl' => $externalProformaFileUrl];
    }

}