<?php

/**
 * Class Workshop_WorkorderInvoiceReportController
 */
class Workshop_WorkorderInvoiceReportController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'generate' => 'json',
            'fast-invoice' => 'json',
        ])->initContext();
    }

    public function indexAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $workorderService = new Workshop_Service_Workorder();
        $workorderReportService = new Workshop_Service_WorkorderReport();
        $languageService = new Default_Service_Language();
        $workorderInvoiceValidator = new BAS_Shared_Validate_WorkorderInvoiceValidator;

        $workorder = $workorderService->findById($workorderId);
        $reportData = $workorderReportService->getFinalInvoiceData($workorderId);

        $languageOptions = $languageService->getListOfLanguages();
        $defaultLanguage = $workorderService->getInvoiceLanguage($workorderId);
        $isAccountGroupMissed = $workorderService->isDepotAccountGroupMissedForWorkorderDetails($workorderId);
        $isInternalWorkorder = $workorderService->isInternalWorkorder($workorder->getType());
        $isCustomerApplicable = $workorderService->isCustomerApplicableByWorkorderType($workorder->getType());
        $defaultDebtorId = $workorderService->getDefaultDebtorNumber($workorder);
        $canBeInvoiced = $workorderInvoiceValidator->canBeInvoiced($workorder);
        $canBeInvoicedError = $workorderInvoiceValidator->getComposedErrorMessage();

        $this->getView()->assign([
            'workorderId' => $workorderId,
            'workorder' => $workorder,
            'reportData' => $reportData,
            'defaultLanguage' => $defaultLanguage,
            'languageOptions' => $languageOptions,
            'isInternalWorkorder' => $isInternalWorkorder,
            'isCustomerApplicable' => $isCustomerApplicable,
            'defaultDebtorId' => $defaultDebtorId,
            'isAccountGroupMissed' => $isAccountGroupMissed,
            'canBeInvoiced' => $canBeInvoiced,
            'canBeInvoicedError' => $canBeInvoicedError,
        ]);
    }

    public function generateAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);
        $printHeaderFooter = (bool)$this->getParam('printHeaderFooter', true);
        $extraInformation = $this->getParam('extraInformation', '');
        $languageCode = $this->getParam('language', strtoupper(BAS_Shared_Model_Order::DEFAULT_INVOICE_LANGUAGE_CODE));

        $workorderReportService = new Workshop_Service_WorkorderReport();
        $fileService = new BAS_Shared_Service_FileService();

        $reportFiles = $workorderReportService->generateFinalInvoices(
            $workorderId,
            $extraInformation,
            $languageCode,
            $printHeaderFooter,
            $this->getUserInfo()->getId(),
            $this->getHelper('depot')->getActiveDepotId()    
        );

        /** @var BAS_Shared_Model_Workshop_OrderWorkshopFile $externalInvoiceFile */
        $externalInvoiceFile = $reportFiles['externalInvoiceFile'];
        $externalInvoiceFileUrl = $externalInvoiceFile === null ? null : $fileService->getFileUrl(
            $externalInvoiceFile->getFileName(),
            $externalInvoiceFile->getToken()
        );

        $this->view->clearVars();
        $this->view->data = ['reportUrl' => $externalInvoiceFileUrl];
    }

    public function fastInvoiceAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);
        $printHeaderFooter = true;
        $extraInformation = '';
        $languageCode = strtoupper(BAS_Shared_Model_Order::DEFAULT_INVOICE_LANGUAGE_CODE);
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $workorderReportService = new Workshop_Service_WorkorderReport();

        $workorder = $workorderService->findById($workorderId);

        $workorderService->closeWorkorder($workorder, $userId);
        $workorderReportService->generateFinalInvoices(
            $workorderId,
            $extraInformation,
            $languageCode,
            $printHeaderFooter,
            $userId
        );

        $this->view->clearVars();
        $this->view->assign(['redirectUrl' => $this->view->workorderListUrl($workorder)]);
    }

}
