<?php

/**
 * Class Workshop_WorkorderGeneralTabController
 */
class Workshop_WorkorderGeneralTabController extends BAS_Shared_Controller_Action_Abstract
{
    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'save' => 'json',
            'cancel-workorder' => 'json',
        ])->initContext();
    }

    public function getAction()
    {
        $id = (int)$this->getParam('id', 0);
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $workorderTimeService = new Workshop_Service_WorkorderTime();

        $workorder = $workorderService->findById($id);
        $isReadonly = $workorderService->isWorkorderReadonly($workorder, $userId);
        $isInternalWorkorder = $workorderService->isInternalWorkorder($workorder->getType());

        $vehicleSource = $workorder->getVehicleSource() === null
            ? $workorderService->getVehicleSource($workorder->getType(), $workorder->getContactId())
            : $workorder->getVehicleSource();

        $this->view->assign([
            'workorder' => $workorder,
            'isReadonly' => $isReadonly,
            'isInternalWorkorder' => $isInternalWorkorder,
            'isCustomerApplicable' => $workorderService->isCustomerApplicableByWorkorderType($workorder->getType()),
            'isFleetVehicle' => $vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_FLEET,
            'isStockVehicle' => $vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_STOCK,
            'isOrderVehicle' => $vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_ORDER_VEHICLE,
            'isTimeSpentAttachedToWorkorder' => $workorderTimeService->isTimeSpentAttachedToWorkorder($workorder->getWorkorderId()),
        ]);
    }

    public function saveAction()
    {
        $workorderId = (int)$this->getParam('id', 0);
        $postParams = $this->getAllParams();
        $vehicle = $this->getParam('vehicle', []);
        $vehicleId = isset($vehicle['vehicleId']) ? $vehicle['vehicleId'] : 0;
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $workorder = $workorderService->findById($workorderId);

        $form = new Workshop_Form_WorkorderGeneralTab([
            'depotId' => $workorder->getDepotId(),
            'userId' => $userId,
            'workshopId' => isset($postParams['general']['workshopId']) ? $postParams['general']['workshopId'] : null,
            'validationUnknownVehicle' => $vehicleId === 'unknown',
            Workshop_Form_WorkorderGeneralTab::SECTION_UNKNOWN_VEHICLE => $this->getParam(Workshop_Form_WorkorderGeneralTab::SECTION_UNKNOWN_VEHICLE, []),
            'workorder' => $workorder,
        ]);

        $this->view->clearVars();

        if ($form->isValid($postParams)) {
            $newWorkorderModel = $workorderService->saveWorkorderGeneralTabData($workorderId, $postParams, $userId);

            $this->view->assign([
                'vehicleId' => $newWorkorderModel->vehicleId,
            ]);
            return;
        }

        $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);

        $messageFormatter = new BAS_Shared_Form_Formatter_Messages($form);
        $this->view->assign([
            'messages' => $messageFormatter->getMessages(),
        ]);
    }

    public function generalAction()
    {
        $workorderId = (int) $this->getParam('workorderId', 0);
        $belongsTo = (string) $this->getParam('belongTo', 'general');
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $workorder = $workorderService->findById($workorderId);

        $settingService = new Management_Service_Setting();
        $mainAccountingDepotId = $settingService->getMainAccountingDepot($workorder->depotId);

        $generalForm = new Workshop_Form_WorkorderGeneral([
            'direction' => Workshop_Form_WorkorderGeneral::DIRECTION_OUTPUT,
            'workshopId' => $mainAccountingDepotId,
            'depotId' => $mainAccountingDepotId,
            'userId' => $userId,
            'workorder' => $workorder,
        ]);
        $generalForm->setElementsBelongTo($belongsTo);
        $generalForm->populate(array_filter($workorder->toArray()));

        $this->view->assign([
            'generalForm' => $generalForm,
            'workorder' => $workorder,
        ]);
    }

    public function getWarehouseElementAction()
    {
        $workshopId = (int)$this->getParam('workshopId', 0);

        $warehouseElement = Workshop_Form_WorkorderGeneral::getWarehouseElement($workshopId);
        $warehouseElement->setBelongsTo('general');

        $this->view->assign('warehouseElement', $warehouseElement);
    }

    public function selectVehicleAction()
    {
        $contactId = (int)$this->getParam('contactId', 0);
        $vehicleId = (int)$this->getParam('contactVehicleId', 0);
        $workorderType = (int)$this->getParam('workorderType', 0);
        $isReadonly = (bool)$this->getParam('isReadonly');

        $workorderService = new Workshop_Service_Workorder();
        $vehicleSource = $workorderService->getVehicleSource($workorderType, $contactId);

        $this->view->assign([
            'contactId' => $contactId,
            'isReadonly' => $isReadonly,
            'vehicleId' => $vehicleId,
            'workorderType' => $workorderType,
            'isFleetVehicle' => $vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_FLEET,
            'isStockVehicle' => $vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_STOCK,
            'isOrderVehicle' => $vehicleSource === BAS_Shared_Model_OrderWorkshop::VEHICLE_SOURCE_ORDER_VEHICLE,
        ]);
    }

    public function selectCustomerAction()
    {
        $contactId = (int)$this->getParam('contactId', 0);
        $personId = (int)$this->getParam('personId', 0);
        $isReadonly = filter_var($this->getParam('isReadonly'), FILTER_VALIDATE_BOOLEAN);
        $depotId = (int)$this->getParam('depotId');
        $isInternalWorkorder = filter_var($this->getParam('isInternalWorkorder'), FILTER_VALIDATE_BOOLEAN);
        $isCustomerApplicable = filter_var($this->getParam('isCustomerApplicable'), FILTER_VALIDATE_BOOLEAN);

        $defaultDebtorId = null;
        if ($isInternalWorkorder) {
            $depotSettingService = new Management_Service_DepotSetting();
            $defaultDebtorId = (int)$depotSettingService->getSettingValue($depotId, BAS_Shared_Model_Setting::DEBTOR_DEFAULT_NUMBER);
        }
        $depotService = new Management_Service_Depot();
        $workorderDepotName = $depotService->getNameByDepotId($depotId);

        $settingService = new Management_Service_Setting();
        $mainAccountingDepotId = $settingService->getMainAccountingDepot($depotId);

        $this->view->assign([
            'contactId' => $contactId,
            'personId' => $personId,
            'isReadonly' => $isReadonly,
            'isInternalWorkorder' => $isInternalWorkorder,
            'isCustomerApplicable' => $isCustomerApplicable,
            'defaultDebtorId' => $defaultDebtorId,
            'workorderDepotName' => $workorderDepotName,
            'accountingDepotId' => $mainAccountingDepotId,
        ]);

    }

    public function getCancelWorkorderAction()
    {
        $workorderId = $this->getParam('workorderId');

        $workorderService = new Workshop_Service_Workorder();
        $workorder = $workorderService->findById($workorderId);
        $form = new Workshop_Form_CancelWorkorder(['depotId' => $workorder->getDepotId()]);

        $this->view->assign([
            'form' => $form,
            'workorder' => $workorder,
        ]);
    }

    public function cancelWorkorderAction()
    {
        $workorderId = (int)$this->getParam('workorderId');
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $workorder = $workorderService->findById($workorderId);
        $form = new Workshop_Form_CancelWorkorder(['depotId' => $workorder->getDepotId()]);

        $this->view->clearVars();

        if ($form->isValid($this->getAllParams())) {
            $reasonId = $form->getValue('reasonId');
            $reasonText = $form->getValue('reasonText');

            $workorderService->cancelWorkorder($workorder, $reasonId, $reasonText, $userId);
            $redirectUrl = $this->view->workorderListUrl($workorder);

            $this->view->assign('redirectUrl', $redirectUrl);

        } else {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::BAD_REQUEST);

            $messageFormatter = new BAS_Shared_Form_Formatter_Messages($form);
            $this->view->assign([
                'messages' => $messageFormatter->getMessages(),
            ]);
        }
    }

    public function selectStockVehicleAction()
    {
        $vehicleId = (int)$this->getParam('vehicleId', 0);
        $workorderType = (int)$this->getParam('workorderType', 0);
        $searchTruckId = (int)$this->getParam('searchTruckId', 0);
        $isReadonly = (bool)$this->getParam('isReadonly', 0);
        $belongsTo = $this->getParam('belongsTo', 'vehicle');

        if ($workorderType === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER) {
            $isReadonly = true;
        }

        $workorderService = new Workshop_Service_Workorder();
        $vehicleService = new Vehicles_Service_Vehicle();
        $orderVehicleService = new Order_Service_OrderVehicle();

        $vehicle = null;
        $searchVehicle = null;
        $openWorkordersForVehicle = [];

        if ($vehicleId > 0) {
            try {
                if ($workorderType === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER) {
                    $orderVehicleId = $vehicleId;
                    $orderVehicle = $orderVehicleService->find($orderVehicleId);
                    $vehicleId = $orderVehicle->getLegacyVoertuigId();
                }

                $vehicle = $vehicleService->findByVehicleId($vehicleId);
            } catch (Ibuildings_Exception_NotFound $e) {
                $vehicle = null;
            }
        }

        if ($searchTruckId > 0) {
            $searchVehicle = $vehicleService->find($searchTruckId);

            if ($searchVehicle !== null) {
                $openWorkordersForVehicle = $workorderService->getOpenWorkordersForVehicle($searchVehicle->getVehicleId());

                if ($openWorkordersForVehicle === []) {
                    //automatically submit if no workorders
                    $vehicle = $searchVehicle;
                    $vehicleId = $searchVehicle->getVehicleId();
                    $searchTruckId = 0;
                    $searchVehicle = null;
                }
            }
        }

        $this->appendJsEntryPoint();

        $this->view->assign([
            'vehicleId' => $vehicleId,
            'vehicle' => $vehicle,
            'isReadonly' => $isReadonly,
            'searchTruckId' => $searchTruckId,
            'searchVehicle' => $searchVehicle,
            'openWorkordersForVehicle' => $openWorkordersForVehicle,
            'belongsTo' => $belongsTo,
        ]);
    }

    public function selectFleetVehicleAction()
    {
        $contactId = (int)$this->getParam('contactId', 0);
        $contactVehicleId = (int)$this->getParam('contactVehicleId', 0);
        $isReadonly = (bool)$this->getParam('isReadonly', 0);
        $belongTo = $this->getParam('belongTo', Default_View_Helper_FormFieldName::EMPTY_BELONGTO);

        $contactVehicleOverviewGrid = new Contact_Service_ContactVehicleOverviewGrid($this->view, $contactId);
        $contactVehicleOverviewGrid->showVehicleRadioColumn = true;
        $contactVehicleOverviewGrid->contactVehicleId = $contactVehicleId;

        $contactVehicleOverviewService = new Contacts_Service_ContactVehicleOverview();
        $overviewSelect = $contactVehicleOverviewService->getContactVehicleOverview($contactId);

        $vehicleServiceVehicleType = new Vehicles_Service_VehicleType();
        $vehicleTypes = $vehicleServiceVehicleType->getTruckVehicleType(true);

        $request = $this->getRequest();
        $grid = $contactVehicleOverviewGrid->getGrid($overviewSelect, array_merge($this->getAllParams(), [
            'module' => $request->getModuleName(),
            'controller' => $request->getControllerName(),
            'action' => $request->getActionName(),
        ]));

        $this->view->assign([
            'grid' => $grid,
            'contactId' => $contactId,
            'isReadonly' => $isReadonly,
            'contactVehicleId' => $contactVehicleId,
            'vehicleTypes' => $vehicleTypes,
            'belongTo' => $belongTo,
        ]);

    }

}