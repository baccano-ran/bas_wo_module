<?php

/**
 * Class Workshop_WorkorderDetailsTabController
 */
class Workshop_WorkorderDetailsTabController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'order-details' => 'json',
            'add-workorder-details' => 'json',
            'save-workorder-details' => 'json',
            'save-spent-time' => 'json',
            'delete-workorder-detail' => 'json',
            'delete-workorder-time' => 'json',
            'edit-workorder-time' => 'json',
            'sync-workorder-detail-account-group-id' => 'json',
            'can-be-closed' => 'json',
            'can-be-reopened' => 'json',
            'can-be-invoiced' => 'json',
            'can-be-printed' => 'json',
            'can-be-fast-invoiced' => 'json',
            'check-items-exist-in-composed-tasks' => 'json',
            'change-sales-preparation-status' => 'json',
        ])->initContext();
    }

    public function getAction()
    {
        $workorderId = (int)$this->getParam('id', 0);

        $workorderService = new Workshop_Service_Workorder();
        $workorder = $workorderService->findById($workorderId);

        $this->view->assign([
            'workorder' => $workorder,
        ]);
    }

    public function salesPreparationStatusAction()
    {
        $workorderId = (int) $this->getParam('workorderId');
        $availableStatuses = (array) $this->getParam('availableStatuses', [
            BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_HOLD,
            BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_ACTION_REQUIRED,
            BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_APPROVED,
        ]);
        $moduleLabels = (string) $this->getParam('moduleLabels', BAS_Shared_Model_OrderWorkshop::LABEL_MODULE_WORKSHOP);

        $workorderService = new Workshop_Service_Workorder();

        $workorder = $workorderService->findById($workorderId);

        $this->view->assign([
            'workorder' => $workorder,
            'availableStatuses' => $availableStatuses,
            'moduleLabels' => $moduleLabels,
        ]);
    }

    public function changeSalesPreparationStatusAction()
    {
        $workorderId = (int) $this->getParam('workorderId');
        $status = (int) $this->getParam('status');
        $comment = null;

        $workorderService = new Workshop_Service_Workorder();

        $userId = $this->getUserInfo()->id;

        $updatedCount = $workorderService->changeSalesPreparationStatus(
            $workorderId,
            $status,
            $comment,
            $userId
        );

        $this->view->assign([
            'updated' => $updatedCount !== null,
        ]);
    }

    public function buttonsAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $workorderService = new Workshop_Service_Workorder();

        $workorder = $workorderService->findById($workorderId);
        $orderVehicle = $workorderService->getWorkorderOrderVehicle($workorder);
        $isWorkorderLocked = $workorderService->isWorkorderStatusLocked($workorder);
        $containsOnlyInternalDetails = $workorderService->containsOnlyInternalDetails($workorder->getWorkorderId());

        $this->view->assign([
            'workorder' => $workorder,
            'internalApiUrl' => $this->getConfig()->api->internal_base_url,
            'orderVehicle' => $orderVehicle,
            'isWorkorderLocked' => $isWorkorderLocked,
            'containsOnlyInternalDetails' => $containsOnlyInternalDetails,
        ]);
    }

    public function canBeClosedAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $workorderService = new Workshop_Service_Workorder();
        $workorderCloseValidator = new BAS_Shared_Validate_WorkorderCloseValidator();

        $workorder = $workorderService->findById($workorderId);
        $canBeClosed = $workorderCloseValidator->canBeClosed($workorder);

        $this->view->clearVars();
        if ($canBeClosed) {
            $this->view->assign(['result' => 'OK']);
        } else {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
            $this->view->assign(['message' => $workorderCloseValidator->getComposedErrorMessage()]);
        }
    }

    public function canBeReopenedAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $workorderService = new Workshop_Service_Workorder();
        $workorderReopenValidator = new BAS_Shared_Validate_WorkorderReopenValidator();

        $workorder = $workorderService->findById($workorderId);
        $canBeReopened = $workorderReopenValidator->canBeReopened($workorder);

        $this->view->clearVars();
        if ($canBeReopened) {
            $this->view->assign(['result' => 'OK']);
        } else {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
            $this->view->assign(['message' => $workorderReopenValidator->getComposedErrorMessage()]);
        }
    }

    public function canBeInvoicedAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $workorderService = new Workshop_Service_Workorder();
        $workorderInvoiceValidator = new BAS_Shared_Validate_WorkorderInvoiceValidator();

        $workorder = $workorderService->findById($workorderId);
        $canBeInvoiced = $workorderInvoiceValidator->canBeInvoiced($workorder);

        $this->view->clearVars();
        if ($canBeInvoiced) {
            $this->view->assign(['result' => 'OK']);
        } else {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
            $this->view->assign(['message' => $workorderInvoiceValidator->getComposedErrorMessage()]);
        }
    }

    public function canBePrintedAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $workorderService = new Workshop_Service_Workorder();

        $workorder = $workorderService->findById($workorderId);
        $canBePrinted = false;

        if ($workorder !== null) {
            $canBePrinted = $workorder->getType() !== BAS_Shared_Model_OrderWorkshop::TYPE_SALES_PREPARATION || in_array($workorder->getSalesPreparationStatus(), [
                BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_NA,
                BAS_Shared_Model_OrderWorkshop::SALES_PREPARATION_STATUS_APPROVED,
            ], true);
        }

        $this->view->clearVars();
        if ($canBePrinted) {
            $this->view->assign(['result' => 'OK']);
        } else {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
            $this->view->assign(['message' => sprintf($this->translate('not_able_to_print_due_sales_status'),
                BAS_Shared_Model_OrderWorkshop::$salesPreparationStatusLabels[BAS_Shared_Model_OrderWorkshop::LABEL_MODULE_WORKSHOP][$workorder->getSalesPreparationStatus()]
            )]);
        }
    }

    public function canBeFastInvoicedAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $workorderService = new Workshop_Service_Workorder();
        $workorderCloseValidator = new BAS_Shared_Validate_WorkorderCloseValidator();
        $workorderInvoiceValidator = new BAS_Shared_Validate_WorkorderInvoiceValidator();

        $workorder = $workorderService->findById($workorderId);

        $this->view->clearVars();
        if ($workorderCloseValidator->canBeClosed($workorder) &&
            $workorder->setStatus(BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED) &&
            $workorderInvoiceValidator->canBeInvoiced($workorder)
        ) {
            $this->view->assign(['result' => 'OK']);
        } else {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
            $this->view->assign(['message' =>
                $workorderCloseValidator->getComposedErrorMessage() .
                $workorderInvoiceValidator->getComposedErrorMessage()
            ]);
        }
    }

    public function orderDetailsAction()
    {
        $workorderId = $this->getParam('workorderId');

        $workorderService = new Workshop_Service_Workorder();
        $workorderTimeService = new Workshop_Service_WorkorderTime();
        $workorderDetailService = new Workshop_Service_OrderWorkshopDetail();
        $stockDefectsService = new Stock_Service_StockDefects();
        $contactTypeSupplierFeeSettingService = new Contacts_Service_ContactTypeSupplierFeeSetting();

        $workorder = $workorderService->findById($workorderId);
        $workorderInvoicedTime = null;
        $workorderSpentTime = null;
        $workorderDefects = null;
        $threshold = null;

        if ($workorder !== null) {
            $workorderInvoicedTime = $workorderDetailService->getWorkorderInvoicedTime($workorderId);
            $workorderSpentTime = $workorderTimeService->getWorkorderSpentTime($workorderId);
            $workorderDefects = $stockDefectsService->findByWorkorderId($workorderId);
            $thresholdSetting = $contactTypeSupplierFeeSettingService->getSalesPreparationThresholdByContactAndDepotId(
                $workorder->getContactId(),
                $this->getLoggedInDepotId()
            );
            $threshold = $thresholdSetting->getValue();
        }

        $this->view->assign([
            'workorder' => $workorder,
            'threshold' => $threshold,
            'workorderInvoicedTime' => $workorderInvoicedTime,
            'workorderSpentTime' => $workorderSpentTime,
            'workorderDefects' => $workorderDefects,
        ]);
    }

    public function totalsAction()
    {
        $workorderId = $this->getParam('workorderId');
        $workorderService = new Workshop_Service_Workorder();

        $workorder = $workorderService->findById($workorderId);

        $this->view->assign([
            'workorder' => $workorder,
            'grossAmount' => $workorderService->calcGrossAmountByWorkorder($workorder),
            'netAmount' => $workorderService->calcNetAmountByWorkorder($workorder),
            'vatAmount' => $workorderService->calcExternalVatAmountByWorkorder($workorder),
            'totalExclVat' => $workorderService->calcExternalTotalExclVatByWorkorder($workorder),
            'totalInclVat' => $workorderService->calcExternalTotalInclVatByWorkorder($workorder),
            'finalTotals' => $workorderService->calcFinalTotalsByWorkorder($workorder),
        ]);
    }

    public function customerInformationAction()
    {
        $workorderId = $this->getParam('workorderId');

        $workorderService = new Workshop_Service_Workorder();

        $workorder = $workorderService->findById($workorderId);
        $depotId = $workorder->getDepotId();

        $isInternalWorkorder = $workorderService->isInternalWorkorder($workorder->getType());

        $customerInformation = $workorderService->getCustomerInformation($workorder);

        $defaultDebtorId = null;
        if ($isInternalWorkorder) {
            $depotSettingService = new Management_Service_DepotSetting();
            $defaultDebtorId = (int)$depotSettingService->getSettingValue($depotId, BAS_Shared_Model_Setting::DEBTOR_DEFAULT_NUMBER);
        }
        $depotService = new Management_Service_Depot();
        $workorderDepotName = $depotService->getNameByDepotId($depotId);

        $this->view->assign([
            'workorder' => $workorder,
            'customerInformation' => $customerInformation,
            'isInternalWorkorder' => $isInternalWorkorder,
            'isCustomerApplicable' => $workorderService->isCustomerApplicableByWorkorderType($workorder->getType()),
            'defaultDebtorId' => $defaultDebtorId,
            'workorderDepotName' => $workorderDepotName,
        ]);
    }

    public function vehicleInformationAction()
    {
        $workorderId = $this->getParam('workorderId');

        $workorderService = new Workshop_Service_Workorder();

        $workorder = $workorderService->findById($workorderId);
        $vehicleInformation = $workorderService->getVehicleInformation($workorder);
        $vehicleInformation['workorder'] = $workorder;

        $this->view->assign($vehicleInformation);
    }

    /**
     * Workorder Tab2 Details widget
     */
    public function detailsAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $workorderDetailService = new Workshop_Service_OrderWorkshopDetail();
        $depotAffiliateService = new Management_Service_DepotAffiliate();
        $settingService = new Management_Service_Setting();

        $workorder = $workorderService->findById($workorderId);
        $workorderDetailsInfo = $workorderDetailService->getDetailsInfoByWorkorderId($workorderId);
        $warehouseDepotIds = $depotAffiliateService->getDepotAndAffiliateDepotIds($workorder->getDepotId(), BAS_Shared_Model_DepotAffiliate::TYPE_WAREHOUSE_STOCK_BUY_SELL);
        $mainAccountingDepotId = $settingService->getMainAccountingDepot($workorder->getDepotId());

        $form = new Workshop_Form_WorkorderDetailsTab([
            'isWorkorderReadonly' => $workorderService->isWorkorderReadonly($workorder, $userId),
            'workorderDetailsInfo' => $workorderDetailsInfo,
            'depotId' => $mainAccountingDepotId,
            'direction' => Workshop_Form_WorkorderDetailsTab::DIRECTION_OUTPUT,
        ]);

        $form->populate(['workorderDetails' => $workorderDetailsInfo]);

        $this->appendJsEntryPoint();
        $this->view->assign([
            'workorder' => $workorder,
            'workorderDetailsInfo' => $workorderDetailsInfo,
            'detailsSubForm' => $form->getSubForm('workorderDetails'),
            'isWorkorderReadonly' => $workorderService->isWorkorderReadonly($workorder, $userId),
            'warehouseDepotIds' => $warehouseDepotIds,
            'workorderDepotId' => $workorder->getDepotId(),
            'workorderInvoicedTimeTotal' => $workorderDetailService->getWorkorderInvoicedTime($workorderId),
            'workorderSpentTimeTotal' => $workorderDetailService->getWorkorderSpentTime($workorderId),
        ]);
    }

    public function addWorkorderDetailsAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);
        $detailsData = (array)$this->getParam('detailsData', []);
        $tradeInPrices = (array)$this->getParam('tradeInPrices', []);

        $userId = $this->getUserInfo()->getId();

        $workorderDetailService = new Workshop_Service_OrderWorkshopDetail();

        try {
            $workorderDetailService->addDetails($workorderId, $detailsData, $tradeInPrices, $userId);
        } catch (BAS_Shared_Exception_ItemStockException $e) {
            $this->getResponseHelper()
                ->setErrors([$e->getMessage()])
                ->setStatusCode(BAS_Shared_Http_StatusCode::BAD_REQUEST)
                ->json();
        }

        $this->view->clearVars();
    }

    public function deleteWorkorderDetailAction()
    {
        $detailId = (int)$this->getParam('id', 0);
        $userId = $this->getUserInfo()->getId();

        $workorderService = new Workshop_Service_Workorder();
        $workorderService->deleteWorkorderDetail($detailId, $userId);

        $this->view->clearVars();
    }

    public function saveWorkorderDetailsAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $depotId = $this->getLoggedInDepotId();
        $userId = $this->getUserInfo()->getId();

        $settingService = new Management_Service_Setting();
        $mainAccountingDepotId = $settingService->getMainAccountingDepot($depotId);

        $workorderService = new Workshop_Service_Workorder();
        $workorderDetailService = new Workshop_Service_OrderWorkshopDetail();

        $workorder = $workorderService->findById($workorderId);
        $originalDetailsInfo = $workorderDetailService->getDetailsInfoByWorkorderId($workorderId);

        $form = new Workshop_Form_WorkorderDetailsTab([
            'isWorkorderReadonly' => $workorderService->isWorkorderReadonly($workorder, $userId),
            'workorderDetailsInfo' => $originalDetailsInfo,
            'depotId' => $mainAccountingDepotId,
            'direction' => Workshop_Form_WorkorderDetailsTab::DIRECTION_INPUT,
        ]);

        if ($form->isValid($this->getAllParams())) {
            $detailsData = $form->getValue('workorderDetails');

            try {
                $workorderService->saveWorkorderDetails($workorderId, $detailsData, $userId);
            } catch (BAS_Shared_Exception_ItemStockException $e) {
                $this->getResponseHelper()
                    ->setErrors([$e->getMessage()])
                    ->setStatusCode(BAS_Shared_Http_StatusCode::BAD_REQUEST)
                    ->json();
            }
        } else {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::BAD_REQUEST);

            $this->view->clearVars();

            $messageFormatter = new BAS_Shared_Form_Formatter_Messages($form);
            $this->view->assign([
                'messages' => $messageFormatter->getMessages(),
            ]);
        }
    }

    public function saveSpentTimeAction()
    {
        $detailId = (int)$this->getParam('id', 0);
        $spentTime = $this->getParam('spentTime', '');

        $workorderService = new Workshop_Service_OrderWorkshopDetail();
        $workorderService->saveSpentTimeByDetailId($detailId, $spentTime, $this->getUserInfo()->getId());
    }

    public function timeTrackGridAction()
    {
        $workorderId = (int)$this->getParam('workorderId', 0);

        $workorderTimeService = new Workshop_Service_WorkorderTime();
        $timeTrackGridService = new Workshop_Service_TimeTrackGrid($this->getView(), $this->getLoggedInDepotId(), $workorderId);

        $grid = $timeTrackGridService->getGrid($workorderTimeService->getTimeTrackGridSource($workorderId));

        $this->getView()->assign([
            'workorderId' => $workorderId,
            'totalSpentTime' => $workorderTimeService->getWorkorderSpentTime($workorderId),
            'grid' => $grid,
        ]);
    }

    public function editWorkorderTimeAction()
    {
        $orderWorkshopTimeId = (int)$this->getParam('id', 0);
        $workorderTimeService = new Workshop_Service_WorkorderTime();

        $workorderTimeRecord = $workorderTimeService->getById($orderWorkshopTimeId);
        $this->view->clearVars();
        
        $form = new Workshop_Form_OrderWorkshopTimeEdit(['isNew' => false], $this->getLoggedInDepotId(), $workorderTimeRecord);

        if (! $this->getRequest()->isPost()) {
            $personService = new Management_Service_Person();
            /** @var BAS_Shared_Model_Person $person */
            $person = $personService->find($workorderTimeRecord->getPersonId());
            $workorderTimeRecord
                ->setPersonName($person->getName())
                ->setPersonEmployeeNumber($person->getEmployeeNumber());

            $form->populateFirstFormFromModel($workorderTimeRecord);

            $this->getView()->assign([
                'orderWorkshopTimeId' => $orderWorkshopTimeId,
                'workorderTimeRecord' => $workorderTimeRecord,
                'form' => $form,
            ]);
            return;
        }

        if (! $form->isValid($this->getAllParams())) {
            $this->getResponse()->setHttpResponseCode(BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);

            $this->view->assign([
                'errors' => $form->getMessages(),
            ]);
            return;
        }

        $workorderTimeRecord = $form->getEntityToUpdate($workorderTimeRecord, $this->getUserInfo()->getId());

        $workorderTimeService->saveModel($workorderTimeRecord, BAS_Shared_Model_AbstractMapper::SAVE_TYPE_UPDATE);

        $workorderTimeRecords = $form->getEntitiesToInsert($workorderTimeRecord, $this->getAllParams(), $this->getUserInfo()->getId());

        if ([] !== $workorderTimeRecords) {
            foreach ($workorderTimeRecords as $workorderTimeRecord) {
                $workorderTimeService->saveModel($workorderTimeRecord, BAS_Shared_Model_AbstractMapper::SAVE_TYPE_INSERT);
            }
        }
    }

    public function deleteWorkorderTimeAction()
    {
        $orderWorkshopTimeId = (int)$this->getParam('id', 0);
        $workorderTimeService = new Workshop_Service_WorkorderTime();

        $workorderTimeRecord = $workorderTimeService->getById($orderWorkshopTimeId);
        $workorderTimeRecord
            ->setUpdatedBy($this->getUserInfo()->getId())
            ->setUpdatedAt($workorderTimeService->getCurrentDate())
            ->setArchived(BAS_Shared_Model_Workshop_OrderWorkshopTime::ARCHIVED);

        $workorderTimeService->saveModel($workorderTimeRecord, BAS_Shared_Model_AbstractMapper::SAVE_TYPE_UPDATE);

        $this->view->clearVars();
    }

    public function syncWorkorderDetailAccountGroupIdAction()
    {
        $detailId = (int)$this->getParam('detailId', 0);

        $workorderDetailService = new Workshop_Service_OrderWorkshopDetail();
        $actualAccountGroupId = $workorderDetailService->syncAccountGroupId($detailId);

        $this->view->clearVars();
        $this->view->actualAccountGroupId = $actualAccountGroupId;
    }

    public function checkItemsExistInComposedTasksAction()
    {
        $taskIds = $this->getParam('taskIds', []);

        $productCompositionService = new Order_Service_ProductComposition();
        $itemsExist = $productCompositionService->checkItemsExistInComposedProducts($taskIds);

        $this->view->clearVars();
        $this->view->assign([
            'result' => $itemsExist,
        ]);
    }

    /**
     * @return BAS_Shared_Controller_Action_Helper_Response
     */
    private function getResponseHelper()
    {
        return $this->getHelper('Response');
    }

}