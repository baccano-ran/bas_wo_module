<?php

/**
 * Class Workshop_SurchargeController
 */
class Workshop_SurchargeController extends BAS_Shared_Controller_Action_Abstract
{

    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'add' => 'json',
            'edit' => 'json',
            'save' => 'json',
            'delete' => 'json',
        ])->initContext();
    }

    public function indexAction()
    {
        $surchargeService = new Workshop_Service_Surcharge();
        $surchargeGridService = new Workshop_Service_SurchargeGrid($this->view);

        $this->view->grid = $surchargeGridService->getGrid($surchargeService->getGridSource($this->getLoggedInDepotId()));
    }

}