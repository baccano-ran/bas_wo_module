<?php
/**
 * Class Workshop_WorkorderAdministrationTabController
 */
class Workshop_WorkorderAdministrationTabController extends BAS_Shared_Controller_Action_Abstract
{
    /**
     * Initialize
     */
    public function init()
    {
        parent::init();

        /** @var Zend_Controller_Action_Helper_AjaxContext $ajaxContextHelper */
        $ajaxContextHelper = $this->getHelper('ajaxContext');
        $ajaxContextHelper->addActionContexts([
            'make-order' => 'json',
        ])->initContext();
    }

    /**
     * Get workshop order payment records.
     */
    public function getAction()
    {
        $orderId = (int) $this->getParam('id');
        $isReadonly = (bool) $this->getParam('isReadonly', false);

        $variousOrderService = new VariousOrder_Service_OrderVarious();
        $orderWorkshopService = new Order_Service_OrderWorkshop();
        $orderVariousInvoice = new VariousOrder_Service_OrderVariousInvoice();
        $customerDetails = $orderWorkshopService->getCustomerDetailsByWorkOrderId($orderId);
        $invoiceDetail = $orderVariousInvoice->isOrderInvoiced($orderId);
        $payments = $orderWorkshopService->getWorkorderPaymentInvoices($orderId);
        $orderDetails = $variousOrderService->getOrderDetails($orderId);
        $lockService = new Payment_Service_Locking();
        $processPaymentService = new Payment_Service_ProcessPayment();
        $paymentService = new Payment_Service_VehiclePayment();
        $lockData = [];
        $user = $this->getUserInfo();
        $loginUserId = $user->getId();

        if ($processPaymentService->isAllowed($user, 'administration.vehicle-payment.payments-sold-vehicles-process')) {
            $lockData['lockExist'] = $lockService->processLock($loginUserId, null, $orderId);
            // If lock exist then get locked data
            if ((!$lockData['lockExist']) ) {
                $lockData['lockData'] = $lockService->getLock();
            }
        }
        $localizeFilter = new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => 2]);

        $this->view->paymentType = $this->_constants->constant->PAYMENT_TYPE->toArray();
        $this->view->assign([
            'payments' => $payments['invoices'],
            'totalPaidPrice' => $payments['totalPrice'],
            'salesPrice' => $localizeFilter->filter($payments['total']),
            'currency' => $payments['currency'],
            'totalToPay' => ($payments['total'] - $payments['totalPrice']),
            'customerDetails' => $customerDetails,
            'currencyList' => $paymentService->getCurrencyList(),
            'orderId' => $orderId,
            'lockData' => $lockData,
            'lockExist' => isset($lockData['lockExist']) ? $lockData['lockExist'] : '',
            'invoiceDetail' => $invoiceDetail,
            'order' => $orderDetails,
            'isReadonly' => $isReadonly,
            'referenceType' => BAS_Shared_Model_PaymentOrder::REFERENCE_TYPE_WORKSHOP,
            'orderType' => 'workOrder'
        ]);

    }
}