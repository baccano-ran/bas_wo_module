<?php

/**
 * Class Workshop_Form_WorkorderDetailsTab
 */
class Workshop_Form_WorkorderDetailsTab extends BAS_Shared_Form_Abstract
{
    /**
     * @var BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    private $workorderDetailsInfo = [];
    private $depotId;
    private $isWorkorderReadonly;

    private $bookingCodeTypeMultiOptions;
    private $bookingCodeOptions;
    private $onInvoiceMultiOptions;
    private $labourRateOptions;

    public function init()
    {
        $this->addSubForm($this->getWorkorderDetailsSubForm(), 'workorderDetails');

        if ($this->direction === self::DIRECTION_OUTPUT) {
            $this->loadDefaultDecorators();
            $this->render();
        }
    }

    public function getWorkorderDetailsSubForm()
    {
        $subForm = new Zend_Form_SubForm();

        foreach ($this->workorderDetailsInfo as $key => $workorderDetailInfo) {
            $subForm->addSubForm($this->getWorkorderDetailSubForm($workorderDetailInfo), $key);
        }

        return $subForm;
    }

    public function getWorkorderDetailSubForm(BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo $detailInfo)
    {
        $elements = [];

        $isItem = $detailInfo->getType() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM;
        $isTask = $detailInfo->getType() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK;

        $isTradeInItem = $isItem && $detailInfo->getTradeIn() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TRADE_IN_YES;
        $isComposedSubDetail = $detailInfo->isComposedSubDetail();

        $isStockItem = $isItem && $detailInfo->getNoStockItem() === BAS_Shared_Model_Item::NO_STOCK_ITEM_NO;
        $isNoStockItem = $isItem && $detailInfo->getNoStockItem() === BAS_Shared_Model_Item::NO_STOCK_ITEM_YES;

        $isHourBasedTask = $isTask && $detailInfo->getWorkshopTaskType() === BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_HOUR_BASED;
        $isTimeActual = $isHourBasedTask && $detailInfo->getWorkshopTimeType() === BAS_Shared_Model_Product::WORKSHOP_TIME_TYPE_ACTUAL;
        $isTimeFixed = $isHourBasedTask && $detailInfo->getWorkshopTimeType() === BAS_Shared_Model_Product::WORKSHOP_TIME_TYPE_FIXED;

        $isEmptyWorkshopTaskType = $isTask && $detailInfo->getWorkshopTaskType() === null;
        $isFixedPriceTask = $isTask && $detailInfo->getWorkshopTaskType() === BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_FIXED_PRICE;
        $isFixedPriceOptional = $isFixedPriceTask && $detailInfo->getFixedPriceOptional() === BAS_Shared_Model_Product::FIXED_PRICE_OPTIONAL_YES;
        $isFixedPriceNotOptional = $isFixedPriceTask && $detailInfo->getFixedPriceOptional() === BAS_Shared_Model_Product::FIXED_PRICE_OPTIONAL_NO;

        $isExternalTask = $isTask && $detailInfo->getWorkshopTaskType() === BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL;
        $isNotExternalTask = $isTask && $detailInfo->getWorkshopTaskType() !== BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL;

        $isComposedTask = $isTask && $detailInfo->getWorkshopTaskType() === BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_COMPOSED;
        $isCompositionPriceComposed = $isComposedTask && $detailInfo->getCompositionPriceType() === BAS_Shared_Model_Product::COMPOSITION_PRICE_TYPE_COMPOSED;
        $isCompositionPriceFixed = $isComposedTask && $detailInfo->getCompositionPriceType() === BAS_Shared_Model_Product::COMPOSITION_PRICE_TYPE_FIXED;
        $isMainProductPriceFixed = $detailInfo->getMainProductCompositionPriceType() === BAS_Shared_Model_Product::COMPOSITION_PRICE_TYPE_FIXED;
        $isMainProductPriceComposed = $detailInfo->getMainProductCompositionPriceType() === BAS_Shared_Model_Product::COMPOSITION_PRICE_TYPE_COMPOSED;

        $isAllowedDiscount = $this->getView()->isAllowed('workshop.workorder.discount');

        $isWorkorderReadonly = $this->isWorkorderReadonly;
        $isInvoiceDescriptionDisabled = $isWorkorderReadonly || $isMainProductPriceFixed;

        $isQuantityDisabled = $isWorkorderReadonly
            || $isTimeFixed
            || $isCompositionPriceComposed
            || $isMainProductPriceFixed
        ;
        $isQuantityHidden = false;

        $isOriginalPriceDisabled = $isWorkorderReadonly;
        $isOriginalPriceHidden = true;
        $isOriginalPriceTimeBased = false;

        $isOrderPriceDisabled =
            $isWorkorderReadonly
            || !$isAllowedDiscount
            || $isTradeInItem
            || $isHourBasedTask
            || $isFixedPriceNotOptional
            || $isComposedTask
            || $isMainProductPriceFixed
        ;

        $isOrderPriceHidden = false;
        $isOrderPriceTimeBased = $isTimeActual || $isFixedPriceOptional;
        $isInvoiceTimePriceBased = $isFixedPriceOptional;

        $isDiscountDisabled =
            $isWorkorderReadonly
            || !$isAllowedDiscount
            || $isTradeInItem
            || $isNoStockItem
            || $isHourBasedTask
            || $isExternalTask
            || $isFixedPriceTask
            || $isComposedTask
            || $isMainProductPriceFixed
        ;
        $isDiscountHidden = $isTradeInItem
            || $isNoStockItem
            || $isHourBasedTask
            || $isExternalTask
            || $isFixedPriceTask
            || $isComposedTask
            || $isMainProductPriceFixed
        ;

        $isDepotBookingCodeDisabled = $isWorkorderReadonly || $isComposedSubDetail;
        $isDepotBookingCodeHidden = false;

        $isOnInvoiceDisabled = $isWorkorderReadonly || $isMainProductPriceFixed;

        $isLabourRateDisabled = $isWorkorderReadonly
            || $isTimeFixed
            || ($isFixedPriceTask && !$isFixedPriceOptional)
            || $isMainProductPriceFixed
        ;
        $isLabourRateHidden = $isItem || $isExternalTask || $isComposedTask || $isEmptyWorkshopTaskType;

        $isInvoiceTimeDisabled = $isWorkorderReadonly
            || $isFixedPriceNotOptional
            || $isTimeFixed
            || $isMainProductPriceFixed
        ;
        $isInvoiceTimeHidden = $isItem || $isExternalTask || $isComposedTask || $isEmptyWorkshopTaskType;

        $isSpentTimeDisabled = $isWorkorderReadonly || $isMainProductPriceFixed;
        $isSpentTimeHidden = $isInvoiceTimeHidden;

        $isCostPriceHidden = $isTradeInItem || $isStockItem || $isNotExternalTask;

        $isSupplierHidden = $isNotExternalTask || $isStockItem;
        $isSupplierRequired = $isExternalTask || ($isNoStockItem && $detailInfo->getDepotAccountGroupId() === BAS_Shared_Model_DepotAccountGroup::ACCOUNT_GROUP_VARIOUS_PARTS);
        $isSupplierDisabled = $isWorkorderReadonly;

        $elements[] = $this->createElement('Hidden', 'id', [
            'decorators' => ['ViewHelper'],
            'attribs' => ['data-detail_info' => json_encode($detailInfo->toArray())],
            'class' => 'js-detail_info',
        ]);

        $elements[] = $this->createElement('Hidden', 'referenceId', [
            'decorators' => ['ViewHelper'],
        ]);

        $elements[] = $this->createElement('Hidden', 'sequence', [
            'decorators' => ['ViewHelper'],
            'class' => 'js-sequence-number',
        ]);

        $elements[] = $this->createElement($isOriginalPriceHidden ? 'Hidden' : 'Text', 'priceOriginal', [
            'decorators' => ['ViewHelper'],
            'class' => 'js-detail-original-price',
            'validators' => [new Zend_Validate_Float('en_GB')],
            'attribs' => [
                'disabled' => $isOriginalPriceDisabled ? 'disabled' : null,
                'data-is_price_time_based' => $isOriginalPriceTimeBased,
            ],
        ]);

        $elements[] = $this->createElement('Text', 'invoiceDescription', [
            'placeholder' => 'placeholder_enter_invoice_description',
            'class' => 'js-detail-description',
            'decorators' => ['ViewHelper'],
            'attribs' => ['disabled' => $isInvoiceDescriptionDisabled ? 'disabled' : null],
        ]);

        $quantityValidators = [
            $this->getQuantityCreditValidator($detailInfo->getType()),
            $this->getQuantityChangeValidator($detailInfo->getType(), $detailInfo->getQuantity()),
        ];

        if ($detailInfo->getType() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM &&
            $detailInfo->getUnit() === BAS_Shared_Model_Item::UNIT_PRODUCT
        ) {
            $quantityValidators[] = new Zend_Validate_Int();
        } else {
            $quantityValidators[] = new Zend_Validate_Float('en_GB');
        }

        $elements[] = $this->createElement($isQuantityHidden ? 'Hidden' : 'Text', 'quantity', [
            'placeholder' => 'placeholder_enter_quantity',
            'decorators' => ['ViewHelper'],
            'class' => 'js-detail-quantity',
            'validators' => $quantityValidators,
            'attribs' => [
                'disabled' => $isQuantityDisabled ? 'disabled' : null,
                'data-composition_quantity' => $detailInfo->getCompositionQuantity(),
            ],
        ]);

        $elements[] = $this->createElement($isOrderPriceHidden ? 'Hidden' : 'Text', 'priceOrder', [
            'placeholder' => 'placeholder_enter_price',
            'decorators' => ['ViewHelper'],
            'class' => 'js-detail-order-price',
            'validators' => [new Zend_Validate_Float('en_GB')],
            'attribs' => [
                'disabled' => $isOrderPriceDisabled ? 'disabled' : null,
                'data-is_price_time_based' => $isOrderPriceTimeBased,
            ],
        ]);

        $elements[] = $this->createElement($isDiscountHidden ? 'Hidden' : 'Text', 'discount', [
            'placeholder' => 'placeholder_enter_discount',
            'decorators' => ['ViewHelper'],
            'class' => 'js-detail-discount',
            'attribs' => ['disabled' => $isDiscountDisabled ? 'disabled' : null],
            'validators' => [
                new Zend_Validate_Float('en_GB'),
            ],
        ]);

        $elements[] = $this->createElement($isDepotBookingCodeHidden ? 'Hidden' : 'Select', 'bookingCodeType', [
            'decorators' => ['ViewHelper'],
            'multiOptions' => $this->getBookingCodeTypeMultiOptions(),
            'class' => 'js-booking-code-type',
            'attribs' => ['disabled' => $isDepotBookingCodeDisabled ? 'disabled' : null],
        ]);

        $elements[] = $this->createElement($isDepotBookingCodeHidden ? 'Hidden' : 'Select', 'depotBookingCodeId', array_merge([
            'required' => $isDepotBookingCodeDisabled ? false : true,
            'decorators' => ['ViewHelper'],
            'class' => 'js-booking-code-id',
            'attribs' => ['disabled' => $isDepotBookingCodeDisabled ? 'disabled' : null],
        ], $this->getBookingCodeOptions($detailInfo->getBookingCodeType())));

        $elements[] = $this->createElement('Select', 'onInvoice', [
            'decorators' => ['ViewHelper'],
            'multiOptions' => $this->getOnInvoiceMultiOptions(),
            'attribs' => ['disabled' => $isOnInvoiceDisabled ? 'disabled' : null],
        ]);

        $elements[] = $this->createElement($isLabourRateHidden ? 'Hidden' : 'Select', 'labourRateId', array_merge([
            'decorators' => ['ViewHelper'],
            'class' => 'js-labour-rate-id',
            'required' => $isLabourRateDisabled || $isLabourRateHidden ? false : true,
            'attribs' => ['disabled' => $isLabourRateDisabled ? 'disabled' : null],
        ], $this->getLabourRateOptions()));

        $elements[] = $this->createElement($isLabourRateHidden ? 'Hidden' : 'Note', 'labourRateRate', [
            'decorators' => ['ViewHelper'],
        ]);

        $elements[] = $this->createElement($isInvoiceTimeHidden ? 'Hidden' : 'Text', 'invoiceTime', [
            'placeholder' => 'placeholder_enter_invoice_hours',
            'decorators' => ['ViewHelper'],
            'class' => 'js-detail-invoice-time',
            'validators' => [new Zend_Validate_Float('en_GB')],
            'attribs' => [
                'disabled' => $isInvoiceTimeDisabled ? 'disabled' : null,
                'data-is_time_price_based' => $isInvoiceTimePriceBased,
            ],
        ]);

        $elements[] = $this->createElement($isSpentTimeHidden ? 'Hidden' : 'Text', 'spentTime', [
            'placeholder' => 'placeholder_enter_spent_hours',
            'decorators' => ['ViewHelper'],
            'class' => 'hide',
            'validators' => [new Zend_Validate_Float('en_GB')],
            'attribs' => ['disabled' => $isSpentTimeDisabled ? 'disabled' : null],
        ]);

        $elements[] = $this->createElement($isCostPriceHidden ? 'Hidden' : 'Text', 'costPrice', [
            'required' => $isCostPriceHidden ? false : true,
            'placeholder' => 'placeholder_enter_cost_price',
            'decorators' => ['ViewHelper'],
            'class' => 'js-detail-cost-price',
            'validators' => [new Zend_Validate_Float('en_GB')],
            'attribs' => ['disabled' => $isCostPriceHidden || $isWorkorderReadonly ? 'disabled' : null],
        ]);

        $elements[] = $this->createElement($isSupplierHidden ? 'Hidden' : 'Text', 'supplierName', [
            'placeholder' => 'placeholder_supplier',
            'decorators' => ['ViewHelper'],
            'class' => 'js-detail-supplier-name',
            'attribs' => ['disabled' => $isSupplierDisabled ? 'disabled' : null],
        ]);

        $elements[] = $this->createElement('Hidden', 'supplierId', [
            'decorators' => ['ViewHelper'],
            'class' => 'js-detail-supplier-id',
            'validators' => [new Zend_Validate_Int()],
            'required' => $isSupplierRequired,
            'validationLabel' => $this->getView()->translate('supplier'),
            'validationName' => 'supplierName',
        ]);

        $subForm = new Zend_Form_SubForm();
        $subForm->addElements($elements);

        if ($this->direction === self::DIRECTION_OUTPUT) {
            $this->setDetailFormOutputFilters($subForm, $detailInfo);
        } elseif ($this->direction === self::DIRECTION_INPUT) {
            $this->setDetailFormInputFilters($subForm);
        }

        return $subForm;
    }

    /**
     * @param int $detailType
     * @return Zend_Validate_Callback
     */
    private function getQuantityCreditValidator($detailType)
    {
        $validator = new Zend_Validate_Callback(function($value, $context) use ($detailType) {

            if ($value >= 0) {
                return true;
            }

            $isCreditAllowed = $detailType === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM
                ? $this->getView()->isAllowed('workshop.workorder.credit-items')
                : $this->getView()->isAllowed('workshop.workorder.credit-products')
            ;

            return $isCreditAllowed;
        });

        $detailMessage = $detailType === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM ? 'items' : 'products';
        $validator->setMessages([
            Zend_Validate_Callback::INVALID_VALUE => 'You don\'t have rights to credit ' . $detailMessage,
        ]);

        return $validator;
    }

    /**
     * @param int $detailType
     * @param float $originalQuantity
     * @return Zend_Validate_Callback
     */
    private function getQuantityChangeValidator($detailType, $originalQuantity)
    {
        $validator = new Zend_Validate_Callback(function($value, $context) use ($detailType, $originalQuantity) {

            if ($detailType === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK) {
                return true;
            }

            if ($value >= 0 && $originalQuantity >= 0) {
                return true;
            }

            if ($value < 0 && $originalQuantity < 0) {
                return true;
            }

            return false;
        });

        $validator->setMessages([
            Zend_Validate_Callback::INVALID_VALUE => 'you can\'t change the quantity sign of the item',
        ]);

        return $validator;
    }

    private function setDetailFormOutputFilters(Zend_Form $detailForm, BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo $detailInfo)
    {
        $detailForm->getElement('priceOriginal')->setFilters([
            $this->getNullableNormalizedToLocalizedFilter(),
        ]);

        $detailForm->getElement('quantity')->setFilters([
            new Zend_Filter_Callback(function($value) use ($detailInfo) {
                if ($value === null) {
                    return '';
                }

                $precision = $detailInfo->getUnit() === BAS_Shared_Model_Item::UNIT_PRODUCT ? 0 : 2;
                $normalizedToLocalizedFilter = new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => $precision]);
                $value = $normalizedToLocalizedFilter->filter($value);

                return $value;
            }),
        ]);

        $detailForm->getElement('priceOrder')->setFilters([
            $this->getNullableNormalizedToLocalizedFilter(),
        ]);
        $detailForm->getElement('costPrice')->setFilters([
            $this->getNullableNormalizedToLocalizedFilter(),
        ]);
        $detailForm->getElement('discount')->setFilters([
            $this->getOutputPercentFilter(),
        ]);
        $detailForm->getElement('invoiceTime')->setFilters([
            new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => 2]),
        ]);
        $detailForm->getElement('spentTime')->setFilters([
            new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => 2]),
        ]);
        $detailForm->getElement('spentTime')->setFilters([
            new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => 2]),
        ]);
        $detailForm->getElement('labourRateRate')->setFilters([
            $this->getNullableNormalizedToLocalizedFilter(),
        ]);
    }

    private function setDetailFormInputFilters(Zend_Form $detailForm)
    {
        $detailForm->getElement('invoiceDescription')->setFilters([
            new Zend_Filter_StringTrim(),
        ]);
        $detailForm->getElement('quantity')->setFilters([
            new Zend_Filter_LocalizedToNormalized(['locale' => 'nl_NL']),
        ]);
        $detailForm->getElement('priceOriginal')->setFilters([
            new Zend_Filter_LocalizedToNormalized(['locale' => 'nl_NL']),
        ]);
        $detailForm->getElement('costPrice')->setFilters([
            new Zend_Filter_LocalizedToNormalized(['locale' => 'nl_NL']),
        ]);
        $detailForm->getElement('priceOrder')->setFilters([
            new Zend_Filter_LocalizedToNormalized(['locale' => 'nl_NL']),
        ]);
        $detailForm->getElement('discount')->setFilters([
            $this->getInputPercentFilter(),
        ]);
        $detailForm->getElement('invoiceTime')->setFilters([
            new Zend_Filter_LocalizedToNormalized(['locale' => 'nl_NL']),
        ]);
        $detailForm->getElement('spentTime')->setFilters([
            new Zend_Filter_LocalizedToNormalized(['locale' => 'nl_NL']),
        ]);
    }

    /**
     * @return Zend_Filter_Callback
     */
    private function getNullableNormalizedToLocalizedFilter()
    {
        return new Zend_Filter_Callback(function($value) {
            if ($value === null) {
                return '';
            }

            $normalizedToLocalizedFilter = new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => 2]);
            $value = $normalizedToLocalizedFilter->filter($value);

            return $value;
        });
    }

    /**
     * @return Zend_Filter_Callback
     */
    private function getOutputPercentFilter()
    {
        return new Zend_Filter_Callback(function($value) {
            $normalizedToLocalizedFilter = new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => 2]);

            $value = (float)$value * 100;
            $value = $normalizedToLocalizedFilter->filter($value);
            $value = $value . ' %';

            return $value;
        });
    }

    /**
     * @return Zend_Filter_Callback
     */
    private function getInputPercentFilter()
    {
        return new Zend_Filter_Callback(function($originalValue) {
            $localizedToNormalizedFilter = new Zend_Filter_LocalizedToNormalized(['locale' => 'nl_NL', 'precision' => 2]);
            $value = $originalValue;

            $value = str_replace(' %', '', $value);
            $value = $localizedToNormalizedFilter->filter($value);

            if (!is_numeric($value)) {
                return $originalValue;
            }

            return $value / 100;
        });
    }

    /**
     * @param array $workorderDetailsInfo
     * @return $this
     */
    public function setWorkorderDetailsInfo(array $workorderDetailsInfo)
    {
        $this->workorderDetailsInfo = [];

        foreach ($workorderDetailsInfo as $key => $workorderDetailInfo) {

            $this->workorderDetailsInfo[$key] = is_array($workorderDetailInfo)
                ? new BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo($workorderDetailInfo)
                : $workorderDetailInfo;
        }

        return $this;
    }

    /**
     * @param int $depotId
     * @return $this
     */
    public function setDepotId($depotId)
    {
        $this->depotId = $depotId;
        return $this;
    }

    /**
     * @param bool $direction
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    private function getBookingCodeTypeMultiOptions()
    {
        if ($this->bookingCodeTypeMultiOptions !== null) {
            return $this->bookingCodeTypeMultiOptions;
        }

        $this->bookingCodeTypeMultiOptions = [
            '' => 'placeholder_select_booking_code_type',
            BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_EXTERNAL => BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_EXTERNAL_NAME,
            BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_INTERNAL => BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_INTERNAL_NAME,
            BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_GUARANTEE => BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_GUARANTEE_NAME,
            BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_CONTRA_BOOKING => BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_CONTRA_NAME,
        ];

        return $this->bookingCodeTypeMultiOptions;
    }

    private function getBookingCodeOptions($detailBookingCodeType)
    {
        if ($this->bookingCodeOptions !== null) {
            return $this->bookingCodeOptions;
        }

        $postingSetupService = new Management_Service_DepotPostingSetup();
        $bookingCodes = $postingSetupService->findBookingCodesByPostingSetupModule(
            BAS_Shared_Model_DepotPostingSetup::MODULE_WORKSHOP,
            $this->depotId
        );

        $this->bookingCodeOptions = [];
        $this->bookingCodeOptions['multiOptions'] = ['' => 'placeholder_select_booking_code'];
        $this->bookingCodeOptions['multiOptions'] += BAS_Shared_Utils_Array::listData($bookingCodes, 'id', 'name');

        $this->bookingCodeOptions['optionClasses'] = ['' => ''];

        foreach ($bookingCodes as $bookingCode) {
            $this->bookingCodeOptions['optionClasses'][$bookingCode->getId()] = sprintf('" data-type="%s', $bookingCode->getType());
        }

        return $this->bookingCodeOptions;
    }

    private function getOnInvoiceMultiOptions()
    {
        if ($this->onInvoiceMultiOptions !== null) {
            return $this->onInvoiceMultiOptions;
        }

        $this->onInvoiceMultiOptions = [
            BAS_Shared_Model_Workshop_OrderWorkshopDetail::ON_INVOICE_YES => 'yes',
            BAS_Shared_Model_Workshop_OrderWorkshopDetail::ON_INVOICE_NO => 'no',
        ];

        return $this->onInvoiceMultiOptions;
    }

    private function getLabourRateOptions()
    {
        if ($this->labourRateOptions !== null) {
            return $this->labourRateOptions;
        }

        $labourRateService = new Workshop_Service_LabourRate;

        $this->labourRateOptions = [];
        $this->labourRateOptions['multiOptions'] = ['' => 'placeholder_select_rate'];
        $this->labourRateOptions['optionClasses'] = ['' => ''];

        $rates = $labourRateService->findAllByDepotAndDepartment($this->depotId, BAS_Shared_Model_Department::DEPARTMENT_ID_WORKSHOP);

        foreach ($rates as $rate) {
            $this->labourRateOptions['multiOptions'][$rate->getId()] = $rate->getName();
            $this->labourRateOptions['optionClasses'][$rate->getId()] = sprintf('" data-rate="%s', $rate->getRate());
        }

        return $this->labourRateOptions;
    }

    /**
     * @param bool $isWorkorderReadonly
     */
    public function setIsWorkorderReadonly($isWorkorderReadonly)
    {
        $this->isWorkorderReadonly = $isWorkorderReadonly;
    }

}
