<?php

class Bam_Form_ResponseTemplate_Update extends Bam_Form_ResponseTemplate_Create
{
    /**
     * @param BAS_Shared_Model_Crawler_ResponseTemplate $model
     * @return BAS_Shared_Model_Crawler_ResponseTemplate
     */
    protected function updateEntity(BAS_Shared_Model_Crawler_ResponseTemplate $model)
    {
        return $model
            ->setUpdatedAt(new DateTime())
            ->setUpdatedBy(BAS_Shared_Auth_Service::getIdentity()->getId())
        ;
    }

    protected function _getElements()
    {
        $createdAt = new Bam_Form_Element_NoteWithHiddenValue('createdAt');
        $createdAt
            ->setDisplayDateFormat('d-m-Y H:i')
            ->setLabel('created_at')
        ;

        $createdBy = new Bam_Form_Element_NoteWithHiddenValue('created_name');
        $createdBy
            ->setLabel('created_by')
            ->setAttrib('maxlength', 7)
            ->addValidators([
                new Zend_Validate_Int(),
                new Zend_Validate_Between(0, BAS_Shared_Db_MySQL_TypesRange::SMALLINT_MAX, true),
            ])
        ;

        $updatedAt = new Bam_Form_Element_NoteWithHiddenValue('updatedAt');
        $updatedAt
            ->setDisplayDateFormat('d-m-Y H:i')
            ->setLabel('updated_at')
        ;

        $updatedBy = new Bam_Form_Element_NoteWithHiddenValue('updated_name');
        $updatedBy
            ->setLabel('updated_by')
            ->setAttrib('maxlength', 7)
            ->addValidators([
                new Zend_Validate_Int(),
                new Zend_Validate_Between(0, BAS_Shared_Db_MySQL_TypesRange::SMALLINT_MAX, true),
            ])
        ;

        return array_merge(parent::_getElements(), [
            $createdAt,
            $createdBy,
            $updatedAt,
            $updatedBy,
        ]);
    }

    protected function _getLastElements()
    {
        $buttons = new Bam_Form_Element_ControlButton('controlButton');
        $urlToIndex = $this->getView()->url([
            'module' => 'bam',
            'controller' => 'response-template',
            'action' => 'index',
        ], null, true);

        $buttons->setButtons([
            Bam_Form_Element_ControlButton::BUTTON_SAVEANDCANCEL,
            Bam_Form_Element_ControlButton::BUTTON_SAVE,
            Bam_Form_Element_ControlButton::BUTTON_CANCEL,
        ]);
        $buttons
            ->getButton(Bam_Form_Element_ControlButton::BUTTON_CANCEL)
            ->setAttrib('data-url', $urlToIndex);
        $buttons
            ->getButton(Bam_Form_Element_ControlButton::BUTTON_SAVEANDCANCEL)
            ->setAttrib('data-url', $urlToIndex);

        return [$buttons];
    }
}