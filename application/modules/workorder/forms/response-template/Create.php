<?php

class Bam_Form_ResponseTemplate_Create extends Bam_Form_Base
{
    const MAIN_GROUP = 'response_template';

    /**
     * @return int
     */
    public function getAuthId()
    {
        return BAS_Shared_Auth_Service::getIdentity()->getId();
    }

    /**
     * @param BAS_Shared_Model_Crawler_ResponseTemplate $model
     * @return BAS_Shared_Model_Crawler_ResponseTemplate
     */
    protected function updateEntity(BAS_Shared_Model_Crawler_ResponseTemplate $model)
    {
        return $model
            ->setCreatedAt(new DateTime())
            ->setCreatedBy($this->getAuthId())
        ;
    }

    /**
     * @return BAS_Shared_Model_Crawler_ResponseTemplate
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function getEntity()
    {
        $model = new BAS_Shared_Model_Crawler_ResponseTemplate();
        $values = $this->getValues();
        $values = $values['response_template'];
        /** @var BAS_Shared_Model_Crawler_ResponseTemplateMapper $mapper */
        $mapper = BAS_Shared_Model_MapperRegistry::get('Crawler_ResponseTemplate');
        $model->populate(array_intersect_key($values, $mapper->getMap()));
        return $this->updateEntity($model);
    }

    protected function _getElements()
    {
        $elements = [];

        $id = new Zend_Form_Element_Hidden('id');
        $id
            ->setValue($this->getAttrib('id'))
        ;
        $this->setHiddenElement($id);
        $elements[] = $id;

        $subject = new Zend_Form_Element_Text('subject');
        $subject
            ->setLabel('subject')
            ->setAttrib('maxlength', 255)
            ->addValidator(new Zend_Validate_StringLength(['max' => 255]))
        ;
        $this->setRequiredElement($subject);
        $elements[] = $subject;

        $name = new Zend_Form_Element_Text('name');
        $name
            ->setLabel('name')
            ->setAttrib('maxlength', 255)
            ->addValidator(new Zend_Validate_StringLength(['max' => 255]))
        ;
        $this->setRequiredElement($name);
        $elements[] = $name;

        $body = new Zend_Form_Element_Textarea('body');
        $body
            ->setLabel('message')
        ;
        $this->setRequiredElement($body);
        $elements[] = $body;

        return $elements;
    }

    protected function _getLastElements()
    {
        $buttons = new Bam_Form_Element_ControlButton('controlButton');
        $urlToIndex = $this->getView()->url([
            'module' => 'bam',
            'controller' => 'response-template',
            'action' => 'index',
        ], null, true);

        $buttons->setButtons([
                Bam_Form_Element_ControlButton::BUTTON_SAVE,
                Bam_Form_Element_ControlButton::BUTTON_CANCEL,
            ]);
        $buttons
            ->getButton(Bam_Form_Element_ControlButton::BUTTON_CANCEL)
            ->setAttrib('data-url', $urlToIndex);
        $buttons
            ->getButton(Bam_Form_Element_ControlButton::BUTTON_SAVE)
            ->setAttrib('data-url', $urlToIndex);

        return [$buttons];
    }

    public function init()
    {
        $elements = array_merge($this->_getElements(),
            $this->_getLastElements() + ['closing' => new Bam_Form_Element_LastClosing('closing')]);

        /** @var Zend_Form_Element $element */
        foreach ($elements as $element) {
            $element->setBelongsTo(static::MAIN_GROUP);
        }

        $this->addElements($elements);
    }

    private function setHiddenElement(Zend_Form_Element $element)
    {
        $element
            ->removeDecorator('DtDdWrapper')
            ->removeDecorator('Label')
            ->getDecorator('HtmlTag')->setOption('class', 'form-hidden_field')
        ;
    }

    private function setRequiredElement(Zend_Form_Element $element)
    {
        $element
            ->addValidators([
                new Zend_Validate_NotEmpty
            ])
            ->setRequired(true)
            ->getDecorator('Label')->setOption('class', 'form-element-label form-required_field')
        ;
    }

}