<?php

/**
 * Class Bam_Form_Element_Grid
 * @property BAS_Shared_Grid_Bvb_Table $_grid
 */
class Bam_Form_Element_Grid extends Zend_Form_Element_Xhtml
{

    /** @var BAS_Shared_Grid_Bvb_Table */
    public $grid = null;
    public $addRecordLink = null;
    public $multiUpdateAction = null;


    public function init()
    {
        parent::init();
    }

    public function render(Zend_View_Interface $view = null)
    {
        ob_start();
        echo $this->getGrid();
        $gridHtml = ob_get_contents();
        ob_end_clean();

        $res = sprintf('<div class="embedded-grid item-list%s"><div class="confirmation-dialog"></div>', ($this->getGrid() ? '' : ' no-padding disabled-link'));

        /** @var Zend_Form_Decorator_HtmlTag $tagDecorator */
        $tagDecorator = $this->getDecorator('HtmlTag');
        $tagDecorator->setOption('openOnly', true);
        $tag = $tagDecorator->getTag();

        if ($this->getAddRecordLink()) {

            $res .= sprintf(
                '<div class="add-entity-block"><a href="%s" class="add-new-grid-record" data-confirm-title="%s" data-confirm-text="%s" data-confirm-button-yes="%s" data-confirm-button-no="%s"><img class="verticalAlignMiddle" src="%s" />%s</a></div>',
                $this->getAddRecordLink(),
                ucfirst($this->getView()->translate('confirm')),
                ucfirst($this->getView()->translate('unsaved_data_confirmation')),
                ucfirst($this->getView()->translate('yes')),
                ucfirst($this->getView()->translate('cancel')),
                $this->getView()->baseUrl('/images/add.png'),
                $this->getView()->translate('add_new_item'));

        }

        $gridTotalRecords = $this->getGrid() ? (int)$this->getGrid()->getSource()->getTotalRecords() : 0;

        if ($gridTotalRecords && $this->getMultiUpdateAction()) {

            $res .= sprintf(
                '<div class="multi-edit-block"><a href="%s">%s</a> %s %s</div>',
                $this->getMultiUpdateAction(),
                $this->getView()->formButton('multiEditSave', $this->getView()->translate('multi_edit_save'),
                    [
                        'class'       => 'grid-multi-edit-save-btn',
                        'style'       => 'display:none',
                    ]
                ),
                $this->getView()->formButton('multiEdit', $this->getView()->translate('multi_edit'),
                    [
                        'class'       => 'grid-multi-edit-btn',
                    ]
                ),
                $this->getView()->formButton('multiEditCancel', $this->getView()->translate('multi_edit_cancel'),
                    [
                        'class'       => 'grid-multi-edit-cancel-btn',
                        'style'       => 'display:none',
                    ]
                )
            );

        }

        $res .= $gridHtml;

        return $res . '</div></' . $tag . '>';
    }

    /**
     * @return BAS_Shared_Grid_Bvb_Table|null
     */
    public function getGrid()
    {
        return $this->grid;
    }

    /**
     * @param BAS_Shared_Grid_Bvb_Table $grid
     * @return $this
     */
    public function setGrid(BAS_Shared_Grid_Bvb_Table $grid)
    {
        $this->grid = $grid;
        return $this;
    }

    /**
     * @return null
     */
    public function getAddRecordLink()
    {
        return $this->addRecordLink;
    }

    /**
     * @param null $addRecordLink
     * @return Bam_Form_Element_Grid
     */
    public function setAddRecordLink($addRecordLink)
    {
        $this->addRecordLink = $addRecordLink;

        return $this;
    }

    /**
     * @return null
     */
    public function getMultiUpdateAction()
    {
        return $this->multiUpdateAction;
    }

    /**
     * @param null $multiUpdateAction
     * @return $this
     */
    public function setMultiUpdateAction($multiUpdateAction)
    {
        $this->multiUpdateAction = $multiUpdateAction;

        return $this;
    }

    /**
     * @param mixed $value
     * @param null $context
     * @return bool
     */
    public function isValid($value, $context = null)
    {
        return true;
    }

    protected function _removeDefaultDecorators(Zend_Form_Element $element)
    {
        $element
            ->removeDecorator('DtDdWrapper')
            ->removeDecorator('HtmlTag')
            ->removeDecorator('Label')
            ->removeDecorator('Errors');
    }

}
