<?php

/**
 * Class Bam_Form_Element_NoteWithHiddenValue
 */
class Bam_Form_Element_NoteWithHiddenValue extends Zend_Form_Element_Note
{
    protected $_renderIfEmpty = false;
    protected $_displayDateFormat = null;
    protected $_origValue = null;

    /**
     * @param Zend_View_Interface|null $view
     * @return string
     */
    public function render(Zend_View_Interface $view = null)
    {
        $value = $this->getValue();
        if ($value instanceof DateTime) {
            $value = $value->format('Y-m-d H:i:s');
        }
        $this->_origValue = $value;
        
        if (!$this->_renderIfEmpty && empty($value)) {
            return '';
        } else {
            if (null !== $this->getDisplayDateFormat()) {
                $this->setValue(date($this->getDisplayDateFormat(), strtotime($value)));
            }
            $this->getDecorator('Label')->setOption('class', 'form-element-label');
            return parent::render($view) . $this->_buildHiddenElement()->render($view);
        }
    }

    /**
     * @return Zend_Form_Element_Hidden
     */
    protected function _buildHiddenElement()
    {
        $element = new Zend_Form_Element_Hidden($this->getName());
        $element
            ->setValue($this->_getOrigValue())
            ->setBelongsTo($this->getBelongsTo())
            ->removeDecorator('DtDdWrapper')
            ->removeDecorator('Label')
            ->getDecorator('HtmlTag')->setOption('class', 'form-hidden_field')
        ;
        return $element;
    }

    /**
     * @param $format
     * @return $this
     */
    public function setDisplayDateFormat($format) {
        $this->_displayDateFormat = $format;
        
        return $this;
    }

    /**
     * @return boolean
     */
    public function getDisplayDateFormat()
    {
        return $this->_displayDateFormat;
    }

    /**
     * @return null
     */
    protected function _getOrigValue()
    {
        return $this->_origValue;
    }
    
    
}