<?php

/**
 * Class Bam_Form_Element_DateSelect
 * @property DateTime $_date
 */
class Bam_Form_Element_DateSelect extends Zend_Form_Element_Hidden
{
    const START_YEAR = 1965;

    const NOT_SELECT_DAY = 'select_day';
    const NOT_SELECT_MONTH = 'select_month';
    const NOT_SELECT_YEAR = 'select_year';

    protected $_hashObj;
    protected $_time = [
        'full'  => 0,
        'day'   => null,
        'month' => null,
        'year'  => null,
    ];

    /** @var  int */
    protected $_endYear;

    protected $_inputVisible = [
        'day'   => true,
        'month' => true,
        'year'  => true,
    ];
    protected $_noSelectValue = [
        'day' => false,
        'month' => false,
        'year' => true,
    ];

    public function init()
    {
        parent::init();
        $this->_hashObj = spl_object_hash($this);
        $this->_endYear = date('Y');
    }

    /**
     * @param int $endYear
     * @return Bam_Form_Element_DateSelect
     */
    public function setEndYear($endYear)
    {
        $this->_endYear = $endYear;

        return $this;
    }

    /**
     * @param array $visible
     * @return $this
     */
    public function setInputVisible(array $visible)
    {
        $this->_inputVisible = array_merge($this->_inputVisible, $visible);
        return $this;
    }

    /**
     * @param array $selects
     * @return $this
     */
    public function setNoSelectValue(array $selects)
    {
        $this->_noSelectValue = array_merge($this->_noSelectValue, $selects);
        return $this;
    }

    /**
     * @param Zend_View_Interface|null $view
     * @return string
     */
    public function render(Zend_View_Interface $view = null)
    {

        $this->_refreshDate();

        /** @var Zend_Form_Decorator_HtmlTag $tagDecorator */
        $tagDecorator = $this->getDecorator('HtmlTag');
        $tagDecorator->setOption('openOnly', true);
        $tag = $tagDecorator->getTag();

        $res = '<div data-hash="' . $this->_hashObj . '">' . parent::render($view);
        foreach ($this->_buildAllSelects() as $select) {
            $res .= $select;
        }

        $this->_removeDefaultDecorators($this);

        return $res . '</' . $tag . '></div>' . $this->getScript();
    }

    protected function _refreshDate()
    {
        $dateSource = $this->getValue();

        if ($dateSource) {
            $ms = strtotime($dateSource);
            $this->_time = [
                'full'  => $ms,
                'day'   => date('d', $ms),
                'month' => date('m', $ms),
                'year'  => date('Y', $ms),
            ];
        }
    }

    protected function _buildAllSelects()
    {
        $selects[] = $this->_inputVisible['day'] ?
            $this->_buildSelect('day', $this->_getDayOptions(), $this->_getDayValue()) : '';

        $selects[] = $this->_inputVisible['month'] ?
            $this->_buildSelect('month', $this->_getMonthOptions(), $this->_getMonthValue()) : '';

        $selects[] = $this->_inputVisible['year'] ?
            $this->_buildSelect('year', $this->_getYearOptions(), $this->_getYearValue()) : '';

        return $selects;
    }

    protected function _removeDefaultDecorators(Zend_Form_Element $element)
    {
        $element
            ->removeDecorator('DtDdWrapper')
            ->removeDecorator('HtmlTag')
            ->removeDecorator('Label')
            ->removeDecorator('Errors');
    }

    /**
     * @param string $name
     * @param array $options
     * @param string|null $value
     * @param bool|true $removeDecorator
     * @return Zend_Form_Element_Select
     * @throws Zend_Form_Exception
     */
    protected function _buildSelect($name, array $options, $value = null, $removeDecorator = true)
    {
        $select = new Zend_Form_Element_Select($name);
        $select->setMultiOptions($options);
        $select->setAttrib('data-date_type', $name);

        if ($removeDecorator) {
            $this->_removeDefaultDecorators($select);
        }

        if ($value !== null) {
            $select->setValue($value);
        } elseif (isset($this->_noSelectValue[$name]) && $this->_noSelectValue[$name] === true) {
            $select->setValue('-1');
        }

        if ($this->_belongsTo) {
            $select->setBelongsTo($this->_belongsTo . '[' . $this->getName() . '_parts]');
        }

        return $select;
    }

    /**
     * @return array
     */
    protected function _getDayOptions()
    {
        $days = array_combine(range(1, 31), range(1, 31));

        if ($this->_noSelectValue['day'] === true) {
            $days = [-1 => self::NOT_SELECT_DAY] + $days;
        }

        return $days;
    }

    /**
     * @return array
     */
    protected function _getMonthOptions()
    {
        $month = [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
        ];

        $month = array_combine(range(1, 12), $month);

        if ($this->_noSelectValue['month'] === true) {
            $month = [-1 => self::NOT_SELECT_MONTH] + $month;
        }

        return $month;
    }

    /**
     * @return array
     */
    protected function _getYearOptions()
    {
        $range = range($this->_endYear, static::START_YEAR);
        $range = array_combine($range, $range);

        if ($this->_noSelectValue['year'] === true) {
            $range = [-1 => self::NOT_SELECT_YEAR] + $range;
        }

        return $range;
    }

    protected function _getDayValue()
    {
        return $this->_time['day'];
    }

    protected function _getMonthValue()
    {
        return $this->_time['month'];
    }

    protected function _getYearValue()
    {
        return $this->_time['year'];
    }

    /**
     * @return string
     */
    public function getScript()
    {
        return <<<HTML
        <script>
        $(function() {

            var container = $('div[data-hash="{$this->_hashObj}"]'),
                hiddenInput = container.find('input[type="hidden"]'),
                selectDay = container.find('select[data-date_type="day"]'),
                selectMonth = container.find('select[data-date_type="month"]'),
                selectYear = container.find('select[data-date_type="year"]')
            ;

            selectDay.on('change', function() {
                var date = getCurrentDate();
                date.setDate(this.value);
                hiddenInput.val(getFullDate(date));
            });
            selectMonth.on('change', function() {
                var date = getCurrentDate();
                date.setMonth(this.value - 1);
                hiddenInput.val(getFullDate(date));
                checkSelectMonth();
            });
            selectYear.on('change', function() {
                var date = getCurrentDate();
                date.setYear(this.value);
                hiddenInput.val(getFullDate(date));
                checkSelectYear();
            });

            function checkSelectYear() {
                if (selectMonth.length) {
                    var selectedYear = selectYear.val() == '-1';
                    selectMonth.prop('disabled', selectedYear);
                    hiddenInput.prop('disabled', selectedYear);
                }
            }

            function checkSelectMonth() {
                if (selectDay.length && selectMonth.length) {
                    selectDay.prop('disabled', selectMonth.val() == '-1');
                }
            }

            function getCurrentDate() {
                var dateVal = hiddenInput.val().replace(/-/g, ' ');

                if ((!dateVal) || (dateVal == '0000-00-00')) {
                    return new Date();
                }

                return new Date(dateVal);
            }

            function getFullDate(date) {
                return (date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate());
            }

            checkSelectYear();
            checkSelectMonth();
        })
        </script>
HTML;
    }

}