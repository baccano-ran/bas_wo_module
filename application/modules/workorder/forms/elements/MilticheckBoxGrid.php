<?php

class Bam_Form_Element_MilticheckBoxGrid extends Zend_Form_Element_Hidden
{
    protected $_colNumber = 4;
    protected $_multiOptions = [];

    public function render(Zend_View_Interface $view = null)
    {
        $checked = $this->getValue() ? $this->getValue() : [];
        $label = $this->getLabel() ? $this->getLabel() : $this->getName();
        $groupName = $this->getBelongsTo() ?
            $this->getBelongsTo() . '[' . $this->getName() . ']' :
            $this->getName();

        return $this->getView()->fieldMultiCheckbox([
            'checked' => $checked,
            'options' => $this->_multiOptions,
            'name' => $this->getName(),
            'label' => $label,
            'inputName' => $this->getName(),
            'groupName' => $groupName,
            'colSize' => $this->_colNumber,
            'viewName' => 'fieldMultiCheckbox2.phtml',
        ]);
    }

    public function setMultiOptions($value)
    {
        $this->_multiOptions = $value;
        return $this;
    }

    public function setColNumber($value)
    {
        $this->_colNumber = $value;
        return $this;
    }
}

