<?php

/**
 * Class Form_Element_ShowOnly
 */
class Bam_Form_Element_ShowOnly extends Zend_Form_Element
{
    /**
     * @param Zend_View_Interface|null $view
     * @return mixed
     */
    public function render(Zend_View_Interface $view = null)
    {
        return $this->getValue();
    }
}