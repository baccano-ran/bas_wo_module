<?php

class Bam_Form_Element_LastClosing extends Zend_Form_Element
{
    const HIDDEN_FIELD_CLASS = 'form-closing_field';

    public function init()
    {
        $tagDecorator = new Zend_Form_Decorator_HtmlTag([
            'tag' => 'div',
            'class' => static::HIDDEN_FIELD_CLASS,
        ]);
        $this->addDecorator($tagDecorator);
    }
}
