<?php

/**
 * Class Workshop_Form_OrderWorkshopTimeEdit
 */
class Workshop_Form_OrderWorkshopTimeEdit extends BAS_Shared_Form_Abstract
{
    const BELONG_ELEMENTS = 'orderWorkshopTime';

    /** @var int */
    private $activeDepotId;

    /** @var BAS_Shared_Model_Workshop_OrderWorkshopTime */
    private $sourceWorkorderTime;

    /**
     * Workshop_Form_OrderWorkshopTimeEdit constructor.
     * @param array|null $options
     * @param int $activeDepotid
     * @param float $sourceSpentTime
     */
    public function __construct($options = null, $activeDepotid, BAS_Shared_Model_Workshop_OrderWorkshopTime $sourceWorkorderTime)
    {
        $this
            ->setActiveDepotId($activeDepotid)
            ->setSourceWorkorderTime($sourceWorkorderTime);;
        parent::__construct($options);
    }

    public function init()
    {
        $this->clearSubForms();
        $this->createAndAddSubform(0);
    }

    /**
     * @return null|Zend_Form
     */
    public function getFirstSubform()
    {
        return $this->getSubForm(sprintf('%s-%d', self::BELONG_ELEMENTS, 0));
    }

    public function createAndAddSubform($formIndex)
    {
        $subForm = new Zend_Form_SubForm($formIndex);
        $subForm->setIsArray(true)->setName('subform');
        $subForm->removeDecorator('Form');
        $subForm->setElementsBelongTo(sprintf('%s[%d]', self::BELONG_ELEMENTS, $formIndex));

        /**
         * subform elements
         */
        $localizedToNormalizedFilter = new Zend_Filter_LocalizedToNormalized(['locale' => 'nl_NL', 'precision' => 2]);

        $spentTime = new Zend_Form_Element_Text('spentTime');
        $spentTime
            ->setDecorators(['ViewHelper'])
            ->setRequired(true)
            ->setAttribs(['class' => 'js-spent-time'])
            ->addFilter('StripTags')
            ->addValidators([
                new Zend_Validate_NotEmpty(),
                new Zend_Validate_Float(),
                new Zend_Validate_Between(0, BAS_Shared_Db_MySQL_TypesRange::INT_MAX, true),
            ]);
        $spentTime->addFilter(
            new Zend_Filter_Callback(function($value) use ($localizedToNormalizedFilter) {
                if ($value === null) {
                    return '';
                }
                return (float)$localizedToNormalizedFilter->filter($value);
            })
        );

        $workorderId = new Zend_Form_Element_Text('orderWorkshopId');
        $workorderId
            ->setDecorators(['ViewHelper'])
            ->setRequired(true)
            ->addFilter('StripTags')
            ->addValidators([
                new Zend_Validate_NotEmpty(),
                new Zend_Validate_Int(),
                new Zend_Validate_Between(0, BAS_Shared_Db_MySQL_TypesRange::INT_MAX, true),
            ]);

        $labourActivityId = new Zend_Form_Element_Select('labourActivityId', [
            'decorators'   => ['ViewHelper'],
            'class'        => 'width150 js-general-activity',
            'multiOptions' => $this->getLabourActivityOptions(),
            'required'     => true,
        ]);

        $labourActivityId
            ->addValidators([
                new Zend_Validate_NotEmpty(),
                new Zend_Validate_Int(),
                new Zend_Validate_Between(0, BAS_Shared_Db_MySQL_TypesRange::INT_MAX, true),
            ]);

        $subForm->addElements([$spentTime, $workorderId, $labourActivityId]);

        $this->addSubform($subForm, sprintf('%s-%d', self::BELONG_ELEMENTS, $formIndex));
    }

    /**
     * @return array
     */
    private function getLabourActivityOptions()
    {
        $labourActivityService = new Workshop_Service_LabourActivity();

        $labourActivities = $labourActivityService->findByDepartmentId(
            BAS_Shared_Model_Department::DEPARTMENT_ID_WORKSHOP,
            $this->getActiveDepotId()
        );

        $options = array_map(function ($activityName) {
            return $this->getView()->translate($activityName);
        }, BAS_Shared_Utils_Array::listData($labourActivities, 'id', 'name'));

        return $options;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopTime $orderWorkshopTime
     */
    public function populateFirstFormFromModel(BAS_Shared_Model_Workshop_OrderWorkshopTime $orderWorkshopTime)
    {
        $this->getFirstSubform()->populate([
            'spentTime'        => $orderWorkshopTime->getSpentTime(),
            'orderWorkshopId'  => $orderWorkshopTime->getOrderWorkshopId(),
            'labourActivityId' => $orderWorkshopTime->getLabourActivityId(),
        ]);
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopTime $orderWorkshopTime
     * @param int $updatedBy
     * @return BAS_Shared_Model_Workshop_OrderWorkshopTime
     */
    public function getEntityToUpdate(BAS_Shared_Model_Workshop_OrderWorkshopTime $orderWorkshopTime, $updatedBy)
    {
        $firstForm = $this->getFirstSubform();

        $orderWorkshopTime
            ->setManual(1)
            ->setSpentTime($firstForm->getValue('spentTime'))
            ->setOrderWorkshopId($firstForm->getValue('orderWorkshopId'))
            ->setLabourActivityId($firstForm->getValue('labourActivityId'))
            ->setUpdatedAt(date('Y-m-d H:i:s'))
            ->setUpdatedBy($updatedBy);

        return $orderWorkshopTime;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopTime $orderWorkshopTime
     * @param array $formValues
     * @param int $createdBy
     * @return array|BAS_Shared_Model_Workshop_OrderWorkshopTime[]
     */
    public function getEntitiesToInsert(BAS_Shared_Model_Workshop_OrderWorkshopTime $orderWorkshopTimeOrig, array $formValues, $createdBy)
    {
        $firstForm = $this->getFirstSubform();
        $orderWorkshopTimeRecords = [];
        foreach ($this->getSubForms() as $subForm) {
            if ($subForm === $firstForm) {
                continue;
            }

            $orderWorkshopTime = clone $orderWorkshopTimeOrig;
            $orderWorkshopTime
                ->setId(null)
                ->setManual(1)
                ->setCreatedAt(date('Y-m-d H:i:s'))
                ->setCreatedBy($createdBy)
                ->setUpdatedAt(null)
                ->setUpdatedBy(null)
                ->setSpentTime($subForm->getValue('spentTime'))
                ->setOrderWorkshopId($subForm->getValue('orderWorkshopId'))
                ->setLabourActivityId($subForm->getValue('labourActivityId'));

            $orderWorkshopTimeRecords[] = $orderWorkshopTime;
        }

        return $orderWorkshopTimeRecords;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function isValid($data)
    {
        $createdRowCount = count($data[self::BELONG_ELEMENTS]);
        for($i=0; $i<=($createdRowCount-1); $i++) {
            $this->createAndAddSubform($i);
        }

        $isValid = parent::isValid($data);

        if (!$isValid) {
            return false;
        }

        $workorderService = new Workshop_Service_Workorder();

        $sourceSpentTime = (float)$this->getSourceWorkorderTime()->getSpentTime();

        $totalSpentTime = 0;

        /** @var Zend_Form_SubForm $subForm */
        foreach ($this->getSubForms() as $subForm) {
            $totalSpentTime += (float)$subForm->getValue('spentTime');

            /**
             * 1. Check workorder id
             */
            if ($this->getSourceWorkorderTime()->getOrderWorkshopId() !== (int)$subForm->getValue('orderWorkshopId')) {
                $workorder = $workorderService->findById((int)$subForm->getValue('orderWorkshopId'));
                if (!$workorder || $workorder->getArchived()) {
                    $this->addError(sprintf($this->getView()->translate('workorder_not_found_or_not_available'), $subForm->getValue('orderWorkshopId')));
                }

            }
        }

        /**
         * 2. Check source spent time not equals to all row total spent time sum
         */
        if ((float)sprintf('%.2f',$totalSpentTime) != (float)sprintf('%.2f', $sourceSpentTime)) {
            $this->addError(sprintf($this->getView()->translate('total_spent_time_no_match'), $sourceSpentTime));
        }

        return !$this->hasErrors();
    }


    /**
     * @return int
     */
    public function getActiveDepotId()
    {
        return $this->activeDepotId;
    }

    /**
     * @param int $activeDepotId
     * @return Workshop_Form_OrderWorkshopTimeEdit
     */
    public function setActiveDepotId($activeDepotId)
    {
        $this->activeDepotId = $activeDepotId;

        return $this;
    }

    /**
     * @return BAS_Shared_Model_Workshop_OrderWorkshopTime
     */
    public function getSourceWorkorderTime()
    {
        return $this->sourceWorkorderTime;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopTime $sourceWorkorderTime
     * @return Workshop_Form_OrderWorkshopTimeEdit
     */
    public function setSourceWorkorderTime($sourceWorkorderTime)
    {
        $this->sourceWorkorderTime = $sourceWorkorderTime;

        return $this;
    }

}