<?php

/**
 * Class Workshop_Form_WorkorderGeneralTab
 */
class Workshop_Form_WorkorderGeneralTab extends BAS_Shared_Form_Abstract
{
    const SECTION_GENERAL = 'general';
    const SECTION_CUSTOMER = 'customer';
    const SECTION_ADDRESS = 'address';
    const SECTION_VEHICLE = 'vehicle';
    const SECTION_UNKNOWN_VEHICLE = 'unknownVehicle';

    private $userId;
    private $depotId;
    private $workshopId;
    private $workorder;

    public function init()
    {
        $this
            ->addSubForm($this->getGeneralSubForm(), self::SECTION_GENERAL)
            ->addSubForm($this->getVehicleSubForm(), self::SECTION_VEHICLE)
            ->addSubForm($this->getCustomerSubForm(), self::SECTION_CUSTOMER)
            ->addSubForm($this->getInvoiceAddressSubForm(), self::SECTION_ADDRESS)
        ;

        if ($this->getAttrib('validationUnknownVehicle')) {
            $this->addSubForm($this->getSelectFleetVehicleSubForm(), self::SECTION_UNKNOWN_VEHICLE);
        }
    }

    private function getCustomerSubForm()
    {
        $elements = [];

        $elements[] = new Zend_Form_Element('contactId', [

        ]);

        $elements[] = new Zend_Form_Element('personId', [
        ]);

        $subForm = new Zend_Form_SubForm();
        $subForm->setElements($elements);

        return $subForm;
    }

    private function getInvoiceAddressSubForm()
    {
        $elements = [];

        $elements[] = new Zend_Form_Element('addressId', [

        ]);

        $subForm = new Zend_Form_SubForm();
        $subForm->setElements($elements);

        return $subForm;
    }

    /**
     * @return Workshop_Form_SelectUnknownVehicle
     */
    private function getSelectFleetVehicleSubForm()
    {
        return new Workshop_Form_SelectUnknownVehicle(['unknownVehicle' => $this->getAttrib('unknownVehicle')]);
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @param int $depotId
     * @return $this
     */
    public function setDepotId($depotId)
    {
        $this->depotId = $depotId;
        return $this;
    }

    /**
     * @param int $workshopId
     * @return $this
     */
    public function setWorkshopId($workshopId)
    {
        $this->workshopId = $workshopId;
        return $this;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return $this
     */
    public function setWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        $this->workorder = $workorder;
        return $this;
    }

    public function getVehicleSubForm()
    {
        $vehicleSubForm = new Zend_Form_SubForm();

        $vehicleId = new Zend_Form_Element_Text('vehicleId', [
            'validationLabel' => $this->getView()->translate('vehicle'),
        ]);

        $vehicleId->setRequired();

        $vehicleSubForm->setElements([
            $vehicleId
        ]);

        return $vehicleSubForm;
    }

    private function getGeneralSubForm()
    {
        $settingService = new Management_Service_Setting();
        $mainAccountingDepotId = $settingService->getMainAccountingDepot($this->depotId);

        return new Workshop_Form_WorkorderGeneral([
            'direction' => Workshop_Form_WorkorderGeneral::DIRECTION_INPUT,
            'workshopId' => $this->workshopId,
            'userId' => $this->userId,
            'depotId' => $mainAccountingDepotId,
            'workorder' => $this->workorder,
        ]);
    }

}