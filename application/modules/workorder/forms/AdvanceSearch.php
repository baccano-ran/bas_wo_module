<?php
/**
 * Class Workshop_Form_AdvanceSearch
 */
class Workshop_Form_AdvanceSearch extends Default_Form_Abstract
{
    /**
     * Init
     */
    public function init()
    {
        $this->setMethod('get');

        $salesNumber = new Zend_Form_Element_Text('salesNumber');
        $salesNumber->setAttribs([
            'name' => 'salesNumber',
            'onkeyup' => 'validateNumericInput(this)',
        ]);
        
        $workOrderId = clone $salesNumber;
        $workOrderId->setName('workOrderId')
            ->setAttribs([
                'name' => 'workOrderId'
        ]);
        
        $referenceNumbers = clone $salesNumber;
        $referenceNumbers->setName('referenceNumbers')
            ->setAttribs([
                'name' => 'referenceNumbers',
        ]);
        
        $as400Id = clone $salesNumber;
        $as400Id->setName('as400Id')
            ->setAttribs([
                'name' => 'as400Id',
        ]);
        
        $type = new Zend_Form_Element_Multiselect('type');
        $type->setAttribs([
            'name' => 'type[]',
            'class' => 'width150',
        ]);
        
        $registrationFrom = new Zend_Form_Element_Select('registrationFrom');
        $registrationFrom->setAttribs([
            'name' => 'registrationFrom',
            'onchange' => 'changeRegistrationTo(this.value)'
        ]);
        
        $registrationTo = clone $registrationFrom;
        $registrationTo->setName('registrationTo')
            ->setAttribs([
                'name' => 'registrationTo'
        ]);
        
        $chassisNumber = new Zend_Form_Element_Text('chassisNumber');
        $chassisNumber->setAttribs([
            'name' => 'chassisNumber'
        ]);
        
        $licensePlate = clone $chassisNumber;
        $licensePlate->setName('licensePlate')
            ->setAttribs([
                'name' => 'licensePlate'
        ]);
        
        $brand = new Zend_Form_Element_Select('brand');
        $brand->setAttribs([
            'name' => 'brand',
        ]);
        
        $description = clone $salesNumber;
        $description->setName('description')
            ->setAttribs([
                'name' => 'description',
                'onkeyup' => 'validateAlphaNumericInput(this)'
        ]);
        
        $name = clone $salesNumber;
        $name->setName('name')
            ->setAttribs([
                'name' => 'name',
                'onkeyup' => 'validateAlphaNumericInput(this)'
        ]);
        
        $incoTerm = clone $brand;
        $incoTerm->setName('incoterm')
            ->setAttribs([
                'name' => 'incoterm',
                'id' => ''
        ]);
        
        $finalDestinationCountry = clone $brand;
        $finalDestinationCountry->setName('finalDestinationCountry')
            ->setAttribs([
                'name' => 'finalDestinationCountry',
        ]);
        
        $transportationMethod = clone $brand;
        $transportationMethod->setName('transportationMethod')
            ->setAttribs([
                'name' => 'transportationMethod',
        ]);
        
        $salesPerson = clone $brand;
        $salesPerson->setName('salesPerson')
            ->setAttribs([
                'name' => 'salesPerson',
        ]);
        
        $salesPriceFrom = new Zend_Form_Element_Text('salesPriceFrom');
        $salesPriceFrom->setAttribs([
            'name' => 'salesPriceFrom',
            'onkeyup' => 'validateNumericInput(this)',
            'class' => 'advanceSearchText'
        ]);
        
        $salesPriceTo = clone $salesPriceFrom;
        $salesPriceTo->setName('salesPriceTo')
            ->setAttribs([
                'name' => 'salesPriceTo'
        ]);
        
        
        $salesDateFromDate = clone $brand;
        $salesDateFromDate->setName('salesDateFromDate')
            ->setAttribs([
                'name' => 'salesDateFromDate',
        ]);
        
        $salesDateFromMonth = clone $brand;
        $salesDateFromMonth->setName('salesDateFromMonth')
            ->setAttribs([
                'name' => 'salesDateFromMonth',
        ]);
        
        $salesDateFrom = new Zend_Form_Element_Text('salesDateFrom');
        $salesDateFrom->setAttribs([
            'name' => 'salesDateFrom',
            'class' => 'dateText'
        ]);
        
        $salesDateToDate = clone $brand;
        $salesDateToDate->setName('salesDateToDate')
            ->setAttribs([
                'name' => 'salesDateToDate',
        ]);
        
        $salesDateToMonth = clone $brand;
        $salesDateToMonth->setName('salesDateToMonth')
            ->setAttribs([
                'name' => 'salesDateToMonth',
        ]);
        
        $salesDateTo = clone $salesDateFrom;
        $salesDateTo->setName('salesDateTo')
            ->setAttribs([
                'name' => 'salesDateTo'
        ]);
        
        $receivedFrom = clone $salesPriceFrom;
        $receivedFrom->setName('receivedFrom')
            ->setAttribs([
                'name' => 'receivedFrom',
        ]);
        
        $receivedTo = clone $salesPriceFrom;
        $receivedTo->setName('receivedTo')
            ->setAttribs([
                'name' => 'receivedTo'
        ]);
        
        $deadlineFromDate = clone $brand;
        $deadlineFromDate->setName('deadlineFromDate')
            ->setAttribs([
                'name' => 'deadlineFromDate',
        ]);
        
        $deadlineFromMonth = clone $brand;
        $deadlineFromMonth->setName('deadlineFromMonth')
            ->setAttribs([
                'name' => 'deadlineFromMonth',
        ]);
        
        $deadlineFrom = clone $salesDateFrom;
        $deadlineFrom->setName('deadlineFrom')
            ->setAttribs([
                'name' => 'deadlineFrom'
        ]);
        
        $deadlineToDate = clone $brand;
        $deadlineToDate->setName('deadlineToDate')
            ->setAttribs([
                'name' => 'deadlineToDate',
        ]);
        
        $deadlineToMonth = clone $brand;
        $deadlineToMonth->setName('deadlineToMonth')
            ->setAttribs([
                'name' => 'deadlineToMonth',
        ]);
        
        $deadlineTo = clone $salesDateFrom;
        $deadlineTo->setName('deadlineTo')
            ->setAttribs([
                'name' => 'deadlineTo'
        ]);
        
        $entryFromDate = clone $brand;
        $entryFromDate->setName('entryFromDate')
            ->setAttribs([
                'name' => 'entryFromDate',
        ]);
        
        $entryFromMonth = clone $brand;
        $entryFromMonth->setName('entryFromMonth')
            ->setAttribs([
                'name' => 'entryFromMonth',
        ]);
        
        $entryFrom = clone $salesDateFrom;
        $entryFrom->setName('entryFrom')
            ->setAttribs([
                'name' => 'entryFrom'
        ]);
        
        $entryToDate = clone $brand;
        $entryToDate->setName('entryToDate')
            ->setAttribs([
                'name' => 'entryToDate',
        ]);
        
        $entryToMonth = clone $brand;
        $entryToMonth->setName('entryToMonth')
            ->setAttribs([
                'name' => 'entryToMonth',
        ]);
        
        $entryTo = clone $salesDateFrom;
        $entryTo->setName('entryTo')
            ->setAttribs([
                'name' => 'entryTo'
        ]);
        
        $workshopVocaFrom = clone $salesPriceFrom;
        $workshopVocaFrom->setName('workshopVocaFrom')
            ->setAttribs([
                'name' => 'workshopVocaFrom'
        ]);
        
        $workshopVocaTo = clone $salesPriceFrom;
        $workshopVocaTo->setName('workshopVocaTo')
            ->setAttribs([
                'name' => 'workshopVocaTo'
        ]);
        
        $deliveredFromDate = clone $brand;
        $deliveredFromDate->setName('deliveredFromDate')
            ->setAttribs([
                'name' => 'deliveredFromDate',
        ]);
        
        $deliveredFromMonth = clone $brand;
        $deliveredFromMonth->setName('deliveredFromMonth')
            ->setAttribs([
                'name' => 'deliveredFromMonth',
        ]);
        
        $deliveredFrom = clone $salesDateFrom;
        $deliveredFrom->setName('deliveredFrom')
            ->setAttribs([
                'name' => 'deliveredFrom'
        ]);
        
        $deliveredToDate = clone $brand;
        $deliveredToDate->setName('deliveredToDate')
            ->setAttribs([
                'name' => 'deliveredToDate',
        ]);
        
        $deliveredToMonth = clone $brand;
        $deliveredToMonth->setName('deliveredToMonth')
            ->setAttribs([
                'name' => 'deliveredToMonth',
        ]);
        
        $deliveredTo = clone $salesDateFrom;
        $deliveredTo->setName('deliveredTo')
            ->setAttribs([
                'name' => 'deliveredTo'
        ]);

        $submit = new Zend_Form_Element_Submit('submit');
        $submit->setAttribs(['class' => 'zoekenButton'])
            ->setLabel($this->getTranslator()->translate('search'));
        $reset = new Zend_Form_Element_Submit('reset');
        $reset->setLabel($this->getTranslator()->translate('reset'));
        
        $this->addElements([
            $salesNumber,
            $workOrderId,
            $referenceNumbers,
            $as400Id,
            $type,
            $registrationFrom,
            $registrationTo,
            $chassisNumber,
            $licensePlate,
            $brand,
            $description,
            $name,
            $incoTerm,
            $finalDestinationCountry,
            $transportationMethod,
            $salesPerson,
            $salesPriceFrom,
            $salesPriceTo,
            $salesDateFromDate,
            $salesDateFromMonth,
            $salesDateFrom,
            $salesDateToDate,
            $salesDateToMonth,
            $salesDateTo,
            $receivedFrom,
            $receivedTo,
            $deadlineFromDate,
            $deadlineFromMonth,
            $deadlineFrom,
            $deadlineToDate,
            $deadlineToMonth,
            $deadlineTo,
            $entryFromDate,
            $entryFromMonth,
            $entryFrom,
            $entryToDate,
            $entryToMonth,
            $entryTo,            
            $workshopVocaFrom,
            $workshopVocaTo,
            $deliveredFromDate,
            $deliveredFromMonth,
            $deliveredFrom,
            $deliveredToDate,
            $deliveredToMonth,
            $deliveredTo,
            $submit,
            $reset
        ]);
        
        $this->removeAllDecorator($this->getElements());
    }
    
    /**
     * Populate data
     * 
     * @param array $values
     */
    public function populate(array $values)
    {
        $transportData = [];
        $this->type->addMultiOptions(array_replace(['' => 'all_select'], $values['vehicleType']));
        $this->registrationFrom->addMultiOptions($this->prepareRegistrationDateValues(true));
        $this->registrationTo->addMultiOptions($this->prepareRegistrationDateValues());
        $this->brand->addMultiOptions(array_merge(['' => 'all_select'], $values['vehicleBrand']));
        $this->incoterm->addMultiOptions(array_merge(['' => 'all_select'], $values['incoTerm']));
        $this->finalDestinationCountry->addMultiOptions(array_merge(['' => 'all_select'], $values['country']));
        if (!empty($values['transportMethod'])) {
            $transportData = $this->prepareTransportList($values['transportMethod']);
        }
        $this->transportationMethod->addMultiOptions($transportData);
        if (!empty($values['user'])) {
            $userList = $this->prepareSalesPersonList($values['user']);
        }
        $this->salesPerson->addMultiOptions($userList);
        
        $this->salesDateFromDate->addMultiOptions($this->prepareDate());
        $this->salesDateFromMonth->addMultiOptions($this->prepareMonth());
        $this->salesDateToDate->addMultiOptions($this->prepareDate());
        $this->salesDateToMonth->addMultiOptions($this->prepareMonth());
        $this->deadlineFromDate->addMultiOptions($this->prepareDate());
        $this->deadlineFromMonth->addMultiOptions($this->prepareMonth());
        $this->deadlineToDate->addMultiOptions($this->prepareDate());
        $this->deadlineToMonth->addMultiOptions($this->prepareMonth());
        $this->entryFromDate->addMultiOptions($this->prepareDate());
        $this->entryFromMonth->addMultiOptions($this->prepareMonth());
        $this->entryToDate->addMultiOptions($this->prepareDate());
        $this->entryToMonth->addMultiOptions($this->prepareMonth());
        $this->deliveredFromDate->addMultiOptions($this->prepareDate());
        $this->deliveredFromMonth->addMultiOptions($this->prepareMonth());
        $this->deliveredToDate->addMultiOptions($this->prepareDate());
        $this->deliveredToMonth->addMultiOptions($this->prepareMonth());
        parent::populate($values);
    }

    /**
     * Prepare registration dropdown
     * 
     * @param boolean $from
     * @return array
     */
    public function prepareRegistrationDateValues($from = false)
    {
        if ($from) {
            $registrationValues[BAS_Shared_Model_Vehicle::YEAR_0000] = BAS_Shared_Model_Vehicle::YEAR_0000;
        }
        $registrationValues[BAS_Shared_Model_Vehicle::YEAR_9999] = BAS_Shared_Model_Vehicle::YEAR_NEW;
        for ($i = date('Y'); $i >= BAS_Shared_Model_ProductTypeRegistrationYear::REGISTRATION_MIN_YEAR; $i--) {
            $registrationValues[$i] = $i;
        }
        $registrationValues[BAS_Shared_Model_Vehicle::YEAR_1111] = BAS_Shared_Model_Vehicle::YEAR_1111;
        
        return $registrationValues;
    }
    
    /**
     * Prepare transportData
     * 
     * @param array $transportData
     * @return array
     */
    public function prepareTransportList($transportData)
    {
        $data = ['' => 'all_select'];
        foreach ($transportData as $transport) {
            if (!empty($transport)) {
                $data[$transport->id] = 'tabvehicles_vehicledata_transportmethod' . $transport->id;
            }
        }
        
        return $data;
    }
    
    /**
     * Prepare users list 
     * 
     * @param array $userList
     * @return array
     */
    public function prepareSalesPersonList($userList)
    {
        $data = ['' => 'all_select'];
        foreach ($userList as $user) {
            $data[$user->Afkorting] = $user->Gebruikersnaam;
        }
        
        return $data;
    }
    
    /**
     * Prepare date values
     * 
     * @return array
     */
    public function prepareDate()
    {
        $value = ['' => ''];
        for ($i = 1; $i < 32; $i++) {
            $index = str_pad($i, 2, '0', STR_PAD_LEFT);
            $value[$index] = str_pad($i, 2, '0', STR_PAD_LEFT);
        }
        
        return $value;
    }
    
    /**
     * Prepare month data
     * 
     * @return array
     */
    public function prepareMonth()
    {
        $monthData = ['' => ''];
        $locale = new Zend_Locale(); 
        $monthlist = $locale->getTranslationList('months'); 
        for ($i = 1; $i < 13; $i++) {
            $index = str_pad($i, 2, '0', STR_PAD_LEFT);
            $monthData[$index] = $monthlist['format']['wide'][$i]; 
        }

        return $monthData;
    }
}