<?php

/**
 * Class Form_Criteria_Create
 */
class Bam_Form_Criteria_Update extends Bam_Form_Criteria_Create
{
    /**
     * @return BAS_Shared_Model_Crawler_Criteria
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function getEntity()
    {
        $model = new BAS_Shared_Model_Crawler_Criteria();
        $values = $this->getValues();
        $values = $values['criteria'];

        foreach (BAS_Shared_Model_Crawler_Criteria::$checkboxProps as $field) {
            if (!empty($values[$field])) {
                if(!is_string($values[$field])) {
                    $values[ $field ] = $this->_criteriaService->implodeMultiValue($values[ $field ]);
                }
            }
        }

        /** @var BAS_Shared_Model_Crawler_CriteriaMapper $mapper */
        $mapper = BAS_Shared_Model_MapperRegistry::get('Crawler_Criteria');
        $data = array_intersect_key($values, $mapper->getMap());

        $model->populate($data);

        $model->setUpdatedAt(new DateTime());
        $model->setUpdatedBy(BAS_Shared_Auth_Service::getIdentity()->getId());

        return $model;
    }

    protected function _getExtElements()
    {
        $vehicleMatchesNow = new Zend_Form_Element_Note('vehicleMatchesNow');
        $vehicleMatchesNow
            ->setLabel('vehicle_matches_now')
        ;

        $vehicleMatchesPastWeek = new Zend_Form_Element_Note('vehicleMatchesPastWeek');
        $vehicleMatchesPastWeek
            ->setLabel('vehicle_matches_past_week')
        ;

        return [
            $vehicleMatchesNow,
            $vehicleMatchesPastWeek,
        ];
    }
}
