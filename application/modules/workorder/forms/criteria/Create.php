<?php

/**
 * Class Form_Criteria_Create
 */
class Bam_Form_Criteria_Create extends Bam_Form_Base
{
    /** @var  Bam_Service_Criteria $_criteriaService */
    protected $_criteriaService;
    protected $_currYear;
    const MAIN_GROUP = 'criteria';

    protected $_configuration = [
        BAS_Shared_Model_Crawler_Vehicles::TRACTION_4x2,
        BAS_Shared_Model_Crawler_Vehicles::TRACTION_4x4,
        BAS_Shared_Model_Crawler_Vehicles::TRACTION_6x2,
        BAS_Shared_Model_Crawler_Vehicles::TRACTION_6x4,
        BAS_Shared_Model_Crawler_Vehicles::TRACTION_6x6,
        BAS_Shared_Model_Crawler_Vehicles::TRACTION_8x4,
        BAS_Shared_Model_Crawler_Vehicles::TRACTION_8x6,
        BAS_Shared_Model_Crawler_Vehicles::TRACTION_8x8,
    ];

    protected $_dealerStatus = [
        BAS_Shared_Model_Crawler_Dealer::STATUS_ACTIVE,
        BAS_Shared_Model_Crawler_Dealer::STATUS_INACTIVE,
        BAS_Shared_Model_Crawler_Dealer::STATUS_IGNORED,
    ];

    protected $_vehicleStatus = [
        BAS_Shared_Model_Crawler_Vehicles::STATUS_FOR_SALE,
        BAS_Shared_Model_Crawler_Vehicles::STATUS_OFFERED,
        BAS_Shared_Model_Crawler_Vehicles::STATUS_BOUGHT,
        BAS_Shared_Model_Crawler_Vehicles::STATUS_REMOVED,
        BAS_Shared_Model_Crawler_Vehicles::STATUS_IGNORE,
        BAS_Shared_Model_Crawler_Vehicles::STATUS_SEEN,
    ];

    protected $_emission = [
        BAS_Shared_Model_Crawler_Vehicles::EMISSION_CLASS_EURO1,
        BAS_Shared_Model_Crawler_Vehicles::EMISSION_CLASS_EURO2,
        BAS_Shared_Model_Crawler_Vehicles::EMISSION_CLASS_EURO3,
        BAS_Shared_Model_Crawler_Vehicles::EMISSION_CLASS_EURO4,
        BAS_Shared_Model_Crawler_Vehicles::EMISSION_CLASS_EURO5,
        BAS_Shared_Model_Crawler_Vehicles::EMISSION_CLASS_EURO6,
        BAS_Shared_Model_Crawler_Vehicles::EMISSION_CLASS_EMPTY,
    ];

    protected $_gearbox = [
        BAS_Shared_Model_Crawler_Vehicles::GEARBOX_MANUAL,
        BAS_Shared_Model_Crawler_Vehicles::GEARBOX_AUTO,
        BAS_Shared_Model_Crawler_Vehicles::GEARBOX_SEMI_AUTO,
        BAS_Shared_Model_Crawler_Vehicles::GEARBOX_EMPTY,
    ];

    /**
     * @param array $data
     * @return null
     */
    public function nullStrToNull($data)
    {
        if (is_array($data)) {
            foreach ($data as &$item) {
                if (is_array($item)) {
                    $item = $this->nullStrToNull($item);
                } elseif ($item === '') {
                    $item = null;
                }
            }
        } elseif ($data === '') {
            $data = null;
        }

        return $data;
    }

    /**
     * @param bool|false $suppressArrayNotation
     * @return null
     */
    public function getValues($suppressArrayNotation = false)
    {
        return $this->nullStrToNull(parent::getValues($suppressArrayNotation));
    }

    /**
     * @return int
     */
    public function updateAndGetSeq()
    {
        /** @var BAS_Shared_Model_DbSequenceMapper $sequenceMapper $newCriteriaId */
        $sequenceMapper = BAS_Shared_Model_MapperRegistry::get('DbSequence');
        $seq = $sequenceMapper->findBySeqName(BAS_Shared_Model_Crawler_Criteria::SEQ_NAME);
        $sequenceMapper->updateBySeqName(BAS_Shared_Model_Crawler_Criteria::SEQ_NAME, $seq->nextid + 1);

        return $seq->nextid;
    }

    /**
     * @return BAS_Shared_Model_Crawler_Criteria
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function getEntity()
    {
        $model = new BAS_Shared_Model_Crawler_Criteria();
        $values = $this->getValues();
        $values = $values['criteria'];
        foreach (BAS_Shared_Model_Crawler_Criteria::$checkboxProps as $field) {
            if (!empty($values[ $field ])) {
                $values[ $field ] = $this->_criteriaService->implodeMultiValue($values[ $field ]);
            }
        }

        /** @var BAS_Shared_Model_Crawler_CriteriaMapper $mapper */
        $mapper = BAS_Shared_Model_MapperRegistry::get('Crawler_Criteria');
        $data = array_intersect_key($values, $mapper->getMap());

        $model->populate($data);
        $model->setId($this->updateAndGetSeq());

        $model->setCreatedAt(new DateTime());
        $model->setCreatedBy(BAS_Shared_Auth_Service::getIdentity()->getId());

        return $model;
    }

    public function init()
    {
        if ($this->getReturnUrl()) {
            $this->setAction(sprintf('%s?returnUrl=%s', $this->getAction(), $this->getReturnUrl()));
        }
        $this->_currYear = date('Y');
        $this->_criteriaService = new Bam_Service_Criteria();

        $elements = $this->_getElements();

        /** @var Zend_Form_Element $element */
        foreach ($elements as $element) {
            $element->setBelongsTo(self::MAIN_GROUP);
        }

        $this->addElements($elements);
    }

    protected function _getElements()
    {
        $elements = [];

        $searchId = new Zend_Form_Element_Hidden('searchId');
        $searchId
            ->removeDecorator('DtDdWrapper')
            ->removeDecorator('Label')
            ->getDecorator('HtmlTag')->setOption('class', 'form-hidden_field');
        $elements[] = $searchId;

        $criteriaId = new Zend_Form_Element_Hidden('id');
        $criteriaId
            ->removeDecorator('DtDdWrapper')
            ->removeDecorator('Label')
            ->getDecorator('HtmlTag')->setOption('class', 'form-hidden_field');
        $elements[] = $criteriaId;

        $categories = new Bam_Form_Element_MilticheckBoxGrid('categoryId');
        $categories
            ->setMultiOptions(
                BAS_Shared_Utils_Array::listData($this->getVisibleCategories(), 'id', 'name'))
            ->setLabel('category');
        $elements[] = $categories;

        $kind = new Zend_Form_Element_Text('kind');
        $kind->setLabel('kind');
        $elements[] = $kind;

        $typeKind = new Zend_Form_Element_Note('kindNote');
        $typeKind
            ->setValue(sprintf(
                '%s<br/>%s',
                $this->getView()->translate('tip_use_composite_values'),
                $this->getView()->translate('note_take_kinds_from_site')
            ))
        ;
        $elements[] = $typeKind;

        $brands = new Bam_Form_Element_MilticheckBoxGrid('brand');
        $brands
            ->setMultiOptions($this->_getBrandOptions())
            ->setLabel('brands')
        ;
        $elements[] = $brands;

        $type = new Zend_Form_Element_Text('type');
        $type
            ->setLabel('type');
        $elements[] = $type;

        $typeNote = new Zend_Form_Element_Note('typeNote');
        $typeNote
            ->setValue(sprintf(
                '%s<br/>%s',
                $this->getView()->translate('tip_use_composite_values'),
                $this->getView()->translate('note_eg_boolean_keywords')
            ))
        ;
        $elements[] = $typeNote;

        $status = new Zend_Form_Element_Select('status');
        $status
            ->setMultiOptions($this->getVehicleStatusOptions())
            ->setLabel('status');
        $elements[] = $status;

        //year
        $yearFrom = new Zend_Form_Element_Select('yearFrom');
        $yearFrom->setMultiOptions($this->_getYearOptions())
            ->setValidators([new Zend_Validate_Int()])
            ->setAllowEmpty(true)
            ->setLabel('year_from');
        $elements[] = $yearFrom;

        $yearTo = new Zend_Form_Element_Select('yearTo');
        $yearTo->setMultiOptions($this->_getYearOptions())
            ->setValidators([new Zend_Validate_Int()])
            ->setAllowEmpty(true)
            ->setLabel('year_to');
        $elements[] = $yearTo;

        //first registration
        $firstRegistrationFrom = new Bam_Form_Element_DateSelect('firstRegistrationFrom');
        $firstRegistrationFrom
            ->setInputVisible(['day' => false])
            ->setLabel('first_registration_from');
        $elements[] = $firstRegistrationFrom;

        $firstRegistrationTo = new Bam_Form_Element_DateSelect('firstRegistrationTo');
        $firstRegistrationTo
            ->setInputVisible(['day' => false])
            ->setLabel('first_registration_to');
        $elements[] = $firstRegistrationTo;

        $mileageFrom = new Zend_Form_Element_Text('mileageFrom');
        $mileageFrom
            ->setLabel('mileage_from');
        $elements[] = $mileageFrom;

        $mileageTo = new Zend_Form_Element_Text('mileageTo');
        $mileageTo
            ->setLabel('mileage_to');
        $elements[] = $mileageTo;

        $powerFrom = new Zend_Form_Element_Text('powerFrom');
        $powerFrom
            ->setLabel('power_from');
        $elements[] = $powerFrom;

        $powerTo = new Zend_Form_Element_Text('powerTo');
        $powerTo
            ->setLabel('power_to');
        $elements[] = $powerTo;

        $traction = new Bam_Form_Element_MilticheckBoxGrid('traction');
        $traction
            ->setMultiOptions($this->getConfigurationOptions())
            ->setColNumber(3);
        $elements[] = $traction;

        $gearbox = new Bam_Form_Element_MilticheckBoxGrid('gearBox');
        $gearbox
            ->setLabel('gearbox')
            ->setMultiOptions($this->getGearboxOptions())
            ->setColNumber(3);
        $elements[] = $gearbox;

        //cabine
        $cabine = new Bam_Form_Element_MilticheckBoxGrid('cabine');
        $cabine
            ->setLabel('cabine')
            ->setMultiOptions($this->getCabineOptions())
            ->setColNumber(3);
        $elements[] = $cabine;

        //fuel_type
        $fuelType = new Bam_Form_Element_MilticheckBoxGrid('fuelType');
        $fuelType
            ->setLabel('fuel_type')
            ->setMultiOptions($this->getFuelTypeOptions())
            ->setColNumber(3);
        $elements[] = $fuelType;

        $priceNetFrom = new Zend_Form_Element_Text('priceNetFrom');
        $priceNetFrom
            ->setLabel('price_net_from')
            ->setValidators([
                new BAS_Shared_Form_Validate_Numeric(),
            ]);

        $elements[] = $priceNetFrom;

        $priceNetTo = new Zend_Form_Element_Text('priceNetTo');
        $priceNetTo
            ->setLabel('price_net_to')
            ->setValidators([
                new BAS_Shared_Form_Validate_Numeric(),
            ]);

        $elements[] = $priceNetTo;

        $priceMargin = new Zend_Form_Element_Text('priceMargin');
        $priceMargin
            ->setLabel('price_margin')
            ->setValidators([
                new BAS_Shared_Form_Validate_Numeric(),
            ]);
        $elements[] = $priceMargin;

        $mileageTo = new Zend_Form_Element_Text('mileageTo');
        $mileageTo
            ->setLabel('mileage_to');
        $elements[] = $mileageTo;

        $emission = new Bam_Form_Element_MilticheckBoxGrid('emissionClass');
        $emission
            ->setLabel('emission_class')
            ->setMultiOptions($this->getEmissionOptions())
            ->setColNumber(3);
        $elements[] = $emission;

        $daysOnline = new Zend_Form_Element_Text('daysOnline');
        $daysOnline
            ->setLabel('days_online');
        $elements[] = $daysOnline;

        $excludeKeywords = new Zend_Form_Element_Textarea('filter');
        $excludeKeywords
            ->setAttrib('rows', 15)
            ->setLabel('filter');
        $elements[] = $excludeKeywords;

        $fexNote = new Zend_Form_Element_Note('fexNote');
        $fexNote
            ->setValue($this->getView()->translate('enter_one_word_per_line'));
        $elements[] = $fexNote;

        $axels = new Zend_Form_Element_Text('axels');
        $axels
            ->setLabel('axels');
        $elements[] = $axels;

        $retarded = new Zend_Form_Element_Checkbox('retarder');
        $retarded
            ->setLabel('retarder');
        $elements[] = $retarded;

        // power_steering
        $powerSteering = new Zend_Form_Element_Checkbox('powerSteering');
        $powerSteering
            ->setLabel('power_steering')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_VANS)
        ;
        $powerSteering->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $powerSteering->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $powerSteering;

        // central_door_lock
        $centralDoorLock = new Zend_Form_Element_Checkbox('centralDoorLock');
        $centralDoorLock
            ->setLabel('central_door_lock')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_VANS)
        ;
        $centralDoorLock->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $centralDoorLock->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $centralDoorLock;

        // partition_wall
        $partitionWall = new Zend_Form_Element_Checkbox('partitionWall');
        $partitionWall
            ->setLabel('partition_wall')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_VANS)
        ;
        $partitionWall->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $partitionWall->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $partitionWall;

        // sliding_door
        $slidingDoor = new Zend_Form_Element_Checkbox('slidingDoor');
        $slidingDoor
            ->setLabel('sliding_door')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_VANS)
        ;
        $slidingDoor->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $slidingDoor->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $slidingDoor;

        // quick_attachment
        $quickAttachment = new Zend_Form_Element_Checkbox('quickAttachment');
        $quickAttachment
            ->setLabel('quick_attachment')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_CONSTRUCTION)
        ;
        $quickAttachment->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $quickAttachment->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $quickAttachment;

        // particulate_filter
        $particulateFilter = new Zend_Form_Element_Checkbox('particulateFilter');
        $particulateFilter
            ->setLabel('particulate_filter')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_CONSTRUCTION)
        ;
        $particulateFilter->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $particulateFilter->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $particulateFilter;

        // boom_suspension_system
        $boomSuspensionSystem = new Zend_Form_Element_Checkbox('boomSuspensionSystem');
        $boomSuspensionSystem
            ->setLabel('boom_suspension_system')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_CONSTRUCTION)
        ;
        $boomSuspensionSystem->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $boomSuspensionSystem->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $boomSuspensionSystem;

        // central_lubrication_system
        $centralLubricationSystem = new Zend_Form_Element_Checkbox('centralLubricationSystem');
        $centralLubricationSystem
            ->setLabel('central_lubrication_system')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_CONSTRUCTION)
        ;
        $centralLubricationSystem->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $centralLubricationSystem->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $centralLubricationSystem;

        // allowed_on_roads
        $allowedOnRoads = new Zend_Form_Element_Checkbox('allowedOnRoads');
        $allowedOnRoads
            ->setLabel('allowed_on_roads')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_CONSTRUCTION)
        ;
        $allowedOnRoads->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $allowedOnRoads->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $allowedOnRoads;

        // all_wheel_drive
        $allWheelDrive = new Zend_Form_Element_Checkbox('allWheelDrive');
        $allWheelDrive
            ->setLabel('all_wheel_drive')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_CONSTRUCTION)
        ;
        $allWheelDrive->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $allWheelDrive->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $allWheelDrive;

        // trailer_coupling
        $trailerCoupling = new Zend_Form_Element_Checkbox('trailerCoupling');
        $trailerCoupling
            ->setLabel('trailer_coupling')
            ->setAttrib('cat-id', BAS_Shared_Model_Crawler_Category::CATEGORY_BUSES)
        ;
        $trailerCoupling->getDecorator('HtmlTag')->setOption('class', 'catOption');
        $trailerCoupling->getDecorator('Label')->setOption('tagClass', 'catOption');
        $elements[] = $trailerCoupling;

        $createdBy = new Bam_Form_Element_NoteWithHiddenValue('createdBy');
        $createdBy
            ->setLabel('created_by');
        $elements[] = $createdBy;

        $createdAt = new Bam_Form_Element_NoteWithHiddenValue('createdAt');
        $createdAt
            ->setDisplayDateFormat('d-m-Y H:i')
            ->setLabel('created_at');
        $elements[] = $createdAt;

        $updatedBy = new Bam_Form_Element_NoteWithHiddenValue('updatedBy');
        $updatedBy
            ->setLabel('updated_by');
        $elements[] = $updatedBy;

        $updatedAt = new Bam_Form_Element_NoteWithHiddenValue('updatedAt');
        $updatedAt
            ->setDisplayDateFormat('d-m-Y H:i')
            ->setLabel('updated_at');
        $elements[] = $updatedAt;

        $elements = array_merge($elements, $this->_getExtElements());

        $controlButton = new Bam_Form_Element_ControlButton('controlButton');
        $controlButton->setButtons([
            Bam_Form_Element_ControlButton::BUTTON_SAVE,
            Bam_Form_Element_ControlButton::BUTTON_CANCEL,
        ]);
        $controlButton
            ->getButton(Bam_Form_Element_ControlButton::BUTTON_SAVE)
            ->setAttrib('data-return-url', $this->getReturnUrl());
        $controlButton
            ->getButton(Bam_Form_Element_ControlButton::BUTTON_CANCEL)
            ->setAttrib('data-return-url', $this->getReturnUrl());

        $elements[] = $controlButton;

        $lastElement = new Bam_Form_Element_LastClosing('closing');
        $elements[] = $lastElement;

        return $elements;
    }

    /**
     * @return array
     */
    protected function _getExtElements()
    {
        return [];
    }

    /**
     * @return array
     */
    protected function _getYearOptions()
    {
        $years = array_reverse(range(1965, intval($this->_currYear)));
        $options = ['' => 'select_year'] + array_combine($years, $years);

        return $options;
    }

    /**
     * @return array
     */
    public function getVehicleStatusOptions()
    {
        return array_combine($this->_vehicleStatus, $this->_vehicleStatus);
    }

    /**
     * @return array|BAS_Shared_Model_Crawler_Category[]
     */
    public function getVisibleCategories()
    {
        /** @var BAS_Shared_Model_Crawler_CategoryMapper $categoryMapper */
        $categoryMapper = BAS_Shared_Model_MapperRegistry::get('Crawler_Category');

        return $categoryMapper->findVisible();
    }

    /**
     * @return array
     */
    public function getConfigurationOptions()
    {
        return array_combine($this->_configuration, $this->_configuration);
    }

    /**
     * @return array
     */
    public function getGearboxOptions()
    {
        $gearBoxes = array_combine($this->_gearbox, $this->_gearbox);

        $gearBoxes = array_map(function($val) {
            return $this->getView()->translate($val);
        }, $gearBoxes);

        return $gearBoxes;
    }

    /**
     * @return array
     */
    public function getFuelTypeOptions()
    {
        $fuelTypes = array_combine(BAS_Shared_Model_Crawler_Vehicles::getFuelTypes(), BAS_Shared_Model_Crawler_Vehicles::getFuelTypes());

        $fuelTypes = array_map(function($val) {
            return $this->getView()->translate($val);
        }, $fuelTypes);

        return $fuelTypes;
    }

    /**
     * @return array
     */
    public function getCabineOptions()
    {
        $cabineTypes = array_combine(BAS_Shared_Model_Crawler_Vehicles::getCabines(), BAS_Shared_Model_Crawler_Vehicles::getCabines());

        $cabineTypes = array_map(function($val) {
            return $this->getView()->translate($val);
        }, $cabineTypes);

        return $cabineTypes;
    }

    /**
     * @return array
     */
    public function getEmissionOptions()
    {
        return array_combine($this->_emission, $this->_emission);
    }

    /**
     * @return array
     */
    protected function _getBrandOptions()
    {
        $brandService = new Bam_Service_Brand();
        $brandOptions = $brandService->getBrandOptions();

        return $brandOptions;
    }

}
