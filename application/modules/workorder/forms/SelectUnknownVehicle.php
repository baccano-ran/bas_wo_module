<?php

/**
 * Class Workshop_Form_SelectUnknownVehicle
 */
class Workshop_Form_SelectUnknownVehicle extends Zend_Form_SubForm
{
    const DATE_FORMAT = 'd-m-Y';

    public function init()
    {
        $elements = [];

        $unknownVehicle = $this->getAttrib(Workshop_Form_WorkorderGeneralTab::SECTION_UNKNOWN_VEHICLE);
        $vehicleTypeId = array_key_exists('vehicle_type_id', $unknownVehicle) ? $unknownVehicle['vehicle_type_id'] : 0;

        $vehicleTypeService = new Vehicles_Service_VehicleType();
        $vehicleBrandService = new Vehicles_Service_VehicleBrand();

        $identificationValidatorValid = true;
        $identificationValidator = new Zend_Validate_Callback(function ($value, $data) use (&$identificationValidatorValid) {

            if (array_key_exists('vehicle', $data) && is_array($data['vehicle'])) {
                $data = $data['vehicle'];
            }

            $vin = $data['vin'];
            $licensePlate = $data['license_plate'];

            $emptyValidator = new Zend_Validate_NotEmpty();

            $isValid = $emptyValidator->isValid($vin) || $emptyValidator->isValid($licensePlate);

            if ($identificationValidatorValid === false && $isValid === false) {
                return true;
            } else {
                return $identificationValidatorValid = $isValid;
            }
        });

        $identificationValidator->setMessage('error_fill_license_plate_or_chassisnamber', Zend_Validate_Callback::INVALID_VALUE);

        $elements[] = new Zend_Form_Element_Text('vehicleId', [
            'hidden' => true,
        ]);

        $elements[] = new Zend_Form_Element_Text('contactId', [
            'hidden' => true,
        ]);

        $elements[] = new Zend_Form_Element_Text('vin', [
            'required' => true,
            'autoInsertNotEmptyValidator' => false,
            'validators' => [$identificationValidator],
            'validationLabel' => $this->getView()->translate('identification'),
        ]);

        $elements[] = new Zend_Form_Element_Text('license_plate', [
            'required' => true,
            'autoInsertNotEmptyValidator' => false,
            'validators' => [$identificationValidator],
            'validationLabel' => $this->getView()->translate('license_plate'),
        ]);

        $elements[] = new Zend_Form_Element_Select('vehicle_type_id', [
            'required' => true,
            'multiOptions' => $vehicleTypeService->getTruckVehicleType(false),
            'validationLabel' => $this->getView()->translate('vehicle_type'),
        ]);

        $elements[] = new Zend_Form_Element_Select('brand_id', [
            'multiOptions' => $vehicleBrandService->getVehicleBrands($vehicleTypeId),
            'required' => true,
            'validationLabel' => $this->getView()->translate('brand'),
        ]);

        $elements[] = new Zend_Form_Element_Text('type');

        $elements[] = new Zend_Form_Element_Text('registration', [
            'validators' => [new Zend_Validate_Date(['format' => self::DATE_FORMAT])],
            'validationLabel' => $this->getView()->translate('registration'),
        ]);

        $this->setElements($elements);
    }

}
