<?php

/**
 * Class Workshop_Form_TradeInPrices
 */
class Workshop_Form_TradeInPrices extends BAS_Shared_Form_Abstract
{

    private $tradeInPrices = [];

    public function init()
    {
        foreach ($this->tradeInPrices as $index => $tradeInPriceData) {
            $this->addSubForm($this->createTradeInPriceForm(), $index);
        }

        $this->setElementsBelongTo('prices');

        if ($this->direction === self::DIRECTION_OUTPUT) {
            $this->loadDefaultDecorators();
            $this->render();
        }
    }

    private function createTradeInPriceForm()
    {
        $elements = [];
        $priceSubForm = new Zend_Form_SubForm();

        $elements[] = $this->createElement('Hidden', 'itemId', [
            'decorators' => ['ViewHelper'],
        ]);

        $elements[] = $this->createElement('Hidden', 'depotId', [
            'decorators' => ['ViewHelper'],
        ]);

        $elements[] = $this->createElement('Note', 'itemName', [
            'decorators' => ['ViewHelper'],
        ]);

        $elements[] = $this->createElement('Text', 'tradeInPrice', [
            'decorators' => ['ViewHelper'],
            'required' => true,
            'placeholder' => $this->getView()->translate('placeholder_enter_price'),
            'class' => 'js-price-input',
            'validators' => [
                new Zend_Validate_Float('en_GB')
            ],
        ]);

        $priceSubForm->setElements($elements);

        if ($this->direction === self::DIRECTION_OUTPUT) {
            $this->applyOutputFilters($priceSubForm);
        } elseif ($this->direction === self::DIRECTION_INPUT) {
            $this->applyInputFilters($priceSubForm);
        }

        return $priceSubForm;
    }

    /**
     * @param Zend_Form $priceForm
     */
    private function applyOutputFilters(Zend_Form $priceForm)
    {
        $priceForm->getElement('tradeInPrice')->setFilters([
            $this->getNullableNormalizedToLocalizedFilter()
        ]);
    }

    /**
     * @param Zend_Form $priceForm
     */
    private function applyInputFilters(Zend_Form $priceForm)
    {
        $priceForm->getElement('tradeInPrice')->setFilters([
            new Zend_Filter_LocalizedToNormalized(['locale' => 'nl_NL']),
        ]);
    }

    /**
     * @return Zend_Filter_Callback
     */
    private function getNullableNormalizedToLocalizedFilter()
    {
        return new Zend_Filter_Callback(function($value) {
            if ($value === null) {
                return '';
            }

            $normalizedToLocalizedFilter = new Zend_Filter_NormalizedToLocalized(['locale' => 'nl_NL', 'precision' => 2]);
            $value = $normalizedToLocalizedFilter->filter($value);

            return $value;
        });
    }

    /**
     * @param array $tradeInPrices
     */
    public function setTradeInPrices($tradeInPrices)
    {
        $this->tradeInPrices = $tradeInPrices;
    }

    /**
     * @param mixed $direction
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
    }

}