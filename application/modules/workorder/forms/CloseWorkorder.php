<?php

/**
 * Class Workshop_Form_CloseWorkorder
 */
class Workshop_Form_CloseWorkorder extends BAS_Shared_Form_Abstract
{
    const BELONG_ELEMENTS = 'closeWorkorder';

    private $workorderId;
    private $netAmount = [];

    public function init()
    {
        $elements = [];

        $elements[] = new Zend_Form_Element_Radio('radio_consumable', [
            'multiOptions' => [
                '0' => 'No',
                '1' => 'Yes',
            ],
            'value' => '1',
            'separator' => '&nbsp;',
        ]);

        $elements[] = new Zend_Form_Element_Radio('radio_environment', [
            'multiOptions' => [
                '0' => 'No',
                '1' => 'Yes',
            ],
            'value' => '1',
            'separator' => '&nbsp;',
        ]);

        $isGuaranteeReadonly = true;
        $isInternalReadonly = empty($this->netAmount['internal']) ? true : null;
        $isExternalReadonly = empty($this->netAmount['external']) ? true : null;

        $elements[] = new Zend_Form_Element_Text('price_internal_consumables', [
            'class' => 'js-price_internal_consumables js-float-input' . ($isInternalReadonly ? ' border-none' : ''),
            'readonly' => $isInternalReadonly,
        ]);
        $elements[] = new Zend_Form_Element_Text('price_guarantee_consumables', [
            'class' => 'js-price_guarantee_consumables js-float-input' . ($isGuaranteeReadonly ? ' border-none' : ''),
            'readonly' => $isGuaranteeReadonly,
        ]);
        $elements[] = new Zend_Form_Element_Text('price_external_consumables', [
            'class' => 'js-price_external_consumables js-float-input' . ($isExternalReadonly ? ' border-none' : ''),
            'readonly' => $isExternalReadonly,
        ]);

        $elements[] = new Zend_Form_Element_Text('price_internal_environment', [
            'class' => 'js-price_internal_environment js-float-input' . ($isInternalReadonly ? ' border-none' : ''),
            'readonly' => $isInternalReadonly,
        ]);
        $elements[] = new Zend_Form_Element_Text('price_guarantee_environment', [
            'class' => 'js-price_guarantee_environment js-float-input' . ($isGuaranteeReadonly ? ' border-none' : ''),
            'readonly' => $isGuaranteeReadonly,
        ]);
        $elements[] = new Zend_Form_Element_Text('price_external_environment', [
            'class' => 'js-price_external_environment js-float-input' . ($isExternalReadonly ? ' border-none' : ''),
            'readonly' => $isExternalReadonly,
        ]);

        /** @var Zend_Form_Element $element */
        foreach ($elements as $element) {
            $element
                ->removeDecorator('HtmlTag')
                ->removeDecorator('label')
            ;
        }

        $this->setElements($elements);
        $this->setElementsBelongTo(self::BELONG_ELEMENTS);
    }

    /**
     * @param bool $suppressArrayNotation
     * @return array
     */
    public function getValues($suppressArrayNotation = false)
    {
        $values = parent::getValues($suppressArrayNotation);

        $values = $values[self::BELONG_ELEMENTS];

        if ((int) $values['radio_consumable'] === 0) {
            $values['price_internal_consumables'] = 0;
            $values['price_guarantee_consumables'] = 0;
            $values['price_external_consumables'] = 0;
        }

        if ((int) $values['radio_environment'] === 0) {
            $values['price_internal_environment'] = 0;
            $values['price_guarantee_environment'] = 0;
            $values['price_external_environment'] = 0;
        }

        return [self::BELONG_ELEMENTS => $values];
    }

    /**
     * @param array $values
     * @return $this
     */
    public function populate(array $values)
    {
        parent::populate($values);

        $elements = $this->getElements();

        foreach ($elements as $element) {
            if ($element instanceof Zend_Form_Element_Text) {
                $element->setAttrib('data-init_value', $element->getValue());
            }
        }

        return $this;
    }

    /**
     * @param array $data
     * @return bool
     */
    public function isValid($data)
    {
        $radioConsumable = array_key_exists('radio_consumable', $data) ? (bool) $data['radio_consumable'] : false;
        $radioEnvironment = array_key_exists('radio_environment', $data) ? (bool) $data['radio_environment'] : false;

        if (!$radioConsumable && !$radioEnvironment) {
            return parent::isValid($data);
        }

        $workorderService = new Workshop_Service_Workorder();
        $workorder = $workorderService->findById($this->workorderId);

        $priceFilter = new Zend_Filter_LocalizedToNormalized([
            'locale' => 'nl_NL',
            'precision' => 2,
        ]);

        if ($radioConsumable) {
            $internalAmount = array_key_exists('price_internal_consumables', $data) ?
                $priceFilter->filter($data['price_internal_consumables']) : 0;

            $guaranteeAmount = array_key_exists('price_guarantee_consumables', $data) ?
                $priceFilter->filter($data['price_guarantee_consumables']) : 0;

            $externalAmount = array_key_exists('price_external_consumables', $data) ?
                $priceFilter->filter($data['price_external_consumables']) : 0;

            if (!$workorderService->isEqualConsumablesSum($workorder, $internalAmount, $guaranteeAmount, $externalAmount)) {
                $this->addError('consumable');
            }
        }

        if ($radioEnvironment) {
            $internalAmount = array_key_exists('price_internal_environment', $data) ?
                $priceFilter->filter($data['price_internal_environment']) : 0;

            $guaranteeAmount = array_key_exists('price_guarantee_environment', $data) ?
                $priceFilter->filter($data['price_guarantee_environment']) : 0;

            $externalAmount = array_key_exists('price_external_environment', $data) ?
                $priceFilter->filter($data['price_external_environment']) : 0;

            if (!$workorderService->isEqualEnvironmentSum($workorder, $internalAmount, $guaranteeAmount, $externalAmount)) {
                $this->addError('environment');
            }
        }

        return parent::isValid($data);
    }

    /**
     * @param array $netAmount
     */
    public function setNetAmount($netAmount)
    {
        $this->netAmount = $netAmount;
    }

    /**
     * @param int $workorderId
     */
    public function setWorkorderId($workorderId)
    {
        $this->workorderId = $workorderId;
    }

}