<?php

/**
 * Class Workshop_Form_CancelWorkorder
 */
class Workshop_Form_CancelWorkorder extends BAS_Shared_Form_Abstract
{
    const OTHER_CANCEL_REASON_ID = 0;

    private $depotId;


    public function init()
    {

        $elements = [];

        $elements[] = $this->createElement('Select', 'reasonId', [
            'required' => true,
            'decorators' => ['ViewHelper'],
            'multiOptions' => $this->getReasonIdOptions(),
        ]);

        $elements[] = $this->createElement('Text', 'reasonText', [
            'decorators' => ['ViewHelper'],
            'placeholder' => 'reason_textbox_placeholder',
            'class' => 'width355',
        ]);

        $this->setElements($elements);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function isValid($data)
    {
        if ($data['reasonId'] === (string)self::OTHER_CANCEL_REASON_ID) {
            $this->getElement('reasonText')->setRequired(true);
        }

        return parent::isValid($data);
    }

    /**
     * @param int $depotId
     * @return $this
     */
    public function setDepotId($depotId)
    {
        $this->depotId = $depotId;
        return $this;
    }

    /**
     * @return array
     */
    private function getReasonIdOptions()
    {
        $options = [
            '' => 'select_close_reason',
            self::OTHER_CANCEL_REASON_ID => 'other',
        ];

        $workorderService = new Workshop_Service_Workorder();
        $reasons = $workorderService->getListReasonCancelWorkorder($this->depotId);

        foreach ($reasons as $reason) {
            $options[$reason->id] = $reason->reason;
        }

        return $options;
    }

}