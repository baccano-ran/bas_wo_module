<?php

/**
 * Class Workshop_Form_WorkorderGeneral
 */
class Workshop_Form_WorkorderGeneral extends BAS_Shared_Form_Abstract
{
    const DATE_FORMAT = 'd-m-Y';

    /** @var BAS_Shared_Model_OrderWorkshop */
    private $workorder;
    private $userId;
    private $depotId;
    private $workshopId;

    public function init()
    {
        $elements = [];

        $workorderService = new Workshop_Service_Workorder();
        $workorderDetailService = new Workshop_Service_OrderWorkshopDetail();

        $workorderContainsDetailsWithLabourRate = $workorderDetailService->workorderContainsDetailsWithLabourRate($this->workorder->getWorkorderId());
        $isWorkorderReadonly = $workorderService->isWorkorderReadonly($this->workorder, $this->userId);
        $required = !$isWorkorderReadonly;
        $disabled = $isWorkorderReadonly ? true : null;
        $isTypeDisabled = $isWorkorderReadonly || $this->workorder->getType() === BAS_Shared_Model_OrderWorkshop::TYPE_ORDER;
        $isTypeRequired = !$isTypeDisabled;
        $isLockedDisabled = $isWorkorderReadonly;

        $elements[] = new Zend_Form_Element_Select('type', [
            'decorators' => ['ViewHelper'],
            'class' => 'width250 js-general-source',
            'multiOptions' => $this->getWorkorderTypeMultiOptions(),
            'optionClasses' => [
                BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT => '"" disabled="disabled',
                BAS_Shared_Model_OrderWorkshop::TYPE_ORDER => '"" disabled="disabled',
                BAS_Shared_Model_OrderWorkshop::TYPE_STOCK => '"" disabled="disabled',
            ],
            'required' => $isTypeRequired,
            'disabled' => $isTypeDisabled ? true : null,
        ]);

        $elements[] = new Zend_Form_Element_Select('defaultDepotBookingCodeId', [
            'decorators' => ['ViewHelper'],
            'class' => 'width250 js-general-type',
            'multiOptions' => $this->getBookingCodeMultiOptions(),
            'required' => $required,
            'disabled' => $disabled
        ]);

        $workshopMultiOptions = $this->getWorkshopMultiOptions();

        if ($this->workshopId === null && count($workshopMultiOptions) === 2) {
            //preselect single workshop
            $workshopMultiOptionsKeys = array_keys($workshopMultiOptions);
            $this->workshopId = $workshopMultiOptionsKeys[1];
        }

        $elements[] = new Zend_Form_Element_Select('workshopId', [
            'value' => $this->workshopId,
            'decorators' => ['ViewHelper'],
            'class' => 'width250 js-general-affiliate_depots',
            'multiOptions' => $workshopMultiOptions,
            'required' => $required,
            'disabled' => $disabled
        ]);

        $warehouseElement = self::getWarehouseElement($this->workshopId);
        if($isWorkorderReadonly){
            $warehouseElement->setAttrib('disabled', true);
            $warehouseElement->setRequired(false);
        }
        $elements[] = $warehouseElement;

        $elements[] = new Zend_Form_Element_Select('defaultLabourActivityId', [
            'decorators' => ['ViewHelper'],
            'class' => 'width250 js-general-activity',
            'multiOptions' => $this->getLabourActivityMultiOptions(),
            'required' => $required,
            'disabled' => $disabled
        ]);

        $elements[] = new Zend_Form_Element_Text('mileage', [
            'decorators' => ['ViewHelper'],
            'class' => 'width250 js-general-mileage',
            'required' => false,
            'disabled' => $disabled,
            'validators' => [
                new Zend_Validate_Between([
                    'min' => 0,
                    'max' => BAS_Shared_Model_OrderWorkshop::MAX_VEHICLE_MILEAGE,
                    'inclusive' => true,
                ]),
                new Zend_Validate_Int(),
            ],
        ]);

        $elements[] = new Zend_Form_Element_Select('defaultLabourRateId', [
            'decorators' => ['ViewHelper'],
            'class' => 'width250 js-general-rate',
            'multiOptions' => $this->getLabourRateMultiOptions(),
            'required' => $required,
            'disabled' => $disabled,
            'data-original_value' => $this->workorder->getDefaultLabourRateId(),
            'data-workorder_contains_details_with_labour_rate' => $workorderContainsDetailsWithLabourRate,
        ]);

        $defaultUserOptions = ['' => 'placeholder_select_receptionist'];
        if ($this->workorder->userId !== null) {
            $defaultUserOptions[$this->workorder->userId] = $this->getView()->personName($this->workorder->userId);
        }

        $elements[] = new Zend_Form_Element_Select('userId', [
            'decorators' => ['ViewHelper'],
            'class' => 'width250 js-general-receptionist',
            'multiOptions' => $defaultUserOptions + $workorderService->getUserOptions($this->depotId),
            'disabled' => $disabled
        ]);

        $elements[] = new Zend_Form_Element_Text('startDate', [
            'decorators' => ['ViewHelper'],
            'class' => 'js-general-datepicker js-general-deadline',
            'validators' => [new Zend_Validate_Date(['format' => self::DATE_FORMAT])],
            'disabled' => $disabled
        ]);

        $elements[] = new Zend_Form_Element_Text('deadlineDate', [
            'decorators' => ['ViewHelper'],
            'class' => 'js-general-datepicker js-general-startdate',
            'validators' => [
                new Zend_Validate_Date(['format' => self::DATE_FORMAT]),
                $this->getFinishedAndDeadlineDateValidator(),
            ],
            'disabled' => $disabled
        ]);

        $elements[] = new Zend_Form_Element_Hidden('deadlineDateOriginal',[
            'decorators' => ['ViewHelper'],
            'value' => $this->workorder->deadlineDate,
            'allowEmpty' => false,
            'validators' => [$this->getModifiedDeadlineDateValidator()],
        ]);

        $elements[] = new Zend_Form_Element_Hidden('workorderId',[
            'decorators' => ['ViewHelper'],
            'value' => $this->workorder->workorderId
        ]);

        $elements[] = new Zend_Form_Element_Text('finishedDate', [
            'decorators' => ['ViewHelper'],
            'class' => 'js-general-datepicker js-general-finaldate',
            'validators' => [
                new Zend_Validate_Date(['format' => self::DATE_FORMAT]),
                $this->getFinishedAndDeadlineDateValidator(),
            ],
            'disabled' => $disabled
        ]);

        $elements[] = new Zend_Form_Element_Text('customerReferenceNo', [
            'decorators' => ['ViewHelper'],
            'class' => 'width250',
            'disabled' => $disabled,
        ]);

        $elements[] = new Zend_Form_Element_Radio('locked', [
            'decorators' => ['ViewHelper'],
            'multiOptions' => [
                BAS_Shared_Model_OrderWorkshop::NOT_LOCKED => 'no',
                BAS_Shared_Model_OrderWorkshop::LOCKED => 'yes',
            ],
            'separator' => '',
            'disabled' => $isLockedDisabled ? true : null,
            'value' => BAS_Shared_Model_OrderWorkshop::NOT_LOCKED,
        ]);

        $elements[] = $this->createElement('Hidden', 'updateDetailsLabourRate', [
            'decorators' => ['ViewHelper'],
            'class' => 'js-update-details-labour-rates',
            'value' => 0,
        ]);

        $this->setElements($elements);

        $this->direction === self::DIRECTION_INPUT
            ? $this->applyInputFilters()
            : $this->applyOutputFilters();
    }

    /**
     * @param int $depotId
     * @return Zend_Form_Element_Select
     */
    public static function getWarehouseElement($depotId)
    {
        $warehouseService = new Warehouse_Service_Warehouse;
        $depotSettingService = new Management_Service_DepotSetting();

        $defaultWarehouseSetting = null;
        $multiOptions = ['' => 'placeholder_select_warehouse'];

        if ($depotId > 0) {
            $defaultWarehouseSetting = $depotSettingService->getSettingValue($depotId, 'workshop_default_warehouse');
            $warehouses = $warehouseService->findAllInDepot($depotId);
            $multiOptions += BAS_Shared_Utils_Array::listData($warehouses, 'id', 'name');
        }

        return new Zend_Form_Element_Select('warehouseId', [
            'value' => $defaultWarehouseSetting,
            'decorators' => ['ViewHelper'],
            'class' => 'width250 js-general-warehouse_id',
            'multiOptions' => $multiOptions,
            'required' => true,
        ]);
    }

    /**
     * @param int $userId
     * @return $this
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $depotId
     * @return $this
     */
    public function setDepotId($depotId)
    {
        $this->depotId = $depotId;
        return $this;
    }

    /**
     * @param int $workshopId
     * @return $this
     */
    public function setWorkshopId($workshopId)
    {
        $this->workshopId = $workshopId;
        return $this;
    }

    /**
     * @param int $direction
     * @return $this
     */
    public function setDirection($direction)
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return $this
     */
    public function setWorkorder(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        $this->workorder = $workorder;
        return $this;
    }

    private function applyInputFilters()
    {

    }

    private function applyOutputFilters()
    {
        $dateFilter = new Zend_Filter_Callback(function($value) {
            return $this->getView()->formatDate($value, self::DATE_FORMAT);
        });

        $this->getElement('startDate')->addFilter($dateFilter);
        $this->getElement('deadlineDate')->addFilter($dateFilter);
        $this->getElement('finishedDate')->addFilter($dateFilter);
    }

    /**
     * @return Zend_Validate_Abstract
     */
    private function getFinishedAndDeadlineDateValidator()
    {
        $validator = new BAS_Shared_Form_Validate_FinishedAndDeadlineDateValidator();
        return $validator;
    }

    private function getModifiedDeadlineDateValidator()
    {
        $validator = new BAS_Shared_Form_Validate_ChangedDateValidator();
        return $validator;
    }

    /**
     * @return array
     */
    private function getWorkorderTypeMultiOptions()
    {
        return ['' => 'placeholder_select_workorder_source'] + BAS_Shared_Model_OrderWorkshop::$typeLabel;
    }

    /**
     * @return array
     */
    private function getBookingCodeMultiOptions()
    {
        $postingSetupService = new Management_Service_DepotPostingSetup;

        $bookingCodes = $postingSetupService->findBookingCodesByPostingSetupModule(
            BAS_Shared_Model_DepotPostingSetup::MODULE_WORKSHOP,
            $this->depotId
        );

        return $options = ['' => 'select_placeholder_workorder_type'] + BAS_Shared_Utils_Array::listData($bookingCodes, 'id', 'name');
    }

    /**
     * @return array
     */
    private function getWorkshopMultiOptions()
    {
        $depotAffiliateService = new Management_Service_DepotAffiliate;
        $depotService = new Management_Service_Depot;

        $depot = $depotService->getDepotById($this->depotId);
        $affiliateDepots = $depotAffiliateService->getAffiliateDepots($depot->id, BAS_Shared_Model_DepotAffiliate::TYPE_WORKSHOP);
        $options = ['' => 'placeholder_select_workshop'] + array_replace([$depot->id => $depot->name], $affiliateDepots);

        return $options;
    }

    /**
     * @return array
     */
    private function getLabourActivityMultiOptions()
    {
        $labourActivityService = new Workshop_Service_LabourActivity;

        $labourActivities = $labourActivityService->findByDepartmentId(
            BAS_Shared_Model_Department::DEPARTMENT_ID_WORKSHOP,
            $this->depotId
        );

        return [ '' => 'placeholder_select_activity'] + BAS_Shared_Utils_Array::listData($labourActivities, 'id', 'name');
    }

    /**
     * @return array
     */
    private function getLabourRateMultiOptions()
    {
        $labourRateService = new Workshop_Service_LabourRate;
        $labourRates = $labourRateService->findAllByDepotAndDepartment(
            $this->depotId,
            BAS_Shared_Model_Department::DEPARTMENT_ID_WORKSHOP
        );

        $options = ['' => 'placeholder_select_rate'];

        foreach ($labourRates as $labourRate) {
            $options[$labourRate->getId()] = sprintf('%s (%s)', $labourRate->getName(), $labourRate->getRate());
        }

        return $options;
    }

}