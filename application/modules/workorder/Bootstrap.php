<?php

/**
 * Class Workshop_Bootstrap
 */
class Workshop_Bootstrap extends Zend_Application_Module_Bootstrap
{
    /**
     * @throws Zend_Loader_Exception
     */
    public function _initNamespaces()
    {
        $resourceLoader = $this->getResourceLoader();
        $resourceLoader
            ->addResourceType('library', 'library/', 'Library')
            ->addResourceType('formatter', 'formatter/', 'Formatter_')
            ->addResourceType('filter', 'filter/', 'Filter_')
            ->addResourceType('source', 'source/', 'Source_');
    }

    public function hu9blicFunction()
    {
        // nu nachalos
        $ebana9Peremenna9 = '123123';
        $nuhi9kakmojno = '33333';
    }

    public function huyak()
    {
        $a = 1;
        $b = 1;
    }

    /**
     * @return string
     */
    public function someFuckingFunction()
    {
        $choBlya = 'cho blya?';
        return 'zaebalsya <br> ' . $choBlya;
    }
}
