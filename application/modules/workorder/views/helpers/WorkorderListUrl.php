<?php

/**
 * Class Workshop_View_Helper_WorkorderListUrl
 */
class Workshop_View_Helper_WorkorderListUrl extends Zend_View_Helper_Abstract
{

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @return string
     */
    public function workorderListUrl(BAS_Shared_Model_OrderWorkshop $workorder)
    {
        switch ($workorder->getType()) {

            case  BAS_Shared_Model_OrderWorkshop::TYPE_INCOMING_VEHICLES:
            case  BAS_Shared_Model_OrderWorkshop::TYPE_SALES_PREPARATION:
            case  BAS_Shared_Model_OrderWorkshop::TYPE_STOCK:
                $redirectUrl = $this->view->url([
                    'module' => 'workshop',
                    'controller' => 'workorder',
                    'action' => 'vehicles-in',
                ], null, true);
                break;

            case  BAS_Shared_Model_OrderWorkshop::TYPE_ORDER:
                $redirectUrl = $this->view->url([
                    'module' => 'workshop',
                    'controller' => 'index',
                    'action' => 'index',
                    'page' => 1,
                ], null, true);
                break;

            case  BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT:
                $redirectUrl = $this->view->url([
                    'module' => 'defect',
                    'controller' => 'workshop',
                    'action' => 'overview',
                ], null, true);
                break;

            default:
                $redirectUrl = $this->view->url([
                    'module' => 'workshop',
                    'controller' => 'workorder',
                    'action' => 'open-workorder-list',
                ], null, true);
        }

        return $redirectUrl;
    }
}
