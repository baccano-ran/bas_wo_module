<?php
/**
 * view helper to get plannings array week wise
 * and its depend on planning settings
 */
class Workshop_View_Helper_PlanningWeeks extends Zend_View_Helper_FormElement
{
    /**
     * @param integer $dayCount
     * @param DATETIME $date
     * @param string $title
     * @return string
     */
    public function planningWeeks($dayCount, $date, $title = '')
    {
        if ($title !== '') {
            return '<br />' . $this->view->translate($title);
        }

        $strToTime = strtotime($date);
        $day = date("d", $strToTime);
        $month = date("F", $strToTime);
        $title = '';

        switch ($dayCount) {
            case 1:
                $title = 'this';
                break;
            case 2:
                $title = 'next';
                break;
        }

        if ($title === '') {
            $incrementalWeek = date('W', $strToTime);
            $title = $this->view->translate('week') .' '. $incrementalWeek;
        } else {
            $title = $this->view->translate($title) .' '. $this->view->translate('week');
        }

        $title .= ' <br />' . $day . '-' . $this->view->translate($month);

        return $title;
    }
}