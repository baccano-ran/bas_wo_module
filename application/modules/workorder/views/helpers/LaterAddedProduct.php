<?php

/**
 * view helper to get later added product icon
 */
class Workshop_View_Helper_LaterAddedProduct extends Zend_View_Helper_FormElement
{
    /**
     * Later added product in order workshop
     * 
     * @param string $ovpCreatedAt
     * @param string $statusOrderDate
     * @param string $ovCreatedAt
     * @return boolean
     */
    public function laterAddedProduct($ovpCreatedAt, $statusOrderDate, $ovCreatedAt)
    {
        $orderDate = ($statusOrderDate > $ovCreatedAt) ? $statusOrderDate : $ovCreatedAt;
        
        return ($ovpCreatedAt > $orderDate);
    }
}