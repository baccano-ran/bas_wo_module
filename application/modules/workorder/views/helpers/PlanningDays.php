<?php

/**
 * Class Workshop_View_Helper_PlanningDays
 */
class Workshop_View_Helper_PlanningDays extends Zend_View_Helper_FormElement
{
    /**
     * Planning days
     * @param $dayCount
     * @param $balloonDate
     * @param string $title
     * @return string
     */
    public function planningDays($dayCount, $balloonDate, $title = '')
    {
        if ($title !== '') {
            return $this->view->translate($title);
        }
        if ($dayCount > 0) {
            $dayString = date('l', strtotime($balloonDate));
            return $this->view->translate($dayString);
        }
    }

}