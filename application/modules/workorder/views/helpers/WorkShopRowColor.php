<?php
/**
 * view helper to color class name for workshopGrid
 * and its depend on settings
 */
class Workshop_View_Helper_WorkShopRowColor extends Zend_View_Helper_FormElement
{
    const MIN_DAY_DIFF = 0;
    const MAX_DAY_DIFF = 3;

    /**
     * Get row class for color.
     *
     * @param array $item
     * @param integer $highlightWorkOrderSetting
     * @param array $redBackgroundWorkOrder
     * 
     * @return string $classString
     */
    public function workShopRowColor($item,
         $highlightWorkOrderSetting,
         $redBackgroundWorkOrder)
    {

        $unwantedDatesArray = [
            (int)BAS_Shared_Model_ContactCheckin::UNIX_TIME_YEAR,
            BAS_Shared_Model_Vehicle::YEAR_9999,
            1111
        ];
        $classString = '';
        switch ((int)$highlightWorkOrderSetting) {
            case BAS_Shared_Model_DepotSetting::WORKSHOP_DEADLINE_IN_ORDER_VEHICLE_SETTING:
                if ($item['saleType'] == BAS_Shared_Model_OrderVehicle::ORDER_SALE_TYPE_LEASE) {
                    $classString = 'table_tr_yellow';

                } elseif (abs($item['Days_to_schedule']) > self::MIN_DAY_DIFF
                    && abs($item['Days_to_schedule']) <= self::MAX_DAY_DIFF) {
                    $classString = 'table_tr_red';
                }
                if (strtotime($item['Scheduled_Deadline']) < strtotime('now')
                    && !in_array(date("Y",strtotime($item['Scheduled_Deadline'])), $unwantedDatesArray)) {
                    $classString ='table_tr_red';
                }
                break;
            case BAS_Shared_Model_DepotSetting::START_DATE_AND_STATUS_IN_ORDER_WORKSHOP_SETTING:
                if (isset($item['OrderVehicleId']) && in_array($item['OrderVehicleId'], $redBackgroundWorkOrder)) {
                    $classString = 'table_tr_red';
                }
                break;
        }

        return $classString;
    }

}