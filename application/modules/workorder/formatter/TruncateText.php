<?php

class Workshop_Formatter_TruncateText
    extends Workshop_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /**
     * maximum length of text which will be after that truncated
     */
    const MAX_TEXT_LENGTH = 40;

    /**
     * Constructor.
     *
     * @param array $options
     */
    public function __construct($options = array())
    {
    }

    /**
     * Formats a given value
     *
     * @param $value
     * @return string
     */
    public function format($value)
    {

        if (strlen($value) > self::MAX_TEXT_LENGTH) {
            $chunks = str_split($value, self::MAX_TEXT_LENGTH);
            return implode("<br/>", $chunks);
        }

        return $value;
    }
}