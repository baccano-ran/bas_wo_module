<?php
/**
 * Formatter for truck details shown in workshop agenda grid
 */
class Workshop_Formatter_TruckDetails extends Default_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /**
     * Format value for truck details
     *
     * @param mixed $value
     * @return null|string
     */
    public function format($value)
    {
        if (empty($value)) {
            return NULL;
        }
        $truckId = '<a target="_blank" href="' . $this->_options['baseUrl'] . '/default/vehicle/detail/vehicleid/'
            . $this->_options['vehicleId'] . ' ">'
            . $this->_options['truckId'] . '</a><br><br>';

        $supplierReference = '';
        if ($this->_options['supplierReferenceAllowed'] && '' != $this->_options['supplierReference']) {
            $supplierReference = '<div>' . $this->_options['supplierReference'] . '</div><br>';
        } else {
            $supplierReference = '<div></div><br><br>';
        }

        $workshopPrinter = '';
        if ($this->_options['preparationApprovedValue'] <= $this->_options['preparationApproved']) {
            if ($this->_options['workshopAdminAllowed']) {
                if (1 === (int)$this->_options['changeWorkorderStatusOnPrint']) {
                    $workshopPrinter = '<img class="printFastButton cursorPointer width17px" src="' . $this->_options['imageUrl'] . '/print.png" '
                        . 'onclick="printAndForwardWorkorders(' . $this->_options['vehicleId'] . ', ' . $this->_options['orderId'] . ', ' . $this->_options['orderVehicleId'] . ', \'print\', ' . $this->_options['truckId'] . ')"/>';
                    $workshopPrinter .= '<img class="printFastButton cursorPointer workshopForward width17px" src="' . $this->_options['imageUrl'] . '/icons/mailforward.png" '
                        . 'onclick="printAndForwardWorkorders(' . $this->_options['vehicleId'] . ', ' . $this->_options['orderId'] . ', ' . $this->_options['orderVehicleId'] . ', \'mail\', ' . $this->_options['truckId'] . ')" />';
                } else {
                    $workshopPrinter = '<img class="printFastButton cursorPointer width17px" src="' . $this->_options['imageUrl'] . '/print.png" '
                        . 'onclick="openWorkorderWithoutChangingStatus(' . $this->_options['orderId'] . ', ' . $this->_options['vehicleId'] . ', ' . $this->_options['orderVehicleId'] . ', 0, \'print\')"/>';
                    $workshopPrinter .= '<img class="printFastButton cursorPointer workshopForward width17px" src="' . $this->_options['imageUrl'] . '/icons/mailforward.png" '
                        . 'onclick="openWorkorderWithoutChangingStatus(' . $this->_options['orderId'] . ', ' . $this->_options['vehicleId'] . ', ' . $this->_options['orderVehicleId'] . ', 0, \'mail\')" />';
                }
            }
        }

        $element = $truckId . ' ' . $supplierReference . '<div class="workshopPrintIcons">' . $workshopPrinter .
            $this->getView()->vehicleMemo($this->_options['vehicleId']) .
            '</div>';

        return $element;
    }
}