<?php

class Workshop_Formatter_WorkshopName
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /** @var Contacts_Service_ContactCompanyRoles */
    protected $_contactRoleService;

    /** @var array */
    protected $_contactList;
    /**
     * Constructor.
     *
     * @param array $options
     */
    public function __construct($options = []) {
        $this->_options = $options;
    }

    /**
     * @return Contacts_Service_ContactCompanyRoles
     */
    public function getContactCompanyRoleService()
    {
        if (NULL === $this->_contactRoleService) {
            $this->_contactRoleService = new Contacts_Service_ContactCompanyRoles();
        }
        return $this->_contactRoleService;
    }

    /**
     * @return array
     */
    public function getContactList()
    {
        if (NULL === $this->_contactList) {
            $this->_contactList = $this->getContactCompanyRoleService()->findDepotContactsByRole(
                BAS_Shared_Auth_Service::getActiveDepotId(),
                BAS_Shared_Model_ContactCompanyRole::WORKSHOP,
                true
            );
        }
        return $this->_contactList;
    }

    /**
     * Formats a given value
     *
     * @param $status
     * @return string
     */
    public function format($workshopId)
    {
        if (BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK === (int)$this->_options['type']) {
            // get contacts with workshop role
            $contactList = $this->getContactList();
            return (isset($contactList[$workshopId])) ? $contactList[$workshopId] : $workshopId;
        } else {
            // get depots
            $session = Zend_Session::namespaceGet('basext');
            $depotList = (isset($session['depots']['affiliatedDepotMastersWithActiveDepot'])) ?
                $session['depots']['affiliatedDepotMastersWithActiveDepot'] : [];
            return (isset($depotList[$workshopId])) ? $depotList[$workshopId] : $workshopId;
        }
    }
}