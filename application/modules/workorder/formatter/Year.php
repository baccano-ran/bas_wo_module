<?php

class Workshop_Formatter_Year
    implements Bvb_Grid_Formatter_FormatterInterface
{

    /**
     * Constructor.
     *
     * @param array $options
     */
    public function __construct($options = array())
    {
    }

    /**
     * Formats a given value
     *
     * @param $value
     * @return string
     */
    public function format($value)
    {
        $dt = new DateTime($value);
        $value = date_format($dt,'Y');
        return $value;
    }
}