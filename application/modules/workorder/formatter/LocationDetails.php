<?php
/**
 * Show truck location link
 */
class Workshop_Formatter_LocationDetails extends Default_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /**
     * Formats a given value
     *
     * @param string $value
     * @return string
     */
    public function format($value)
    {
        if (!empty($this->_options['tagId']) && 1 == $this->_options['tagId']) {
            $element = $this->_options['deliverLabel'];
        } else if ($this->_options['basDepot'] == $this->_options['depotIdLocation']) {
            $link = $this->_options['extranetUrl'] . '/map/index/index?z=' . $this->_options['location'];
            $element = '<span class="spanToLink" onclick="openPopupWindow(\'' . $link . '\', 1035, 620);">'
                . $this->_options['location']
                . '</span>';
        } else {
            $element = $this->_options['location'];
        }

        return $element;
    }
}