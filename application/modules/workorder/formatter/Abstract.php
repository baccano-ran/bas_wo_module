<?php

class Workshop_Formatter_Abstract
{
    /**
     * escapes input
     *
     * @param $string
     * @return string
     */
    public function escape($string)
    {
        return htmlspecialchars($string);
    }

    /**
     * outputs defect icon in formatters
     *
     * @param $name image filename in specific folder
     * @param string $title title of image
     * @param array $customAttributes attributes for image
     * @param string $class custom wrapper div class
     * @return string
     */
    public function icon($name, $title = '', $customAttributes = array(), $class = 'middle')
    {
        $attrs = '';

        foreach($customAttributes as $k => $v) {
            $attrs .= $k . '="' . $this->escape($v) . '" ';
        }

        return '<div class="' . $class . '"><img src="'
            . Workshop_Service_BaseVehicleStockTable::ICON_PATH
            . '/'
            . $this->escape($name)
            . '" border="0" title="' . $this->escape($title) . '" width="16" height="16" ' . $attrs . '/>'
            .'</div>';
    }
}