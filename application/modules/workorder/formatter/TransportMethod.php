<?php
/**
 * Formatter for transport method of workshop grid
 */
class Workshop_Formatter_TransportMethod extends Default_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /**
     * Format transport method for workshop agenda grid
     *
     * @param string $value
     * @return null
     */
    public function format($value)
    {
        if (empty($value)) {
            return null;
        }

        return $this->getView()->translate($value);
    }
    
}