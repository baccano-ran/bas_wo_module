<?php

class Workshop_Formatter_WorkshopStatus
    extends Workshop_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{

    /**
     * Constructor.
     *
     * @param array $options
     */
    public function __construct($options = array())
    {
    }

    /**
     * Formats a given value
     *
     * @param $value
     * @return string
     */
    public function format($value)
    {
        $value = $value === '' ? null : (int)$value;

        $properties = array(
            'class' => 'status',
            'data-value' => $value,
            'onclick' => 'undoWorkorderPrint({{workorder_id}}, '. BAS_Shared_Model_OrderWorkshop::TYPE_INCOMING_VEHICLES .')'
        );

        $t = Zend_Registry::get('Zend_Translate');
        switch(true) {
            case ($value === BAS_Shared_Model_OrderWorkshop::WORKORDER_OFFER):
                return '';
            case ($value === BAS_Shared_Model_OrderWorkshop::WORKORDER_START):
                return $this->icon('cross_red.png',
                    $t->translate('wokshop_start')
                );
            case ($value === BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED):
                return $this->icon('cross_orange.png',
                    $t->translate('workshop_printed'),
                    $properties
                );
            case ($value === BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED):
                return $this->icon('tick.png',
                    $t->translate('workshop_closed'),
                    $properties
                );
        }
        return $value;
    }
}