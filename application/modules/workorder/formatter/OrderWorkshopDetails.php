<?php
/**
 * Formatter for order and workshop details shown in workshop agenda grid
 */
class Workshop_Formatter_OrderWorkshopDetails extends Default_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /**
     * Format value for workshop details
     *
     * @param mixed $value
     * @return null|string
     */
    public function format($value)
    {
        if (empty($value)) {
            return NULL;
        }
        
        $orderId = $this->_options['orderId'];
        $vehicleId = $this->_options['vehicleId'];
        $element = '<a href="' . $this->_options['baseUrl'] . '/order/overview?id=' . $orderId . '" target="_blank">'
            . $orderId . '</a>';
        $element .= '<br><br>' . $this->portalVehicleIcon($this->_options['contactTypeSupplierFeeContactId'], $this->_options['portalIconRight'], 'workshop.index');
        $element .= '<div>
            <strong>
                <span class="work-link spanToLink" id="workOrderLink_' . $orderId . '_' . $vehicleId . '"
                    orderId="' . $orderId . '"
                    vehicleId="' . $vehicleId . '"
                    orderVehicleId="' . $this->_options['orderVehicleId'] . '"
                    truckId="' . $this->_options['truckId'] . '">'
            . $this->_options['workOrderLabel']
            . '</span>
            </strong>
        </div>';

        return $element;
    }

    /**
     * Generate HTML for showing portal vehicle icon
     *
     * @param integer $supplierId
     * @param boolean $portalIconRight
     * @param string $listName
     * @return string
     */
    public function portalVehicleIcon($supplierId, $portalIconRight, $listName = null)
    {
        $interSupplierIds = isset($this->_options['interSupplierIds']) ?
            unserialize($this->_options['interSupplierIds']) : [];
        $userList = isset($this->_options['supplierList']) ?
            unserialize($this->_options['supplierList']) : [];
        
        // check if user has right to see this icon
        if (!$portalIconRight
            || 0 === (int)$supplierId
            || in_array($supplierId, $interSupplierIds)
            || (int)$supplierId <= $this->_options['supplierFeeUnknown']) {

            $defaultReturnValue = [
                'workshop.index' => '<br/>',
            ];
            
            return isset($defaultReturnValue[$listName]) ? $defaultReturnValue[$listName] : null;
        }

        // fetch supplier name
        $abbreviation = (isset($userList[$supplierId])) ?
            $userList[$supplierId] :
            $this->_options['unknownLabel'];

        return '<img src="' . $this->_options['imageUrl'] . '/icons/partner.png" alt="'
            . $abbreviation . '" title="' . $abbreviation . '" class="additional-product-notification" />';
    }
}