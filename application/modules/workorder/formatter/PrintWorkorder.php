<?php

class Workshop_Formatter_PrintWorkorder
    implements Bvb_Grid_Formatter_FormatterInterface
{
    protected $_options;
    /**
     * Constructor.
     *
     * @param array $options
     */
    public function __construct($options = []) {
        $this->_options = $options;
    }

    /**
     * Formats a given value
     *
     * @param int $workorderId
     * @return string
     */
    public function format($workorderId)
    {
        if ($this->_options['type'] == BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT) {
            return $this->_formatDefects($workorderId, $this->_options);
        }
        
        $functionName = 'setProductToWorkorder';
        if ($this->_options['canAssignWorkManually']) {
            $functionName = 'updateWorkorderStatus';
        }
        
        $onClick = sprintf("%s('%d','%d','%d','%d','%s','%d');",
                $functionName,
                $this->_options['orderId'],
                $this->_options['vehicleId'],
                $this->_options['orderVehicleId'],
                $workorderId,
                $this->_options['serviceUrl'],
                $this->_options['truckId']
            );

        if (0 === (int)$this->_options['changeWorkorderStatusOnPrint']) {
            $sendMail = sprintf('openWorkorderWithoutChangingStatus(%d, %d, %d, %d, \'%s\');',
                $this->_options['orderId'],
                $this->_options['vehicleId'],
                $this->_options['orderVehicleId'],
                $workorderId,
                'mail');
            $workorderPrintHtml = $this->_getWorkorderLink($workorderId, false);
        } else {
            $sendMail = sprintf("prepareWorkorderEmail(%d, %d, %d, %d, %d, %d);",
                $workorderId,
                $this->_options['workorderStatus'],
                $this->_options['orderId'],
                $this->_options['orderVehicleId'],
                $this->_options['vehicleId'],
                $this->_options['truckId']
            );

            if ((int)$this->_options['workorderStatus'] === BAS_Shared_Model_OrderWorkshop::WORKORDER_START) {
                $workorderPrintHtml = '<img
                    width="17px" class="printFastButton"
                    style="float:bottom; margin:30px -10px 20px 10px; cursor:hand;"
                    src="/themes/bas/images/print.png"
                    onclick="' . $onClick . '"/> ';
            } elseif (BAS_Shared_Model_OrderWorkshop::WORKORDER_OFFER === $this->_options['workorderStatus']) {
                $onClick = sprintf('makeOrderBeforePrint(%d, function() { %s })', $workorderId, $onClick);
            } else {
                $workorderPrintHtml = '<a href="' .
                    $this->_options['serviceUrl'] .
                    '/v1/order/' . $this->_options['orderId'] .
                    '/workorder?vehicleId=' . $this->_options['vehicleId'] .
                    '&workorderId=' . $workorderId . '" ' .
                    'target="_blank">'
                    . '<img style="float:bottom; margin:30px -10px 20px 10px; cursor:hand;" '
                    . 'src="/themes/bas/images/print.png" width="17"/>'
                    . '</a> ';
            }
        }

        $workorderIdFormatted = $this->_formatWorkorderIdByExternalWorkorderSetting($workorderId, $this->_options['depotHasExtranetWorkorderSetting']);

        $formattedCell = '<div>
            <div>' . $workorderIdFormatted . '</div>
            <div>' . $workorderPrintHtml .
                '<img 
                    widht="17px" class="printFastButton" 
                    style="float bottom; margin:30px -43px 15px 20px;cursor:pointer; cursor:hand;" 
                    src="/themes/bas/images/send_mail.png"
                    onclick="' . $sendMail . '" />
            </div>
        </div>';
                
        return $formattedCell;
    }

    /**
     * Prepare links for defects
     * @param $workorderId
     * @param array $options
     * @return string
     */
    public function _formatDefects($workorderId, $options)
    {
        $onClick = sprintf("printWorkOrder('%d','%d','%d','%d');",
                    $options['vehicleId'],
                    $options['defectId'],
                    $workorderId,
                    $options['as400Id']
                );
        
        $defectPrintHtml = '<img 
                    width="17px" class="printFastButton" 
                    style="float:bottom; margin:30px -10px 20px 10px; cursor:hand;" 
                    src="/themes/bas/images/print.png" 
                    onclick="' . $onClick . '"/> ';
        
        $defectHtml = '<a target="_blank"
                href="/vbd/public/defect/card?vehicle=' . $options['vehicleId'] . '" >
                <img
                    style="float bottom; margin:30px -43px 19px 20px;cursor:pointer; cursor:hand;"
                    width="16" height="16" border="0" title="" 
                    src="/vbd/public/images/icons/defects/defect.png" />
                </a>';
        
        $formattedCell = '<div>
            <div>' . $workorderId . '</div>
            <div>' . $defectPrintHtml . $defectHtml .
            '</div>
        </div>';
                
        return $formattedCell;
    }

    /**
     * Prepare link for workorder id dependent of depot_setting.extranet_workorder
     * @param $workorderId
     * @param $depotHasExtranetWorkorderSetting
     * @return string
     */
    private function _formatWorkorderIdByExternalWorkorderSetting($workorderId, $depotHasExtranetWorkorderSetting)
    {
        if($depotHasExtranetWorkorderSetting){
            return '<a href="/vbd/public/workshop/workorder/edit/id/'.$workorderId.'" target="_blank">'.$workorderId.'</a>';
        }
        return $workorderId;
    }


    /**
     * @param int $workorderId
     * @param bool $isMail
     * @return string
     */
    private function _getWorkorderLink($workorderId, $isMail = false)
    {
        $mailOption = (true === $isMail)? '&actionType=mail': '';

        return '<a href="' .
            $this->_options['serviceUrl'] .
            '/v1/order/' . $this->_options['orderId'] .
            '/workorder?vehicleId=' . $this->_options['vehicleId'] .
            '&workorderId=' . $workorderId . $mailOption . '" ' .
            'target="_blank">'
            . '<img style="float:bottom; margin:30px -10px 20px 10px; cursor:hand;" '
            . 'src="/themes/bas/images/print.png" width="17"/>'
            . '</a> ';
    }
}