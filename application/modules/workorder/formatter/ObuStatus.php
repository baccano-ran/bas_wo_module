<?php
/**
 * Formatter for obu status
 */
class Workshop_Formatter_ObuStatus extends Default_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /**
     * Format workshop status for workshop grid
     *
     * @param mixed $value
     * @return NULL|string
     */
    public function format($value)
    {
        $onclick = '';
        $obuStatusIcon = '';

        if ((int)$this->_options['obuStatus'] === BAS_Shared_Model_OrderVehicle::OBU_STATUS_IN_PROCESS) {
            $onclick = sprintf(
                'updateObuStatus(%d, %d, %d, %s);',
                $this->_options['orderId'],
                $this->_options['orderVehicleId'],
                BAS_Shared_Model_OrderVehicle::OBU_STATUS_COMPLETED,
                $this->_options['obuStatus']
            );
        }

        //Status icon
        switch ($this->_options['obuStatus']) {
            case BAS_Shared_Model_OrderVehicle::OBU_STATUS_ACTION_REQUIRED :
                $obuStatusIcon = 0; //red
                break;
            case BAS_Shared_Model_OrderVehicle::OBU_STATUS_IN_PROCESS :
                $obuStatusIcon = 1; //orange
                break;
            case BAS_Shared_Model_OrderVehicle::OBU_STATUS_COMPLETED :
                $obuStatusIcon = 3;//Green
                break;
        }

        $source = $this->_options['imageUrl'] . '/action_' . $obuStatusIcon . '.png';

        $class = (!empty($onclick) ? 'cursorPointer' : '');

        return sprintf('<div class="obuStatus"><img id="IMG_OBUStatus_%d" src="%s" onclick="%s" class="%s"/></div>',
            $this->_options['orderVehicleId'],
            $source,
            $onclick,
            $class
        );
    }
}