<?php

class Workshop_Formatter_WorkorderType
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /** @var array */
    protected $_options;
    
    /**
     * Constructor.
     *
     * @param array $options
     */
    public function __construct($options = []) {
        $this->_options = $options;
    }

    /**
     * Formats a given value
     *
     * @param $status
     * @return string
     */
    public function format($status)
    {
        $translator = Zend_Registry::get('Zend_Translate');
        $typeText = '';
        switch ($status) {
            case BAS_Shared_Model_OrderWorkshop::TYPE_ORDER:
                $typeText = $translator->translate('order');
                break;
            case BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT:
                $typeText = $translator->translate('defects');
                break;
            case BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK:
                $typeText = $translator->translate('external');
                break;
        }
        
        return $typeText;
    }
}