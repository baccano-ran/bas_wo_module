<?php

class Workshop_Formatter_WorkorderStatus
    implements Bvb_Grid_Formatter_FormatterInterface
{

    /** @var mixed Zend_Translate */
    protected $_translator;

    /**
     * Constructor.
     *
     * @param array $options
     */
    public function __construct($options = [])
    {
        $this->_options = $options;
        $this->_translator = Zend_Registry::get('Zend_Translate');
    }

    private function _getTranslator()
    {
        return $this->_translator;
    }

    /**
     * Formats a given value
     *
     * @param $status
     * @return string
     */
    public function format($status)
    {
        $onClick = '';
        $style = '';
        $workshopImageName = '';
        $statusText = '';
        $nextPossibleStatus = null;
        $imageAttributes = [];

        if (BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK === (int)$this->_options['workorderType']) {
            $options = $this->_prepareExternalWorkorderParams($status);
        } elseif (BAS_Shared_Model_OrderWorkshop::TYPE_ORDER === (int)$this->_options['workorderType']
        || BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT === (int)$this->_options['workorderType']) {
            $options = $this->_prepareInternalWorkorderParams($status);
        }
        extract($options);

        if (NULL !== $nextPossibleStatus || 0 === (int)$this->_options['changeWorkorderStatusOnPrint']) {
            $imageAttributes = $this->_getStatusImageTagAttributes($this->_options['workorderType']);
        }
        extract($imageAttributes);

        $imageIcon = $this->_getOrderCommentIcon();

        return '<div class="alignCenter"><img id="workorderStatus_' . $this->_options['workorderId'] . '" src="/themes/bas/icons/fatcow/16x16/' . $workshopImageName .
        '" title="' . $statusText . '" ' . $onClick . $style . ' > ' . $imageIcon . ' </div>';
    }

    /**
     * @param int $status
     * @return array
     */
    private function _prepareExternalWorkorderParams($status)
    {
        $translator = $this->_getTranslator();
        $statusWiseParameter = [
            BAS_Shared_Model_OrderWorkshop::WORKORDER_START => [
                'workshopImageName' => 'action_0.png',
                'statusText' => $translator->translate('not_started'),
                'nextPossibleStatus' => BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED,
            ],
            BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED => [
                'workshopImageName' => 'action_1.png',
                'statusText' => $translator->translate('started'),
                'nextPossibleStatus' => BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED,
            ],
            BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED => [
                'workshopImageName' => 'action_3.png',
                'statusText' => $translator->translate('complete'),
                'nextPossibleStatus' => BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED,
            ],
            BAS_Shared_Model_OrderWorkshop::WORKORDER_ARCHIVED => [
                'workshopImageName' => 'action_3.png',
                'statusText' => $translator->translate('archived'),
                'nextPossibleStatus' => null,
            ],
        ];
        return (isset($statusWiseParameter[$status])) ? $statusWiseParameter[$status] : [];
    }


    /**
     * @param int $status
     * @return array
     */
    private function _prepareInternalWorkorderParams($status)
    {
        $translator = $this->_getTranslator();
        $statusWiseParameter = [
            BAS_Shared_Model_OrderWorkshop::WORKORDER_START => [
                'workshopImageName' => 'action_0.png',
                'statusText' => $translator->translate('not_started'),
                'nextPossibleStatus' => null,
            ],
            BAS_Shared_Model_OrderWorkshop::WORKORDER_PRINTED => [
                'workshopImageName' => 'action_1.png',
                'statusText' => $translator->translate('started'),
                'nextPossibleStatus' => BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED,
            ],
            BAS_Shared_Model_OrderWorkshop::WORKORDER_CLOSED => [
                'workshopImageName' => 'action_3.png',
                'statusText' => $translator->translate('complete'),
                'nextPossibleStatus' => null,
            ],
            BAS_Shared_Model_OrderWorkshop::WORKORDER_ARCHIVED => [
                'workshopImageName' => 'action_3.png',
                'statusText' => $translator->translate('archived'),
                'nextPossibleStatus' => null,
            ],
        ];
        return (isset($statusWiseParameter[$status])) ? $statusWiseParameter[$status] : [];
    }

    /**
     * @param int $status
     * @return array
     */
    private function _getStatusImageTagAttributes($status)
    {
        $imageTagAttributes = [
            BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK => [
                'onClick' => sprintf('onclick="updateExternalWorkorderStatus(%d);"',
                    $this->_options['workorderId']),
                'style' => ' style="cursor:pointer;"'
            ]
        ];

        if (true === (bool)$this->_options['canCompleteInternalWorkorder']) {
            $imageTagAttributes[BAS_Shared_Model_OrderWorkshop::TYPE_ORDER] = [
                'onClick' => sprintf('onclick="completeInternalWorkorder(%d, %d, %d);"',
                    $this->_options['workorderId'], $this->_options['orderVehicleId'], $this->_options['orderId']),
                'style' => ' style="cursor:pointer;"'
            ];

            $imageTagAttributes[BAS_Shared_Model_OrderWorkshop::TYPE_DEFECT] = [
                'onClick' => sprintf('onclick="completeInternalWorkorder(%d, %d, %d);"',
                    $this->_options['workorderId'], $this->_options['orderVehicleId'], $this->_options['orderId']),
                'style' => ' style="cursor:pointer;"'
            ];
        }
        return (isset($imageTagAttributes[$status])) ? $imageTagAttributes[$status] : [];
    }

    /**
     * Get order commnet icon
     * @return string
     */
    protected function _getOrderCommentIcon()
    {
        if ((int)$this->_options['orderId'] == 0) {
            return;
        }

        $isWorkOrderIdPresent = (isset($this->_options['workorderId'])  && $this->_options['workorderId'] > 0) ? true :false;
        $imageIcon = '<div class="widgetPosition" style="position: relative;"><img class="commentMessageIcon" ';

        if ($isWorkOrderIdPresent) {
            $imageIcon .= sprintf('onclick="displayCommentUsingExtraString(%d, %d, %d, %d);" ' ,
                BAS_Shared_Model_OrderComment::COMMENT_TYPE_WORKSHOP,
                $this->_options['vehicleId'],
                $this->_options['orderId'],
                $this->_options['workorderId']
            );
        } else {
            $imageIcon .= sprintf('onclick="displayComment(%d, %d, %d);" ' ,
                BAS_Shared_Model_OrderComment::COMMENT_TYPE_WORKSHOP,
                $this->_options['vehicleId'],
                $this->_options['orderId']
            );
        }

        $imageIcon .= sprintf('id="orderComment_%d" ' , $this->_options['orderId']);
        $icon = '/themes/bas/icons/fatcow/16x16/add.png';
        if (!empty($this->_options['commentId'])) {
            $icon = '/themes/bas/icons/fatcow/16x16/comment.png';
        }
        $imageIcon .= sprintf('src="%s" />', $icon);

        if ($isWorkOrderIdPresent) {
            $imageIcon .= sprintf('<div id="displayComment_%d_%d_%d" class="commentBoxOuter"></div>',
                $this->_options['vehicleId'],
                $this->_options['orderId'],
                $this->_options['workorderId']
            );
        } else {
            $imageIcon .= sprintf('<div id="displayComment_%d" class="commentBoxOuter"></div>',
                $this->_options['vehicleId']
            );
        }

        $imageIcon .=  '</div>';

        return $imageIcon;
    }
}