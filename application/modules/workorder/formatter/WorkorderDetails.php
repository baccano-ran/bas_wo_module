<?php

class Workshop_Formatter_WorkorderDetails
    implements Bvb_Grid_Formatter_FormatterInterface
{
    protected $_options;
    /**
     * Constructor.
     *
     * @param array $options
     */
    public function __construct($options = []) {
        $this->_options = $options;
    }

    /**
     * Formats a given value
     *
     * @param int $truckId
     * @return string
     */
    public function format($truckId)
    {
        $workorderLink = '';
        
        if (BAS_Shared_Model_OrderWorkshop::TYPE_ORDER === (int)$this->_options['type'] ||
            BAS_Shared_Model_OrderWorkshop::TYPE_EXTERNAL_WORK === (int)$this->_options['type']) {
            $onClick = sprintf("showWorkorderDetails(this,'%d','%d','%d','%d','%d');",
                $this->_options['vehicleId'],
                $this->_options['orderId'],
                $this->_options['orderVehicleId'],
                $this->_options['truckId'],
                $this->_options['workorderId']
            );
            $workorderLink = '<span class="spanToLink" id="workorder-details-' . $this->_options['workorderId'] . '" onclick="' . $onClick . '">
                <b>' . $this->_options['workOrderLabel'] . '</b>
            </span>';
        } else {
            $onClick = sprintf("showDefectDetails(this,'%d','%d','%d');",
                $this->_options['vehicleId'],
                $this->_options['workorderId'],
                $this->_options['defectId']
            );
            $workorderLink = '<span class="spanToLink" id="workorder-details-' . $this->_options['workorderId'] . '" onclick="' . $onClick . '">
                <b>' . $this->_options['detailLabel'] . '</b>
            </span>';
        }

        $supplierReference = '';
        if ($this->_options['isSupplierReferenceVisible']) {
            $supplierReference = $this->_options['supplierReference'];
        }

        $formattedCell = '<div><a href="/vbd/public/default/vehicle/detail?truckId='. $truckId .'">' . 
                $truckId . '</a><br/><br/>' . $supplierReference . '<br/><br/>' . $workorderLink . '</div>';

        return $formattedCell;
    }
}