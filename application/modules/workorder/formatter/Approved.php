<?php
/**
 * Class for formatting preparation approved value of workshop grid
 */
class Workshop_Formatter_Approved
    extends Default_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /**
     * Formats a given value
     *
     * @param $value
     * @return string
     */
    public function format($value)
    {
        $cache = BAS_Shared_Registry::get('cache');
        $cacheKey = 'userListDepot' . $this->_options['activeDepotId'];
        $userList = $cache->load($cacheKey);

        $approvedAt = $this->_options['preparationApprovedAt'];
        $preparationApprovedBy = isset($userList[$this->_options['preparationApprovedBy']]) ?
            $userList[$this->_options['preparationApprovedBy']]['abbreviation'] . ' ' : '';
        if ($approvedAt) {
            $approvedAt = date('d-m-Y H:i:s', strtotime($approvedAt));
        }
        $preparationRemark = '';
        if ($this->_options['preparationRemark']) {
            $preparationRemark .= '<img src="' . $this->_options['imagePath'] . '/comment.png" id="comment_' . $this->_options['vehicleId'] . '" class="width20 height20"/>'
                . '<span id="comment_text_' . $this->_options['vehicleId'] . '" class="commentText displayNone position-absolute">'
                . '<div>' . $this->_options['preparationRemark'] . '<br><br>' . $this->_options['commentBy'] . '<br>' . $preparationApprovedBy . ' ' . $this->_options['commentAt'] . ' ' . $approvedAt . '</div>'
                . '</span>';
        }
        if ($this->_options['unknownLabel'] == $value) {
            $value = null;
        }

        $approvedText = '<span class="relative commentSpan" id="' . $this->_options['vehicleId'] . '">'
            . '<img src="' . $this->_options['imagePath'] . '/icons/thumb_' . $value . '.png"/>' . $preparationRemark . '</span>';

        return $approvedText;
    }
}