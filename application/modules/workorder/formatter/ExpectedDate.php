<?php
/**
 * Formatter for expected date of workshop grid
 */
class Workshop_Formatter_ExpectedDate extends Default_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /**
     * Default deadline color class
     */
    const DEADLINE_COLOR_CLASS = 'red';

    /**
     * Format expected date for workshop grid
     * 
     * @param mixed $value
     * @return NULL|string
     */
    public function format($value)
    {
        if (empty($value)) {
            return NULL;
        }

        $fontStyleClass = '';

        $deadLineDate = $this->_options['expectedArrival'];
        if (0 >= strtotime($this->_options['expectedDate'])) {
            $fontStyleClass = 'fontStyleItalic';
        }

        if (BAS_Shared_Model_Vehicle::DEFAULT_EMPTY_DATE == $deadLineDate) {
            return null;
        }

        $deadlineClass = (strtotime($deadLineDate) < strtotime('+3 week')) ? 1 : 0;
        $workOrderClass = ($this->_options['workorderId'] == '') ? 1 : 0 ;
        $fontColor = $deadlineClass ? ($workOrderClass ? self::DEADLINE_COLOR_CLASS : ''): '';

        return '<span class="workshop-expected-date ' . $fontStyleClass . ' ' . $fontColor . '">'
            . date('d-m-Y', strtotime($deadLineDate))
            . '</span>';
    }
}