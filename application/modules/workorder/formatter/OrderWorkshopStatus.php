<?php
/**
 * Formatter for workshop status
 */
class Workshop_Formatter_OrderWorkshopStatus extends Default_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /**
     * Format workshop status for workshop grid
     *
     * @param mixed $value
     * @return NULL|string
     */
    public function format($value)
    {
        $onclick = $class = $commentMessageIcon = '';
        $displayCommentBox = "commentBoxOuterZeroComment";
        $function = 'displayComment(' . $this->_options['workshopCommentType'] . ', '
            . $this->_options['vehicleId'] . ', ' . $this->_options['orderId'] . ');';
        $source = $this->_options['imageUrl'] . '/action_' . (int)$this->_options['preparationWorkshop'] . '.png';
        if ((!isset($this->_options['commentId']) && empty($this->options['commentId']))
            || empty($this->_options['commentId'])) {
            $onclick = $function;
            $class = 'commentMessageIcon';
        }

        if (!empty($this->_options['commentId']) ) {
            $commentMessageIcon = '<img class="commentMessageIcon imageWidthHeight16 zIndex0" onclick="' . $function . '" '
                . ' id="orderComment_' . $this->_options['orderId'] . '" src="' . $this->_options['imageUrl'] . '/comment.png"/>';
            $displayCommentBox = '';
        }
        $commentBox = '<div id="displayComment_' . $this->_options['vehicleId'] . '" class="commentBoxOuter zIndex1 '
            . $displayCommentBox.'"></div>';

        return $element = '<div class="workshopStatus"><img id="IMGWorkshopStatus_' . $this->_options['orderVehicleId']
            . '" src="' . $source . '" '
            . 'onclick="' . $onclick . '" class="' . $class . '"/>'
            . $commentMessageIcon . ' ' . $commentBox . '</div>';
    }
}