<?php
/**
 * Formatter for customer
 *
 * Provide link for customer info, which appear on mouse over of link icon
 */
class Workshop_Formatter_CustomerDetails extends Default_Formatter_Abstract
    implements Bvb_Grid_Formatter_FormatterInterface
{
    /**
     * Formats a given value
     *
     * @param string $value
     * @return string
     */
    public function format($value)
    {
        if (empty($this->_options['contactId'])) {
            return '';
        }

        $displayPopup = ((isset($this->_options['ignoreRights'])  && $this->_options['ignoreRights'])
            || $this->_options['customerInfoAllowed']);

        if (!$displayPopup) {
            return '';
        }

        $customerLink = '<a target="_blank" href="' . $this->_options['baseUrl'] . '/contact/contact-card-page/index/id/' . $this->_options['contactId'] . '">'
            . $this->_options['client']
            . '</a>';

        return '<div class="widgetPosition"><div>
                <img src="' . $this->_options['imageUrl'] . '/icons/expand.png"
                    alt="' . $this->_options['contactDetailLabel'] . '"
                    class="contactPopUp"
                    contactId="' . $this->_options['contactId'] . '"/>'
            . $customerLink
            . '</div></div>';
    }
}