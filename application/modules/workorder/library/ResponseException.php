<?php

/**
 * Class Workshop_Library_ResponseException
 */
class Workshop_Library_ResponseException extends BAS_Shared_Exception
{
    /**
     * @var array
     */
    private $_responseData;

    /**
     * Warehouse_Library_ResponseException constructor.
     *
     * @param array $responseData
     */
    public function __construct(array $responseData)
    {
        parent::__construct();
        $this->_responseData = $responseData;
    }

    /**
     * @param array $data
     */
    public function setResponseData(array $data)
    {
        $this->_responseData = $data;
    }

    /**
     * @return array
     */
    public function getResponseData()
    {
        return $this->_responseData;
    }
}