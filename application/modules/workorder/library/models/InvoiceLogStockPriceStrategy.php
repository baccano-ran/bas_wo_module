<?php

/**
 * Class BAS_Shared_Model_Workshop_InvoiceLogStockPrice
 */
class BAS_Shared_Model_Workshop_InvoiceLogStockPriceStrategy
{

    protected $itemStockTransactionService;
    protected $itemStockService;
    protected $itemService;
    protected $productService;

    /**
     * BAS_Shared_Model_Workshop_InvoiceLogStockPrice constructor.
     * @param Warehouse_Service_ItemStockTransaction $itemStockTransactionService
     * @param Warehouse_Service_ItemStock $itemStockService
     * @param Warehouse_Service_Item $itemService
     * @param Order_Service_Product $productService
     */
    public function __construct(
        Warehouse_Service_ItemStockTransaction $itemStockTransactionService,
        Warehouse_Service_ItemStock $itemStockService,
        Warehouse_Service_Item $itemService,
        Order_Service_Product $productService
    ) {
        $this->itemStockTransactionService = $itemStockTransactionService;
        $this->itemStockService = $itemStockService;
        $this->itemService = $itemService;
        $this->productService = $productService;
    }

    /**
     * Calculates stock price
     *
     * @param BAS_Shared_Service_ItemStock_ReferenceLine_ReferenceLineInterface $referenceLine
     * @param bool $isCreditInvoice
     * @param int $depotId
     * @return float
     */
    public function getPrice(
        BAS_Shared_Service_ItemStock_ReferenceLine_ReferenceLineInterface $referenceLine,
        $isCreditInvoice,
        $depotId
    ) {
        $isExternalTask = false;
        $isNoStockItem = false;
        $isStockItem = false;

        if ($referenceLine->isItem()) {
            $item = $this->itemService->findById($referenceLine->getItemId());
            $isNoStockItem = $item->isNoStockItem();
            $isStockItem = $item->isStockItem();
        } elseif ($referenceLine->isTask()) {
            $task = $this->productService->findByIdAndDepotId($referenceLine->getItemId(), $depotId);
            $isExternalTask = $task->getWorkshopTaskType() === BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL;
        }

        if ($isExternalTask || $isNoStockItem) {
            return $referenceLine->getCostPrice();
        }

        if ($isStockItem) {
            if ($referenceLine->isTradeIn()) {
                return $referenceLine->getPriceOrder();
            } else {
                if ($isCreditInvoice) {
                    return $this->itemStockService->getItemStockPrice($referenceLine->getItemId(), $depotId);
                } else {
                    $outboundTransactions = $this->getOutboundTransactions($referenceLine->getTransactionReferenceType(), $referenceLine->getId());
                    return $this->calcAvgStockPriceByTransactions($outboundTransactions);
                }
            }
        }

        return 0;
    }

    /**
     * @param int $referenceType
     * @param int $referenceLineId
     * @return BAS_Shared_Model_Warehouse_ItemStockTransaction[]
     */
    private function getOutboundTransactions($referenceType, $referenceLineId)
    {
        $outboundTransactions = [];

        if ($referenceType === BAS_Shared_Model_Warehouse_ItemStockTransaction::REFERENCE_TYPE_WORKORDER) {
            $outboundTransactions = $this->itemStockTransactionService->getWorkorderItemOutboundTransactionsForInvoiceLog($referenceLineId);
        } elseif ($referenceType === BAS_Shared_Model_Warehouse_ItemStockTransaction::REFERENCE_TYPE_ORDER) {
            $outboundTransactions = $this->itemStockTransactionService->getOrderItemOutboundTransactionsForInvoiceLog($referenceLineId);
        }

        return $outboundTransactions;
    }

    /**
     * @param BAS_Shared_Model_Warehouse_ItemStockTransaction[] $transactions
     * @return float
     */
    private function calcAvgStockPriceByTransactions(array $transactions)
    {
        $avgStockPrice = 0;
        $sumStockAmount = 0;
        $sumQuantity = 0;

        foreach ($transactions as $transaction) {
            $sumStockAmount = BAS_Shared_Utils_Math::add($sumStockAmount, $transaction->getStockAmount(), BAS_Shared_Utils_Math::SCALE_4);
            $sumQuantity = BAS_Shared_Utils_Math::add($sumQuantity, $transaction->getQuantity(), BAS_Shared_Utils_Math::SCALE_2);
        }

        if (abs($sumQuantity) > 0) {
            $avgStockPrice = BAS_Shared_Utils_Math::div($sumStockAmount, $sumQuantity, BAS_Shared_Utils_Math::SCALE_4);
        }

        return abs($avgStockPrice);
    }

}