<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopInvoiceLogFactory
 */
class BAS_Shared_Model_Workshop_OrderWorkshopInvoiceLogFactory
{

    /**
     * @param $invoiceBookingType
     * @return BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceLog|BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceLog
     * @throws BAS_Shared_InvalidArgumentException
     */
    public static function create($invoiceBookingType)
    {
        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL) {
            return new BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceLog();
        } elseif ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL) {
            return new BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceLog();
        } else {
            throw new BAS_Shared_InvalidArgumentException('Invalid invoice booking type value given:' . $invoiceBookingType);
        }
    }
}