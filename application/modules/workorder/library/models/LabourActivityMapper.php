<?php

/**
 * Class BAS_Shared_Model_Workshop_LabourActivityMapper
 */
class BAS_Shared_Model_Workshop_LabourActivityMapper extends BAS_Shared_Model_AbstractMapper
{

    protected $_modelClass = 'BAS_Shared_Model_Workshop_LabourActivity';

    protected $_map = [
        'id' => 'id',
        'depotId' => 'depot_id',
        'departmentId' => 'department_id',
        'name' => 'name',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
        'updatedAt' => 'updated_at',
        'updatedBy' => 'updated_by',
    ];

    public function getGridSource($depotId)
    {
        $dao = $this->getDao();
        $select = $dao->select()
            ->setIntegrityCheck(false)
            ->from(['la' => 'labour_activity'], [])
            ->joinLeft(['d' => 'department'], 'd.id = la.department_id AND d.depot_id = la.depot_id', [])
            ->where('la.depot_id = ?', (int)$depotId)
            ->order('la.id')
            ->columns([
                'activity_id' => 'la.id',
                'department_id' => 'd.id',
                'department_name' => 'd.name',
                'activity_name' => 'la.name',
            ])
        ;

        return $select;
    }

}