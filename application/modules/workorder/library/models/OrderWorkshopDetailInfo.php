<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo
 */
class BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo extends BAS_Shared_Model_Workshop_OrderWorkshopDetail
{
    public $as400Id;
    public $bookingCodeType;
    public $bookingCodeName;
    public $compositionPriceType;
    public $compositionSequence;
    public $compositionQuantity;
    public $description;
    public $discount;
    public $fixedPriceOptional;
    public $labourRateRate;
    public $labourRateCostPrice;
    public $mainDetailType;
    public $mainOrderWorkshopDetailQuantity;
    public $mainProductCompositionPriceType;
    public $workshopTaskType;
    public $workshopTimeType;
    public $subDetailsInfo = [];
    public $itemType;
    public $unit;
    public $itemCategoryName;
    public $itemName;
    public $itemBrandId;
    public $itemAdditionalInfo;
    public $priceTotal;
    public $noStockItem;
    public $stockQuantity;
    public $supplierName;
    public $itemRequiredOptional;

    /**
     * @return string
     */
    public function getAs400Id()
    {
        return $this->as400Id;
    }

    /**
     * @param string
     * @return $this
     */
    public function setAs400Id($value)
    {
        $this->as400Id = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string
     * @return $this
     */
    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param float
     * @return $this
     */
    public function setDiscount($value)
    {
        $this->discount = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getBookingCodeType()
    {
        return $this->bookingCodeType;
    }

    /**
     * @param int
     * @return $this
     */
    public function setBookingCodeType($value)
    {
        $this->bookingCodeType = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getBookingCodeName()
    {
        return $this->bookingCodeName;
    }

    /**
     * @param string
     * @return $this
     */
    public function setBookingCodeName($value)
    {
        $this->bookingCodeName = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabourRateRate()
    {
        return $this->labourRateRate;
    }

    /**
     * @param string
     * @return $this
     */
    public function setLabourRateRate($value)
    {
        $this->labourRateRate = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getLabourRateCostPrice()
    {
        return $this->labourRateCostPrice;
    }

    /**
     * @param float
     * @return $this
     */
    public function setLabourRateCostPrice($value)
    {
        $this->labourRateCostPrice = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getWorkshopTaskType()
    {
        return $this->workshopTaskType;
    }

    /**
     * @param int
     * @return $this
     */
    public function setWorkshopTaskType($value)
    {
        $this->workshopTaskType = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getWorkshopTimeType()
    {
        return $this->workshopTimeType;
    }

    /**
     * @param int
     * @return $this
     */
    public function setWorkshopTimeType($value)
    {
        $this->workshopTimeType = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getMainOrderWorkshopDetailQuantity()
    {
        return $this->mainOrderWorkshopDetailQuantity;
    }

    /**
     * @param int
     * @return $this
     */
    public function setMainOrderWorkshopDetailQuantity($value)
    {
        $this->mainOrderWorkshopDetailQuantity = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getMainProductCompositionPriceType()
    {
        return $this->mainProductCompositionPriceType;
    }

    /**
     * @param int
     * @return $this
     */
    public function setMainProductCompositionPriceType($value)
    {
        $this->mainProductCompositionPriceType = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getFixedPriceOptional()
    {
        return $this->fixedPriceOptional;
    }

    /**
     * @param int
     * @return $this
     */
    public function setFixedPriceOptional($value)
    {
        $this->fixedPriceOptional = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCompositionPriceType()
    {
        return $this->compositionPriceType;
    }

    /**
     * @param int
     * @return $this
     */
    public function setCompositionPriceType($value)
    {
        $this->compositionPriceType = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCompositionSequence()
    {
        return $this->compositionSequence;
    }

    /**
     * @param int
     * @return $this
     */
    public function setCompositionSequence($value)
    {
        $this->compositionSequence = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getItemType()
    {
        return $this->itemType;
    }

    /**
     * @return int
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setUnit($value)
    {
        $this->unit = $value;
        return $this;
    }

    /**
     * @param mixed $itemType
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo
     */
    public function setItemType($itemType)
    {
        $this->itemType = $itemType;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getItemCategoryName()
    {
        return $this->itemCategoryName;
    }

    /**
     * @param mixed $itemCategoryName
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo
     */
    public function setItemCategoryName($itemCategoryName)
    {
        $this->itemCategoryName = $itemCategoryName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getItemName()
    {
        return $this->itemName;
    }

    /**
     * @param mixed $itemName
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo
     */
    public function setItemName($itemName)
    {
        $this->itemName = $itemName;

        return $this;
    }
    
    /**
     * @return int
     */
    public function getItemBrandId()
    {
        return $this->itemBrandId;
    }
    
    /**
     * @param int $value
     * @return $this
     */
    public function setItemBrandId($value)
    {
        $this->itemBrandId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getItemAdditionalInfo()
    {
        return $this->itemAdditionalInfo;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setItemAdditionalInfo($value)
    {
        $this->itemAdditionalInfo = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriceTotal()
    {
        return $this->priceTotal;
    }

    /**
     * @param mixed $priceTotal
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo
     */
    public function setPriceTotal($priceTotal)
    {
        $this->priceTotal = $priceTotal;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNoStockItem()
    {
        return $this->noStockItem;
    }

    /**
     * @param mixed $noStockItem
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo
     */
    public function setNoStockItem($noStockItem)
    {
        $this->noStockItem = $noStockItem;

        return $this;
    }

    /**
     * @return bool
     */
    public function isStockItem()
    {
        return $this->getNoStockItem() === BAS_Shared_Model_Item::NO_STOCK_ITEM_NO;
    }

    /**
     * @return mixed
     */
    public function getStockQuantity()
    {
        return $this->stockQuantity;
    }

    /**
     * @param mixed $stockQuantity
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo
     */
    public function setStockQuantity($stockQuantity)
    {
        $this->stockQuantity = $stockQuantity;

        return $this;
    }

    /**
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getSubDetailsInfo()
    {
        return $this->subDetailsInfo;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[] $value
     * @return $this
     */
    public function setSubDetailsInfo(array $value)
    {
        $this->subDetailsInfo = $value;
        return $this;
    }

    /**
     * @param BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo $detailInfo
     * @return $this
     */
    public function addSubDetailInfo(BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo $detailInfo)
    {
        $this->subDetailsInfo[] = $detailInfo;
        return $this;
    }

    /**
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getSubDetailsInfoSorted()
    {
        $sorted = $this->subDetailsInfo;

        usort($sorted, function(
            BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo $a,
            BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo $b
        ) {
            if ($a->getCompositionSequence() === null) {
                return 1;
            }

            if ($a->getCompositionSequence() === $b->getCompositionSequence()) {
                return 0;
            }

            return ($a->getCompositionSequence() < $b->getCompositionSequence()) ? -1 : 1;
        });

        return $sorted;
    }

    /**
     * @return bool
     */
    public function isGuaranteeDetail()
    {
        return $this->bookingCodeType === BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_GUARANTEE;
    }

    /**
     * @return float
     */
    public function getCompositionQuantity()
    {
        return $this->compositionQuantity;
    }

    /**
     * @param float $compositionQuantity
     */
    public function setCompositionQuantity($compositionQuantity)
    {
        $this->compositionQuantity = $compositionQuantity;
    }

    /**
     * @return int
     */
    public function getMainDetailType()
    {
        return $this->mainDetailType;
    }

    /**
     * @param int $mainDetailType
     * @return $this
     */
    public function setMainDetailType($mainDetailType)
    {
        $this->mainDetailType = $mainDetailType;

        return $this;
    }

    /**
     * @return bool
     */
    public function isComposedSubDetail()
    {
        return
            $this->isSubDetail() &&
            (int)$this->getMainDetailType() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK;
    }

    /**
     * @return bool
     */
    public function isRequiredItemSubDetail()
    {
        return
            $this->isItem() &&
            $this->isSubDetail() &&
            (int)$this->getMainDetailType() === BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM;
    }

    /**
     * @return int
     */
    public function getItemRequiredOptional()
    {
        return $this->itemRequiredOptional;
    }

    /**
     * @param int $itemRequiredOptional
     * @return $this
     */
    public function setItemRequiredOptional($itemRequiredOptional)
    {
        $this->itemRequiredOptional = $itemRequiredOptional;

        return $this;
    }

    /**
     * @return bool
     */
    public function isItemRequiredOptional()
    {
        return
            $this->isRequiredItemSubDetail() &&
            (int)$this->itemRequiredOptional === BAS_Shared_Model_Warehouse_ItemRequired::OPTIONAL_YES;
    }

    /**
     * @return bool
     */
    public function isItemRequiredNotOptional()
    {
        return
            $this->isRequiredItemSubDetail() &&
            (int)$this->itemRequiredOptional === BAS_Shared_Model_Warehouse_ItemRequired::OPTIONAL_NO;
    }

}