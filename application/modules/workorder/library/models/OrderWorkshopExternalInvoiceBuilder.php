<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceBuilder
 */
class BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceBuilder extends BAS_Shared_Model_Workshop_OrderWorkshopInvoiceBuilder
{

    /**
     * @return BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoice
     */
    protected function create()
    {
        return new BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoice();
    }

    /**
     * @inheritdoc
     */
    public function build(
        BAS_Shared_Model_OrderWorkshop $workorder,
        $invoiceType,
        $invoiceFileId,
        $languageCode,
        $invoiceBookingType,
        $depotId,
        $userId
    ) {
        $invoice = parent::build(
            $workorder,
            $invoiceType,
            $invoiceFileId,
            $languageCode,
            $invoiceBookingType,
            $depotId,
            $userId
        );

        $vatAmount = $this->workorderService->calcExternalVatAmountByWorkorder($workorder);
        $consumablesAmount = $workorder->getPriceExternalConsumables();
        $environmentAmount = $workorder->getPriceExternalEnvironment();

        $invoice
            ->setVatAmount($vatAmount)
            ->setPriceConsumables($consumablesAmount)
            ->setPriceEnvironment($environmentAmount);

        return $invoice;
    }
}