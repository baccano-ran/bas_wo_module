<?php

/**
 * Class BAS_Shared_Model_Workshop_ContactTypeSupplierFeeSettingMapper
 */
class BAS_Shared_Model_Workshop_ContactTypeSupplierFeeSettingMapper extends BAS_Shared_Model_AbstractMapper
{
    protected $_modelClass = 'BAS_Shared_Model_Workshop_ContactTypeSupplierFeeSetting';

    protected $_map = [
        'id' => 'id',
        'depotId' => 'depot_id',
        'contactId' => 'contact_id',
        'category' => 'category',
        'valueType' => 'value_type',
        'value' => 'value',
        'archived' => 'archived',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
        'updatedAt' => 'updated_at',
        'updatedBy' => 'updated_by',
    ];
}