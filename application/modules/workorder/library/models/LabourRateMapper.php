<?php

/**
 * Class BAS_Shared_Model_Workshop_LabourRateMapper
 */
class BAS_Shared_Model_Workshop_LabourRateMapper extends BAS_Shared_Model_AbstractMapper
{

    protected $_modelClass = 'BAS_Shared_Model_Workshop_LabourRate';

    protected $_map = [
        'id' => 'id',
        'depotId' => 'depot_id',
        'departmentId' => 'department_id',
        'name' => 'name',
        'rate' => 'rate',
        'endDate' => 'end_date',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
        'updatedAt' => 'updated_at',
        'updatedBy' => 'updated_by',
    ];

    /**
     * @param int $depotId
     * @return Zend_Db_Select
     * @throws Zend_Db_Select_Exception
     */
    public function getGridSource($depotId)
    {
        $select = $this->getDao()
            ->select()
            ->setIntegrityCheck(false)
            ->from(['lr' => 'labour_rate'], [])
            ->joinLeft(['d' => 'department'], 'd.id = lr.department_id AND d.depot_id = lr.depot_id', [])
            ->where('lr.depot_id = ?', (int)$depotId)
            ->order('lr.id')
            ->columns([
                'rate_id' => 'lr.id',
                'department_id' => 'd.id',
                'department_name' => 'd.name',
                'rate_name' => 'lr.name',
                'rate' => 'lr.rate',
                'cost_price' => new Zend_Db_Expr('(' . $this->getCostPriceSelect('lr.id') . ')'),
                'end_date' => 'lr.end_date',
            ])
        ;

        return $select;
    }

    /**
     * @param int|string $labourRateId
     * @return Zend_Db_Select
     */
    public function getCostPriceSelect($labourRateId)
    {
        $labourRateField = is_int($labourRateId) ? '?' : (string)$labourRateId;
        $labourRateValue = is_int($labourRateId) ? (int)$labourRateId : null;

        $costPriceSelect = $this->getDao()
            ->select()
            ->setIntegrityCheck(false)
            ->from(['lc' => 'labour_cost'], ['cost_price'])
            ->where('lc.labour_rate_id = ' . $labourRateField, $labourRateValue)
            ->where('lc.end_date >= CURDATE() OR lc.end_date = "0000-00-00"')
            ->order(new Zend_Db_Expr('(CASE lc.end_date WHEN "0000-00-00" THEN 0 ELSE 1 END) DESC, lc.end_date ASC'))
            ->limit(1);

        return $costPriceSelect;
    }

}