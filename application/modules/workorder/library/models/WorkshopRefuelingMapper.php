<?php

/**
 * Class BAS_Shared_Model_Workshop_WorkshopRefuelingMapper
 */
class BAS_Shared_Model_Workshop_WorkshopRefuelingMapper extends BAS_Shared_Model_AbstractMapper
{
    protected $_modelClass = 'BAS_Shared_Model_Workshop_WorkshopRefueling';

    protected $_map = [
        'id' => 'id',
        'depotId' => 'depot_id',
        'receiptNumber' => 'receipt_number',
        'refuelingTimestamp' => 'refueling_timestamp',
        'quantity' => 'quantity',
        'price' => 'price',
        'amount' => 'amount',
        'cardNumber' => 'card_number',
        'username' => 'username',
        'manualNumber' => 'manual_number',
        'vehicleLicensePlate' => 'vehicle_license_plate',
        'mileage' => 'mileage',
        'status' => 'status',
        'xmlName' => 'xml_name',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
        'updatedAt' => 'updated_at',
        'updatedBy' => 'updated_by',
    ];

    /**
     * @param int $depotId
     * @param int $status
     * @return Zend_Db_Select
     */
    public function getGridSelectByDepotAndStatus($depotId, $status)
    {
        $dao = $this->getDao();
        $tableName = $this->getTableName();

        $selectDate = $dao
            ->select()
            ->from(['wr' => $tableName], [])
            ->columns([
                'id',
                '_date_time' => new Zend_Db_Expr("DATE_FORMAT(refueling_timestamp, '%d-%m-%Y %H:%i')"),
            ])
        ;

        return $dao
            ->select()
            ->setIntegrityCheck(false)
            ->from(['wr' => $tableName], [])
            ->joinLeft(['_wr' => new Zend_Db_Expr('(' . $selectDate . ')')], 'wr.id = _wr.id', [])
            ->where('depot_id = ?', (int) $depotId)
            ->where('status = ?', (int) $status)
            ->columns([
                'id',
                '_date_time' => '_wr._date_time',
                'quantity',
                'amount',
                'card_number',
                'manual_number',
                'vehicle_license_plate',
                'mileage',
                'username',
            ])
        ;
    }


    /**
     * @param string $xmlFileName
     * @return bool
     */
    public function checkXmlProcessed($xmlFileName)
    {
        $recordSelectQuery = $this->getDao()->select()->setIntegrityCheck(false);
        $recordSelectQuery
            ->from($this->getTableName(), [])
            ->where('xml_name = ?', $xmlFileName)
            ->columns([
                new Zend_Db_Expr('1')
            ]);

        $select = $this->getDao()->select()
                ->where('EXISTS(?)', new Zend_Db_Expr($recordSelectQuery));

        $stmt = $select->query();

        return (boolean)$stmt->fetchColumn();
    }

}