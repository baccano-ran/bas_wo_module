<?php

/**
 * Class BAS_Shared_Model_Workshop_AddDetailRequest
 */
class BAS_Shared_Model_Workshop_AddDetailRequest extends BAS_Shared_Model_AbstractModel
{
    public $workorderId;
    public $referenceId;
    public $referenceType;
    public $detailDepotId;
    public $tradeIn;
    public $quantity;
    public $mainDetail;
    public $tradeInPrices;
    public $refuelingId;

    /**
     * @return mixed
     */
    public function getWorkorderId()
    {
        return $this->workorderId;
    }

    /**
     * @return mixed
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @return mixed
     */
    public function getReferenceType()
    {
        return $this->referenceType;
    }

    /**
     * @return mixed
     */
    public function getDetailDepotId()
    {
        return $this->detailDepotId;
    }

    /**
     * @return mixed
     */
    public function getTradeIn()
    {
        return $this->tradeIn;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetail
     */
    public function getMainDetail()
    {
        return $this->mainDetail;
    }

    /**
     * @param int $itemId
     * @param int $depotId
     * @return bool|float
     */
    public function getTradeInPrice($itemId, $depotId)
    {
        foreach ($this->tradeInPrices as $tradeInPrice) {
            if ((int)$tradeInPrice['itemId'] === (int)$itemId &&
                (int)$tradeInPrice['depotId'] === (int)$depotId
            ) {
                return (float)$tradeInPrice['tradeInPrice'];
            }
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function getRefuelingId()
    {
        return $this->refuelingId;
    }

    /**
     * @return mixed
     */
    public function getTradeInPrices()
    {
        return $this->tradeInPrices;
    }

}
