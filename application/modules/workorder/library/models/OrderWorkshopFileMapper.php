<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopFileMapper
 */
class BAS_Shared_Model_Workshop_OrderWorkshopFileMapper extends BAS_Shared_Model_AbstractMapper
{
    protected $_map = [
        'id'          => 'id',
        'workorderId' => 'workorder_id',
        'type'        => 'type',
        'fileName'    => 'file_name',
        'title'       => 'title',
        'token'       => 'token',
        'createdBy'   => 'created_by',
        'createdAt'   => 'created_at',
        'isRead'      => 'is_read',
        'archived'    => 'archived',
    ];

    protected $_modelClass = 'BAS_Shared_Model_Workshop_OrderWorkshopFile';

    /**
     * @param int $orderWorkshopId
     * @param int $depotId
     * @return Zend_Db_Select
     */
    public function getGridSelect($orderWorkshopId, $depotId)
    {
        $dao = $this->getDao();
        $select = $dao->select()
            ->setIntegrityCheck(false)
            ->from(['owf' => 'order_workshop_file'], [
                'id', 'created_by', 'document_type' => 'type', 'token', 'reference_id'  => 'owf.workorder_id',
                'order_id' => 'workorder_id', 'file_name', 'title', 'created_at',
            ])
            ->joinLeft(['p_created' => 'person'], 'p_created.id = owf.created_by', ['p_created.name AS created_by_name'])
            ->joinLeft(['p_created_depot' => 'person_depot'], 'p_created_depot.person_id = owf.created_by', ['p_created_depot.abbreviation AS created_by_abbr'])
            ->where('owf.workorder_id = ?', (int)$orderWorkshopId)
            ->where('p_created_depot.depot_id = ?', (int)$depotId)
            ->order('owf.created_at DESC');

        return $select;
    }

    /**
     * @param int $orderWorkshopId
     * @param int $depotId
     * @return array
     */
    public function getCreatedByOptionsByOrderWorkshopId($orderWorkshopId, $depotId)
    {
        $dao = $this->getDao();
        $select = $dao->select()
            ->setIntegrityCheck(false)
            ->from(['owf' => 'order_workshop_file'], [
                'id' => 'created_by',
            ])
            ->joinLeft(['p_created' => 'person'], 'p_created.id = owf.created_by', ['p_created.name AS name'])
            ->joinLeft(['p_created_depot' => 'person_depot'], 'p_created_depot.person_id = owf.created_by', ['p_created_depot.abbreviation AS created_by_abbr'])
            ->where('owf.workorder_id = ?', (int)$orderWorkshopId)
            ->where('p_created_depot.depot_id = ?', (int)$depotId)
            ->group('p_created.name')
            ->order('p_created.name');

        $this->setMapTo(static::MAP_TO_ARRAY);

        return $this->findAllByCondition($select);
    }

    /**
     * @param int $orderWorkshopId
     * @return array
     */
    public function getFileTypeOptionsByOrderWorkshopId($orderWorkshopId)
    {
        $dao = $this->getDao();
        $select = $dao->select()
            ->from(['owf' => 'order_workshop_file'], [
                'id', 'type',
            ])
            ->where('owf.workorder_id = ?', (int)$orderWorkshopId)
            ->group('owf.type')
            ->order('owf.type');
        $this->setMapTo(static::MAP_TO_ARRAY);

        return $this->findAllByCondition($select);
    }

    /**
     * @param int $workorderId
     * @param int $depotId
     * @param bool $onMaster
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     * @throws BAS_Shared_NotFoundException
     */
    public function findByWorkorderIdAndDepot($workorderId, $depotId, $onMaster = false)
    {
        $dao = $this->getDao();
        $select = $dao->select()
            ->setIntegrityCheck(false)
            ->from(['owf' => 'order_workshop_file'])
            ->join('person_depot AS pd', 'owf.created_by = pd.person_id',
                ['pd.abbreviation AS creator'])
            ->where('owf.workorder_id = ?', (int)$workorderId)
            ->where('p_created_depot.depot_id = ?', (int)$depotId)
            ->order('owf.created_at desc');

        $select = $select->assemble();
        if ($onMaster === true) {
            $select = self::SWITCH_MASTER_HINT . $select;
        }

        $row = $dao->getAdapter()->fetchRow($select);
        if ($row === false) {
            throw new BAS_Shared_NotFoundException(sprintf('Workorder with id %d not found in %d depot', $workorderId, $depotId));
        }

        return $this->mapToModel($row);
    }

}
