<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceLogMapper
 */
class BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceLogMapper extends BAS_Shared_Model_AbstractMapper
{
    protected $_modelClass = 'BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceLog';

    protected $_map = [
        'id' => 'id',
        'invoiceId' => 'invoice_id',
        'orderWorkshopId' => 'order_workshop_id',
        'orderWorkshopDetailId' => 'order_workshop_detail_id',
        'type' => 'type',
        'referenceId' => 'reference_id',
        'depotBookingCodeId' => 'depot_booking_code_id',
        'depotAccountGroupId' => 'depot_account_group_id',
        'quantity' => 'quantity',
        'price' => 'price',
        'amount' => 'amount',
        'stockPrice' => 'stock_price',
        'stockAmount' => 'stock_amount',
        'discountAmount' => 'discount_amount',
    ];
    
    /**
     * this function is used to get the journal entry csv lines for products and items.
     * 
     * @param array $depotIds
     * @return array|null
     */
    public function getWorkorderExternalInvoiceLogCsvRecords($depotIds) 
    {

        if (array() === $depotIds) {
            return array();
        }

        $dao = $this->getDao();
        $select = $dao->select()->setIntegrityCheck(false);

        $select->from(['oweil' => 'order_workshop_external_invoice_log'], [])
                ->joinInner(['owei' => 'order_workshop_external_invoice'], 'owei.order_workshop_id=oweil.order_workshop_id and owei.invoice_id=oweil.invoice_id', [
                    'invoiceId' => 'owei.invoice_id',
                    'orderId' => 'owei.order_workshop_id',
                    'depotId' => 'owei.depot_id',
                    'type' => 'oweil.type',
                    'subtotalSale' => new Zend_Db_Expr('SUM(oweil.amount)'),
                    'subtotalSaleInOtherCurrency' => new Zend_Db_Expr('SUM(oweil.amount)*owei.exchange_rate'),
                    'subtotalCostprice' => new Zend_Db_Expr('SUM(oweil.stock_amount)'),
                    'subtotalCostpriceInOtherCurrency' => new Zend_Db_Expr('SUM(oweil.stock_amount)*owei.exchange_rate'),
                    'subtotalDiscount' => new Zend_Db_Expr('SUM(oweil.discount_amount)'),
                    'subtotalDiscountInOtherCurrency' => new Zend_Db_Expr('SUM(oweil.discount_amount)*owei.exchange_rate'),
                    'depotAccountGroupId' => 'oweil.depot_account_group_id',
                    'depotBookingCodeId' => 'oweil.depot_booking_code_id'
                        ]
                )
                ->joinInner(['owd' => 'order_workshop_detail'], 'owd.id=oweil.order_workshop_detail_id', [
                    'tradeInItem' => 'owd.trade_in'
                ])
                ->joinLeft(['id' => 'item_depot'], 'oweil.type = ' . BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceLog::TYPE_ITEM
                        . ' AND oweil.reference_id=id.item_id '
                        . ' AND id.depot_id=owd.depot_id ', [])
                ->joinLeft(['i' => 'item'], 'i.id = id.item_id', [
                    'noStockItem' => new Zend_Db_Expr('IFNULL(i.no_stock_item,0)')
                        ]
                )
                ->joinLeft(['ir' => 'item_receipt'], ' ir.return_order_id = owd.order_workshop_id' , [
                    'itemReceiptId' => new Zend_Db_Expr('if(i.no_stock_item = '. BAS_Shared_Model_Item::NO_STOCK_ITEM_NO .' AND ' .
                        'owd.trade_in = '. BAS_Shared_Model_Warehouse_OrderItem::TRADE_IN_YES .', ir.id, null)')
                ])
                ->joinLeft(['dbc' => 'depot_booking_code'], 'dbc.id = oweil.depot_booking_code_id', [
                    'depotBookingCodeType' => 'dbc.type'
                    ]
                )
                ->where('owei.depot_id in (?)', $depotIds)
                ->where('invoice_booked = ? ', BAS_Shared_Model_Warehouse_OrderItemInvoice::INVOICE_NOT_BOOKED)
                ->group('invoiceId')
                ->group('orderId')
                ->group('depotId')
                ->group('oweil.type')
                ->group('noStockItem')
                ->group('tradeInItem')
                ->group('depotBookingCodeId')
                ->group('depotAccountGroupId');

        $result = $dao->fetchAll($select);

        if (!empty($result)) {
            return $result->toArray();
        }

        return null;
    }

}