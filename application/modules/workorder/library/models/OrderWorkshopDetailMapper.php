<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper
 */
class BAS_Shared_Model_Workshop_OrderWorkshopDetailMapper extends BAS_Shared_Model_AbstractMapper
{

    protected $_modelClass = 'BAS_Shared_Model_Workshop_OrderWorkshopDetail';

    protected $_map = [
        'id' => 'id',
        'orderWorkshopId' => 'order_workshop_id',
        'mainOrderWorkshopDetailId' => 'main_order_workshop_detail_id',
        'type' => 'type',
        'referenceId' => 'reference_id',
        'depotId' => 'depot_id',
        'onInvoice' => 'on_invoice',
        'quantity' => 'quantity',
        'priceOrder' => 'price_order',
        'priceOriginal' => 'price_original',
        'costPrice' => 'cost_price',
        'supplierId' => 'supplier_id',
        'tradeIn' => 'trade_in',
        'labourRateId' => 'labour_rate_id',
        'plannedTime' => 'planned_time',
        'spentTime' => 'spent_time',
        'invoiceTime' => 'invoice_time',
        'depotBookingCodeId' => 'depot_booking_code_id',
        'depotAccountGroupId' => 'depot_account_group_id',
        'invoiceDescription' => 'invoice_description',
        'itemPicked' => 'item_picked',
        'sequence' => 'sequence',
        'imageCount' => 'image_count',
        'archived' => 'archived',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
        'updatedAt' => 'updated_at',
        'updatedBy' => 'updated_by',
    ];

    /**
     * @param int $workorderId
     * @return float
     */
    public function getWorkorderInvoicedTime($workorderId)
    {
        $select = $this->getDao()->select()
            ->setIntegrityCheck(false)
            ->from(['owd' => 'order_workshop_detail'], [])
            ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
            ->columns([
                new Zend_Db_Expr('SUM(owd.invoice_time * owd.quantity)')
            ]);

        $stmt = $select->query();

        return (float)$stmt->fetchColumn();
    }

    /**
     * @param int $workorderId
     * @return float
     */
    public function getWorkorderSpentTime($workorderId)
    {
        $select = $this->getDao()->select()
            ->setIntegrityCheck(false)
            ->from(['owd' => 'order_workshop_detail'], [])
            ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
            ->columns([
                new Zend_Db_Expr('SUM(owd.spent_time)')
            ]);

        $stmt = $select->query();

        return (float)$stmt->fetchColumn();
    }

    /**
     * @param int $workorderId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getWorkorderItemsToPick($workorderId)
    {
        $detailsInfoSelect = $this->getWorkorderDetailsInfoSelect();
        $detailsInfoSelect
            ->where('owd.type = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM)
            ->where('owd.item_picked = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::ITEM_PICKED_TO_BE_PICKED)
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
            ->group('owd.id')
            ->order('owd.sequence')
        ;

        $data = $this
            ->setMapTo(self::MAP_TO_ARRAY)
            ->findAllByCondition($detailsInfoSelect);

        $detailsInfo = [];

        foreach ($data as $rowData) {
            $detailsInfo[] = new BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo($rowData);
        }

        return $detailsInfo;
    }

    /**
     * @param int $workorderId
     * @param int[] $bookingCodeTypes
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getInvoiceDetailInfoList($workorderId, array $bookingCodeTypes)
    {
        $detailsInfoSelect = $this->getWorkorderDetailsInfoSelect();
        $detailsInfoSelect
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
            ->where('owd.on_invoice = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::ON_INVOICE_YES)
            ->where('dbc.type IN(?) OR mdbc.type IN(?)', $bookingCodeTypes)
            ->order('owd.sequence')
        ;

        $data = $this
            ->setMapTo(self::MAP_TO_ARRAY)
            ->findAllByCondition($detailsInfoSelect);

        $detailsInfo = [];

        foreach ($data as $rowData) {
            $detailsInfo[$rowData['id']] = new BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo($rowData);
        }

        return $detailsInfo;
    }

    /**
     * @param int $workorderId
     * @param int[] $bookingCodeTypes
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     */
    public function getInvoiceLogDetailInfoList($workorderId, array $bookingCodeTypes)
    {
        $detailsInfoSelect = $this->getWorkorderDetailsInfoSelect();
        $detailsInfoSelect
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
            ->where('dbc.type IN(?) OR mdbc.type IN(?)', $bookingCodeTypes)
            ->order('owd.sequence')
        ;

        $data = $this
            ->setMapTo(self::MAP_TO_ARRAY)
            ->findAllByCondition($detailsInfoSelect);

        $detailsInfo = [];

        foreach ($data as $rowData) {
            $detailsInfo[$rowData['id']] = new BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo($rowData);
        }

        return $detailsInfo;
    }

    /**
     * @param int $workorderId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo[]
     * @throws Zend_Db_Select_Exception
     */
    public function getDetailsInfoByWorkorderId($workorderId)
    {
        $detailsInfoSelect = $this->getWorkorderDetailsInfoSelect();
        $detailsInfoSelect
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
            ->order('owd.sequence')
        ;

        $data = $this
            ->setMapTo(self::MAP_TO_ARRAY)
            ->findAllByCondition($detailsInfoSelect);

        $detailsInfo = [];

        foreach ($data as $rowData) {
            $detailsInfo[$rowData['id']] = new BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo($rowData);
        }

        return $detailsInfo;
    }

    /**
     * @param int $workorderId
     * @param int $detailId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo
     * @throws BAS_Shared_NotFoundException
     */
    public function getDetailInfoByWorkorderIdAndDetailId($workorderId, $detailId)
    {
        $detailsInfoSelect = $this->getWorkorderDetailsInfoSelect();
        $detailsInfoSelect
            ->where('owd.id = ?', (int)$detailId)
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
        ;

        $rowData = $this
            ->setMapTo(self::MAP_TO_ARRAY)
            ->findOneByCondition($detailsInfoSelect);

        return new BAS_Shared_Model_Workshop_OrderWorkshopDetailInfo($rowData);
    }

    /**
     * @return Zend_Db_Select
     * @throws Zend_Db_Select_Exception
     */
    public function getWorkorderDetailsInfoSelect()
    {
        /** @var BAS_Shared_Model_Workshop_LabourRateMapper $labourRateMapper */
        $labourRateMapper = BAS_Shared_Model_MapperRegistry::get('Workshop_LabourRate');

        $select = $this->getDao()
            ->select()
            ->setIntegrityCheck(false)
            ->from(['owd' => 'order_workshop_detail'], [])
            ->join(['ow' => 'order_workshop'],'ow.workorder_id = owd.order_workshop_id', [])
            ->join(['dbc' => 'depot_booking_code'], 'dbc.id = owd.depot_booking_code_id', [])
            ->joinLeft(['mowd' => 'order_workshop_detail'], 'mowd.id = owd.main_order_workshop_detail_id', [])
            ->joinLeft(['mdbc' => 'depot_booking_code'], 'mdbc.id = mowd.depot_booking_code_id', [])

            ->joinLeft(['mp' => 'product'], 'mp.id = mowd.reference_id AND mowd.type = ' . BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK, [])
            ->joinLeft(['lr' => 'labour_rate'], 'lr.id = owd.labour_rate_id', [])
            ->joinLeft(['p' => 'product'], 'p.id = owd.reference_id AND p.depot_id = owd.depot_id AND owd.type = ' . BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK, [])
            ->joinLeft(['pc' => 'product_composition'], 'pc.product_id = mp.id AND pc.reference_id = owd.reference_id AND pc.archived = 0', [])

            ->joinLeft(['mi' => 'item'], 'mi.id = mowd.reference_id AND mowd.type = ' . BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM, [])
            ->joinLeft(['i' => 'item'], 'i.id = owd.reference_id AND owd.type = ' . BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM, [])
            ->joinLeft(['in' => 'item_name'], 'in.id = i.item_name_id', [])
            ->joinLeft(['ic' => 'item_category'], 'ic.id = i.item_category_id AND ic.depot_id = owd.depot_id', [])
            ->joinLeft(['ir' => 'item_required'], 'ir.item_id = mi.id AND ir.required_item_id = i.id', [])

            ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->columns([
                'id' => 'owd.id',
                'orderWorkshopId' => 'owd.order_workshop_id',
                'mainDetailType' => 'mowd.type',
                'mainOrderWorkshopDetailId' => 'mowd.id',
                'mainOrderWorkshopDetailQuantity' => 'mowd.quantity',
                'mainProductCompositionPriceType' => 'mp.composition_price_type',
                'type' => 'owd.type',
                'referenceId' => 'owd.reference_id',
                'depotId' => 'owd.depot_id',
                'as400Id' => new Zend_Db_Expr(
                    'CASE owd.type ' .
                    'WHEN '.BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK.' THEN p.as400_id ' .
                    'WHEN '.BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM.' THEN i.as400_id ' .
                    'END'
                ),
                'description' => new Zend_Db_Expr(
                    'CASE owd.type ' .
                    'WHEN '.BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK.' THEN p.description ' .
                    'WHEN '.BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM.' THEN `in`.name ' .
                    'END'
                ),
                'invoiceDescription' => 'owd.invoice_description',
                'depotAccountGroupId' => 'owd.depot_account_group_id',
                'supplierId' => 'owd.supplier_id',
                'onInvoice' => 'owd.on_invoice',
                'quantity' => 'owd.quantity',
                'priceOrder' => 'owd.price_order',
                'priceOriginal' => 'owd.price_original',
                'costPrice' => 'owd.cost_price',
                'tradeIn' => 'owd.trade_in',
                'bookingCodeType' => 'dbc.type',
                'depotBookingCodeId' => 'dbc.id',
                'labourRateId' => 'lr.id',
                'labourRateRate' => 'lr.rate',
                'labourRateCostPrice' => new Zend_Db_Expr('(' . $labourRateMapper->getCostPriceSelect('lr.id') . ')'),
                'workshopTaskType' => 'p.workshop_task_type',
                'workshopTimeType' => 'p.workshop_time_type',
                'compositionPriceType' => 'p.composition_price_type',
                'fixedPriceOptional' => 'p.fixed_price_optional',
                'invoiceTime' => new Zend_Db_Expr('COALESCE(owd.invoice_time, 0)'),
                'spentTime' => new Zend_Db_Expr('COALESCE(owd.spent_time, 0)'),
                'sequence' => 'owd.sequence',
                'imageCount' => 'owd.image_count',
                'compositionSequence' => 'pc.sequence',
                'compositionQuantity' => 'pc.quantity',
                'itemPicked' => 'owd.item_picked',
                'itemType' => 'i.type',
                'unit' => 'i.unit',
                'noStockItem' => 'i.no_stock_item',
                'itemBrandId' => 'i.brand_id',
                'itemAdditionalInfo' => 'i.additional_info',
                'itemCategoryName' => 'ic.name',
                'itemName' => 'in.name',
                'itemRequiredOptional' => 'ir.optional',
            ]);

        return $select;
    }

    /**
     * @param int $workorderId
     * @return array
     * @throws Zend_Db_Select_Exception
     */
    public function getDetailsPostingSetups($workorderId)
    {
        $select = $this->getDao()
            ->select()
            ->from($this->getTableName(), [])
            ->where('order_workshop_id = ?', $workorderId)
            ->where('archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->group(['depot_booking_code_id', 'depot_account_group_id'])
            ->columns([
                'depotBookingCodeId' => 'depot_booking_code_id',
                'depotAccountGroupId' => 'depot_account_group_id',
            ])
        ;

        return $this
            ->setMapTo(self::MAP_TO_ARRAY)
            ->findAllByCondition($select);
    }

    /**
     * @param int $workorderId
     * @return bool
     */
    public function workorderContainsNoStockItemsWithEmptyCostPrice($workorderId)
    {
        $select = $this->getDao()
            ->select()
            ->from(['owd' => $this->getTableName()])
            ->join(['i' => 'item'], 'i.id = owd.reference_id', [])
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
            ->where('owd.type = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM)
            ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->where('owd.cost_price IS NULL')
            ->where('i.no_stock_item = ?', BAS_Shared_Model_Item::NO_STOCK_ITEM_YES)
        ;

        return $this->getDao()->fetchRow($select) !== null;
    }

    /**
     * @param int $workorderId
     * @return bool
     */
    public function workorderContainsExternalTasksWithEmptyCostPrice($workorderId)
    {
        $select = $this->getDao()
            ->select()
            ->from(['owd' => $this->getTableName()])
            ->join(['p' => 'product'], 'p.id = owd.reference_id AND p.depot_id = owd.depot_id', [])
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
            ->where('owd.type = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK)
            ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->where('owd.cost_price IS NULL')
            ->where('p.workshop_task_type = ?', BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL)
        ;

        return $this->getDao()->fetchRow($select) !== null;
    }

    /**
     * @param int $workorderId
     * @return bool
     */
    public function workorderContainsNoStockItemsWithEmptySupplier($workorderId)
    {
        $select = $this->getDao()
            ->select()
            ->from(['owd' => $this->getTableName()])
            ->join(['i' => 'item'], 'i.id = owd.reference_id', [])
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
            ->where('owd.type = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM)
            ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->where('owd.depot_account_group_id = ?', BAS_Shared_Model_DepotAccountGroup::ACCOUNT_GROUP_VARIOUS_PARTS)
            ->where('owd.supplier_id IS NULL')
            ->where('i.no_stock_item = ?', BAS_Shared_Model_Item::NO_STOCK_ITEM_YES)
        ;

        return $this->getDao()->fetchRow($select) !== null;
    }

    /**
     * @param int $workorderId
     * @return bool
     */
    public function workorderContainsExternalTasksWithEmptySupplier($workorderId)
    {
        $select = $this->getDao()
            ->select()
            ->from(['owd' => $this->getTableName()])
            ->join(['p' => 'product'], 'p.id = owd.reference_id AND p.depot_id = owd.depot_id', [])
            ->where('owd.order_workshop_id = ?', (int)$workorderId)
            ->where('owd.type = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK)
            ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->where('owd.supplier_id IS NULL')
            ->where('p.workshop_task_type = ?', BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_EXTERNAL)
        ;

        return $this->getDao()->fetchRow($select) !== null;
    }

    /**
     * @param int $workorderId
     * @return Zend_Db_Statement_Interface
     * @throws Zend_Db_Select_Exception
     */
    public function updateActualPriceOfWorkorderComposedTasks($workorderId)
    {
        $actualPriceSelect = $this->getActualPricesSelect();
        $actualPriceSelect->where('owd.order_workshop_id = ?', (int)$workorderId);

        $this->getDao()->getAdapter()->query(
            $this->getUpdateActualPriceSql($actualPriceSelect) .
            'WHERE owd.order_workshop_id = ' . (int)$workorderId
        );
    }

    /**
     * @param int $detailId
     * @return Zend_Db_Statement_Interface
     * @throws Zend_Db_Select_Exception
     */
    public function updateActualPriceOfComposedTask($detailId)
    {
        $actualPriceSelect = $this->getActualPricesSelect();
        $actualPriceSelect->where('owd.id = ?', (int)$detailId);

        $this->getDao()->getAdapter()->query(
            $this->getUpdateActualPriceSql($actualPriceSelect) .
            'WHERE owd.id = ' . (int)$detailId
        );
    }

    /**
     * @param Zend_Db_Select $actualPriceSelect
     * @return string
     */
    private function getUpdateActualPriceSql(Zend_Db_Select $actualPriceSelect)
    {
        return
            'UPDATE ' . $this->getTableName() . ' owd ' .
            'JOIN (' . $actualPriceSelect . ') actualPrice ON actualPrice.detailId = owd.id ' .
            'SET owd.price_order = actualPrice.price, ' .
            'owd.price_original = actualPrice.price ';
    }

    /**
     * @return Zend_Db_Select
     * @throws Zend_Db_Select_Exception
     */
    private function getActualPricesSelect()
    {
        return $this->getDao()
            ->select()
            ->setIntegrityCheck(false)
            ->from(['owd' => $this->getTableName()], [])
            ->join(['p' => 'product'], 'p.id=owd.reference_id', [])
            ->join(['sub_owd' => $this->getTableName()], 'sub_owd.main_order_workshop_detail_id = owd.id', [])
            ->where('owd.main_order_workshop_detail_id IS NULL')
            ->where('owd.type = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK)
            ->where('p.workshop_task_type = ?', BAS_Shared_Model_Product::WORKSHOP_TASK_TYPE_COMPOSED)
            ->where('p.composition_price_type = ?', BAS_Shared_Model_Product::COMPOSITION_PRICE_TYPE_COMPOSED)
            ->group('owd.id')
            ->columns([
                'detailId' => 'owd.id',
                'price' => new Zend_Db_Expr('SUM(sub_owd.quantity * sub_owd.price_order) / owd.quantity'),
            ]);
    }
    
    /**
     * to get the work-in-progress (WIP) for workorder items
     * @param $depotIds
     * @return array|null
     */
    public function getItemWorkInProgressJournalCsvData($depotIds) 
    {
        if (array() === $depotIds) {
            return array();
        }

        $dao = $this->getDao();
        $select = $dao->select()->setIntegrityCheck(false);

        $select->from(['owd' => 'order_workshop_detail'], [])
                ->joinInner(['ow' => 'order_workshop'], 'ow.workorder_id = owd.order_workshop_id', [])
                ->joinInner(['id' => 'item_depot'], 'id.item_id = owd.reference_id AND id.depot_id = owd.depot_id', [])
                ->joinLeft(['ist' => 'item_stock_transaction'], 'owd.id=ist.reference_line_id AND owd.order_workshop_id = ist.reference_id', [])
                ->joinLeft(['owii' => 'order_workshop_internal_invoice'], 'owii.order_workshop_id = owd.order_workshop_id', [])
                ->joinLeft(['owei' => 'order_workshop_external_invoice'], 'owei.order_workshop_id = owd.order_workshop_id', [])
                ->columns([
                    'module' => new Zend_Db_Expr(BAS_Shared_Model_DepotPostingSetup::MODULE_WORKSHOP),
                    'bookingCode' => 'owd.depot_booking_code_id',
                    'accountGroup' => 'id.depot_account_group_id',
                    'turnover' => new Zend_Db_Expr('SUM(ROUND(COALESCE(ist.amount-ist.discount_amount,owd.quantity*owd.price_original),2))'),
                    'discount' => new Zend_Db_Expr('SUM(ROUND(COALESCE(ist.discount_amount,owd.quantity*(owd.price_original-owd.price_order)),2))'),
                    'costprice' => new Zend_Db_Expr('SUM(ROUND(COALESCE(ist.stock_amount,owd.quantity*COALESCE(-owd.cost_price,0)),2))')
                        ]
                )
                ->where('(ist.journal_booked_at is null AND owii.invoice_id is null AND owei.invoice_id is null)')
                ->where('owd.archived = ? ' , BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
                ->where('owd.type = ?' , BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_ITEM)
                ->where('ow.status IS NOT NULL')
                ->where('ow.status != ?' , BAS_Shared_Model_OrderWorkshop::WORKORDER_ARCHIVED)
                ->where('owd.quantity <> 0')
                ->where('owd.depot_id IN (?)' , $depotIds)
                ->group('bookingCode')
                ->group('accountGroup')
        ;

        $result = $dao->fetchAll($select);

        if ($result->count()) {
            return $result->toArray();
        }
        
        return [];
    }
    
    /**
     * to get the work-in-progress (WIP) for workorder hours
     * @param $depotIds
     * @return array|null
     */
    public function getHourWorkInProgressJournalCsvData($depotIds) 
    {
        if (array() === $depotIds) {
            return array();
        }

        $dao = $this->getDao();
        //subselect2 gets the time records 
        $subselect2 = $dao->select()->setIntegrityCheck(false);
        $subselect2->from(['owt' => 'order_workshop_time'], [
            'orderWorkshopId' => 'order_workshop_id',
            'spent_time' => new Zend_Db_Expr('SUM(COALESCE(spent_time, 0))'),
        ])
                ->where(new Zend_Db_Expr('YEARWEEK(date) < YEARWEEK(NOW())'))
                ->group('order_workshop_id')
        ;
        //subselects gets the data row wise
        $subSelect = $dao->select()->setIntegrityCheck(false);
        $subSelect->from(['owd' => 'order_workshop_detail'], [])
                ->joinInner(['ow' => 'order_workshop'], 'ow.workorder_id = owd.order_workshop_id', [])
                ->joinLeft(['owii' => 'order_workshop_internal_invoice'], 'owii.order_workshop_id = owd.order_workshop_id ', [])
                ->joinLeft(['owei' => 'order_workshop_external_invoice'], 'owei.order_workshop_id = owd.order_workshop_id ', [])
                ->joinInner(['lr' => 'labour_rate'], 'owd.labour_rate_id=lr.id', [])
                ->joinInner(['owt' => $subselect2], 'owt.orderWorkshopId=owd.order_workshop_id', [])
                ->columns([
                    'bookingCode' => 'owd.depot_booking_code_id',
                    'accountGroup' => 'owd.depot_account_group_id',
                    'amount' => new Zend_Db_Expr('ROUND((SUM(COALESCE(owt.spent_time,0)*lr.rate)/COUNT(DISTINCT(owd.id))),2)'),
                        ]
                )
                ->where(new Zend_Db_Expr('((owii.order_workshop_id is null AND owei.order_workshop_id is null) OR (YEARWEEK(owii.invoice_date) > YEARWEEK(NOW())-1 OR YEARWEEK(owei.invoice_date) > YEARWEEK(NOW())-1))'))
                ->where('owd.type = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::TYPE_TASK)
                ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
                ->where('ow.archived = ?' , BAS_Shared_Model_OrderWorkshop::NOT_ARCHIVED)
                ->where('ow.type <> ?', BAS_Shared_Model_OrderWorkshop::TYPE_INTERNAL_FIXED)
                ->where('owd.depot_account_group_id = ?', BAS_Shared_Model_DepotAccountGroup::ACCOUNT_GROUP_HOURS)
                ->where('owd.depot_id IN (?)' , $depotIds)
                ->group('owd.depot_booking_code_id')
                ->group('owd.order_workshop_id')
        ;

        //main select gets the data summed up.
        $select = $dao->select()->setIntegrityCheck(false);
        $select->from(['x' => $subSelect], [])
                ->columns([
                    'module' => new Zend_Db_Expr(BAS_Shared_Model_DepotPostingSetup::MODULE_WORKSHOP),
                    'bookingCode',
                    'accountGroup',
                    'turnover' => new Zend_Db_Expr('SUM(amount)'),
                    'discount' => new Zend_Db_Expr(0),
                    'costprice' => new Zend_Db_Expr(0)
                        ]
                )
                ->group('bookingCode')
                ->group('accountGroup')
        ;

        $result = $dao->fetchAll($select);

        if ($result->count()) {
            return $result->toArray();
        }
        
        return [];
    }
    
    /**
     * Get total invoice price select query
     * 
     * @return Zend_Db_Select
     */
    public function getTotalInvoicePriceSelect()
    {
        $dao = $this->getDao();
        $select = $dao->select()->setIntegrityCheck(false);
        $select->from(['owd' => 'order_workshop_detail'], 
                ['totalInvoicePrice' => BAS_Shared_Formula_WorkshopOrder::getSalesPriceSql()]
            )
            ->joinInner(['ow' => 'order_workshop'],
                'ow.workorder_id = owd.order_workshop_id',
                ['orderId' => 'ow.workorder_id'])
             ->joinInner(['d' => 'depot'],
                'ow.depot_id = d.id',
                [])
            ->joinInner(['lvr' => 'list_vat_rate'],
                'lvr.country_id = d.country_code and lvr.rate_type = ' . BAS_Shared_Model_ListVatRate::RATE_TYPE_HIGH . ' AND lvr.start_date <= COALESCE(ow.status_order_date, ow.created_at)',
                [])
            ->joinLeft(['lvr2' => 'list_vat_rate'],
                'lvr2.country_id = lvr.country_id and lvr2.rate_type = lvr.rate_type and lvr.start_date < lvr2.start_date',
                [])
            ->joinInner(['lvrl' => 'list_vat_rate'],
                'lvrl.country_id = d.country_code and lvrl.rate_type = ' . BAS_Shared_Model_ListVatRate::RATE_TYPE_LOW . ' AND lvrl.start_date <= COALESCE(ow.status_order_date, ow.created_at)',
                [])
            ->joinLeft(['lvrl2' => 'list_vat_rate'],
                'lvrl2.country_id = lvrl.country_id and lvrl2.rate_type = lvrl.rate_type and lvrl.start_date < lvrl2.start_date',
                [])
            ->where('lvr2.id is null')
            ->where('lvrl2.id IS NULL')
            ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->group('ow.workorder_id');
        
        return $select;
    }
    
    /**
     * Get total invoice price per order
     * 
     * @return array
     */
    public function getTotalInvoicePrice()
    {
        $select = $this->getTotalInvoicePriceSelect();
        $result = $this->getDao()->fetchAll($select);

        if ($result->count()) {
            return $result->toArray();
        }
        
        return [];
    }
      
    /**
     * Get total invoice price by order id
     * 
     * @param integer $orderId
     * @return type
     */
    public function getTotalInvoicePriceByOrderId($orderId)
    {
        $dao = $this->getDao();
        $select = $this->getTotalInvoicePriceSelect();
        $select->where('ow.workorder_id = ?', (int)$orderId);
                
        $result = $dao->fetchRow($select);
        
        if (null == $result) {
            return [];
        }
        
        return $result->toArray();
    }
}
