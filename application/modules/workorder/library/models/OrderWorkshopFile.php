<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopFile
 */
class BAS_Shared_Model_Workshop_OrderWorkshopFile extends BAS_Shared_Model_AbstractModel
{

    /**
     * @var bool
     */
    const ARCHIVED = 1;
    /**
     * @var bool
     */
    const NOT_ARCHIVED = 0;
    /**
     * @var bool
     */
    const IS_READ = 1;
    /**
     * @var bool
     */
    const IS_NOT_READ = 0;

    const FILE_TYPE_OTHER = 'Other';

    const FILE_TYPE_INVOICE = 'invoice';
    const FILE_TYPE_INVOICE_PROFORMA = 'proforma';
    const FILE_TYPE_SERVICE_OFFER = 'service_offer';
    const FILE_TYPE_PICK_ORDER = 'pick_order';

    /**
     * @var array
     */
    public static $allInvoiceTypes = [
        self::FILE_TYPE_OTHER,
        self::FILE_TYPE_INVOICE,
        self::FILE_TYPE_INVOICE_PROFORMA,
        self::FILE_TYPE_SERVICE_OFFER,
        self::FILE_TYPE_PICK_ORDER,
    ];

    /**
     * Primary ID
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $workorderId;

    /**
     * @var DateTime
     */
    public $createdAt;

    /**
     * @var string
     */
    public $createdBy;

    /**
     * @var string
     */
    public $fileName;

    /**
     * @var string
     */
    public $title;

    /**
     * @var string
     */
    public $token;

    /**
     * @var int
     */
    public $type;

    /**
     * @var int
     */
    public $isRead = 0;

    /**
     * @var bool
     */
    public $archived;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getWorkorderId()
    {
        return $this->workorderId;
    }

    /**
     * @param int $workorderId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function setWorkorderId($workorderId)
    {
        $this->workorderId = $workorderId;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param DateTime $createdAt
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getIsRead()
    {
        return $this->isRead;
    }

    /**
     * @param int $isRead
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;

        return $this;
    }

    /**
     * @return boolean
     */
    public function isArchived()
    {
        return $this->archived;
    }

    /**
     * @param boolean $archived
     * @return BAS_Shared_Model_Workshop_OrderWorkshopFile
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }
}
