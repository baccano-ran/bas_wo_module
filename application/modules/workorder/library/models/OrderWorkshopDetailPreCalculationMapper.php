<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculationMapper
 */
class BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculationMapper extends BAS_Shared_Model_AbstractMapper
{
    protected $_modelClass = 'BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculation';

    protected $_map = [
        'orderWorkshopDetailId' => 'order_workshop_detail_id',
        'description' => 'description',
        'amount' => 'amount',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
    ];

    /**
     * @param int $defectId
     * @return array
     */
    public function getDefectPreCalculationsData($defectId)
    {
        $select = $this->getDao()
            ->select()
            ->setIntegrityCheck(false)
            ->from(['owdpc' => $this->getTableName()], [])
            ->join(['owd' => 'order_workshop_detail'], 'owd.id = owdpc.order_workshop_detail_id', [])
            ->join(['sd' => 'stock_defects'], 'sd.workorder_id = owd.order_workshop_id', [])
            ->where('sd.id = ?', (int)$defectId)
            ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED)
            ->columns([
                'detailId' => 'owdpc.order_workshop_detail_id',
                'description' => 'owdpc.description',
                'amount' => 'owdpc.amount',
                'imageCount' => 'owd.image_count',
            ]);

        return $this->setMapTo(self::MAP_TO_ARRAY)->findAllByCondition($select);
    }

    /**
     * @param int $workorderId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculation[]
     */
    public function findByWorkorderId($workorderId)
    {
        $select = $this->getDao()
            ->select()
            ->from(['owdpc' => $this->getTableName()], '*')
            ->join(['owd' => 'order_workshop_detail'], 'owd.id = owdpc.order_workshop_detail_id', [])
            ->join(['ow' => 'order_workshop'], 'ow.workorder_id = owd.order_workshop_id', [])
            ->where('ow.workorder_id = ?', (int)$workorderId)
            ->where('owd.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopDetail::NOT_ARCHIVED);

        return $this->findAllByCondition($select);
    }

    /**
     * @param int $workorderId
     * @param int $userId
     */
    public function makePreCalculationByWorkorderId($workorderId, $userId)
    {
        $workorderId = (int) $workorderId;
        $userId = (int) $userId;

        $dao = $this->getDao();

        $orderWorkshopDetailsSelect = $dao
            ->select()
            ->setIntegrityCheck(false)
            ->from(['owd' => 'order_workshop_detail'], [])
            ->where('order_workshop_id = ?', $workorderId)
            ->where('archived = ?', BAS_Shared_Model_AbstractModel::NOT_ARCHIVED)
            ->where('main_order_workshop_detail_id IS NULL')
            ->columns([
                'order_workshop_detail_id' => 'id',
                'description' => 'invoice_description',
                'amount' => new Zend_Db_Expr('quantity * price_order'),
                'created_at' => new Zend_Db_Expr('NOW()'),
                'created_by' => new Zend_Db_Expr($userId),
            ]);

        $sqlString = sprintf('INSERT INTO %s (%s) %s',
            $this->getTableName(),
            implode(',', $this->getMap()),
            $orderWorkshopDetailsSelect
        );

        $dao->getAdapter()->query($sqlString);
    }

}