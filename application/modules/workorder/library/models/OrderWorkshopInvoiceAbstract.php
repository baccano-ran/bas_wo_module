<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract
 */
abstract class BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract extends BAS_Shared_Model_AbstractModel
{
    const TYPE_INVOICE = 'invoice';
    const TYPE_CREDIT_INVOICE = 'credit-invoice';

    const BOOKING_TYPE_INTERNAL = 1;
    const BOOKING_TYPE_EXTERNAL = 2;
    const DEFAULT_INVOICE_LANGUAGE_CODE = 'en';

    /**
     * Invoice is booked or not status / journal entry to acsys made no or yes
     */
    const INVOICE_BOOKED = 1;
    const INVOICE_NOT_BOOKED = 0;

    const DEFAULT_EXCHANGE_RATE = 1.00;

    /**
     * Mapping between invoice booking types and depot booking types
     * @var array
     */
    public static $depotBookingCodeMap = [
        self::BOOKING_TYPE_INTERNAL => [
            BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_INTERNAL,
            BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_GUARANTEE,
        ],
        self::BOOKING_TYPE_EXTERNAL => [
            BAS_Shared_Model_DepotBookingCode::BOOKING_CODE_TYPE_EXTERNAL,
        ]
    ];

    /**
     * @var int
     */
    public $invoiceId;
    /**
     * @var int
     */
    public $depotId;
    /**
     * @var int
     */
    public $mainAccountingDepotId;
    /**
     * @var int
     */
    public $orderWorkshopId;
    /**
     * @var string
     */
    public $invoiceDate;
    /**
     * @var float
     */
    public $exchangeRate;
    /**
     * @var float
     */
    public $vatExchangeRate;
    /**
     * @var int
     */
    public $currency;
    /**
     * @var string
     */
    public $languageCode;
    /**
     * @var int
     */
    public $debtorId;
    /**
     * @var string
     */
    public $type;
    /**
     * @var int
     */
    public $orderWorkshopFileId;
    /**
     * @var int
     */
    public $invoiceBooked;
    /**
     * @var string
     */
    public $journalBookedAt;
    /**
     * @var int
     */
    public $vatType;
    /**
     * @var text
     */
    public $vatTypeChangeReason;
    /**
     * @var float
     */
    public $vatAmount;
    /**
     * @var float
     */
    public $priceConsumables;
    /**
     * @var float
     */
    public $priceEnvironment;
    /**
     * @var datetime
     */
    public $createdAt;
    /**
     * @var int
     */
    public $createdBy;

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     * @return $this
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * @return int
     */
    public function getDepotId()
    {
        return $this->depotId;
    }

    /**
     * @param int $depotId
     * @return $this
     */
    public function setDepotId($depotId)
    {
        $this->depotId = $depotId;

        return $this;
    }

    /**
     * @return int
     */
    public function getMainAccountingDepotId()
    {
        return $this->mainAccountingDepotId;
    }

    /**
     * @param int $mainAccountingDepotId
     * @return $this
     */
    public function setMainAccountingDepotId($mainAccountingDepotId)
    {
        $this->mainAccountingDepotId = $mainAccountingDepotId;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderWorkshopId()
    {
        return $this->orderWorkshopId;
    }

    /**
     * @param int $orderWorkshopId
     * @return $this
     */
    public function setOrderWorkshopId($orderWorkshopId)
    {
        $this->orderWorkshopId = $orderWorkshopId;

        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceDate()
    {
        return $this->invoiceDate;
    }

    /**
     * @param string $invoiceDate
     * @return $this
     */
    public function setInvoiceDate($invoiceDate)
    {
        $this->invoiceDate = $invoiceDate;

        return $this;
    }

    /**
     * @return float
     */
    public function getExchangeRate()
    {
        return $this->exchangeRate;
    }

    /**
     * @param float $exchangeRate
     * @return $this
     */
    public function setExchangeRate($exchangeRate)
    {
        $this->exchangeRate = $exchangeRate;

        return $this;
    }

    /**
     * @return float
     */
    public function getVatExchangeRate()
    {
        return $this->vatExchangeRate;
    }

    /**
     * @param float $vatExchangeRate
     * @return $this
     */
    public function setVatExchangeRate($vatExchangeRate)
    {
        $this->vatExchangeRate = $vatExchangeRate;

        return $this;
    }

    /**
     * @return int
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param int $currency
     * @return $this
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * @param string $languageCode
     * @return $this
     */
    public function setLanguageCode($languageCode)
    {
        $this->languageCode = $languageCode;

        return $this;
    }

    /**
     * @return int
     */
    public function getDebtorId()
    {
        return $this->debtorId;
    }

    /**
     * @param int $debtorId
     * @return $this
     */
    public function setDebtorId($debtorId)
    {
        $this->debtorId = $debtorId;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderWorkshopFileId()
    {
        return $this->orderWorkshopFileId;
    }

    /**
     * @param int $orderWorkshopFileId
     * @return $this
     */
    public function setOrderWorkshopFileId($orderWorkshopFileId)
    {
        $this->orderWorkshopFileId = $orderWorkshopFileId;

        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceBooked()
    {
        return $this->invoiceBooked;
    }

    /**
     * @param int $invoiceBooked
     * @return $this
     */
    public function setInvoiceBooked($invoiceBooked)
    {
        $this->invoiceBooked = $invoiceBooked;

        return $this;
    }

    /**
     * @return string
     */
    public function getJournalBookedAt()
    {
        return $this->journalBookedAt;
    }

    /**
     * @param string $journalBookedAt
     * @return $this
     */
    public function setJournalBookedAt($journalBookedAt)
    {
        $this->journalBookedAt = $journalBookedAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getVatType()
    {
        return $this->vatType;
    }

    /**
     * @param int $vatType
     * @return $this
     */
    public function setVatType($vatType)
    {
        $this->vatType = $vatType;

        return $this;
    }

    /**
     * @return text
     */
    public function getVatTypeChangeReason()
    {
        return $this->vatTypeChangeReason;
    }

    /**
     * @param text $vatTypeChangeReason
     * @return $this
     */
    public function setVatTypeChangeReason($vatTypeChangeReason)
    {
        $this->vatTypeChangeReason = $vatTypeChangeReason;

        return $this;
    }

    /**
     * @return float
     */
    public function getVatAmount()
    {
        return $this->vatAmount;
    }

    /**
     * @param float $vatAmount
     * @return $this
     */
    public function setVatAmount($vatAmount)
    {
        $this->vatAmount = $vatAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getPriceConsumables()
    {
        return $this->priceConsumables;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setPriceConsumables($value)
    {
        $this->priceConsumables = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceEnvironment()
    {
        return $this->priceEnvironment;
    }

    /**
     * @param float $value
     * @return $this
     */
    public function setPriceEnvironment($value)
    {
        $this->priceEnvironment = $value;
        return $this;
    }

    /**
     * @return datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param datetime $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param int $createdBy
     * @return $this
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

}
