<?php

/**
 * Class BAS_Shared_Model_Workshop_LabourActivity
 */
class BAS_Shared_Model_Workshop_LabourActivity extends BAS_Shared_Model_AbstractModel
{
    /**
     * @return int
     */
    public $id;

    /**
     * @return int
     */
    public $depotId;

    /**
     * @return int
     */
    public $departmentId;

    /**
     * @return string
     */
    public $name;

    /**
     * @return string
     */
    public $createdAt;

    /**
     * @return int
     */
    public $createdBy;

    /**
     * @return string
     */
    public $updatedAt;

    /**
     * @return int
     */
    public $updatedBy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDepotId()
    {
        return $this->depotId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setDepotId($value)
    {
        $this->depotId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDepartmentId()
    {
        return $this->departmentId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setDepartmentId($value)
    {
        $this->departmentId = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string
     * @return $this
     */
    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->createdAt = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param int
     * @return $this
     */
    public function setCreatedBy($value)
    {
        $this->createdBy = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param string
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updatedAt = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param int
     * @return $this
     */
    public function setUpdatedBy($value)
    {
        $this->updatedBy = $value;
        return $this;
    }
}