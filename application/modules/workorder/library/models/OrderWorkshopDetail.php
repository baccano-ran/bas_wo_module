<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopDetail
 */
class BAS_Shared_Model_Workshop_OrderWorkshopDetail extends BAS_Shared_Model_AbstractModel
{

    const TYPE_TASK = 1;
    const TYPE_ITEM = 2;

    const TRADE_IN_YES = 1;
    const TRADE_IN_NO = 0;

    const ITEM_PICKED_OFFER = 0;
    const ITEM_PICKED_ORDER = 1;
    const ITEM_PICKED_TO_BE_PICKED = 2;
    const ITEM_PICKED_PICKED = 3;
    const ITEM_PICKED_DELIVERED = 4;

    const ARCHIVED = 1;
    const NOT_ARCHIVED = 0;

    const ON_INVOICE_YES = 1;
    const ON_INVOICE_NO = 0;

    const TASK_TYPE_INTERNAL = 'internal_task';
    const TASK_TYPE_EXTERNAL = 'external_task';

    const DEFAULT_QUANTITY = 1;
    const DEFAULT_FLUID_QUANTITY = 0.01;
    const EMPTY_BOOKING_CODE = 0;

    /**
     * @var int
     */
    public $id;

    /**
     * @var int
     */
    public $orderWorkshopId;

    /**
     * @var int
     */
    public $mainOrderWorkshopDetailId;

    /**
     * @var int
     */
    public $type;

    /**
     * @var int
     */
    public $referenceId;

    /**
     * @var int
     */
    public $depotId;

    /**
     * @var int
     */
    public $onInvoice;

    /**
     * @var float
     */
    public $quantity;

    /**
     * @var float
     */
    public $priceOrder;

    /**
     * @var float
     */
    public $priceOriginal;

    /**
     * @var float
     */
    public $costPrice;

    /**
     * @var int
     */
    public $supplierId;

    /**
     * @var int
     */
    public $tradeIn;

    /**
     * @var int
     */
    public $labourRateId;

    /**
     * @var float
     */
    public $plannedTime;

    /**
     * @var float
     */
    public $spentTime;

    /**
     * @var float
     */
    public $invoiceTime;

    /**
     * @var int
     */
    public $depotBookingCodeId;

    /**
     * @var int
     */
    public $depotAccountGroupId;

    /**
     * @var string
     */
    public $invoiceDescription;

    /**
     * @var int
     */
    public $itemPicked;

    /**
     * @var int
     */
    public $sequence;

    /**
     * @var int
     */
    public $imageCount;

    /**
     * @var int
     */
    public $archived;

    /**
     * @var string
     */
    public $createdAt;

    /**
     * @var int
     */
    public $createdBy;

    /**
     * @var string
     */
    public $updatedAt;

    /**
     * @var int
     */
    public $updatedBy;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderWorkshopId()
    {
        return $this->orderWorkshopId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setOrderWorkshopId($value)
    {
        $this->orderWorkshopId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getMainOrderWorkshopDetailId()
    {
        return $this->mainOrderWorkshopDetailId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setMainOrderWorkshopDetailId($value)
    {
        $this->mainOrderWorkshopDetailId = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSubDetail()
    {
        return $this->mainOrderWorkshopDetailId > 0;
    }

    /**
     * @return bool
     */
    public function isMainDetail()
    {
        return empty($this->mainOrderWorkshopDetailId);
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int
     * @return $this
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function isItem()
    {
        return (int)$this->getType() === self::TYPE_ITEM;
    }

    /**
     * @return bool
     */
    public function isTask()
    {
        return (int)$this->getType() === self::TYPE_TASK;
    }

    /**
     * @return int
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setReferenceId($value)
    {
        $this->referenceId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDepotId()
    {
        return $this->depotId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setDepotId($value)
    {
        $this->depotId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getOnInvoice()
    {
        return $this->onInvoice;
    }

    /**
     * @param int
     * @return $this
     */
    public function setOnInvoice($value)
    {
        $this->onInvoice = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float
     * @return $this
     */
    public function setQuantity($value)
    {
        $this->quantity = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceOrder()
    {
        return $this->priceOrder;
    }

    /**
     * @param float
     * @return $this
     */
    public function setPriceOrder($value)
    {
        $this->priceOrder = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getPriceOriginal()
    {
        return $this->priceOriginal;
    }

    /**
     * @param float
     * @return $this
     */
    public function setPriceOriginal($value)
    {
        $this->priceOriginal = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getCostPrice()
    {
        return $this->costPrice;
    }

    /**
     * @param float
     * @return $this
     */
    public function setCostPrice($value)
    {
        $this->costPrice = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getLabourRateId()
    {
        return $this->labourRateId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setLabourRateId($value)
    {
        $this->labourRateId = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getPlannedTime()
    {
        return $this->plannedTime;
    }

    /**
     * @param float
     * @return $this
     */
    public function setPlannedTime($value)
    {
        $this->plannedTime = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getSpentTime()
    {
        return $this->spentTime;
    }

    /**
     * @param float
     * @return $this
     */
    public function setSpentTime($value)
    {
        $this->spentTime = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getInvoiceTime()
    {
        return $this->invoiceTime;
    }

    /**
     * @param float
     * @return $this
     */
    public function setInvoiceTime($value)
    {
        $this->invoiceTime = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDepotBookingCodeId()
    {
        return $this->depotBookingCodeId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setDepotBookingCodeId($value)
    {
        $this->depotBookingCodeId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDepotAccountGroupId()
    {
        return $this->depotAccountGroupId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setDepotAccountGroupId($value)
    {
        $this->depotAccountGroupId = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getInvoiceDescription()
    {
        return $this->invoiceDescription;
    }

    /**
     * @param string
     * @return $this
     */
    public function setInvoiceDescription($value)
    {
        $this->invoiceDescription = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getItemPicked()
    {
        return $this->itemPicked;
    }

    /**
     * @param int
     * @return $this
     */
    public function setItemPicked($value)
    {
        $this->itemPicked = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getSequence()
    {
        return $this->sequence;
    }

    /**
     * @param int
     * @return $this
     */
    public function setSequence($value)
    {
        $this->sequence = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param int
     * @return $this
     */
    public function setArchived($value)
    {
        $this->archived = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->createdAt = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param int
     * @return $this
     */
    public function setCreatedBy($value)
    {
        $this->createdBy = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param string
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updatedAt = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param int
     * @return $this
     */
    public function setUpdatedBy($value)
    {
        $this->updatedBy = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTradeIn()
    {
        return $this->tradeIn;
    }

    /**
     * @param mixed $tradeIn
     * @return $this
     */
    public function setTradeIn($tradeIn)
    {
        $this->tradeIn = $tradeIn;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTradeIn()
    {
        return $this->tradeIn === self::TRADE_IN_YES;
    }

    /**
     * @return bool
     */
    public function isCorrection()
    {
        return $this->quantity < 0 && $this->tradeIn === self::TRADE_IN_NO;
    }

    /**
     * @return int
     */
    public function getSupplierId()
    {
        return $this->supplierId;
    }

    /**
     * @param int $supplierId
     * @return $this
     */
    public function setSupplierId($supplierId)
    {
        $this->supplierId = $supplierId;
        return $this;
    }

    /**
     * @return int
     */
    public function getImageCount()
    {
        return $this->imageCount;
    }

    /**
     * @param int $imageCount
     * @return $this
     */
    public function setImageCount($imageCount)
    {
        $this->imageCount = $imageCount;

        return $this;
    }

}
