<?php

/**
 * Class BAS_Shared_Model_Workshop_ContactTypeSupplierFeeSetting
 */
class BAS_Shared_Model_Workshop_ContactTypeSupplierFeeSetting extends BAS_Shared_Model_AbstractModel
{
    const DEFAULT_THRESHOLD_VALUE = 500;
    const CATEGORY_SALES_PREPARATION = 1;
    const VALUE_TYPE_THRESHOLD = 1;

    public $id;
    public $depotId;
    public $contactId;
    public $category;
    public $valueType;
    public $value;
    public $archived;
    public $createdAt;
    public $createdBy;
    public $updatedAt;
    public $updatedBy;

    /**
     * @param int $value
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setDepotId($value)
    {
        $this->depotId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDepotId()
    {
        return $this->depotId;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setContactId($value)
    {
        $this->contactId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setCategory($value)
    {
        $this->category = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setValueType($value)
    {
        $this->valueType = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getValueType()
    {
        return $this->valueType;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setArchived($value)
    {
        $this->archived = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->createdAt = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setCreatedBy($value)
    {
        $this->createdBy = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updatedAt = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setUpdatedBy($value)
    {
        $this->updatedBy = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }


}