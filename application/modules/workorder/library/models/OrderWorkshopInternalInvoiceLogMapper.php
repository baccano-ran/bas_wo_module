<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceLogMapper
 */
class BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceLogMapper extends BAS_Shared_Model_AbstractMapper
{
    protected $_modelClass = 'BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceLog';

    protected $_map = [
        'id' => 'id',
        'invoiceId' => 'invoice_id',
        'orderWorkshopId' => 'order_workshop_id',
        'orderWorkshopDetailId' => 'order_workshop_detail_id',
        'type' => 'type',
        'referenceId' => 'reference_id',
        'depotBookingCodeId' => 'depot_booking_code_id',
        'depotAccountGroupId' => 'depot_account_group_id',
        'quantity' => 'quantity',
        'price' => 'price',
        'amount' => 'amount',
        'stockPrice' => 'stock_price',
        'stockAmount' => 'stock_amount',
        'discountAmount' => 'discount_amount',
    ];

    /**
     * this function is used to get the journal entry csv lines for products and items.
     * 
     * @param array $depotIds
     * @return array|null
     */
    public function getWorkorderInternalInvoiceLogCsvRecords($depotIds) 
    {

        if (array() === $depotIds) {
            return array();
        }

        $dao = $this->getDao();
        $select = $dao->select()->setIntegrityCheck(false);

        $select->from(['owiil' => 'order_workshop_internal_invoice_log'], [])
                ->joinInner(['owii' => 'order_workshop_internal_invoice'], 'owii.order_workshop_id=owiil.order_workshop_id and owii.invoice_id=owiil.invoice_id', [
                    'invoiceId' => 'owii.invoice_id',
                    'orderId' => 'owii.order_workshop_id',
                    'depotId' => 'owii.depot_id',
                    'type' => 'owiil.type',
                    'subtotalSale' => new Zend_Db_Expr('SUM(owiil.amount)'),
                    'subtotalSaleInOtherCurrency' => new Zend_Db_Expr('SUM(owiil.amount)*owii.exchange_rate'),
                    'subtotalCostprice' => new Zend_Db_Expr('SUM(owiil.stock_amount)'),
                    'subtotalCostpriceInOtherCurrency' => new Zend_Db_Expr('SUM(owiil.stock_amount)*owii.exchange_rate'),
                    'subtotalDiscount' => new Zend_Db_Expr('SUM(owiil.discount_amount)'),
                    'subtotalDiscountInOtherCurrency' => new Zend_Db_Expr('SUM(owiil.discount_amount)*owii.exchange_rate'),
                    'depotAccountGroupId' => 'owiil.depot_account_group_id',
                    'depotBookingCodeId' => 'owiil.depot_booking_code_id'
                        ]
                )
                ->joinInner(['owd' => 'order_workshop_detail'], 'owd.id=owiil.order_workshop_detail_id', [
                    'tradeInItem' => 'owd.trade_in'
                ])
                ->joinLeft(['id' => 'item_depot'], 'owiil.type = ' . BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceLog::TYPE_ITEM
                        . ' AND owiil.reference_id=id.item_id '
                        . ' AND id.depot_id=owd.depot_id ', [])
                ->joinLeft(['i' => 'item'], 'i.id = id.item_id', [
                    'noStockItem' => new Zend_Db_Expr('IFNULL(i.no_stock_item,0)')
                        ]
                )
                ->joinLeft(['ir' => 'item_receipt'], ' ir.return_order_id = owd.order_workshop_id' , [
                    'itemReceiptId' => new Zend_Db_Expr('if(i.no_stock_item = '. BAS_Shared_Model_Item::NO_STOCK_ITEM_NO .' AND ' .
                        'owd.trade_in = '. BAS_Shared_Model_Warehouse_OrderItem::TRADE_IN_YES .', ir.id, null)')
                ])
                ->joinLeft(['dbc' => 'depot_booking_code'], 'dbc.id = owiil.depot_booking_code_id', [
                    'depotBookingCodeType' => 'dbc.type'
                    ]
                )
                ->where('owii.depot_id in (?)', $depotIds)
                ->where('invoice_booked = ? ', BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoice::INVOICE_NOT_BOOKED)
                ->group('invoiceId')
                ->group('orderId')
                ->group('depotId')
                ->group('owiil.type')
                ->group('noStockItem')
                ->group('tradeInItem')
                ->group('depotBookingCodeId')
                ->group('depotAccountGroupId');

        $result = $dao->fetchAll($select);

        if (!empty($result)) {
            return $result->toArray();
        }

        return null;
    }
}