<?php

/**
 * Class BAS_Shared_Model_Workshop_LabourCostMapper
 */
class BAS_Shared_Model_Workshop_LabourCostMapper extends BAS_Shared_Model_AbstractMapper
{

    protected $_modelClass = 'BAS_Shared_Model_Workshop_LabourCost';

    protected $_map = [
        'id' => 'id',
        'labourRateId' => 'labour_rate_id',
        'costPrice' => 'cost_price',
        'endDate' => 'end_date',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
        'updatedAt' => 'updated_at',
        'updatedBy' => 'updated_by',
    ];

}