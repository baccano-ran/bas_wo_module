<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopInvoiceBuilderFactory
 */
class BAS_Shared_Model_Workshop_OrderWorkshopInvoiceBuilderFactory
{

    /**
     * @param Workshop_Service_Workorder $workorderService
     * @param Management_Service_DepotSetting $depotSettingService
     * @param Workshop_Service_WorkorderInvoice $workorderInvoiceService
     * @param int $invoiceBookingType
     * @return BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceBuilder|BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceBuilder
     * @throws BAS_Shared_InvalidArgumentException
     */
    public static function create(
        Workshop_Service_Workorder $workorderService,
        Management_Service_DepotSetting $depotSettingService,
        Workshop_Service_WorkorderInvoice $workorderInvoiceService,
        $invoiceBookingType
    ) {
        if ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_EXTERNAL) {
            return new BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceBuilder($workorderService, $depotSettingService, $workorderInvoiceService);
        } elseif ($invoiceBookingType === BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::BOOKING_TYPE_INTERNAL) {
            return new BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceBuilder($workorderService, $depotSettingService, $workorderInvoiceService);
        } else {
            throw new BAS_Shared_InvalidArgumentException('Invalid invoice booking type value given:' . $invoiceBookingType);
        }
    }
}