<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopExternalInvoice
 */
class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopExternalInvoice extends Zend_Db_Table_Abstract
{
    public static $tName = 'order_workshop_external_invoice';
    
    protected $_name = 'order_workshop_external_invoice';
    protected $_primary = ['invoice_id', 'depot_id'];
}