<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_WorkshopRefuelingCard
 */
class BAS_Shared_Model_Workshop_DbTable_WorkshopRefuelingCard extends Zend_Db_Table_Abstract
{
    protected $_schema = 'bastrucks_erp';
    protected $_name = 'workshop_refueling_card';
    protected $_primary = 'id';
}