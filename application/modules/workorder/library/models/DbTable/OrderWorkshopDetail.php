<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopDetail
 */
class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopDetail extends Zend_Db_Table_Abstract
{
    protected $_schema = 'bastruck_erp';
    protected $_name = 'order_workshop_detail';
    protected $_primary = 'id';
}