<?php
class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopFile extends Zend_Db_Table_Abstract
{
    /**
     * @var string
     */
    protected $_schema = 'bastruck_erp';
    /**
     * @var string
     */
    protected $_name = 'order_workshop_file';
    /**
     * @var string
     */
    protected $_primary = 'id';

}