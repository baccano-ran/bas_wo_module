<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_LabourRate
 */
class BAS_Shared_Model_Workshop_DbTable_LabourRate extends Zend_Db_Table_Abstract
{
    protected $_schema = 'bastrucks_erp';
    protected $_name = 'labour_rate';
    protected $_primary = 'id';
}