<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopTime
 */
class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopTime extends Zend_Db_Table_Abstract
{
    protected $_schema = 'bastruck_erp';
    protected $_name = 'order_workshop_time';
    protected $_primary = 'id';
}