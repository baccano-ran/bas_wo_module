<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopExternalInvoiceLog
 */
class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopExternalInvoiceLog extends Zend_Db_Table_Abstract
{
    public static $tName = 'order_workshop_external_invoice_log';

    protected $_name = 'order_workshop_external_invoice_log';
    protected $_primary = 'id';
}