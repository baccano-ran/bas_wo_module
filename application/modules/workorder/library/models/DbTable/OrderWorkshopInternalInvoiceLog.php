<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopInternalInvoiceLog
 */
class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopInternalInvoiceLog extends Zend_Db_Table_Abstract
{
    public static $tName = 'order_workshop_internal_invoice_log';

    protected $_name = 'order_workshop_internal_invoice_log';
    protected $_primary = 'id';
}