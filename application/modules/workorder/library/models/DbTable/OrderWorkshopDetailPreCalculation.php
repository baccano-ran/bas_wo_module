<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopDetailPreCalculation
 */
class BAS_Shared_Model_Workshop_DbTable_OrderWorkshopDetailPreCalculation extends Zend_Db_Table_Abstract
{
    protected $_name = 'order_workshop_detail_pre_calculation';
    protected $_primary = 'order_workshop_detail_id';
}