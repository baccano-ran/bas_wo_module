<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_LabourCost
 */
class BAS_Shared_Model_Workshop_DbTable_LabourCost extends Zend_Db_Table_Abstract
{
    protected $_schema = 'bastruck_erp';
    protected $_name = 'labour_cost';
    protected $_primary = 'id';
}