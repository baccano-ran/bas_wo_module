<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_LabourActivity
 */
class BAS_Shared_Model_Workshop_DbTable_LabourActivity extends Zend_Db_Table_Abstract
{
    protected $_schema = 'bastruck_erp';
    protected $_name = 'labour_activity';
    protected $_primary = 'id';
}