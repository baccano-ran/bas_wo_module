<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_ContactTypeSupplierFeeSetting
 */
class BAS_Shared_Model_Workshop_DbTable_ContactTypeSupplierFeeSetting extends Zend_Db_Table_Abstract
{
    protected $_name = 'contact_type_supplier_fee_setting';
    protected $_primary = 'id';
}