<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_WorkshopSurcharge
 */
class BAS_Shared_Model_Workshop_DbTable_WorkshopSurcharge extends Zend_Db_Table_Abstract
{
    protected $_schema = 'bastruck_erp';
    protected $_name = 'workshop_surcharge';
    protected $_primary = 'id';
}