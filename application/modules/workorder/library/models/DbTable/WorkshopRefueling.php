<?php

/**
 * Class BAS_Shared_Model_Workshop_DbTable_WorkshopRefueling
 */
class BAS_Shared_Model_Workshop_DbTable_WorkshopRefueling extends Zend_Db_Table_Abstract
{
    protected $_schema = 'bastruck_erp';
    protected $_name = 'workshop_refueling';
    protected $_primary = 'id';
}