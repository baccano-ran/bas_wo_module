<?php

/**
 * Class BAS_Shared_Model_Workshop_WorkshopRefuelingCard
 */
class BAS_Shared_Model_Workshop_WorkshopRefuelingCard extends BAS_Shared_Model_AbstractModel
{
    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $cardNumber;
    /**
     * @var
     */
    public $contactVehicleId;
    /**
     * @var
     */
    public $orderWorkshopId;
    /**
     * @var
     */
    public $archived;
    /**
     * @var
     */
    public $createdAt;
    /**
     * @var
     */
    public $createdBy;
    /**
     * @var
     */
    public $updatedAt;
    /**
     * @var
     */
    public $updatedBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BAS_Shared_Model_Workshop_WorkshopRefuelingCard
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param mixed $cardNumber
     * @return BAS_Shared_Model_Workshop_WorkshopRefuelingCard
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactVehicleId()
    {
        return $this->contactVehicleId;
    }

    /**
     * @param mixed $contactVehicleId
     * @return BAS_Shared_Model_Workshop_WorkshopRefuelingCard
     */
    public function setContactVehicleId($contactVehicleId)
    {
        $this->contactVehicleId = $contactVehicleId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrderWorkshopId()
    {
        return $this->orderWorkshopId;
    }

    /**
     * @param mixed $orderWorkshopId
     * @return BAS_Shared_Model_Workshop_WorkshopRefuelingCard
     */
    public function setOrderWorkshopId($orderWorkshopId)
    {
        $this->orderWorkshopId = $orderWorkshopId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param mixed $archived
     * @return BAS_Shared_Model_Workshop_WorkshopRefuelingCard
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return BAS_Shared_Model_Workshop_WorkshopRefuelingCard
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return BAS_Shared_Model_Workshop_WorkshopRefuelingCard
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return BAS_Shared_Model_Workshop_WorkshopRefuelingCard
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param mixed $updatedBy
     * @return BAS_Shared_Model_Workshop_WorkshopRefuelingCard
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }


}