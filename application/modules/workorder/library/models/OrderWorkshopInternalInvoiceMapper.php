<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceMapper
 */
class BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceMapper extends BAS_Shared_Model_AbstractMapper
{
    protected $_modelClass = 'BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoice';

    protected $_map = [
        'invoiceId' => 'invoice_id',
        'depotId' => 'depot_id',
        'mainAccountingDepotId' => 'main_accounting_depot_id',
        'orderWorkshopId' => 'order_workshop_id',
        'invoiceDate' => 'invoice_date',
        'exchangeRate' => 'exchange_rate',
        'vatExchangeRate' => 'vat_exchange_rate',
        'currency' => 'currency',
        'languageCode' => 'language_code',
        'debtorId' => 'debtor_id',
        'type' => 'type',
        'orderWorkshopFileId' => 'order_workshop_file_id',
        'invoiceBooked' => 'invoice_booked',
        'journalBookedAt' => 'journal_booked_at',
        'vatType' => 'vat_type',
        'vatTypeChangeReason' => 'vat_type_change_reason',
        'vatAmount' => 'vat_amount',
        'priceConsumables' => 'price_consumables',
        'priceEnvironment' => 'price_environment',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
    ];

    /**
     * @param int $mainAccountingDepotId
     * @return int
     */
    public function getLastInvoiceId($mainAccountingDepotId)
    {
        $select = $this->getDao()
            ->select()
            ->from('order_workshop_internal_invoice', ['lastInvoiceId' => new Zend_Db_Expr('MAX(invoice_id)')])
            ->where('main_accounting_depot_id = ?', (int)$mainAccountingDepotId)
        ;

        $row = $this->getDao()->fetchRow($select);
        $lastInvoiceId = $row === null ? 0 : (int)$row['lastInvoiceId'];

        return $lastInvoiceId;
    }

    /**
     * function to get the journal entry csv records
     * @param array $depotIds
     * @return array|null
     * @throws Zend_Exception
     */
    public function getWorkorderInternalInvoiceCsvRecords($depotIds)
    {
        $dao = $this->getDao();
        $config = BAS_Shared_Registry::get('bootstrap')->getOptions(); 
        $databaseExtranet = $config['dbname']['legacy_extranet'];
        $select = $dao->select()->setIntegrityCheck(false);

        $select->from(['owii' => 'order_workshop_internal_invoice'], [])
                ->join(['ow' => 'order_workshop'], 'owii.order_workshop_id=ow.workorder_id', [])
                ->join(['owiil' => 'order_workshop_internal_invoice_log'], 'owii.order_workshop_id=owiil.order_workshop_id and owii.invoice_id=owiil.invoice_id', [
                    'invoiceId' => 'owii.invoice_id',
                    'orderId' => 'owii.order_workshop_id',
                    'depotId' => 'owii.depot_id',
                    'type' => 'owii.type',
                    'customer_number' => 'owii.debtor_id',
                    'totalAmountExclVat' => new Zend_Db_Expr('(SUM(ROUND(owiil.amount,2))+ROUND(owii.price_consumables,2)+ROUND(owii.price_environment,2))'),
                    'totalAmountInclVat' => new Zend_Db_Expr('(SUM(ROUND(owiil.amount,2))+ROUND(owii.price_consumables,2)+ROUND(owii.price_environment,2))+ROUND(COALESCE(owii.vat_amount,0),2)'),
                    'totalAmountInOtherCurrencyExclVat' => new Zend_Db_Expr('(SUM(ROUND(owiil.amount*owii.exchange_rate,2))+ROUND(owii.price_consumables*owii.exchange_rate,2)+ROUND(owii.price_environment*owii.exchange_rate,2))'),
                    'totalAmountInOtherCurrencyInclVat' => new Zend_Db_Expr('((SUM(ROUND(owiil.amount*owii.exchange_rate,2))+ROUND(owii.price_consumables*owii.exchange_rate,2)+ROUND(owii.price_environment*owii.exchange_rate,2))+ROUND(COALESCE(owii.vat_amount,0)*owii.exchange_rate,2))'),
                    'totalVatAmount' => 'ROUND(COALESCE(owii.vat_amount,0),2)',
                    'totalVatAmountInOtherCurrency' => new Zend_Db_Expr('ROUND(COALESCE(owii.vat_amount,0)*owii.exchange_rate,2)'),
                    'priceConsumables' => 'ROUND(owii.price_consumables,2)',
                    'priceEnvironment' => 'ROUND(owii.price_environment,2)',
                    'priceConsumablesInOtherCurrency' => new Zend_Db_Expr('ROUND(owii.price_consumables*owii.exchange_rate,2)'),
                    'priceEnvironmentInOtherCurrency' => new Zend_Db_Expr('ROUND(owii.price_environment*owii.exchange_rate,2)'),
                    'accountingVatCode' => new Zend_Db_Expr('NULL'), # always NULL in case of internal workorders
                    'currency' => 'lc.name',
                    'vatPercent' => new Zend_Db_Expr('NULL'), # always NULL in case of internal workorders
                    'remark' => new Zend_Db_Expr("CONCAT('F', owii.invoice_id, ' ', 'O', owii.order_workshop_id, ' ', 'K', owii.debtor_id)"),
                        ]
                )
                ->join(['owd' => 'order_workshop_detail'], 'owiil.order_workshop_detail_id = owd.id AND owd.main_order_workshop_detail_id IS NULL', [])
                ->joinInner(array('lc' => 'list_currency'), "lc.id = owii.currency AND lc.lang = '" . BAS_Shared_Model_ListCurrency::DEFAULT_CURRENCY_LANGUAGE . "'", array())
                ->joinLeft($databaseExtranet . '.Voorraad AS v', sprintf('v.VoertuigID = ow.vehicle_id AND ow.type in (%s)', implode(",", BAS_Shared_Model_OrderWorkshop::$stockVehicleWorkOrderTypes)), [])
                ->joinLeft(array('ov' => 'order_vehicle'), 'ov.id=ow.vehicle_id and ow.type = 1', array())
                ->joinLeft($databaseExtranet . '.Voorraad AS v1', 'v1.VoertuigID = ov.legacy_voertuigid', array(
                    'TruckId' => new Zend_Db_Expr(sprintf('IF(ow.type in (%s), v.TruckId, v1.TruckID)', implode(",", BAS_Shared_Model_OrderWorkshop::$stockVehicleWorkOrderTypes))),
                    'depotIdOwner' => new Zend_Db_Expr(sprintf('IF(ow.type in (%s), v.depot_id_owner, v1.depot_id_owner)', implode(",", BAS_Shared_Model_OrderWorkshop::$stockVehicleWorkOrderTypes))),
                    )
                )
                ->joinLeft(array('cv' => 'contact_vehicle'), sprintf('cv.id = ow.vehicle_id and ow.type IN (%s) and ow.contact_id IS NOT NULL', implode(",", BAS_Shared_Model_OrderWorkshop::$contactVehicleWorkOrderTypes)), array(
                    'licensePlate' => new Zend_Db_Expr('cv.license_plate'),
                    'departmentId' => 'cv.department_id',
                    )
                )
                                
                ->where('owii.depot_id in (?)', $depotIds)
                ->where('owii.invoice_booked = ?', BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoice::INVOICE_NOT_BOOKED)
                ->group('invoiceId')
        ;

        $result = $dao->fetchAll($select);

        if (!empty($result)) {
            return $result->toArray();
        }

        return null;
    }
    
    /**
     * Update data by condition
     *
     * @param  array  $updateValue
     * @param  string $conditions
     * @return integer
     */
    public function updateByCondition($updateValue, $conditions)
    {
        $dao=$this->getDao();

        return $dao->update($updateValue, $conditions);
    }
}
