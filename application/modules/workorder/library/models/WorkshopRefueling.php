<?php

/**
 * Class BAS_Shared_Model_Workshop_WorkshopRefueling
 */
class BAS_Shared_Model_Workshop_WorkshopRefueling extends BAS_Shared_Model_AbstractModel
{
    const STATUS_NEW = 0;
    const STATUS_TO_BE_PROCESSED_MANUAL = 1;
    const STATUS_PROCESSED = 2;

    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $depotId;
    /**
     * @var
     */
    public $receiptNumber;
    /**
     * @var
     */
    public $refuelingTimestamp;
    /**
     * @var
     */
    public $quantity;
    /**
     * @var
     */
    public $price;
    /**
     * @var
     */
    public $amount;
    /**
     * @var
     */
    public $cardNumber;
    /**
     * @var
     */
    public $username;
    /**
     * @var
     */
    public $manualNumber;
    /**
     * @var
     */
    public $vehicleLicensePlate;
    /**
     * @var
     */
    public $mileage;
    /**
     * @var
     */
    public $status;
    /**
     * @var
     */
    public $xmlName;
    /**
     * @var
     */
    public $createdAt;
    /**
     * @var
     */
    public $createdBy;
    /**
     * @var
     */
    public $updatedAt;
    /**
     * @var
     */
    public $updatedBy;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepotId()
    {
        return $this->depotId;
    }

    /**
     * @param mixed $depotId
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setDepotId($depotId)
    {
        $this->depotId = $depotId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getReceiptNumber()
    {
        return $this->receiptNumber;
    }

    /**
     * @param mixed $receiptNumber
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setReceiptNumber($receiptNumber)
    {
        $this->receiptNumber = $receiptNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRefuelingTimestamp()
    {
        return $this->refuelingTimestamp;
    }

    /**
     * @param mixed $refuelingTimestamp
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setRefuelingTimestamp($refuelingTimestamp)
    {
        $this->refuelingTimestamp = $refuelingTimestamp;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param mixed $quantity
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param mixed $amount
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardNumber()
    {
        return $this->cardNumber;
    }

    /**
     * @param mixed $cardNumber
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setCardNumber($cardNumber)
    {
        $this->cardNumber = $cardNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getManualNumber()
    {
        return $this->manualNumber;
    }

    /**
     * @param mixed $manualNumber
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setManualNumber($manualNumber)
    {
        $this->manualNumber = $manualNumber;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getVehicleLicensePlate()
    {
        return $this->vehicleLicensePlate;
    }

    /**
     * @param mixed $vehicleLicensePlate
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setVehicleLicensePlate($vehicleLicensePlate)
    {
        $this->vehicleLicensePlate = $vehicleLicensePlate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getMileage()
    {
        return $this->mileage;
    }

    /**
     * @param mixed $mileage
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setMileage($mileage)
    {
        $this->mileage = $mileage;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getXmlName()
    {
        return $this->xmlName;
    }

    /**
     * @param mixed $xmlName
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setXmlName($xmlName)
    {
        $this->xmlName = $xmlName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param mixed $updatedBy
     * @return BAS_Shared_Model_Workshop_WorkshopRefueling
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }


}