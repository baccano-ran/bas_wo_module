<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopTime
 */
class BAS_Shared_Model_Workshop_OrderWorkshopTime extends BAS_Shared_Model_AbstractModel
{
    const IS_ARCHIVED = 1;
    const NOT_ARCHIVED = 0;
    const NOT_MANUAl = 0;
    const IS_MANUAl = 1;
    /**
     * @return int
     */
    public $id;

    /**
     * @return int
     */
    public $orderWorkshopId;

    /**
     * @return int
     */
    public $personId;

    /**
     * @return string
     */
    public $date;

    /**
     * @return float
     */
    public $spentTime;

    /**
     * @return int
     */
    public $labourActivityId;

    /**
     * @return int
     */
    public $manual;

    /**
     * @return string
     */
    public $xmlFilename;

    /**
     * @return int
     */
    public $archived;

    /**
     * @return string
     */
    public $createdAt;

    /**
     * @return int
     */
    public $createdBy;

    /**
     * @return string
     */
    public $updatedAt;

    /**
     * @return int
     */
    public $updatedBy;

    /**
     * @return string
     */
    private $personName;

    /**
     * @return string
     */
    private $personEmployeeNumber;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderWorkshopId()
    {
        return $this->orderWorkshopId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setOrderWorkshopId($value)
    {
        $this->orderWorkshopId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setPersonId($value)
    {
        $this->personId = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param string
     * @return $this
     */
    public function setDate($value)
    {
        $this->date = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getSpentTime()
    {
        return $this->spentTime;
    }

    /**
     * @param float
     * @return $this
     */
    public function setSpentTime($value)
    {
        $this->spentTime = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getLabourActivityId()
    {
        return $this->labourActivityId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setLabourActivityId($value)
    {
        $this->labourActivityId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getManual()
    {
        return $this->manual;
    }

    /**
     * @param int
     * @return $this
     */
    public function setManual($value)
    {
        $this->manual = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getXmlFilename()
    {
        return $this->xmlFilename;
    }

    /**
     * @param mixed $xmlFilename
     * @return BAS_Shared_Model_Workshop_OrderWorkshopTime
     */
    public function setXmlFilename($xmlFilename)
    {
        $this->xmlFilename = $xmlFilename;

        return $this;
    }

    /**
     * @return int
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * @param int
     * @return $this
     */
    public function setArchived($value)
    {
        $this->archived = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->createdAt = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param int
     * @return $this
     */
    public function setCreatedBy($value)
    {
        $this->createdBy = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param string
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updatedAt = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param int
     * @return $this
     */
    public function setUpdatedBy($value)
    {
        $this->updatedBy = $value;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonName()
    {
        return $this->personName;
    }

    /**
     * @param mixed $personName
     * @return BAS_Shared_Model_Workshop_OrderWorkshopTime
     */
    public function setPersonName($personName)
    {
        $this->personName = $personName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPersonEmployeeNumber()
    {
        return $this->personEmployeeNumber;
    }

    /**
     * @param mixed $personEmployeeNumber
     * @return BAS_Shared_Model_Workshop_OrderWorkshopTime
     */
    public function setPersonEmployeeNumber($personEmployeeNumber)
    {
        $this->personEmployeeNumber = $personEmployeeNumber;

        return $this;
    }


}