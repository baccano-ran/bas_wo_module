<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceBuilder
 */
class BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoiceBuilder extends BAS_Shared_Model_Workshop_OrderWorkshopInvoiceBuilder
{

    /**
     * @return BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoice
     */
    protected function create()
    {
        return new BAS_Shared_Model_Workshop_OrderWorkshopInternalInvoice();
    }

    /**
     * @inheritdoc
     */
    public function build(
        BAS_Shared_Model_OrderWorkshop $workorder,
        $invoiceType,
        $invoiceFileId,
        $languageCode,
        $invoiceBookingType,
        $depotId,
        $userId
    ) {
        $invoice = parent::build(
            $workorder,
            $invoiceType,
            $invoiceFileId,
            $languageCode,
            $invoiceBookingType,
            $depotId,
            $userId
        );

        $vatAmount = 0;

        $consumablesAmount = BAS_Shared_Utils_Math::add(
            $workorder->getPriceInternalConsumables(),
            $workorder->getPriceGuaranteeConsumables(),
            BAS_Shared_Utils_Math::SCALE_4
        );

        $environmentAmount = BAS_Shared_Utils_Math::add(
            $workorder->getPriceInternalEnvironment(),
            $workorder->getPriceGuaranteeEnvironment(),
            BAS_Shared_Utils_Math::SCALE_4
        );

        $invoice
            ->setVatAmount($vatAmount)
            ->setPriceConsumables($consumablesAmount)
            ->setPriceEnvironment($environmentAmount);

        return $invoice;
    }

}