<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopInvoiceLogAbstract
 */
abstract class BAS_Shared_Model_Workshop_OrderWorkshopInvoiceLogAbstract extends BAS_Shared_Model_AbstractModel
{

    const TYPE_TASK = 1;
    const TYPE_ITEM = 2;

    /**
     * @var int
     */
    public $id;
    /**
     * @var int
     */
    public $invoiceId;
    /**
     * @var int
     */
    public $orderWorkshopId;
    /**
     * @var int
     */
    public $orderWorkshopDetailId;
    /**
     * @var int
     */
    public $type;
    /**
     * @var int
     */
    public $referenceId;
    /**
     * @return int
     */
    public $depotBookingCodeId;
    /**
     * @return int
     */
    public $depotAccountGroupId;
    /**
     * @var float
     */
    public $quantity;
    /**
     * @var float
     */
    public $price;
    /**
     * @var float
     */
    public $amount;
    /**
     * @var float
     */
    public $stockPrice;
    /**
     * @var float
     */
    public $stockAmount;
    /**
     * @var float
     */
    public $discountAmount;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     * @return $this
     */
    public function setInvoiceId($invoiceId)
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderWorkshopId()
    {
        return $this->orderWorkshopId;
    }

    /**
     * @param int $orderWorkshopId
     * @return $this
     */
    public function setOrderWorkshopId($orderWorkshopId)
    {
        $this->orderWorkshopId = $orderWorkshopId;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderWorkshopDetailId()
    {
        return $this->orderWorkshopDetailId;
    }

    /**
     * @param int $orderWorkshopDetailId
     * @return $this
     */
    public function setOrderWorkshopDetailId($orderWorkshopDetailId)
    {
        $this->orderWorkshopDetailId = $orderWorkshopDetailId;

        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @param int $referenceId
     * @return $this
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;

        return $this;
    }

    /**
     * @return int
     */
    public function getDepotBookingCodeId()
    {
        return $this->depotBookingCodeId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setDepotBookingCodeId($value)
    {
        $this->depotBookingCodeId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDepotAccountGroupId()
    {
        return $this->depotAccountGroupId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setDepotAccountGroupId($value)
    {
        $this->depotAccountGroupId = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * @return float
     */
    public function getStockPrice()
    {
        return $this->stockPrice;
    }

    /**
     * @param float $stockPrice
     * @return $this
     */
    public function setStockPrice($stockPrice)
    {
        $this->stockPrice = $stockPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getStockAmount()
    {
        return $this->stockAmount;
    }

    /**
     * @param float $stockAmount
     * @return $this
     */
    public function setStockAmount($stockAmount)
    {
        $this->stockAmount = $stockAmount;

        return $this;
    }

    /**
     * @return float
     */
    public function getDiscountAmount()
    {
        return $this->discountAmount;
    }

    /**
     * @param float $discountAmount
     * @return $this
     */
    public function setDiscountAmount($discountAmount)
    {
        $this->discountAmount = $discountAmount;

        return $this;
    }

}
