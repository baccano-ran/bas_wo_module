<?php

/**
 * Class BAS_Shared_Model_Workshop_WorkshopRefuelingCardMapper
 */
class BAS_Shared_Model_Workshop_WorkshopRefuelingCardMapper extends BAS_Shared_Model_AbstractMapper
{
    protected $_modelClass = 'BAS_Shared_Model_Workshop_WorkshopRefuelingCard';

    protected $_map = [
        'id' => 'id',
        'cardNumber' => 'card_number',
        'contactVehicleId' => 'contact_vehicle_id',
        'orderWorkshopId' => 'order_workshop_id',
        'archived' => 'archived',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
        'updatedAt' => 'updated_at',
        'updatedBy' => 'updated_by',
    ];

    /**
     * @return array|BAS_Shared_Model_Workshop_WorkshopRefuelingCard[]
     */
    public function findAllAvailablePerCardNumber()
    {
        $dao = $this->getDao();
        $select = $dao->select()
            ->from(['wrc' => $this->getTableName()], ['*'])
            ->where('wrc.archived = ?', BAS_Shared_Model_AbstractModel::NOT_ARCHIVED)
        ;

        $cardNumbers = $this->findAllByCondition($select);

        $refuelingCards = [];
        /** @var BAS_Shared_Model_Workshop_WorkshopRefuelingCard $cardNumber */
        foreach ($cardNumbers as $cardNumber) {
            $refuelingCards[$cardNumber->getCardNumber()] = $cardNumber;
        }

        return $refuelingCards;
    }

}