<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculation
 */
class BAS_Shared_Model_Workshop_OrderWorkshopDetailPreCalculation extends BAS_Shared_Model_AbstractModel
{
    public $orderWorkshopDetailId;
    public $description;
    public $amount;
    public $createdAt;
    public $createdBy;

    /**
     * @param int $value
     * @return $this
     */
    public function setOrderWorkshopDetailId($value)
    {
        $this->orderWorkshopDetailId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderWorkshopDetailId()
    {
        return $this->orderWorkshopDetailId;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setAmount($value)
    {
        $this->amount = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->createdAt = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setCreatedBy($value)
    {
        $this->createdBy = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

}