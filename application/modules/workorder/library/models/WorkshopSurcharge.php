<?php

/**
 * Class BAS_Shared_Model_Workshop_WorkshopSurcharge
 */
class BAS_Shared_Model_Workshop_WorkshopSurcharge extends BAS_Shared_Model_AbstractModel
{

    const TYPE_CONSUMABLES = 1;
    const TYPE_ENVIRONMENT = 2;

    const CALCULATION_TYPE_OVER_TASKS = 1;
    const CALCULATION_TYPE_OVER_ITEMS = 2;
    const CALCULATION_TYPE_OVER_TASKS_AND_ITEMS = 3;

    /**
     * @return int
     */
    public $id;

    /**
     * @return int
     */
    public $depotId;

    /**
     * @return int
     */
    public $departmentId;

    /**
     * @return int
     */
    public $type;

    /**
     * @return float
     */
    public $rate;

    /**
     * @return int
     */
    public $calculationType;

    /**
     * @return float
     */
    public $calculationMaxAmount;

    /**
     * @return string
     */
    public $endDate;

    /**
     * @return string
     */
    public $createdAt;

    /**
     * @return int
     */
    public $createdBy;

    /**
     * @return string
     */
    public $updatedAt;

    /**
     * @return int
     */
    public $updatedBy;
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int
     * @return $this
     */
    public function setId($value)
    {
        $this->id = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDepotId()
    {
        return $this->depotId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setDepotId($value)
    {
        $this->depotId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getDepartmentId()
    {
        return $this->departmentId;
    }

    /**
     * @param int
     * @return $this
     */
    public function setDepartmentId($value)
    {
        $this->departmentId = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int
     * @return $this
     */
    public function setType($value)
    {
        $this->type = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param float
     * @return $this
     */
    public function setRate($value)
    {
        $this->rate = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCalculationType()
    {
        return $this->calculationType;
    }

    /**
     * @param int
     * @return $this
     */
    public function setCalculationType($value)
    {
        $this->calculationType = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getCalculationMaxAmount()
    {
        return $this->calculationMaxAmount;
    }

    /**
     * @param float
     * @return $this
     */
    public function setCalculationMaxAmount($value)
    {
        $this->calculationMaxAmount = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param string
     * @return $this
     */
    public function setEndDate($value)
    {
        $this->endDate = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string
     * @return $this
     */
    public function setCreatedAt($value)
    {
        $this->createdAt = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param int
     * @return $this
     */
    public function setCreatedBy($value)
    {
        $this->createdBy = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param string
     * @return $this
     */
    public function setUpdatedAt($value)
    {
        $this->updatedAt = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * @param int
     * @return $this
     */
    public function setUpdatedBy($value)
    {
        $this->updatedBy = $value;
        return $this;
    }

}