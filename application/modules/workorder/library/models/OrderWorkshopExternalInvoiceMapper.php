<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceMapper
 */
class BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoiceMapper extends BAS_Shared_Model_AbstractMapper
{
    protected $_modelClass = 'BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoice';

    protected $_map = [
        'invoiceId' => 'invoice_id',
        'depotId' => 'depot_id',
        'mainAccountingDepotId' => 'main_accounting_depot_id',
        'orderWorkshopId' => 'order_workshop_id',
        'invoiceDate' => 'invoice_date',
        'exchangeRate' => 'exchange_rate',
        'vatExchangeRate' => 'vat_exchange_rate',
        'currency' => 'currency',
        'languageCode' => 'language_code',
        'debtorId' => 'debtor_id',
        'type' => 'type',
        'orderWorkshopFileId' => 'order_workshop_file_id',
        'invoiceBooked' => 'invoice_booked',
        'journalBookedAt' => 'journal_booked_at',
        'vatType' => 'vat_type',
        'vatTypeChangeReason' => 'vat_type_change_reason',
        'vatAmount' => 'vat_amount',
        'priceConsumables' => 'price_consumables',
        'priceEnvironment' => 'price_environment',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
    ];

    /**
     * @param int $mainAccountingDepotId
     * @return int
     */
    public function getLastInvoiceId($mainAccountingDepotId)
    {
        $select = $this->getDao()
            ->select()
            ->from('order_workshop_external_invoice', ['lastInvoiceId' => new Zend_Db_Expr('MAX(invoice_id)')])
            ->where('main_accounting_depot_id = ?', (int)$mainAccountingDepotId)
        ;

        $row = $this->getDao()->fetchRow($select);
        $lastInvoiceId = $row === null ? 0 : (int)$row['lastInvoiceId'];

        return $lastInvoiceId;
    }

    /**
     * function to get the journal entry csv records
     * @param array $depotIds 
     * @return array|null
     */
    public function getWorkorderExternalInvoiceCsvRecords($depotIds)
    {
        $dao = $this->getDao();
        $select = $dao->select()->setIntegrityCheck(false);

        //sub queries for fetching latest applicable VAT rate
        $subSelectlvr = $dao->select()->setIntegrityCheck(false);
        $subSelectlvr->from(array('owei' => 'order_workshop_external_invoice'), array('owei.invoice_id'))
                ->joinInner(array('d' => 'depot'), 'd.id = owei.depot_id', array())
                ->joinInner(array('lvr2' => 'list_vat_rate'), '(LEFT(d.vat_code,2) = lvr2.country_id) ANd lvr2.rate_type = 1 AND owei.invoice_date >= lvr2.start_date', array(new Zend_Db_Expr('MAX(lvr2.start_date) AS latest_rate'), 'lvr2.country_id'))
                ->group('owei.invoice_id')
                ->order(new Zend_Db_Expr('owei.invoice_id DESC'));

        $subSelectlvr2 = $dao->select()->setIntegrityCheck(false);
        $subSelectlvr2->from(array('lvr3' => $subSelectlvr), array('lvr3.invoice_id'))
                ->joinLeft(array('lvr' => 'list_vat_rate'), 'lvr3.latest_rate = lvr.start_date AND lvr3.country_id = lvr.country_id', array('lvr.rate')
        );

        $select->from(['owei' => 'order_workshop_external_invoice'], [])
                ->join(['ow' => 'order_workshop'], 'owei.order_workshop_id=ow.workorder_id', [])
                ->join(['oweil' => 'order_workshop_external_invoice_log'], 'owei.order_workshop_id=oweil.order_workshop_id and owei.invoice_id=oweil.invoice_id', [
                    'invoiceId' => 'owei.invoice_id',
                    'orderId' => 'owei.order_workshop_id',
                    'depotId' => 'owei.depot_id',
                    'type' => 'owei.type',
                    'customer_number' => 'owei.debtor_id',
                    'totalAmountExclVat' => new Zend_Db_Expr('(SUM(ROUND(oweil.amount,2))+ROUND(owei.price_consumables,2)+ROUND(owei.price_environment,2))'),
                    'totalAmountInclVat' => new Zend_Db_Expr('(SUM(ROUND(oweil.amount,2))+ROUND(owei.price_consumables,2)+ROUND(owei.price_environment,2))+ROUND(COALESCE(owei.vat_amount,0),2)'),
                    'totalAmountInOtherCurrencyExclVat' => new Zend_Db_Expr('(SUM(ROUND(oweil.amount*owei.exchange_rate,2))+ROUND(owei.price_consumables*owei.exchange_rate,2)+ROUND(owei.price_environment*owei.exchange_rate,2))'),
                    'totalAmountInOtherCurrencyInclVat' => new Zend_Db_Expr('((SUM(ROUND(oweil.amount*owei.exchange_rate,2))+ROUND(owei.price_consumables*owei.exchange_rate,2)+ROUND(owei.price_environment*owei.exchange_rate,2))+ROUND(COALESCE(owei.vat_amount,0)*owei.exchange_rate,2))'),
                    'totalVatAmount' => 'ROUND(COALESCE(owei.vat_amount,0),2)',
                    'totalVatAmountInOtherCurrency' => new Zend_Db_Expr('ROUND(COALESCE(owei.vat_amount,0)*owei.exchange_rate,2)'),
                    'priceConsumables' => 'ROUND(owei.price_consumables,2)',
                    'priceEnvironment' => 'ROUND(owei.price_environment,2)',
                    'priceConsumablesInOtherCurrency' => new Zend_Db_Expr('ROUND(owei.price_consumables*owei.exchange_rate,2)'),
                    'priceEnvironmentInOtherCurrency' => new Zend_Db_Expr('ROUND(owei.price_environment*owei.exchange_rate,2)'),
                    'accountingVatCode' => 'dvt.accounting_vat_code',
                    'currency' => 'lc.name',
                    'vatPercent' => new Zend_Db_Expr("IF(owei.vat_type=1, lvr.rate, NULL)"),
                    'remark' => new Zend_Db_Expr("CONCAT('F', owei.invoice_id, ' ', 'O', owei.order_workshop_id, ' ', 'K', owei.debtor_id)"),
                        ]
                )
                ->join(['owd' => 'order_workshop_detail'], 'oweil.order_workshop_detail_id = owd.id AND owd.main_order_workshop_detail_id IS NULL', [])
                ->joinInner(array('lvt' => 'list_vat_type'), 'owei.vat_type = lvt.id', array())
                ->joinInner(array('dvt' => 'depot_vat_type'), 'lvt.id = dvt.list_vat_type_id AND dvt.depot_id = owei.depot_id AND owei.invoice_date >= dvt.start_date', array())
                ->joinLeft(array('dvt2' => 'depot_vat_type'), 'dvt.list_vat_type_id = dvt2.list_vat_type_id AND dvt.depot_id = dvt2.depot_id AND dvt2.start_date > dvt.start_date and owei.invoice_date >= dvt2.start_date', array())
                ->joinInner(array('lvr' => $subSelectlvr2), 'lvr.invoice_id = owei.invoice_id', array())
                ->joinInner(array('lc' => 'list_currency'), "lc.id = owei.currency AND lc.lang = '" . BAS_Shared_Model_ListCurrency::DEFAULT_CURRENCY_LANGUAGE . "'", array())
                ->where('owei.depot_id in (?)', $depotIds)
                ->where('dvt2.id IS NULL')
                ->where('owei.invoice_booked = ?', BAS_Shared_Model_Workshop_OrderWorkshopExternalInvoice::INVOICE_NOT_BOOKED)
                ->group('invoiceId')
        ;

        $result = $dao->fetchAll($select);

        if (!empty($result)) {
            return $result->toArray();
        }

        return null;
    }
        
    /**
     * Update data by condition
     *
     * @param  array  $updateValue
     * @param  string $conditions
     * @return integer
     */
    public function updateByCondition($updateValue, $conditions)
    {
        $dao=$this->getDao();

        return $dao->update($updateValue, $conditions);
    }
}
