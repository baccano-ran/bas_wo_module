<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopTimeMapper
 */
class BAS_Shared_Model_Workshop_OrderWorkshopTimeMapper extends BAS_Shared_Model_AbstractMapper
{

    protected $_modelClass = 'BAS_Shared_Model_Workshop_OrderWorkshopTime';

    protected $_map = [
        'id' => 'id',
        'orderWorkshopId' => 'order_workshop_id',
        'personId' => 'person_id',
        'date' => 'date',
        'spentTime' => 'spent_time',
        'labourActivityId' => 'labour_activity_id',
        'manual' => 'manual',
        'xmlFilename' => 'xml_filename',
        'archived' => 'archived',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
        'updatedAt' => 'updated_at',
        'updatedBy' => 'updated_by',
    ];

    /**
     * @param string $xmlFilename
     * @return bool
     */
    public function isTimexXmlAlreadyProcessed($xmlFilename)
    {
        $select = $this->getDao()->select();
        $select
            ->from($this->getTableName(), [])
            ->where('xml_filename = ?', $xmlFilename)
            ->columns([
                'cnt' => new Zend_Db_Expr('COUNT(*)')
            ]);

        $stmt = $select->query();

        return (boolean)$stmt->fetchColumn();
    }

    /**
     * @param int $workorderId
     * @return float
     */
    public function getWorkorderSpentTime($workorderId)
    {
        $select = $this->getDao()->select()
            ->setIntegrityCheck(false)
            ->from(['owt' => 'order_workshop_time'], [])
            ->where('owt.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopTime::NOT_ARCHIVED)
            ->where('owt.order_workshop_id = ?', (int)$workorderId)
            ->columns([
                new Zend_Db_Expr('SUM(owt.spent_time)')
            ]);

        $stmt = $select->query();

        return (float)$stmt->fetchColumn();
    }

    /**
     * @param int $workorderId
     * @return Zend_Db_Select
     * @throws Zend_Db_Select_Exception
     */
    public function getTimeTrackGridSource($workorderId)
    {
        $dao = $this->getDao();
        $select = $dao->select()
            ->setIntegrityCheck(false)
            ->from(['owt' => 'order_workshop_time'], [])
            ->join(['ow' => 'order_workshop'], 'ow.workorder_id = owt.order_workshop_id', [])
            ->joinLeft(['pd' => 'person_depot'], 'pd.person_id = owt.person_id AND pd.depot_id = ow.depot_id', [])
            ->joinLeft(['p' => 'person'], 'p.id = pd.person_id', [])
            ->joinLeft(['la' => 'labour_activity'], 'owt.labour_activity_id = la.id', [])
            ->where('owt.archived = ?', BAS_Shared_Model_Workshop_OrderWorkshopTime::NOT_ARCHIVED)
            ->where('ow.workorder_id = ?', (int)$workorderId)
            ->columns([
                'id' => 'owt.id',
                'employee_number' => 'p.employee_number',
                'employee_name' => 'p.name',
                'date' => 'owt.date',
                'spent_time' => 'owt.spent_time',
                'activity_name' => 'la.name',
            ])
        ;

        return $select;
    }

}