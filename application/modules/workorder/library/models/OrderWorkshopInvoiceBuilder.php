<?php

/**
 * Class BAS_Shared_Model_Workshop_OrderWorkshopInvoiceBuilder
 */
abstract class BAS_Shared_Model_Workshop_OrderWorkshopInvoiceBuilder
{

    protected $workorderService;
    protected $depotSettingsService;
    protected $workorderInvoiceService;

    /**
     * BAS_Shared_Model_Workshop_OrderWorkshopInvoiceBuilder constructor.
     * @param Workshop_Service_Workorder $workorderService
     * @param Management_Service_DepotSetting $depotSettingsService
     * @param Workshop_Service_WorkorderInvoice $workorderInvoiceService
     */
    public function __construct(
        Workshop_Service_Workorder $workorderService,
        Management_Service_DepotSetting $depotSettingsService,
        Workshop_Service_WorkorderInvoice $workorderInvoiceService
    ) {
        $this->workorderService = $workorderService;
        $this->depotSettingsService = $depotSettingsService;
        $this->workorderInvoiceService = $workorderInvoiceService;
    }

    /**
     * @return BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract
     */
    protected abstract function create();

    /**
     * @param BAS_Shared_Model_OrderWorkshop $workorder
     * @param $invoiceType
     * @param $invoiceFileId
     * @param $languageCode
     * @param $invoiceBookingType
     * @param $depotId
     * @param $userId
     * @return BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract
     * @throws BAS_Shared_InvalidArgumentException
     */
    public function build(
        BAS_Shared_Model_OrderWorkshop $workorder,
        $invoiceType,
        $invoiceFileId,
        $languageCode,
        $invoiceBookingType,
        $depotId,
        $userId
    ) {
        $mainAccountingDepotId = $this->depotSettingsService->getMainAccountingDepot($workorder->getDepotId());
        $debtorId = $this->workorderService->getDebtorNumberByWorkorder($workorder);

        $invoice = $this->create();
        $invoice
            ->setInvoiceId($this->workorderInvoiceService->getNextInvoiceId($invoiceBookingType, $depotId))
            ->setDepotId($depotId)
            ->setMainAccountingDepotId($mainAccountingDepotId)
            ->setOrderWorkshopId($workorder->getWorkorderId())
            ->setInvoiceDate(date('Y-m-d'))
            ->setExchangeRate(BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::DEFAULT_EXCHANGE_RATE)
            ->setVatExchangeRate(BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::DEFAULT_EXCHANGE_RATE)
            ->setCurrency(BAS_Shared_Model_ListCurrency::EURO_CURRENCY_ID)
            ->setLanguageCode($languageCode)
            ->setDebtorId($debtorId)
            ->setType($invoiceType)
            ->setOrderWorkshopFileId($invoiceFileId)
            ->setInvoiceBooked(BAS_Shared_Model_Workshop_OrderWorkshopInvoiceAbstract::INVOICE_NOT_BOOKED)
            ->setVatType($workorder->getInvoiceSelectedVatType())
            ->setVatTypeChangeReason($workorder->getInvoiceVatTypeChangeReason())
            ->setCreatedAt(new DateTime())
            ->setCreatedBy($userId)
        ;

        return $invoice;
    }

}