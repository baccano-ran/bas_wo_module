<?php

/**
 * Class BAS_Shared_Model_Workshop_WorkshopSurchargeMapper
 */
class BAS_Shared_Model_Workshop_WorkshopSurchargeMapper extends BAS_Shared_Model_AbstractMapper
{

    protected $_modelClass = 'BAS_Shared_Model_Workshop_WorkshopSurcharge';

    protected $_map = [
        'id' => 'id',
        'depotId' => 'depot_id',
        'departmentId' => 'department_id',
        'type' => 'type',
        'rate' => 'rate',
        'calculationType' => 'calculation_type',
        'calculationMaxAmount' => 'calculation_max_amount',
        'endDate' => 'end_date',
        'createdAt' => 'created_at',
        'createdBy' => 'created_by',
        'updatedAt' => 'updated_at',
        'updatedBy' => 'updated_by',
    ];

    /**
     * @param int $depotId
     * @return Zend_Db_Select
     * @throws Zend_Db_Select_Exception
     */
    public function getGridSource($depotId)
    {
        $dao = $this->getDao();
        $select = $dao->select()
            ->setIntegrityCheck(false)
            ->from(['ws' => 'workshop_surcharge'], [])
            ->where('ws.depot_id = ?', (int)$depotId)
            ->order('ws.id')
            ->columns([
                'id' => 'id',
                'type' => 'type',
                'rate' => 'rate',
                'calculation_type' => 'calculation_type',
                'max_amount' => 'calculation_max_amount',
                'end_date' => 'end_date',
            ])
        ;

        return $select;
    }

    /**
     * @param int $type
     * @return array|Bas_Shared_Model_AbstractModel
     */
    public function findOneByType($type)
    {
        $select = $this->getDao()->select()
            ->where('type = ?', (int) $type)
            ->where('(end_date IS NULL) OR (end_date > NOW())')
        ;

        return $this->findOneByCondition($select);
    }

}